-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Sep 08, 2016 at 11:44 PM
-- Server version: 5.5.49-cll-lve
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `megaprofix`
--

-- --------------------------------------------------------

--
-- Table structure for table `ans`
--

CREATE TABLE IF NOT EXISTS `ans` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `msg_des` varchar(1000) NOT NULL,
  `ans` varchar(100) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `ans`
--

INSERT INTO `ans` (`id`, `user_id`, `user_name`, `title`, `msg_des`, `ans`, `user_email`) VALUES
(18, 11, 'SUNIL', 'hello', 'hellllloooo', 'bsbsbsbsbsbsbsbs', 'sbsunilbhatia9@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `backoffice`
--

CREATE TABLE IF NOT EXISTS `backoffice` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `general_url` varchar(1000) NOT NULL,
  `cpopimage` varchar(1000) NOT NULL,
  `cpoplink` varchar(1000) NOT NULL,
  `spopimage` varchar(1000) NOT NULL,
  `spoplink` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `backoffice`
--

INSERT INTO `backoffice` (`id`, `general_url`, `cpopimage`, `cpoplink`, `spopimage`, `spoplink`) VALUES
(1, 'http://megaprofix.com/LP/members/', 'http://megaprofix.com/LP/members/images/Megaprofix.png', 'http://www.megaprofix.com', 'http://megaprofix.com/LP/members/images/Megaprofix.png', 'http://www.youtube.com');

-- --------------------------------------------------------

--
-- Table structure for table `msg_to_users`
--

CREATE TABLE IF NOT EXISTS `msg_to_users` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(1000) NOT NULL,
  `message` varchar(1000) NOT NULL,
  `status` varchar(1000) NOT NULL,
  `date` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=48 ;

--
-- Dumping data for table `msg_to_users`
--

INSERT INTO `msg_to_users` (`id`, `user_id`, `message`, `status`, `date`) VALUES
(36, '11', 'gcuyguyc', '1', 'Wed:Jul:2015'),
(35, '13', 'geeebjhbvjvb', '1', 'Wed:Jul:2015'),
(34, '11', 'geee', '1', 'Wed:Jul:2015'),
(33, '11', 'hellodhbjvb', '1', 'Wed:Jul:2015'),
(32, '11', 'hello', '1', 'Wed:Jul:2015'),
(31, '13', 'hello', '1', 'Wed:Jul:2015'),
(30, '11', 'eddddd', '1', 'Tue:Jul:2015'),
(37, '11', 'gcuyguycvcjhvbbcf', '1', 'Wed:Jul:2015'),
(38, '11', 'vhd hjb d', '1', 'Wed:Jul:2015'),
(39, '13', 'ddddd', '1', 'Wed:Jul:2015'),
(40, '13', 'ffffff', '1', 'Wed:Jul:2015'),
(41, '11', 'chgvghcvsghcvghsvchgscvhgchcvhgvcghvcghcvghcvhgcvhgcvhgcvhgcvhgcvchgvcvvgvghvghcvghvchgfcfgcvgvchgvchgvchgvcghvhcgvcghvgchvhgcvghcvcgkbdvhvjhbvjbv', '1', 'Wed:Jul:2015'),
(42, '11', 'chgcvhgcvhgvchgvchgvchcvchgcvghvchgvcghcvghvcghcvghcvhgcvhgcvvcbvchvcbvcbvcghhbhjhjbhbhbhh', '1', 'Wed:Jul:2015'),
(43, '11', 'cughcughcjbhjcbjhbcjhcbjhbcjhbcjbcjhbchjbchjbchjbhcjbjhcbchbchjbchjbhjcbhjcbjhcbjhcbjhcbjhcbjhbchjbcjhbcjhbcjhbcjhbcjhbchjbchjbcjhbjhcjhsbcjhbcjbcjbcjhcbjhbcjhbcjhcbjhcbjhcbjhcbjhc', '1', 'Wed:Jul:2015'),
(44, '21', 'afecascgavgcvaghcvchgvchgvchgvhcvhcvghvcghvchgvchgcvhgcv', '1', 'Wed:Jul:2015'),
(45, '22', 'Hello Kapil How are You', '1', 'Wed:Jul:2015'),
(46, '22', 'wq', '1', 'Fri:Jul:2015'),
(47, '22', 'ghfhfhf', '1', 'Tue:Sep:2015');

-- --------------------------------------------------------

--
-- Table structure for table `signup`
--

CREATE TABLE IF NOT EXISTS `signup` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `account_currency` varchar(100) NOT NULL,
  `status` enum('1','0') NOT NULL,
  `unique_code` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `signup`
--

INSERT INTO `signup` (`id`, `first_name`, `last_name`, `email`, `password`, `country`, `phone`, `account_currency`, `status`, `unique_code`) VALUES
(21, 'sunil', 'bhatia', 'sbsunilbhatia9@gmail.com', '123', 'India', '3322114455', '$ Dollar', '0', ''),
(22, 'Kapil', 'Dhawan', 'kapil1@gmail.com', '123456', 'India', '8427202310', '$ Dollar', '1', ''),
(23, 'jaspal', 'kumar', 'jass.metrust@gmail.com', '123456', 'India', '9592603780', '$ Dollar', '1', '');

-- --------------------------------------------------------

--
-- Table structure for table `supp_ticket`
--

CREATE TABLE IF NOT EXISTS `supp_ticket` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(1000) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `msg_title` varchar(1000) NOT NULL,
  `msg_desc` varchar(1000) NOT NULL,
  `answer` varchar(1000) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `date` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=54 ;

--
-- Dumping data for table `supp_ticket`
--

INSERT INTO `supp_ticket` (`id`, `user_id`, `user_name`, `msg_title`, `msg_desc`, `answer`, `user_email`, `date`) VALUES
(53, '21', 'sunil', 'hjshdkj', 'sdfsdfsdfsdfsdfsd', 'tfgdgdfgfgdgdgdfgfdgdgdf', 'sbsunilbhatia9@gmail.com', 'Mon:Aug:2015');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

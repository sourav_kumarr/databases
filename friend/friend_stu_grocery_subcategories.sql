-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: friend
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `stu_grocery_subcategories`
--

DROP TABLE IF EXISTS `stu_grocery_subcategories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stu_grocery_subcategories` (
  `subcat_id` int(100) NOT NULL AUTO_INCREMENT,
  `parent_cat` varchar(100) NOT NULL,
  `subcat_name` varchar(100) NOT NULL,
  `subcat_image` varchar(100) NOT NULL,
  `subcat_desc` varchar(100) NOT NULL,
  `subcat_status` varchar(100) NOT NULL,
  PRIMARY KEY (`subcat_id`)
) ENGINE=MyISAM AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stu_grocery_subcategories`
--

LOCK TABLES `stu_grocery_subcategories` WRITE;
/*!40000 ALTER TABLE `stu_grocery_subcategories` DISABLE KEYS */;
INSERT INTO `stu_grocery_subcategories` VALUES (5,'1','Confectionery','subcat1752.jpg','All items of confectionery','A'),(6,'1','Eggs','subcat8955.jpg','All type of eggs','A'),(7,'1','Instant Foods','subcat5458.jpg','All type of instant foods','A'),(8,'1','Breakfast','subcat3796.jpg','All type of breakfast items','A'),(9,'1','Diary & Frozen','subcat1605.jpg','All type of diary ','A'),(10,'1','Sauces & Pickles ','subcat7124.jpg','All type of sauces  ','A'),(12,'1','Organic Food Mart','subcat5878.jpg','All type of organic foods','A'),(14,'2','Edible Oil & Ghee','subcat8246.jpg','All','A'),(15,'2','Rice & Flour','subcat6452.jpg','All','A'),(16,'2','Salt & Sugars','subcat4071.jpg','All','A'),(17,'2','Staples ','subcat9481.jpg','All','A'),(19,'2','Paste & Vinegar','subcat7196.jpg','All','A'),(20,'2','Spices','subcat1191.jpg','ALL','A'),(21,'2','Dry Fruits','subcat97.jpg','ALL','A'),(22,'4','Bath ','subcat4115.jpg','All','A'),(23,'4','Beauty Accessories','subcat8807.jpg','All','A'),(24,'4','Hair Care','subcat8284.jpg','All','A'),(25,'4','Make Up','subcat9396.jpg','ALL','A'),(26,'4','Oral Care','subcat7142.jpg','All','A'),(27,'5','Kitchen Utilities','subcat7503.jpg','All','A'),(28,'5','Lighting ','subcat4791.jpg','All','A'),(29,'5','Pet Care','subcat8406.jpg','All','A'),(30,'5','Pooja Provisions','subcat1166.jpg','All','A'),(32,'5','Home Decor','subcat4016.jpg','All','A'),(33,'6','Vegetables','subcat3300.jpg','All','A'),(34,'6','Fruits','subcat1298.jpg','All','A'),(35,'10','Chyawanprash','subcat7666.jpg','All','A'),(36,'10','Digestives','subcat2744.jpg','All','A'),(37,'10','Otc','subcat8535.jpg','All','A'),(38,'10','Sugar Free','subcat226.jpg','All','A'),(39,'10','Health Drink','subcat7878.jpg','All','A'),(42,'8','Coffee ','subcat1630.jpg','All','A'),(48,'8','Energy Drink ','subcat404.jpg','All','A'),(50,'8','Juices ','subcat1070.jpg','All','A'),(51,'8','Sharbat ','subcat1894.jpg','All','A'),(52,'8','Soft Drinks','subcat5934.jpg','All','A'),(54,'9','Baby Care','subcat4891.jpg','All','A'),(55,'9','Baby Food','subcat3460.jpg','All','A'),(56,'3','Biscuits','subcat1853.jpg','All','A'),(57,'3','Namkeens','subcat5378.jpg','All','A'),(58,'3','Chocolates','subcat9804.jpg','All','A'),(60,'3','Candy','subcat3557.jpg','All','A'),(61,'3','Chips','subcat6351.jpg','All','A'),(62,'3','Mints ','subcat6600.jpg','All','A');
/*!40000 ALTER TABLE `stu_grocery_subcategories` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:59

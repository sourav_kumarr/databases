-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: friend
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `content_pages`
--

DROP TABLE IF EXISTS `content_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_pages` (
  `id` int(11) NOT NULL,
  `page_name` varchar(50) NOT NULL,
  `page_title` varchar(50) NOT NULL,
  `page_content` text NOT NULL,
  `video` text NOT NULL,
  `page_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_pages`
--

LOCK TABLES `content_pages` WRITE;
/*!40000 ALTER TABLE `content_pages` DISABLE KEYS */;
INSERT INTO `content_pages` VALUES (1,'about_us','About Us','<p> The name of  the website is “Music mania”. In  this website punjabi songs , hindi  songs , english songs,himachali songs,  are  kept  into the  Database. User  can   listen songs  online   and    also  download   songs of  their  own  choice .  I  have  tried  my  best  to  make  the  complicated  process  of  music mania  as  simple  as  possible  using  MYSQL.The  main  feature   of   this website is  that  the  user  can  upload  own  songs .  first   of  all  user  create  own  account   through  sign  up  panel  .  one  menu   item  in  my website is  sign  up  which  registered  the  user  after  this  process  user  just  log  in  and   take  the  facility  of   upload .</p>','',1),(2,'help','Help','https://www.youtube.com/embed/6Tc7LBx7XzE','140612.mp4',1),(3,'privacy_policy','Privacy Policy','<div class=\"title\">\r\n	<div class=\"terms_description\" style=\"float: left; font-family: droid_sansregular; margin: 12px 15px 12px 0px; color: rgb(0, 0, 0); font-size: medium; line-height: normal;\">\r\n		<h2 style=\"padding: 0px; margin: 0px; border: none; float: left; color: rgb(55, 110, 180); font-size: 20px;\">\r\n			Lorem Lipsum dummy text</h2>\r\n		<p style=\"padding: 0px; margin: 8px 0px 0px; border: none; float: left; color: rgb(96, 96, 96); font-size: 14px; line-height: 22px;\">\r\n			Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n	</div>\r\n	<div class=\"terms_description\" style=\"float: left; font-family: droid_sansregular; margin: 12px 15px 12px 0px; color: rgb(0, 0, 0); font-size: medium; line-height: normal;\">\r\n		<h2 style=\"padding: 0px; margin: 0px; border: none; float: left; color: rgb(55, 110, 180); font-size: 20px;\">\r\n			Lorem Lipsum dummy text</h2>\r\n		<p style=\"padding: 0px; margin: 8px 0px 0px; border: none; float: left; color: rgb(96, 96, 96); font-size: 14px; line-height: 22px;\">\r\n			Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n	</div>\r\n	<div class=\"terms_description\" style=\"float: left; font-family: droid_sansregular; margin: 12px 15px 12px 0px; color: rgb(0, 0, 0); font-size: medium; line-height: normal;\">\r\n		<h2 style=\"padding: 0px; margin: 0px; border: none; float: left; color: rgb(55, 110, 180); font-size: 20px;\">\r\n			Lorem Lipsum dummy text</h2>\r\n		<p style=\"padding: 0px; margin: 8px 0px 0px; border: none; float: left; color: rgb(96, 96, 96); font-size: 14px; line-height: 22px;\">\r\n			Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>\r\n	</div>\r\n</div>\r\n','',1),(4,'terms_of_use','Terms and conditions','<div class=\"terms_description\" style=\"float: left; font-family: droid_sansregular; margin: 12px 15px 12px 0px; color: rgb(0, 0, 0); font-size: medium; line-height: normal;\">\r\n	<h2 style=\"padding: 0px; margin: 0px; border: none; float: left; color: rgb(55, 110, 180); font-size: 20px;\">\r\n		Lorem Lipsum dummy text</h2>\r\n	<p style=\"padding: 0px; margin: 8px 0px 0px; border: none; float: left; color: rgb(96, 96, 96); font-size: 14px; line-height: 22px;\">\r\n		Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n</div>\r\n<div class=\"terms_description\" style=\"float: left; font-family: droid_sansregular; margin: 12px 15px 12px 0px; color: rgb(0, 0, 0); font-size: medium; line-height: normal;\">\r\n	<h2 style=\"padding: 0px; margin: 0px; border: none; float: left; color: rgb(55, 110, 180); font-size: 20px;\">\r\n		Lorem Lipsum dummy text</h2>\r\n	<p style=\"padding: 0px; margin: 8px 0px 0px; border: none; float: left; color: rgb(96, 96, 96); font-size: 14px; line-height: 22px;\">\r\n		Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n</div>\r\n<div class=\"terms_description\" style=\"float: left; font-family: droid_sansregular; margin: 12px 15px 12px 0px; color: rgb(0, 0, 0); font-size: medium; line-height: normal;\">\r\n	<h2 style=\"padding: 0px; margin: 0px; border: none; float: left; color: rgb(55, 110, 180); font-size: 20px;\">\r\n		Lorem Lipsum dummy text</h2>\r\n	<p style=\"padding: 0px; margin: 8px 0px 0px; border: none; float: left; color: rgb(96, 96, 96); font-size: 14px; line-height: 22px;\">\r\n		Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>\r\n	&nbsp;\r\n	<p style=\"padding: 0px; margin: 8px 0px 0px; border: none; float: left; color: rgb(96, 96, 96); font-size: 14px; line-height: 22px;\">\r\n		&nbsp;</p>\r\n	<p style=\"padding: 0px; margin: 8px 0px 0px; border: none; float: left; color: rgb(96, 96, 96); font-size: 14px; line-height: 22px;\">\r\n		&nbsp;</p>\r\n</div>\r\n','',1);
/*!40000 ALTER TABLE `content_pages` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:59

-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: friend
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ch_childcare`
--

DROP TABLE IF EXISTS `ch_childcare`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ch_childcare` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `Description` text COLLATE latin1_general_ci,
  `LogoPath` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `OpeningTime` time NOT NULL,
  `ClosingTime` time NOT NULL,
  `ChildMinimumAgeInMonth` int(100) NOT NULL,
  `ChildMaximumAgeInMonth` int(100) NOT NULL,
  `Address` text COLLATE latin1_general_ci NOT NULL,
  `latiTude` float NOT NULL,
  `longtiTude` float NOT NULL,
  `Postcode` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `Default_Price` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `Country` int(32) NOT NULL,
  `ChildcareType` int(32) NOT NULL,
  `Contact_Email` varchar(250) COLLATE latin1_general_ci DEFAULT NULL,
  `Account_Email` varchar(250) COLLATE latin1_general_ci DEFAULT NULL,
  `Account_Password` varchar(250) COLLATE latin1_general_ci DEFAULT NULL,
  `Registered` bit(1) DEFAULT NULL,
  `Suburb` varchar(250) COLLATE latin1_general_ci NOT NULL,
  `Business_Number` varchar(250) COLLATE latin1_general_ci NOT NULL,
  `BlackListed` bit(1) NOT NULL,
  `LastLoginDate` datetime NOT NULL,
  `ResetHash` varchar(32) COLLATE latin1_general_ci NOT NULL,
  `PhoneNumber` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `ifpId` int(32) NOT NULL,
  `ProspectNotes` text COLLATE latin1_general_ci NOT NULL,
  `Active` bit(1) NOT NULL,
  PRIMARY KEY (`ID`,`ChildcareType`),
  UNIQUE KEY `ID_UNIQUE` (`ID`),
  KEY `fk_ch_ChildCare_ch_Country1_idx` (`Country`),
  KEY `fk_ch_ChildCare_ch_ChildcareType1_idx` (`ChildcareType`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ch_childcare`
--

LOCK TABLES `ch_childcare` WRITE;
/*!40000 ALTER TABLE `ch_childcare` DISABLE KEYS */;
INSERT INTO `ch_childcare` VALUES (1,'abc1','description','http://www.freedigitalphotos.net/images/img/homepage/87357.jpg','10:00:00','12:00:00',18,36,'sangrur',30.441,76.4718,'148023','3000',0,3,'care@gmail.com','cruch@gmail.com','123456789','\0','','','\0','0000-00-00 00:00:00','','',0,'','\0'),(2,'abc2','description','http://www.freedigitalphotos.net/images/img/homepage/87357.jpg','11:00:00','01:00:00',18,36,'sangrur',30.55,76.5,'148023','2000',0,3,'care1@gmail.com','cruch1@gmail.com','123456789','\0','','','\0','0000-00-00 00:00:00','','',0,'','\0'),(3,'abc3','description','http://www.freedigitalphotos.net/images/img/homepage/87357.jpg','12:00:00','02:00:00',18,36,'sangrur',30.71,76.51,'148023','6000',0,3,'care2@gmail.com','cruch2@gmail.com','123456789','\0','','','\0','0000-00-00 00:00:00','','',0,'','\0'),(4,'abc4','description','http://www.freedigitalphotos.net/images/img/homepage/87357.jpg','01:00:00','03:00:00',18,36,'sangrur',30.25,76.2,'148023','4000',0,3,'care3@gmail.com','cruch3@gmail.com','123456789','\0','','','\0','0000-00-00 00:00:00','','',0,'','\0'),(5,'abc5','description','http://www.freedigitalphotos.net/images/img/homepage/87357.jpg','02:00:00','04:00:00',18,36,'sangrur',30.1,76.1,'148023','5000',0,3,'care4@gmail.com','cruch4@gmail.com','123456789','\0','','','\0','0000-00-00 00:00:00','','',0,'','\0');
/*!40000 ALTER TABLE `ch_childcare` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:49:00

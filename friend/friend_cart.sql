-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: friend
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cart` (
  `cart_id` int(100) NOT NULL AUTO_INCREMENT,
  `item_id` varchar(100) NOT NULL,
  `dated` varchar(100) NOT NULL,
  `quantity` int(100) NOT NULL,
  `item_price` varchar(100) NOT NULL,
  `total_price` varchar(100) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `cookie_id` varchar(100) NOT NULL,
  PRIMARY KEY (`cart_id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart`
--

LOCK TABLES `cart` WRITE;
/*!40000 ALTER TABLE `cart` DISABLE KEYS */;
INSERT INTO `cart` VALUES (6,'106','13/May/2016 12:21:07 AM',1,'Rs. 20','Rs. 20','','ck558898925'),(7,'107','13/May/2016 12:21:09 AM',1,'Rs. 35','Rs. 35','','ck558898925'),(8,'105','13/May/2016 12:21:13 AM',1,'Rs. 150','Rs. 150','','ck558898925'),(9,'212','13/May/2016 12:31:38 AM',1,'Rs. 200','Rs. 200','','ck874755859'),(14,'6','13/May/2016 12:32:05 AM',1,'Rs. 350','Rs. 350','','ck874755859'),(16,'51','13/May/2016 10:36:26 AM',1,'Rs. 390','Rs. 390','','ck389984130'),(17,'51','13/May/2016 10:24:01 PM',1,'Rs. 390','Rs. 390','','ck79132080'),(18,'53','13/May/2016 10:24:04 PM',1,'Rs. 100','Rs. 100','','ck79132080'),(19,'6','13/May/2016 11:30:39 PM',1,'Rs. 350','Rs. 350','','ck950958251'),(20,'100','16/May/2016 10:50:01 AM',1,'Rs. 20','Rs. 20','','ck925842285'),(21,'102','16/May/2016 10:50:04 AM',1,'Rs. 20','Rs. 20','','ck925842285'),(23,'124','16/May/2016 01:01:17 PM',1,'Rs. 46','Rs. 46','','ck712341308'),(24,'58','16/May/2016 05:33:02 PM',1,'Rs. 90','Rs. 90','','ck429107666'),(25,'178','16/May/2016 05:40:51 PM',1,'Rs. 335','Rs. 335','','ck372650146'),(26,'183','17/May/2016 01:41:23 PM',1,'Rs. 410','Rs. 410','','ck29571533'),(27,'52','18/May/2016 01:32:12 PM',1,'Rs. 110','Rs. 110','','ck492889404'),(28,'58','18/May/2016 01:54:50 PM',1,'Rs. 90','Rs. 90','','ck492889404'),(29,'59','18/May/2016 01:55:01 PM',1,'Rs. 57','Rs. 57','','ck492889404'),(30,'59','18/May/2016 01:55:07 PM',1,'Rs. 57','Rs. 57','','ck492889404'),(32,'34','27/May/2016 10:12:28 PM',1,'Rs. 22','Rs. 22','','ck277396299'),(34,'150','27/May/2016 10:15:08 PM',1,'Rs. 56','Rs. 56','','ck772813173'),(35,'66','27/May/2016 10:16:51 PM',1,'Rs. 115','Rs. 115','','ck772813173'),(36,'172','27/May/2016 10:18:23 PM',1,'Rs. 650','Rs. 650','','ck772813173'),(37,'168','27/May/2016 10:18:28 PM',1,'Rs. 45','Rs. 45','','ck772813173'),(38,'170','27/May/2016 10:18:32 PM',1,'Rs. 600','Rs. 600','','ck772813173'),(39,'169','27/May/2016 10:18:40 PM',1,'Rs. 575','Rs. 575','','ck772813173'),(40,'169','27/May/2016 10:18:42 PM',1,'Rs. 575','Rs. 575','','ck772813173'),(41,'168','27/May/2016 10:18:46 PM',1,'Rs. 45','Rs. 45','','ck772813173'),(42,'58','27/May/2016 10:19:20 PM',1,'Rs. 90','Rs. 90','','ck772813173'),(43,'59','27/May/2016 10:19:24 PM',1,'Rs. 57','Rs. 57','','ck772813173'),(44,'57','27/May/2016 10:19:27 PM',1,'Rs. 450','Rs. 450','','ck772813173'),(47,'183','27/May/2016 11:59:01 PM',1,'Rs. 410','Rs. 410','','ck715861035'),(49,'33','28/May/2016 12:02:49 AM',1,'Rs. 24','Rs. 24','','ck715861035'),(50,'72','31/May/2016 09:39:19 PM',1,'Rs. 38','Rs. 38','','ck373054608'),(51,'33','29/Jun/2016 06:18:22 PM',1,'Rs. 24','Rs. 24','','ck45430046');
/*!40000 ALTER TABLE `cart` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:49:06

-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: friend
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `feedingindia_users`
--

DROP TABLE IF EXISTS `feedingindia_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedingindia_users` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `number` varchar(200) NOT NULL,
  `type` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedingindia_users`
--

LOCK TABLES `feedingindia_users` WRITE;
/*!40000 ALTER TABLE `feedingindia_users` DISABLE KEYS */;
INSERT INTO `feedingindia_users` VALUES (1,'g12456789','g12456789','g12456789','1245678',''),(2,'aaddgh','dddd@gmail.xvom','dgygdggfrrt','vdygddggg','email'),(3,'aaddgh','dddd@gmail.xvo','dgygdggfrrt','vdygddgg','email'),(4,'rtuhdgdgg utuytyuty','g1245678','qrtyu','225555','email'),(5,'Sunil Majithia','956361917772301','','','fb'),(23,'Ranjitha','ranjithavisi@gmail.com','Sai@Rama1','9922013636','email'),(8,'Nardeep Sandhu','810925155683171Nardeep Sandhu','','','fb'),(9,'Jai Kumar','jai83338@gmail.com','sedfdsf','','google'),(10,'nardeep sandhu','nardeepsandhu5@gmail.com','','','google'),(19,'hetal padia','hetalpadia333@gmail.com','life@123','9674574298','email'),(20,'Prachi Mishra','prachimishra78@gmail.com','','','google'),(21,'Rohit Agarwal','raagarwalarohit@gmail.com','','','google'),(22,'rohitchawl','rohitchawla.ece@gmail.com','car458wont999','9717791228','email'),(17,'er sunil','suniler9@gmail.com','','','google'),(18,'naro','nar@g.com','124567','9550188228','email'),(24,'ashish arun godbole','godashish@gmail.com','ashish2208','9820073142','email'),(25,'Ankita Jain','84.ankita@gmail.com','ankitaalways','9652912399','email'),(26,'mitali','sendmails2mitali@gmail.com','pavbhaji','9850007418','email'),(27,'Pranav Kumar','pranavroy00@gmail.com','','','google'),(28,'aditigiridih','aditigiridih@gmail.com','Shashi2016','9431366250','email'),(29,'Raj Tiwari','raj.007tt@gmail.com','','','google'),(30,'Darshan Brahmbhatt','brahmbhattdarshand5@gmail.com','','','google'),(31,'anil patil','08anilpatil@gmail.com','MOHITDADA','7276777088','email'),(32,'Naresh lahiri','nareshlahiri@yahoo.com','23532353','9810994799','email'),(33,'shiv','shivlove1@gmail.com','shiv123','9488839671','email'),(34,'Harsh Bhutani','bhutaniharsh66@gmail.com','','','google'),(35,'bittu','bittuminakshi@gmail.com','bittusingh','7307785451','email'),(36,'Raju Bhagat','rajubhagat9540441586@gmail.com','','','google'),(37,'Prakash PRAKASH.G','np007g@gmail.com','','','google'),(38,'md ali majeed','mdalimajeed36@gmail.com','Skreyaz786','9874537892','email'),(39,'Deepakpunshi','deepakpunshii@gmail.com','akudarling','9286666666','email'),(40,'amandeep','meraputt1256@gmail.com','brandeds','8146579505','email'),(41,'Priya Garg','pgarg19464@gmail.com','priyagarg05','9711236237','email'),(42,'kritimalik84','kritimalik84@gmail.com','iluvsasha','9810086433','email'),(43,'kushal bung','kushalbung127@gmail.com','harshubhai','9030191610','email'),(44,'izasali','ghulamtrading@gmail.com','hussain76','7092111121','email'),(45,'saransh vats','saranshvats05@gmail.com','meranaam','8252846989','email'),(46,'Jishad Mohd','jishadmohd87@gmail.com','jishfeed','8281776910','email'),(47,'Shyam','kweb.sharma@gmail.com','twinkle1290','9900313425','email'),(48,'ravindra','nikate.ravindra@yahoo.com','ravin12345','9404233470','email'),(49,'Ismail Mondal','592943170880221Ismail Mondal','','','fb'),(50,'Taranbir Kaur','taranbirkaur.tk@gmail.com','','','google');
/*!40000 ALTER TABLE `feedingindia_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:49:10

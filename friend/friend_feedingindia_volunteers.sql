-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: friend
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `feedingindia_volunteers`
--

DROP TABLE IF EXISTS `feedingindia_volunteers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedingindia_volunteers` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `added_by` varchar(200) NOT NULL,
  `number` varchar(100) NOT NULL,
  `organization` varchar(255) NOT NULL,
  `education` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `three_words` varchar(100) NOT NULL,
  `why_i_join` varchar(100) NOT NULL,
  `give_time` varchar(100) NOT NULL,
  `date` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `image` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=57 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedingindia_volunteers`
--

LOCK TABLES `feedingindia_volunteers` WRITE;
/*!40000 ALTER TABLE `feedingindia_volunteers` DISABLE KEYS */;
INSERT INTO `feedingindia_volunteers` VALUES (32,'Prachi mishra','prachimishra78@gmail.com','prachimishra78@gmail.com','9461762491','lachoo memorial college','m.sc. in physics','19e/19 chopasni housing board jodhpur Rajasthan','I wish to contribute','I want to be a part of this great cause.','Yes','06:47:06 PM 02-Jun-2016','email','http://sachtechsolution.com/FeedingIndia/uploads/1464892854289.png'),(31,'Hetal Padia','hetalpadia333@gmail.com','hetalpadia333@gmail.com','9674574298','future institute of engineering and management','B.tech','Sonarpur, Kolkata, West Bengal, India','helpful,understanding,kind','i want to join this because i too believe that the food wasted deserves the hungry stomachs and not ','Yes','12:01:43 AM 08-May-2016','email',''),(30,'ryyyh','hkhyikgybbhj@gmail.com','suniler9@gmail.com','5525555525','fgfdgggfgg','dgyfdgffdddgfgg','Ambala, Haryana, India','dvggg','dgggg','Yes','11:38:08 AM 09-Jan-2016','email',''),(29,'knjk','n@gm.com','nardeepsandhu5@gmail.com','1234567890','cdsa','sax','Faridabad, Haryana, India','jnjknjk','bhjbhjmbhj','Yes','06:36:59 PM 22-Dec-2015','email',''),(28,'cvvvv','fgh@gmail.com','jai83338@gmail.com','8599852954','vfxvcdnvs','dhbcsbb','Sangrur, Punjab, India','ffgvcsfv','cvbbccc','Yes','06:32:10 PM 22-Dec-2015','email','http://sachtechsolution.com/FeedingIndia/uploads/1450789315579.png'),(27,'wtyyr  ghfghfghfhhfhgfhfhg','adgj@gmail.com','g1245678','5355665555','fgdgrgrdg','drygdgg','dayvxvxv','dfdg','drggg','Yes','08:29:40 PM 17-Dec-2015','email','http://sachtechsolution.com/FeedingIndia/uploads/1450623200580.png'),(26,'wygfdg','addgh@gmail.com','g1245678','5325585268','gdghdgdg','drgdgdgdgdgd','dfggggggggggggg, Sannicandro Garganico, Province of Foggia, Italy','dggg','rfgg','No','08:23:42 PM 17-Dec-2015','email','http://sachtechsolution.com/FeedingIndia/uploads/1450622767102.png'),(24,'gbfghg','qwrtui@gmail.com','g12456789','5228258558','frtdrggg','rrygggg','fdgdffgddrf','ddgggvv','dgdggtyy','No','07:18:57 PM 17-Dec-2015','email',''),(25,'dryyy','dgyhh@gmail.com','g12456789','5255585588','ctggvv','xdgdgdgxvdg','Dr. Ambedkar Nagar, New Delhi, Delhi, India','dgyytt','dryyyy','Yes','07:46:46 PM 17-Dec-2015','email','http://sachtechsolution.com/FeedingIndia/uploads/1450620716696.png'),(23,'adgwtggg','adh@gmail.com','g12456789','5225851395','fdgdggh','dghh','rfghh','frhgh','dghdggh','Yes','05:07:45 PM 17-Dec-2015','email','http://sachtechsolution.com/FeedingIndia/uploads/1450611170013.png'),(33,'Rohit Agarwal','raagarwalarohit@Gmail.com','raagarwalarohit@gmail.com','8100788777','Umesh Chandra College','CA final','12/B B.T. ROAD TITAGARH','I am a kind person','I wannna help people','Yes','11:22:07 AM 19-Jun-2016','email','http://sachtechsolution.com/FeedingIndia/uploads/1466315523017.png'),(34,'Rohit Chawla','rohitchawla.ece@gmail.com','rohitchawla.ece@gmail.com','9717791228','KPMG','MBA','Tower 2, flat 1801, UNITECH FRESCO, Nirvana Country, Sector 50, Gurgaon','Focused, solution driven','I believe that hunger is the most basic humanitarian problems and want to be a part of an initiative','Yes','01:19:27 PM 19-Jun-2016','email',''),(35,'Ranjitha Hari','ranjithavisi@gmail.com','ranjithavisi@gmail.com','9922013636','I have taken a short break from work','MBA in Tourism and Hospitality Management','Pune, Maharashtra, India','Passion, Determination, hard work','I wish to see a hunger free India.','Yes','02:43:28 PM 19-Jun-2016','email',''),(36,'Ankita Jain','84.ankita@gmail.com','84.ankita@gmail.com','9652912399','EXOTIC Interiors','MBA','Kondapur, Hyderabad, Telangana, India','Active, Helpful and Optimistic','want to give my some time serving the poor and needy','Yes','08:43:14 PM 19-Jun-2016','email',''),(37,'pranav Kumar Ro','pranavroy00@gmail.com','pranavroy00@gmail.com','8433802721','Map worldwide services','b . tech','Mira road Mumbai','integrity, honesty, Hard working','serve the society to make others happy','Yes','04:25:32 PM 24-Jun-2016','email','http://sachtechsolution.com/FeedingIndia/uploads/1466765642515.png'),(38,'SHASHIKANT BARN','aditigiridih@gmail.com','aditigiridih@gmail.com','9431366250','GIRIDIH','INTERMEDIATE','ADITI TRADERS BUXIDIH ROAD','No Eating Havy Foods','Social Working','Yes','06:50:53 PM 07-Jul-2016','email',''),(39,'Tarun Tiwari','taruntiwari007@yahoi.com','raj.007tt@gmail.com','8860636646','Triple play','B.A','Jharsa, Gurgaon, Haryana, India','good','for help','Yes','06:53:56 PM 07-Jul-2016','email','http://sachtechsolution.com/FeedingIndia/uploads/1467897585203.png'),(40,'Darshan rao','brahmbhatt.darshan@gmail.com','brahmbhattdarshand5@gmail.com','7621933594','NM zala commerce collage, Ahmedabad','SY B.Com','B / 202 Nisarag dreams near sp ring road shilaj Bopal ahmedabad.','Army me to na ja sake desh ke liye desh me rah kar hi kare.','Desh seva.','Yes','07:06:46 PM 07-Jul-2016','email','http://sachtechsolution.com/FeedingIndia/uploads/1467898530165.png'),(41,'Naresh lahiri','nareshlahiri@yahoo.com','nareshlahiri@yahoo.com','9810994799','individual','diploma engineer','4/5, rajender nager sec 2,sahibabad ghaziabad 201005','want to be helpful','want to serve the needful','Yes','07:10:18 PM 07-Jul-2016','email',''),(42,'shiv','shivlove1@gmail.com','shivlove1@gmail.com','9488839671','VIT','graduated','Arakkonam, India','not let india hungary','dont want or dont let anybody hungary','Yes','07:28:55 PM 07-Jul-2016','email','http://sachtechsolution.com/FeedingIndia/uploads/1467899829793.png'),(43,'Harsh Bhutani','bhutaniharsh66@gmail.com','bhutaniharsh66@gmail.com','9479071841','Disha college','12th pass out','Katora Talab, Raipur, Chhattisgarh, India','sharing called love','Yes, I want to be the part of feeding india because I it gives me happiness and i love those feeling','Yes','10:09:39 PM 07-Jul-2016','email','http://sachtechsolution.com/FeedingIndia/uploads/1467909569417.png'),(44,'bittu singh','bittuminakshi@gmall.com','bittuminakshi@gmail.com','7307785451','army','msc','amritsar','i lv poor pepole','serve our country nd help poor people','Yes','11:12:17 PM 07-Jul-2016','email',''),(45,'Prakash G','prakash.gunashekar@accenture.com','np007g@gmail.com','9740977062','Accenture','BBM','Nagavara, Bengaluru, Karnataka, India','Fun lover, Friendly & Helper','I want to feed India as well','Yes','04:12:14 PM 08-Jul-2016','email','http://sachtechsolution.com/FeedingIndia/uploads/1467974476299.png'),(46,'Md ali majeed','mdalimajeed36@gmail.com','mdalimajeed36@gmail.com','9874537892','abul hasan high school','class 10','3 raj mohan st kolkata 73','ego alwayz hurt','help the needed one','Yes','04:18:00 PM 08-Jul-2016','email',''),(47,'Deepak Punshi','deepakpunshii@gmail.com','deepakpunshii@gmail.com','9286666666','single want to join','not well educated','Rudrapur, Uttarakhand, India','not describe in three words','because i know the value of food','Yes','04:46:20 PM 08-Jul-2016','email',''),(48,'amandeep','meraputt1256@gmail.com','meraputt1256@gmail.com','8146579505','himmatana','secondery','vpo himmatana teh malerkotla distt. sangrur','serve honesty aim','for serve only','Yes','06:31:44 PM 08-Jul-2016','email',''),(49,'kushal bung','kushalbung127@gmail.com','kushalbung127@gmail.com','9030191610','infosys','btech','Sitaram Bagh, Hyderabad, Telangana, India','nature lover','to do my bit to save food','Yes','02:45:41 PM 17-Jul-2016','email',''),(50,'Mir Hussain Ali','ghulamtrading@gmail.com','ghulamtrading@gmail.com','7092111121','Business','Graduate','18/56, Akbar Street, Triplucane','Live for ithers you get Happy','Chennai','Yes','07:32:03 PM 17-Jul-2016','email',''),(51,'Saransh Vats','saranshvats05@gmail.com','saranshvats05@gmail.com','8252846989','A','10th passed','89 aurobindo apartment near NCERT gate adchini','idk','i get food from a mess and every time there is much food wasted','Yes','02:56:57 PM 18-Jul-2016','email',''),(52,'Jishad Mohammed','jishadmohd87@gmail.com','jishadmohd87@gmail.com','8281776910','Future Focus infotech','MSc IT','BTM 2nd Stage, Stage 2, Bengaluru, Karnataka, India','environmentalist, humanitarian optimist','to serve for a better tomorrow s','Yes','10:43:06 AM 19-Jul-2016','email',''),(53,'Narendra Sharma','kweb.sharma@gmail.com','kweb.sharma@gmail.com','9900313425','NetCracker Online Services Pvt. Ltd.','BBA','# 9, Sudhama Nagar, Bangalore - 560027','Compassionate. Industrious. Conscientious.','It makes me feel good & happy to be a part of a social organisation & FI is not feeding the hungers ','Yes','02:19:18 PM 19-Jul-2016','email',''),(54,'Ravindra Nikate','nikate.ravindra@yahoo.com','nikate.ravindra@yahoo.com','9404233470','Arrow Electronics','B.E. Electronics','Kothrud, Pune, Maharashtra, India','Helping nature , Confident ,Positive','Helping needy','Yes','11:51:05 PM 19-Jul-2016','email',''),(55,'Tejbir singh','sportsplayer@Gmail.com','taranbirkaur.tk@gmail.com','9803898006','vivek high school','student','#192 sec-40A CHANDIGARH','helpful,caring,supportive','I want to help poor people','Yes','03:07:36 PM 29-Jul-2016','email',''),(56,'urmeen kaur','urmeenkaur@ymail.com','taranbirkaur.tk@gmail.com','9815348995','sacred heart sr. sec. school','10','#192 sector 40A Chandigarh','responsible,helpful and sensitive','to share','Yes','09:29:43 PM 06-Aug-2016','email','');
/*!40000 ALTER TABLE `feedingindia_volunteers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:58

-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: friend
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_register`
--

DROP TABLE IF EXISTS `tbl_register`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_register` (
  `register_id` int(100) NOT NULL AUTO_INCREMENT,
  `reg_first_name` varchar(100) NOT NULL,
  `reg_last_name` varchar(100) NOT NULL,
  `reg_email_id` varchar(100) NOT NULL,
  `reg_gender` varchar(100) NOT NULL,
  `reg_dateof_birth` text NOT NULL,
  `reg_father_name` varchar(100) NOT NULL,
  `reg_father_contact` varchar(100) NOT NULL,
  `reg_address` varchar(100) NOT NULL,
  `reg_contact` varchar(100) NOT NULL,
  `reg_pincode` varchar(100) NOT NULL,
  `reg_course` varchar(100) NOT NULL,
  `reg_course_duration` varchar(100) NOT NULL,
  `reg_college` varchar(100) NOT NULL,
  `reg_number` text NOT NULL,
  `reg_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reg_userid` varchar(100) NOT NULL,
  PRIMARY KEY (`register_id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_register`
--

LOCK TABLES `tbl_register` WRITE;
/*!40000 ALTER TABLE `tbl_register` DISABLE KEYS */;
INSERT INTO `tbl_register` VALUES (1,'Rahul','Paul','rahulpal610@gmail.com','male','15/08/1998','','9050731060','Sonipat, Haryana','9034556964','131001','Android','6 Weeks','','REG_27317519','2016-04-29 06:45:14',''),(2,'Varun','Bhardwaj','thevarunonilne@gmail.com','male','10/08/1998','','8295741344','Haryana','8295741344','124507','Android','6 Weeks','','REG_48319085','2016-04-29 06:50:10',''),(3,'Ravinder','Pal Singh','singhravinder01@hotmail.com','male','23/10/1994','','8872976320','Chandigarh','8872976320','160055','PHP','6 Weeks','','REG_44930904','2016-04-30 07:02:26',''),(4,'Sanjay ','Kumar','sanjayk2797@gmail.com','male','27/08/1997','','9780531736','Fazilka, Punjab','9780531736','140601','PHP','6 Weeks','','REG_67752518','2016-04-30 07:04:30',''),(5,'Prashant','Pirjapati','prashantpirjapati1997@gmail.com','male','22/11/1997','','9540113966','Faridabad, Haryana','8571011725','121003','Networking','6 Weeks','GP, Sonipat','REG_35888809','2016-05-11 06:33:43',''),(6,'Sonu','Rajera','sonurajera@gmail.com','male','06/07/1998','','8010101093','Faridabad, Haryana','9996898626','121003','Networking','6 Weeks','GP, Sonipat','REG_45921942','2016-05-11 06:36:02',''),(7,'Aniket','.','aniket4161@gmail.com','male','22/12/1995','','9641114900','Sirsa, Haryana','8901119416','125055','Networking','6 Weeks','JCDMCOE, Sirsa','REG_63508169','2016-05-11 12:46:35',''),(8,'Vikash','Dangi','vikashdangi82@gmail.com','male','15/08/1997','','8930511814','Sonipat, Haryana','9467935329','131001','Networking','6 Weeks','GP%2C+Sonipat','REG_57609591','2016-05-12 07:52:09',''),(9,'sandeep','rani','sandeep@gmail.com','female','04/07/1998','','9872110415','bhamian khurdh tajpur road ludhiana','9517617293','141015','Java','6 Weeks','CG Ludhiana','REG_94989357','2016-05-12 09:17:59','1'),(10,'Harpinder','Pal kaur','harpinderjakhu@gmail.com','female','26/12/1998','','8146238602','VPO Mohie Dist. Ludhiana','7355998107','141015','Java','6 Weeks','GP Ludhiana','REG_66053948','2016-05-12 09:20:45',''),(11,'Kush','.','kush@gmail.com','male','16/08/1996','','9872680907','Kangra, HP','9872680907','140601','PHP','6 Weeks','SVIET, Banur','REG_38801887','2016-05-16 12:40:31',''),(12,'Pritam','Kumar','pritamkr362@gmail.com','male','17/03/1995','','8651190117','Muzaffarpur, Bihar','8194949931','842002','Java','6 Weeks','Gurukul, Banur','REG_78569855','2016-05-17 08:03:04',''),(13,'Jatinder','Singh','jatsingh@gmail.com','male','13/05/1996','','9780515574','SAS Nagar, Punjab','9781158005','140601','Networking','6 Weeks','SVIET%2C+Banur','REG_14982154','2016-05-18 09:22:56',''),(14,'Sumit','Kumar','sumitkumar7589477614@gmail.com','male','28/04/1996','','9465433004','Sangrur, Punjab','7589477614','148001','Java','6 Weeks','CGC, L','REG_39032035','2016-05-18 12:35:44',''),(15,'Naresh','Kumar','nishu95pdn@gmail.com','male','02/09/1996','','9876016368','Sangrur, Punjab','9501320606','148001','Java','6 Weeks','CGC%2C+L','REG_12217209','2016-05-18 12:37:14',''),(16,'Rupinder','Kaur','rkaur8694@gmail.com','female','16/07/1996','','9915781729','Patiala, Punjab','9781327826','147201','PHP','6 Months','BGIET, Sangrur','REG_80348533','2016-05-19 05:01:02',''),(17,'Nischal ','Singh','nischaljanu@gmail.com','male','03/10/1995','','9814103735','Sangrur, Punjab','7508003007','148026','PHP','6 Months','BGIET%2C+Sangrur','REG_85667282','2016-05-19 05:02:59',''),(18,'Bhupesh ','Gupta','bhupeshg75@gmail.com','male','19/05/2016','','9530543227','Ludhiana, Punjab','9815770587','141001','Networking','6 Weeks','CGC%2C+L','REG_16604200','2016-05-19 10:57:39',''),(19,'Manoj','Shrees','suwasmgr77@gmail.com','male','06/02/1996','','9805392632','Solan, HP','7508703643','140307','Java','6 Weeks','CGC%2C+L','REG_35210725','2016-05-19 10:57:41',''),(20,'Ashish','Kumar','a.ashishvashist.a@gmail.com','male','21/01/1996','','8195829562','Chandigarh','9592078945','160030','Networking','6 Weeks','CGC%2C+L','REG_24932798','2016-05-21 07:42:47',''),(21,'Sahil','Thakur','sahiladamos@gmail.com','male','08/05/1996','','9988992345','Chandigarh','9815479247','160030','Networking','6 Weeks','CGC%2C+L','REG_60270054','2016-05-21 07:42:54',''),(22,'Kartik','.','kartik_hasija@gmail.com','male','23/10/1993','','9354950512','Mohali, Punjab','7206509091','160061','Android','6 Weeks','CU, Gharuan','REG_65861472','2016-05-23 06:07:49',''),(23,'Milandeep ','Singh','uic.15mca8008@gmail.com','male','24/10/1994','','9888040112','Chandigarh','9888040112','160047','Android','6 Weeks','CU%2C+Gharuan','REG_47673274','2016-05-23 06:09:35',''),(24,'Sonia','.','soniamahi03@gmail.com','male','11/03/1994','','8872177235','Nawashehar, Punjab','8872177235','144524','Networking','6 Weeks','Doaba, Nawashehar','REG_71758258','2016-05-25 06:25:55',''),(25,'Parwinder','Kaur','proyal980@gmail.com','female','17/12/1992','','9463103162','SBS Nagar, Punjab','8195909664','144522','Networking','6 Weeks','Doaba%2C+Nawashehar','REG_88152026','2016-05-26 04:57:50',''),(26,'Jagjeet','Binjal','jagjeetsingh7787@gmail.com','male','10/08/1995','','9501257331','Batala, Punjab','9501257331','143001','PHP','6 Months','Global, ASR','REG_84730730','2016-05-26 07:23:13',''),(27,'Rittu','.','rd45133@gmail.com','female','22/07/1994','','9501257331','Amritsar, Punjab','8872425399','143001','PHP','6 Months','Global, ASR','REG_90558889','2016-05-26 07:23:21',''),(28,'Akash','Saggu','akashsaggu@protonmall.com','male','27/1/1996','','8146625393','Malerkotla','9888343111','148023','Android','6 Months','BGIET%2C+Sangrur','REG_42547797','2016-05-27 07:49:56',''),(29,'Lovepreet','Walia','lovepreetahluwalia1@gmail.com','female','11/07/1996','','9463425090','Banur, Patiala','9888047528','140601','Java','6 Weeks','CGC%2C+L','REG_38049819','2016-05-27 08:37:49',''),(30,'Arushi','.','arushi231996@gmail.com','female','23/06/1996','','7508065816','Chandigarh','7508065816','160062','Android','6 Weeks','CGC%2C+L','REG_12289941','2016-05-27 09:27:08',''),(31,'Manisha','.','manisha251997@gmail.com','female','25/01/1997','','7508118981','Rajpura, Punjab','7508118981','140601','Android','6 Weeks','CGC%2C+L','REG_13240672','2016-05-27 09:28:37',''),(32,'Manpreet','Singh','manpreetsingh@gmail.com','male','01/05/1994','','7696073865','Rajpura, Punjab','7696073865','1406011','Android','6 Weeks','CGC%2C+L','REG_52091045','2016-05-27 09:29:57',''),(33,'Summet','Kumar','mesumeetgf@gmail.com','male','08/02/1996','','9646594193','Mohali, Punjab','9646594193','160062','Android','6 Weeks','','REG_71000773','2016-05-27 09:31:08',''),(34,'Jashanvir','Singh','jasahansabby1@gmail.com','male','13/12/1995','','9501836156','Roopnagar, Punjab','9465588584','140101','Android','6 Weeks','CGC%2C+L','REG_95107839','2016-05-27 09:32:53',''),(35,'Lavish','MIttal','lavish.mittal1995@gmail.com','male','03/04/1995','','9872957718','Sangrur, Punjab','9872957718','148001','Android','6 Weeks','CGC%2C+L','REG_17491352','2016-05-27 09:41:44',''),(36,'Priya','.','as@gmail.com','female','27/05/2016','','1234567890','.','1234567890','141015','Networking','6 Weeks','Doaba%2C+Nawashehar','REG_68839977','2016-05-27 10:06:14',''),(37,'Mandeep ','Kuar','mk@gmail.com','female','09/05/2016','','1234567890','Chandigarh','2345678907','125055','Networking','6 Weeks','Doaba%2C+Nawashehar','REG_51355336','2016-05-27 10:07:08',''),(38,'Garima','.','garima@gmail.com','female','08/02/1998','','9466984160','Karnal, Haryana','9466984160','132117','Networking','6 Weeks','GP, Nilokhaeri','REG_67714952','2016-05-28 04:43:37',''),(39,'Krishna','.','krishna@gmail.com','female','15/02/2000','','9813300249','Karnal, Haryana','9812306142','132117','Networking','6 Weeks','GP, Nilokheri','REG_91635500','2016-05-28 04:44:04','');
/*!40000 ALTER TABLE `tbl_register` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:49:10

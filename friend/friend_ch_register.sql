-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: friend
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ch_register`
--

DROP TABLE IF EXISTS `ch_register`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ch_register` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(100) NOT NULL,
  `LastName` varchar(100) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `PhoneNumber` int(40) NOT NULL,
  `Address` text NOT NULL,
  `Postcode` int(40) NOT NULL,
  `dated` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ch_register`
--

LOCK TABLES `ch_register` WRITE;
/*!40000 ALTER TABLE `ch_register` DISABLE KEYS */;
INSERT INTO `ch_register` VALUES (1,'12','54','21','21',21,'21',12,'0000-00-00'),(2,'FirstName','LastName','Email@gmail.in','dc647eb65e6711e155375218212b3964',0,'Address',0,'0000-00-00'),(3,'harry','kumar','harry@gmail.com','dc647eb65e6711e155375218212b3964',2147483647,'b2/342 nawan shahr',144514,'0000-00-00'),(4,'harry','kumar','harry1@gmail.com','202cb962ac59075b964b07152d234b70',2147483647,'b2/342 nawan shahr',144514,'0000-00-00'),(5,'harry','kumar','harry2@gmail.com','202cb962ac59075b964b07152d234b70',2147483647,'b2/342 nawan shahr',144514,'0000-00-00'),(6,'harry','kumar','harry3@gmail.com','202cb962ac59075b964b07152d234b70',2147483647,'b2/342 nawan shahr',144514,'2016-07-25');
/*!40000 ALTER TABLE `ch_register` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:49:03

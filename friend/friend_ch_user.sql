-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: friend
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ch_user`
--

DROP TABLE IF EXISTS `ch_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ch_user` (
  `Id` int(32) unsigned NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(200) NOT NULL,
  `LastName` varchar(200) DEFAULT NULL,
  `Email` varchar(250) NOT NULL,
  `Password` varchar(40) NOT NULL,
  `PhoneNumber` varchar(20) NOT NULL,
  `Address` text NOT NULL,
  `Postcode` varchar(10) DEFAULT NULL,
  `EmergencyContactNumber` varchar(20) DEFAULT NULL,
  `EmergencyContactName` varchar(255) DEFAULT NULL,
  `LastLoginDate` datetime DEFAULT NULL,
  `Country` int(32) NOT NULL,
  `ResetHash` varchar(32) DEFAULT NULL,
  `OtherInformation` varchar(100) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ch_user`
--

LOCK TABLES `ch_user` WRITE;
/*!40000 ALTER TABLE `ch_user` DISABLE KEYS */;
INSERT INTO `ch_user` VALUES (69,'naro','sandhu','naro@gmail.com','123456','123456','kokkkkk','','455368','hjkk',NULL,0,NULL,''),(70,'Mathieu','Barrault','quazarm117@hotmail.com','123456','042563985','rjjcfj','','065545633','hffjk',NULL,0,NULL,''),(71,'sahil','goyal','sagappu@gmail.com','123456','9915410702','fghhhyhhh','','8885556699','9915410702',NULL,0,NULL,''),(72,'sahil','giyak','goyal.sahil929@gmail.com','123456','991410402','hahaha','','9915410702','hHZH',NULL,0,NULL,'');
/*!40000 ALTER TABLE `ch_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:59

-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: plybaza_test
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `seller_user`
--

DROP TABLE IF EXISTS `seller_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seller_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `designation` varchar(255) DEFAULT NULL,
  `phone` bigint(15) NOT NULL,
  `skype` varchar(200) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `website` varchar(200) DEFAULT NULL,
  `catalog` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `address` text,
  `business` varchar(255) DEFAULT NULL,
  `super_supplier` varchar(100) NOT NULL DEFAULT 'No',
  `prime_member` varchar(255) NOT NULL DEFAULT 'No',
  `verified_member` varchar(200) NOT NULL DEFAULT 'No',
  `trust_member` varchar(200) NOT NULL DEFAULT 'No',
  `created_on` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `phone` (`phone`)
) ENGINE=MyISAM AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seller_user`
--

LOCK TABLES `seller_user` WRITE;
/*!40000 ALTER TABLE `seller_user` DISABLE KEYS */;
INSERT INTO `seller_user` VALUES (61,'nz',' zxhjcsdc','bsnjxjdf',88858,NULL,'xeknke','nddjsd','jscdjcjs','ndjdkned','bxcjsbc','xnckdck','njscke','Service provider','No','No','No','No',NULL),(62,'565','hgjhj','jhghgjhgj',0,NULL,'hjhgj','hjhgghj','hgjhj','hgjg','ghjhj','gjhgj','hgjg','Suplies','No','No','Yes','No',NULL),(63,'Parveen Kansal','Kansal','managing director',9855322217,'','rama plywood pvt. ltd','','http://plybazar.com/Rama-Plywood-47/','patran','punjab','india','Village Nial, Patiala Road Patran-147105 Distt. Patiala Punjab','','No','No','Yes','Yes','2017-03-06 19:07:53'),(25,'abhishek','kumar','ceo',9878498056,'','plybazar','plybazar.com','plybazar.com/demo','mohali','punjab','india','7phase mohali punjab','Manufacturers','No','No','No','No','2016-09-19 18:46:23'),(64,'Admin','ss','ss',123456789,'','sssssssss','sssssssssss','ssss','ssssss','ssss','sss','sssssss','Manufacturers','No','No','No','No','2017-02-20 23:30:33'),(65,'Naveen','Dogra','CEO',8591749133,'','Dogra Info Sys','','','Chandigarh','Punjab','Chandigarh','Chandigarh','Manufacturers','No','No','No','No','2017-02-21 00:33:38'),(66,'parminder','singh','gm',9023920021,'','pinder info','pinder.com','','sirsa','hry','india','sirsa haryana','Manufacturers','No','No','No','No','2017-02-26 17:38:29'),(67,'dfgfgf','fgfg','fgfgf',2258884,NULL,'hfghgh','ghghgh','ghghgh','ghghgh','dfg','fgfdg','fgf','Traders','No','No','No','No',NULL),(68,'dfdf','dfdf','dfdf',46545,NULL,'5ghg','ghg','ghgh','ghg','ghgh','gh','gh','Exporter','No','No','No','No',NULL),(69,'deepak goyal','goyal','md',9815451663,'','ravindra pipes','','  http://plybazar.com/Ravindra-Pipes-126','Ghagga','punjab','india',' Vill.Dedhna P.O. Ghagga Distt.Patiala','','No','No','Yes','No','2017-03-19 13:14:56'),(70,'raj','kumar','md',9023650049,'','rama enterprises','','','patiala','punjab','india','rajpura road patiala','Manufacturers','No','No','No','No','2017-03-28 16:53:00');
/*!40000 ALTER TABLE `seller_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:39

-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: plybaza_test
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `plyusers`
--

DROP TABLE IF EXISTS `plyusers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plyusers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role` int(11) NOT NULL DEFAULT '2',
  `mobile` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `otp` varchar(255) NOT NULL,
  `lotp` varchar(255) NOT NULL DEFAULT '01234',
  `name` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `lname` varchar(100) DEFAULT NULL,
  `company_name` varchar(200) DEFAULT NULL,
  `designation` varchar(255) DEFAULT NULL,
  `address` text,
  `country` varchar(200) DEFAULT NULL,
  `state` varchar(200) DEFAULT NULL,
  `phone` int(10) DEFAULT NULL,
  `website` varchar(200) DEFAULT NULL,
  `pimg` varchar(100) DEFAULT NULL,
  `cimg` varchar(100) DEFAULT NULL,
  `status` varchar(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `state` (`state`)
) ENGINE=MyISAM AUTO_INCREMENT=135 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plyusers`
--

LOCK TABLES `plyusers` WRITE;
/*!40000 ALTER TABLE `plyusers` DISABLE KEYS */;
INSERT INTO `plyusers` VALUES (6,2,'8591749133','nk359971@gmail.com','','01234','Naveen','12345678','Chandigarh sdg asdf as',NULL,NULL,'My companysdg  asdf asd sd f sdf','ceosd gf asdf sdf ','xgdsfdfg sdfds','Indiadfg  asd','UT sdgf asdf ',2147483647,'plybazar.comvvv asdf','img/4534.jpg','img/miss.jpg','0'),(7,2,'','parminderkamboj0001@gmail.com','9872','7615','parminder','12345','test',NULL,NULL,'Ply bazar','test','vdsfvds','test','test',2147483647,'http://plybazar.com/my-profile.php','img/1488286871.jpg','img/media-room.jpg','0'),(8,2,'8607224773','lovely@gmail.com','','01234','lovely','12345','demo',NULL,NULL,'demo','demo','demo','demo','demo',2147483647,'demo.com','img/ppppppppppppppp.png','img/Privacy-Banner_6.jpg','0'),(9,2,'','rajkansal49@gmail.com','9872','7615','raj','12345',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'img/1488286871.jpg','img/1500x500.jpg','0'),(10,2,'9872315196','jasmeet.mce@gmail.com','','01234','jasmeet','123456',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0'),(12,2,'9023750049','gulshan@gmail.com','','01234','gulshan','242047',NULL,'2016-07-29 16:46:14',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'img/IMG-20160424-WA0008.jpg',NULL,'0'),(13,2,'9023650049','','','5757','raj','242047','Chandigarh','2016-08-01 23:23:58',NULL,'Plybazar','Director','chandigarh','india','Punjab',1724003803,'raj.com','img/12472796_1150274984983560_8987241357745079509_n.jpg','img/5.jpg','1'),(14,2,'09501657137','parminderkamboj001@gmail.com','','01234','Parminder Singh','123456',NULL,'2016-08-01 23:24:49',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0'),(15,2,'8427431245','vivek.chandel.146@gmail.com','','01234','vivek','1234567',NULL,'2016-08-01 23:26:31',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'img/GLUE-SPREADER 5.jpg','img/111218.gif','0'),(16,2,'9813921707','sonu@gmail.com','','01234','sonu','12345678',NULL,'2016-08-02 07:50:45',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'img/4.jpg',NULL,'0'),(17,2,'8197312312','surender@gmail.com','','01234','surender','12345678',NULL,'2016-08-02 17:55:07',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'img/aap-icon2d-180-180.png','img/plybazar-facebook.png','0'),(18,2,'9779422150','ak26883@gmail.com','','01234','demo','arun@123',NULL,'2016-08-03 00:23:36',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0'),(19,2,'9888809017','kansal017@gmail.com','','01234','kunal','9888809017',NULL,'2016-08-04 20:55:15',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0'),(20,1,'123456789','info@plybazar.com','','01234','Admin','12345',' asdf','2016-08-07 23:55:20',NULL,'teste','ceo','aasdf','a sdf','asdf ',123456879,'plybazar.com','img/IMG_1573.JPG','img/01.jpg','0'),(21,2,'9780869622','monnu@gmail.com','','01234','monnu','12345',NULL,'2016-08-08 12:08:16',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'img/4.jpg','img/26..jpg','0'),(22,2,'8059405699','pp@gmail.com','','01234','pinder','12345678',NULL,'2016-08-10 17:11:41',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0'),(23,2,'8568080005','13genious13@gmail.com','','1494','Amandeep Singh','admin11',NULL,'2016-08-15 13:11:13',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'img/banner-1.jpg','img/Donald Trump Pounds Hillary Clinton in Portland, ME.jpg','1'),(24,2,'9878498056','abhishek.chandel@gmail.com','','01234','abhishek','12345','test3','2016-08-15 19:51:49',NULL,'test3','test3','test3','testestae3','est3',1234567985,'teasetaset3','img/your-profile-plybazar.jpg','img/cover-image.jpg','0'),(25,2,'9991421707','Parm001@gmail.com','','01234','Parminder Kamboj','12345',NULL,'2016-08-30 09:20:33',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'img/12472796_1150274984983560_8987241357745079509_n.jpg','img/10368274_864961386895670_3057792676331569297_n.jpg','0'),(26,2,'8195099797','gulnaritsolutions@gmail.com','','01234','Gaurav','12345',NULL,'2016-09-09 20:37:45',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0'),(27,2,'9812096967','anil@gmail.com','','01234','anil','12345',NULL,'2016-09-15 00:16:01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0'),(28,2,'9915785659','goru@gmail.com','','01234','goru','12345',NULL,'2016-09-15 22:36:36',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0'),(29,2,'9855108249','raaj@gmail.com','','01234','raj','242047',NULL,'2016-09-17 16:59:05',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0'),(30,2,'8198827637','deepakkansal30@gmail.com','','01234','Deepak Kansal','9074968000',NULL,'2016-09-18 16:44:41',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0'),(31,2,'9023920021','info@punjabnewstimes.com','','9706','parminder','123456',NULL,'2017-01-12 21:28:58',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'img/Pinder kamboj.jpg','img/beautiful-hd-wallpaper-2015_111526537_269.jpg','1'),(42,2,'9988223221','kumarmanjeet02@gmail.com','7008','5772','manjeet','12345',NULL,'2017-01-31 13:02:37',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'img/user.png',NULL,'1'),(33,2,'9812092416','jagmohankalyan@gmail.com','','01234','Jagmohan Singh','123456',NULL,'2017-01-26 12:46:45',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'img/Kalyan_logo.jpg','img/banner.jpg','0'),(44,2,'9988787800','ruchi@gmail.com','','01234','ruchi','242047',NULL,'2017-02-08 17:51:23',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'img/FB_IMG_1486304452172.jpg','img/FB_IMG_1486477021559.jpg','0'),(38,2,'8000965124','digenkeshrani@yahoo.com','','01234','DIGEN PATEL','surface','SURAT','2017-01-30 12:08:15',NULL,'','','','','Gujarat',2147483647,'','img/',NULL,'0'),(46,2,'9464048158','sales@Rajanplywood.com','','01234','Rajan','123456',NULL,'2017-02-11 13:14:51',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'img/rajan-plywood-logo-jpg.jpg','img/rajan-plywood-banner.jpg','0'),(47,2,'9855322217','ramaplywood687@gmail.com','','7923','Parveen Kansal','123456',NULL,'2017-02-11 20:01:08',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'img/Parveen-Kansal.jpg','img/banner03.jpg','0'),(118,2,'9988223221','ffrg@gmail.com','7008','5772','msn','123',NULL,'2017-02-13 18:38:42',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'img/user.png',NULL,'1'),(119,2,'9216331313','ssd@gmail.com','8220','01234','aman','235785',NULL,'2017-02-14 18:56:21',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0'),(122,2,'9876908138','kanwardesk@gmail.com','2957','4110','jasvir','1234',NULL,'2017-02-16 18:41:34',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1'),(121,2,'9646874312','jasvir.webdesker@gmail.com','1383','5976','jasvir','1234',NULL,'2017-02-16 17:17:25',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'img/1488953490.jpg','img/notepad++.exe','1'),(123,2,'9814041028','ambeyplywood217@gmail.com','','01234','Romi','123456',NULL,'2017-02-18 13:19:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0'),(125,2,'9417226665','avmhome.officesolutions@gmail.com','','01234','Anmol Mahajan','123456',NULL,'2017-03-18 11:46:05',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0'),(124,2,'9427496341','test@me.com','8274','01234','test','123456',NULL,'2017-02-24 17:05:52',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1'),(126,2,'9815451663','ravindrapipes49@gmail.com','','01234','deepak goyal','123456',NULL,'2017-03-18 22:30:14',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0'),(127,2,'9815430304','harman.bhalla1992@gmail.com','','01234','Harman','Harmanbhalla1992',NULL,'2017-03-24 17:55:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0'),(128,2,'9888841137','AggarwalBricksCo49@gmail.com','','01234','kiln','123456',NULL,'2017-03-25 13:55:39',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0'),(129,2,'7888603032','babbandeep1994@gmail.com','','01234','babban','123456',NULL,'2017-04-04 10:55:29',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0'),(133,2,'8872292478','sbsunilbhatia9@gmail.com','','01234','Sunil','12345678',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'File768280.png','File134887.png','0');
/*!40000 ALTER TABLE `plyusers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:39

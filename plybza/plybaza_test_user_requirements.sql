-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: plybaza_test
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user_requirements`
--

DROP TABLE IF EXISTS `user_requirements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_requirements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `description` text,
  `approve` varchar(255) NOT NULL DEFAULT 'No',
  `created_on` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_requirements`
--

LOCK TABLES `user_requirements` WRITE;
/*!40000 ALTER TABLE `user_requirements` DISABLE KEYS */;
INSERT INTO `user_requirements` VALUES (1,'Naveen','naveedogra@gmail.com','8591749133','USA','testnew','testing.......asd........','Yes','2016-10-05 23:38:55'),(2,'raj','myplybazar@gmail.com','9023650049','india','hot press','gvhvhyguih','No','2016-09-18 20:49:07'),(3,'anil','parminder@gmail.com','9812096967','india','hot press','demo','No','2017-02-24 15:03:45'),(4,'Admin','aajkamla49@gmail.com','123456789','India','hot press','demo','No','2016-09-20 13:16:45'),(5,'abhishek','abhishek@gmail.com','9878498056','india','Hot press','demo demo','No','2016-09-21 07:11:06'),(6,'raj','kansal.raj@rediffmail.com','9023650049','india','hot press','bbbbmbb nnnm mnmm','No','2016-11-01 22:13:57'),(7,'abhishek','parminderkamboj0001@gmail.com','9878498056','india','hot press','i need 2 hot press machines','No','2016-09-28 08:29:13'),(8,'abhishek','abhishek@gmail.com','9878498056','india','Hot Press','i want Buy Hot press','No','2016-10-02 08:36:43'),(9,'raj','rajkansal49@gmail.com','9023650049','india','plywood','want to buy plywood','No','2016-11-01 22:14:47'),(13,'rrdv','gdvb@gmail.com','9501657435','india','demo','vcxvcnx','No','2017-02-22 20:33:33'),(14,'jasvir man','jasvir.webdeskers@gmail.comfy','','india','Bed','manjeet product herehchfuu','Yes','2017-03-17 11:35:44'),(16,'jasvir','jasvir.webdeskers@gmail.com','9646874312','india','beds','vcucfigonj','No','2017-02-24 15:07:31'),(17,'parminder','parminderkamboj0001@gmail.com','9023920021','india','Demo','http://www.exportersindia.com/advertise/banner-advertising.html','No','2017-02-24 22:22:59'),(18,'parminder','parminderkamboj0001@gmail.com','9023920021','india','Demo','http://www.exportersindia.com/advertise/banner-advertising.html','No','2017-02-24 22:40:01'),(19,'parminder','parminderkamboj0001@gmail.com','9023920021','india','Demo','http://www.exportersindia.com/advertise/banner-advertising.html','No','2017-02-24 22:40:15'),(20,'parminder','parminderkamboj0001@gmail.com','9023920021','india','Demo','http://www.exportersindia.com/advertise/banner-advertising.html','Yes','2017-02-24 23:01:17'),(21,'fvgyb','fghtby','47','byb','gbhn','hb','No','2017-02-28 12:09:14'),(22,'bhbgyg','hyh','5288','nhhh','nhjm','njjkk','No','2017-03-09 14:15:12'),(23,'hjhgigvi','giyyghiy','05558','bhbhjh','buuhu','bhbhg','No','2017-03-17 11:08:11'),(24,'hjhgigvi','giyyghiy','05558','bhbhjh','buuhu','bhbhg','No','2017-03-17 11:08:13'),(25,'hjhgigvi','giyyghiy','05558','bhbhjh','buuhu','bhbhg','No','2017-03-17 11:08:15'),(26,'jmnnn','jnnn','888','mmm','mmmm','mm','No','2017-03-17 11:12:17');
/*!40000 ALTER TABLE `user_requirements` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:39

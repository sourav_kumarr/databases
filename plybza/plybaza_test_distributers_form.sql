-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: plybaza_test
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `distributers_form`
--

DROP TABLE IF EXISTS `distributers_form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `distributers_form` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` bigint(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `gender` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `distributers_form`
--

LOCK TABLES `distributers_form` WRITE;
/*!40000 ALTER TABLE `distributers_form` DISABLE KEYS */;
INSERT INTO `distributers_form` VALUES (25,'vikash','vikash@gmail.com',12345,'india','test message','I am looking for Distributor'),(24,'vikash','vikash@gmail.com',12345,'india','test message','I am looking for Distributor'),(23,'vikash','vikash@gmail.com',12345,'india','test message','I am looking for Distributor'),(21,'fghfg','fghf',57287,'fghfgh','fghfg',''),(22,'fghgf','fghfg',75875,'hfgh','gfhfg',''),(19,'mm','mmm',0,'fghfg','fgh',''),(20,'hfg','fgh',4524242,'vn','fghfgfghfg',''),(18,'hjj','nnj',5588,'nmm','mmm','I am looking for Distributor'),(17,' jnuh','nnbb',8855,'nnnn','nnn','I am looking for Distributor'),(16,'cff','gfff',444444,'gggggg','gghg','I want to become a Distributor'),(15,'nvb','gf',444,'ggggg','ggh','I am looking for Distributor'),(13,'vikash','vikash@gmail.com',12345,'india','test message','I am looking for Distributor'),(14,'bbbv','bh',12,'uju','hhh','false'),(26,'vikash','vikash@gmail.com',12345,'india','test message','I am looking for Distributor'),(27,'vikash','vikash@gmail.com',12345,'india','test message','I am looking for Distributor'),(28,'vikash','vikash@gmail.com',12345,'india','test message','I am looking for Distributor'),(29,'vikash','vikash@gmail.com',12345,'india','test message','I am looking for Distributor'),(30,'vikash','vikash@gmail.com',12345,'india','test message','I am looking for Distributor'),(31,'vikash','vikash@gmail.com',12345,'india','test message','I am looking for Distributor'),(32,'vikash','vikash@gmail.com',12345,'india','test message','I am looking for Distributor'),(33,'vikash','vikash@gmail.com',12345,'india','test message','I am looking for Distributor'),(34,'vikash','vikash@gmail.com',12345,'india','test message','I am looking for Distributor'),(35,'vikash','vikash@gmail.com',12345,'india','test message','I am looking for Distributor'),(36,'vikash','vikash@gmail.com',12345,'india','test message','I am looking for Distributor'),(37,'vikash','vikash@gmail.com',12345,'india','test message','I am looking for Distributor'),(38,'vikash','vikash@gmail.com',12345,'india','test message','I am looking for Distributor'),(39,'vikash','vikash@gmail.com',12345,'india','test message','I am looking for Distributor'),(40,'retge','rtre',25524,'zxcz','dfgvbdf',''),(41,'retge','rtre',25524,'zxcz','dfgvbdf',''),(42,'jasvi','jas@gmail.com',123456,'jasvi','hay ','I am looking for Distributor'),(43,'ghghghghgh','vikash@gmail.com',12345,'india','test message','I am looking for Distributor'),(44,'kjkjkjkj','vikash@gmail.com',12345,'india','test message','I am looking for Distributor'),(45,'jhjhjh','vikash@gmail.com',12345,'india','test message','I am looking for Distributor'),(46,'kkkkkkkkkkkkkkkkkkkkk','vikash@gmail.com',12345,'india','test message','I am looking for Distributor'),(47,'m','vv',788,'bnn','bn','I am looking for Distributor'),(48,'','',0,'','','');
/*!40000 ALTER TABLE `distributers_form` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:37

-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: plybaza_test
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `service_provider_form`
--

DROP TABLE IF EXISTS `service_provider_form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_provider_form` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `svname` varchar(255) NOT NULL,
  `svemail` varchar(255) NOT NULL,
  `svphone` varchar(255) NOT NULL,
  `svcountry` varchar(255) NOT NULL,
  `svmessage` varchar(255) NOT NULL,
  `svlabour` varchar(255) NOT NULL,
  `svgender` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_provider_form`
--

LOCK TABLES `service_provider_form` WRITE;
/*!40000 ALTER TABLE `service_provider_form` DISABLE KEYS */;
INSERT INTO `service_provider_form` VALUES (1,'gff','vikash@gmail.com','54364','fhgf','bvcbcvc','Press Operator','I am looking for Service Provider'),(2,'vikash','vikash@gmail.com','54364','fhgf','bvcbcvc','Press Operator','I am looking for Service Provider'),(3,'kkkkk','vikash@gmail.com','54364','fhgf','bvcbcvc','Press Operator','I am looking for Service Provider'),(4,'ppppppppp','vikash@gmail.com','54364','fhgf','bvcbcvc','Press Operator','I am looking for Service Provider'),(5,'fgfg','vikash@gmail.com','54364','fhgf','bvcbcvc','Press Operator','I am looking for Service Provider'),(6,'vhvj','vjhb','778','bnn','bnm','I am looking for Distributor','Labour Contractor'),(7,'vbff','vjhbvh','778888','bnnbnnm','bnmbnn','I am looking for Distributor','Labour Contractor'),(8,'','','','','','','');
/*!40000 ALTER TABLE `service_provider_form` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:38

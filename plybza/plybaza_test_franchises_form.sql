-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: plybaza_test
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `franchises_form`
--

DROP TABLE IF EXISTS `franchises_form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `franchises_form` (
  `frid` int(20) NOT NULL AUTO_INCREMENT,
  `frname` varchar(255) NOT NULL,
  `fremail` varchar(255) NOT NULL,
  `frphone` bigint(255) NOT NULL,
  `frcountry` varchar(255) NOT NULL,
  `frmessage` text NOT NULL,
  `frgender` varchar(255) NOT NULL,
  PRIMARY KEY (`frid`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `franchises_form`
--

LOCK TABLES `franchises_form` WRITE;
/*!40000 ALTER TABLE `franchises_form` DISABLE KEYS */;
INSERT INTO `franchises_form` VALUES (3,'ghf','vikash@gmail.com',56,'fgdf','fdgdf','I am looking to appoint franchisee'),(4,'ghf','vikash@gmail.com',56,'fgdf','fdgdf','I am looking to appoint franchisee'),(5,'ghf','vikash@gmail.com',56,'fgdf','fdgdf','I am looking to appoint franchisee'),(6,'ghf','vikash@gmail.com',56,'fgdf','fdgdf','I am looking to appoint franchisee'),(7,'ghf','vikash@gmail.com',56,'fgdf','fdgdf','I am looking to appoint franchisee'),(8,'ghf','vikash@gmail.com',56,'fgdf','fdgdf','I am looking to appoint franchisee'),(9,'ghf','vikash@gmail.com',56,'fgdf','fdgdf','I am looking to appoint franchisee'),(10,'ghf','vikash@gmail.com',56,'fgdf','fdgdf','I am looking to appoint franchisee'),(11,'ghf','vikash@gmail.com',56,'fgdf','fdgdf','I am looking to appoint franchisee'),(12,'vikash','vikash@gmail.com',4353,'fdgf','fdgd','I am looking to appoint franchisee'),(13,'vikash','vikash@gmail.com',566,'gfhfg','gfhgh','I am looking to appoint franchisee'),(14,'vikashh','vikash@gmail.com',5764,'gfh','ghfg','I am looking to appoint franchisee'),(15,'','',0,'','','false'),(16,'ncmfmm','mmgbg',0,'mvmv!','mmvmvmv','true'),(17,'mdmdnndnd s','mdnnfnfhgtxgbd',12345,'jfjjf','jdjjf','true'),(18,'gfgfhf','vikashfggmail.com',5565656,'fgdf','fdgdf','I am looking to appoint franchisee'),(19,'gfgffgfgfghf','vikashfg@gmail.com',5565656,'fgdf','fdgdf','I am looking to appoint franchisee'),(20,'rrt','vikashfg@gmail.com',5565656,'fgdf','fdgdf','I am looking to appoint franchisee'),(21,'kkkk',' j8v@gmail.com',0,'vjbb','bkbkn','true'),(22,'njj','mmm',585,'nnm','mm','I am looking for Distributor'),(23,'m','ngh',89654,'jjh','mjk','I want to become a Distributor'),(24,'gvb','bbn',888,'bnn','nnm','I am looking for Distributor');
/*!40000 ALTER TABLE `franchises_form` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:39

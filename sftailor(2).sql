-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 07, 2017 at 07:13 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sftailor`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_menu`
--

CREATE TABLE IF NOT EXISTS `wp_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(255) NOT NULL,
  `menu_link` varchar(255) NOT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `wp_menu`
--

INSERT INTO `wp_menu` (`menu_id`, `menu_name`, `menu_link`) VALUES
(1, 'Price List', 'index.php?type=price_list'),
(2, 'Add Coupons', 'index.php?type=Coupons'),
(3, 'Order Management', 'orders.php'),
(4, 'Stylist Management', 'stores.php'),
(5, 'Style Genie Allowances', 'allowance.php'),
(6, 'Customer Measurement', 'stylegenie.php'),
(7, 'Refer Payment', 'paytostoreowner.php'),
(8, 'Commission Level', 'levelcommission.php'),
(9, 'User Management', 'users.php'),
(10, 'Invoice', 'ordersp.php'),
(11, 'Administrator', 'administrator.php'),
(12, 'Supplier', 'supplier.php'),
(13, 'Supplier Orders', 'supplier_order.php');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

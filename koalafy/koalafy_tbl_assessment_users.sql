-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: koalafy
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_assessment_users`
--

DROP TABLE IF EXISTS `tbl_assessment_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_assessment_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_first_name` varchar(200) NOT NULL,
  `user_last_name` varchar(200) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `user_company` varchar(200) NOT NULL,
  `user_phone` varchar(200) NOT NULL,
  `user_full_name` varchar(200) NOT NULL,
  `user_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_assessment` varchar(200) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_assessment_users`
--

LOCK TABLES `tbl_assessment_users` WRITE;
/*!40000 ALTER TABLE `tbl_assessment_users` DISABLE KEYS */;
INSERT INTO `tbl_assessment_users` VALUES (10,'','','','','','Unknown1','0000-00-00 00:00:00','0777d5c17d4066b82ab86dff8a46af6f'),(11,'','','','','','Unknown2','0000-00-00 00:00:00','0777d5c17d4066b82ab86dff8a46af6f'),(12,'sourav','kumar','d@d.com','','9041634625','','0000-00-00 00:00:00','0777d5c17d4066b82ab86dff8a46af6f'),(13,'dd','dd','a@d.vvv','','','','0000-00-00 00:00:00','d67d8ab4f4c10bf22aa353e27879133c'),(14,'dd','dd','s@d.com','','','','0000-00-00 00:00:00','d67d8ab4f4c10bf22aa353e27879133c'),(15,'ddddddddd','dd','e@f.com','','','','0000-00-00 00:00:00','d67d8ab4f4c10bf22aa353e27879133c'),(16,'ghgh','xxdds','c@s.com','','','','0000-00-00 00:00:00','d67d8ab4f4c10bf22aa353e27879133c'),(17,'dd','fff','w@e.com','','','','0000-00-00 00:00:00','d67d8ab4f4c10bf22aa353e27879133c'),(18,'ddddd','ss','d@gg.com','','','','0000-00-00 00:00:00','d67d8ab4f4c10bf22aa353e27879133c'),(19,'dddddd','dd','df@f.com','','','','0000-00-00 00:00:00','d67d8ab4f4c10bf22aa353e27879133c'),(20,'sourav','kumar','nk3527@gmail.com','','','','0000-00-00 00:00:00','d67d8ab4f4c10bf22aa353e27879133c'),(21,'sd','sds','nk3527@gmail.com','','','','2017-03-16 13:05:35','d67d8ab4f4c10bf22aa353e27879133c'),(22,'','','','','','Unknown1','2017-03-25 13:04:31','7f39f8317fbdb1988ef4c628eba02591'),(23,'','','','','','Unknown2','2017-03-25 13:04:43','7f39f8317fbdb1988ef4c628eba02591');
/*!40000 ALTER TABLE `tbl_assessment_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:19

-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: koalafy
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_assessment_delivery`
--

DROP TABLE IF EXISTS `tbl_assessment_delivery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_assessment_delivery` (
  `del_id` int(100) NOT NULL AUTO_INCREMENT,
  `del_user_id` bigint(200) NOT NULL,
  `del_assessment_id` text NOT NULL,
  `del_path` text NOT NULL,
  `del_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `del_updated_at` text NOT NULL,
  PRIMARY KEY (`del_id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_assessment_delivery`
--

LOCK TABLES `tbl_assessment_delivery` WRITE;
/*!40000 ALTER TABLE `tbl_assessment_delivery` DISABLE KEYS */;
INSERT INTO `tbl_assessment_delivery` VALUES (33,5,'d67d8ab4f4c10bf22aa353e27879133c','http://localhost/koalafy/koalafy.testy/d67d8ab4f4c10bf22aa353e27879133c.php','2017-03-09 13:27:36',''),(34,5,'f033ab37c30201f73f142449d037028d','http://localhost/koalafy/koalafy.testy/f033ab37c30201f73f142449d037028d.php','2017-03-15 09:55:45',''),(35,5,'43ec517d68b6edd3015b3edc9a11367b','http://localhost/koalafy/koalafy.testy/43ec517d68b6edd3015b3edc9a11367b.php','2017-03-15 09:56:39',''),(36,5,'9778d5d219c5080b9a6a17bef029331c','http://localhost/koalafy/koalafy.testy/9778d5d219c5080b9a6a17bef029331c.php','2017-03-15 10:22:16',''),(37,5,'fe9fc289c3ff0af142b6d3bead98a923','http://localhost/koalafy/koalafy.testy/fe9fc289c3ff0af142b6d3bead98a923.php','2017-03-16 07:23:14',''),(38,5,'68d30a9594728bc39aa24be94b319d21','http://localhost/koalafy/koalafy.testy/68d30a9594728bc39aa24be94b319d21.php','2017-03-16 07:27:22',''),(39,5,'3ef815416f775098fe977004015c6193','http://localhost/koalafy/koalafy.testy/3ef815416f775098fe977004015c6193.php','2017-03-16 07:30:58',''),(40,5,'072b030ba126b2f4b2374f342be9ed44','http://localhost/koalafy/koalafy.testy/072b030ba126b2f4b2374f342be9ed44.php','2017-03-17 15:49:38',''),(41,5,'93db85ed909c13838ff95ccfa94cebd9','http://localhost/koalafy/koalafy.testy/93db85ed909c13838ff95ccfa94cebd9.php','2017-03-20 08:04:22',''),(42,5,'c7e1249ffc03eb9ded908c236bd1996d','http://localhost/koalafy/koalafy.testy/c7e1249ffc03eb9ded908c236bd1996d.php','2017-03-20 08:05:50',''),(43,5,'2a38a4a9316c49e5a833517c45d31070','http://localhost/koalafy/koalafy.testy/2a38a4a9316c49e5a833517c45d31070.php','2017-03-20 08:06:26',''),(44,5,'7647966b7343c29048673252e490f736','http://localhost/koalafy/koalafy.testy/7647966b7343c29048673252e490f736.php','2017-03-20 13:56:53',''),(45,5,'8613985ec49eb8f757ae6439e879bb2a','http://localhost/koalafy/koalafy.testy/8613985ec49eb8f757ae6439e879bb2a.php','2017-03-20 13:57:27',''),(46,5,'54229abfcfa5649e7003b83dd4755294','http://localhost/koalafy/koalafy.testy/54229abfcfa5649e7003b83dd4755294.php','2017-03-20 14:04:53',''),(47,5,'92cc227532d17e56e07902b254dfad10','http://localhost/koalafy/koalafy.testy/92cc227532d17e56e07902b254dfad10.php','2017-03-20 14:05:03',''),(48,13,'67c6a1e7ce56d3d6fa748ab6d9af3fd7','http://localhost/koalafy/koalafy.test/67c6a1e7ce56d3d6fa748ab6d9af3fd7.php','2017-03-22 11:56:55',''),(49,5,'98dce83da57b0395e163467c9dae521b','http://localhost/koalafy/koalafy.testy/98dce83da57b0395e163467c9dae521b.php','2017-03-23 07:46:10',''),(50,5,'f4b9ec30ad9f68f89b29639786cb62ef','http://localhost/koalafy/koalafy.testy/f4b9ec30ad9f68f89b29639786cb62ef.php','2017-03-23 07:48:52',''),(51,5,'812b4ba287f5ee0bc9d43bbf5bbe87fb','http://localhost/koalafy/koalafy.testy/812b4ba287f5ee0bc9d43bbf5bbe87fb.php','2017-03-23 07:56:22',''),(52,5,'26657d5ff9020d2abefe558796b99584','http://localhost/koalafy/koalafy.testy/26657d5ff9020d2abefe558796b99584.php','2017-03-23 10:40:57',''),(53,5,'e2ef524fbf3d9fe611d5a8e90fefdc9c','http://localhost/koalafy/koalafy.testy/e2ef524fbf3d9fe611d5a8e90fefdc9c.php','2017-03-23 10:45:46',''),(54,5,'ed3d2c21991e3bef5e069713af9fa6ca','http://localhost/koalafy/koalafy.testy/ed3d2c21991e3bef5e069713af9fa6ca.php','2017-03-23 10:47:51',''),(55,5,'ac627ab1ccbdb62ec96e702f07f6425b','http://localhost/koalafy/koalafy.testy/ac627ab1ccbdb62ec96e702f07f6425b.php','2017-03-23 11:03:56',''),(56,5,'f899139df5e1059396431415e770c6dd','http://localhost/koalafy/koalafy.testy/f899139df5e1059396431415e770c6dd.php','2017-03-23 11:12:17',''),(57,5,'38b3eff8baf56627478ec76a704e9b52','http://localhost/koalafy/koalafy.testy/38b3eff8baf56627478ec76a704e9b52.php','2017-03-23 11:23:12',''),(58,5,'ec8956637a99787bd197eacd77acce5e','http://localhost/koalafy/koalafy.testy/ec8956637a99787bd197eacd77acce5e.php','2017-03-23 11:24:32',''),(59,5,'6974ce5ac660610b44d9b9fed0ff9548','http://localhost/koalafy/koalafy.testy/6974ce5ac660610b44d9b9fed0ff9548.php','2017-03-23 11:40:17',''),(60,5,'c9e1074f5b3f9fc8ea15d152add07294','http://localhost/koalafy/koalafy.testy/c9e1074f5b3f9fc8ea15d152add07294.php','2017-03-23 11:42:51',''),(61,5,'65b9eea6e1cc6bb9f0cd2a47751a186f','http://localhost/koalafy/koalafy.testy/65b9eea6e1cc6bb9f0cd2a47751a186f.php','2017-03-23 13:39:48',''),(62,5,'f0935e4cd5920aa6c7c996a5ee53a70f','http://localhost/koalafy/koalafy.testy/f0935e4cd5920aa6c7c996a5ee53a70f.php','2017-03-23 13:39:50',''),(63,5,'a97da629b098b75c294dffdc3e463904','http://localhost/koalafy/koalafy.testy/a97da629b098b75c294dffdc3e463904.php','2017-03-23 13:39:51',''),(64,5,'a3c65c2974270fd093ee8a9bf8ae7d0b','http://localhost/koalafy/koalafy.testy/a3c65c2974270fd093ee8a9bf8ae7d0b.php','2017-03-23 13:39:53',''),(65,5,'2723d092b63885e0d7c260cc007e8b9d','http://localhost/koalafy/koalafy.testy/2723d092b63885e0d7c260cc007e8b9d.php','2017-03-24 06:16:09',''),(66,5,'5f93f983524def3dca464469d2cf9f3e','http://localhost/koalafy/koalafy.testy/5f93f983524def3dca464469d2cf9f3e.php','2017-03-24 06:19:20',''),(67,5,'698d51a19d8a121ce581499d7b701668','http://localhost/koalafy/koalafy.testy/698d51a19d8a121ce581499d7b701668.php','2017-03-24 06:20:24',''),(68,5,'7f6ffaa6bb0b408017b62254211691b5','http://localhost/koalafy/koalafy.testy/7f6ffaa6bb0b408017b62254211691b5.php','2017-03-24 06:21:23',''),(69,5,'73278a4a86960eeb576a8fd4c9ec6997','http://localhost/koalafy/koalafy.testy/73278a4a86960eeb576a8fd4c9ec6997.php','2017-03-24 06:23:22',''),(70,5,'5fd0b37cd7dbbb00f97ba6ce92bf5add','http://localhost/koalafy/koalafy.testy/5fd0b37cd7dbbb00f97ba6ce92bf5add.php','2017-03-24 06:25:42',''),(71,5,'2b44928ae11fb9384c4cf38708677c48','http://localhost/koalafy/koalafy.testy/2b44928ae11fb9384c4cf38708677c48.php','2017-03-24 06:28:55',''),(72,5,'c45147dee729311ef5b5c3003946c48f','http://localhost/koalafy/koalafy.testy/c45147dee729311ef5b5c3003946c48f.php','2017-03-24 06:29:42',''),(73,5,'eb160de1de89d9058fcb0b968dbbbd68','http://localhost/koalafy/koalafy.testy/eb160de1de89d9058fcb0b968dbbbd68.php','2017-03-24 06:34:27',''),(74,5,'7f39f8317fbdb1988ef4c628eba02591','http://localhost/koalafy/koalafy.testy/7f39f8317fbdb1988ef4c628eba02591.php','2017-03-25 13:02:36',''),(75,5,'44f683a84163b3523afe57c2e008bc8c','http://localhost/koalafy/koalafy.testy/44f683a84163b3523afe57c2e008bc8c.php','2017-03-25 13:15:28',''),(76,5,'03afdbd66e7929b125f8597834fa83a4','http://localhost/koalafy/koalafy.testy/03afdbd66e7929b125f8597834fa83a4.php','2017-03-25 13:17:52',''),(77,5,'ea5d2f1c4608232e07d3aa3d998e5135','http://localhost/koalafy/koalafy.testy/ea5d2f1c4608232e07d3aa3d998e5135.php','2017-03-25 13:21:38',''),(78,5,'fc490ca45c00b1249bbe3554a4fdf6fb','http://localhost/koalafy/koalafy.testy/fc490ca45c00b1249bbe3554a4fdf6fb.php','2017-03-25 13:21:38',''),(79,5,'3295c76acbf4caaed33c36b1b5fc2cb1','http://localhost/koalafy/koalafy.testy/3295c76acbf4caaed33c36b1b5fc2cb1.php','2017-03-25 14:36:54','');
/*!40000 ALTER TABLE `tbl_assessment_delivery` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:47:56

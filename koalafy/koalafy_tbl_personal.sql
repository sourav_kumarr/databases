-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: koalafy
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_personal`
--

DROP TABLE IF EXISTS `tbl_personal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_personal` (
  `personal_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `personal_account_id` bigint(20) NOT NULL,
  `personal_first_name` varchar(255) NOT NULL DEFAULT '',
  `personal_last_name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`personal_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_personal`
--

LOCK TABLES `tbl_personal` WRITE;
/*!40000 ALTER TABLE `tbl_personal` DISABLE KEYS */;
INSERT INTO `tbl_personal` VALUES (1,1,'Baljeet','Verma'),(2,2,'Baljeet','Verma'),(3,3,'Baljeet','Verma'),(4,4,'Baljeet','Verma'),(5,5,'Baljeet','Verma'),(6,6,'Baljeet','Verma'),(7,7,'wazir','singh'),(8,8,'Baljeet','Verma'),(9,9,'Baljeet','Verma'),(10,10,'Baljeet','Verma'),(11,11,'Baljeet','Verma'),(12,12,'wazir','singh'),(13,13,'Koalafy','Test'),(14,14,'Wazir','Singh'),(15,15,'Baljeet','Verma'),(16,16,'Baljeet','Verma'),(17,17,'Baljeet','Verma'),(18,18,'Koalafy Assess',''),(19,19,'Koalafy','Test'),(20,20,'test','singh'),(21,21,'wazir','singh'),(22,22,'vikas','kumar'),(23,23,'wazir','singh'),(24,24,'test','test'),(25,25,'Lvi','dvi'),(26,26,'wazir','singh'),(27,27,'wazir','singh');
/*!40000 ALTER TABLE `tbl_personal` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:47:57

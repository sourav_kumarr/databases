-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: koalafy
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_assessment_results`
--

DROP TABLE IF EXISTS `tbl_assessment_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_assessment_results` (
  `result_id` int(11) NOT NULL AUTO_INCREMENT,
  `result_isRedirect` varchar(10) NOT NULL,
  `result_urls` text NOT NULL,
  `result_msg_title` varchar(200) NOT NULL,
  `result_msg_desc` text NOT NULL,
  `result_assessment_id` varchar(200) NOT NULL,
  `result_stage_id` text NOT NULL,
  `result_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`result_id`)
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_assessment_results`
--

LOCK TABLES `tbl_assessment_results` WRITE;
/*!40000 ALTER TABLE `tbl_assessment_results` DISABLE KEYS */;
INSERT INTO `tbl_assessment_results` VALUES (83,'D','','jjjj','','39','ebb71045453f38676c40deb9864f811d','2017-03-16 13:14:10'),(84,'','','','','80','','2017-03-15 09:55:45'),(85,'','','','','81','','2017-03-15 09:56:39'),(86,'D','','Here Are Your Results','','82','','2017-03-20 13:51:01'),(87,'','','','','83','','2017-03-16 07:23:14'),(88,'A','','Here Are Your Results','','84','','2017-03-20 08:03:24'),(89,'','','','','85','','2017-03-20 08:03:57'),(90,'A','','Here Are Your Results','','60','','2017-03-17 15:49:39'),(91,'','','','','86','','2017-03-20 08:04:22'),(92,'','','','','87','','2017-03-20 08:05:50'),(93,'A','','Here Are Your Results','','88','','2017-03-20 08:06:39'),(94,'','','','','89','','2017-03-20 13:56:53'),(95,'','','','','90','','2017-03-20 13:57:27'),(96,'A','','Here Are Your Results','','91','','2017-03-23 12:05:53'),(97,'A','','Here Are Your Results','','92','','2017-03-23 07:11:27'),(98,'A','','Here Are Your Results','','47','','2017-03-22 11:56:55'),(99,'A','','Here Are Your Results','','93','','2017-03-23 07:46:36'),(100,'A','','Here Are Your Results','','94','','2017-03-23 07:49:02'),(101,'A','','Here Are Your Results','','95','','2017-03-23 07:56:31'),(102,'A','','Here Are Your Results','','96','','2017-03-23 10:41:02'),(103,'A','','Here Are Your Results','','97','','2017-03-23 10:45:57'),(104,'A','','Here Are Your Results','','98','','2017-03-23 10:48:00'),(105,'A','','Here Are Your Results','','99','','2017-03-23 11:04:05'),(106,'A','','Here Are Your Results','','100','','2017-03-23 11:12:24'),(107,'A','','Here Are Your Results','','101','','2017-03-23 11:23:17'),(108,'A','','Here Are Your Results','','102','','2017-03-23 11:24:37'),(109,'A','','Here Are Your Results','','103','','2017-03-23 11:40:21'),(110,'A','','Here Are Your Results','','104','','2017-03-23 11:42:55'),(111,'','','','','105','','2017-03-23 13:39:48'),(112,'','','','','106','','2017-03-23 13:39:50'),(113,'','','','','107','','2017-03-23 13:39:51'),(114,'A','','Here Are Your Results','','108','','2017-03-23 13:39:59'),(115,'','','','','109','','2017-03-24 06:16:09'),(116,'','','','','110','','2017-03-24 06:19:20'),(117,'','','','','111','','2017-03-24 06:20:25'),(118,'','','','','112','','2017-03-24 06:21:23'),(119,'','','','','113','','2017-03-24 06:23:22'),(120,'','','','','114','','2017-03-24 06:25:42'),(121,'','','','','115','','2017-03-24 06:28:55'),(122,'A','','Here Are Your Results','','116','','2017-03-24 06:30:02'),(123,'','','','','117','','2017-03-24 06:34:27'),(124,'D','','','','61','','2017-03-25 13:04:41'),(125,'','','','','62','','2017-03-25 13:15:28'),(126,'A','','Here Are Your Results','','63','','2017-03-25 15:10:20'),(127,'A','','Here Are Your Results','','64','','2017-03-25 13:21:38'),(128,'A','','Here Are Your Results','','65','','2017-03-25 13:21:39'),(129,'A','','Here Are Your Results','','66','','2017-03-25 14:36:54');
/*!40000 ALTER TABLE `tbl_assessment_results` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:06

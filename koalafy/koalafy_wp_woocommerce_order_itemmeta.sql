-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: koalafy
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wp_woocommerce_order_itemmeta`
--

DROP TABLE IF EXISTS `wp_woocommerce_order_itemmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_woocommerce_order_itemmeta` (
  `meta_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_item_id` bigint(20) NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `order_item_id` (`order_item_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM AUTO_INCREMENT=109 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_woocommerce_order_itemmeta`
--

LOCK TABLES `wp_woocommerce_order_itemmeta` WRITE;
/*!40000 ALTER TABLE `wp_woocommerce_order_itemmeta` DISABLE KEYS */;
INSERT INTO `wp_woocommerce_order_itemmeta` VALUES (1,1,'_qty','1'),(2,1,'_tax_class',''),(3,1,'_product_id','272'),(4,1,'_variation_id','0'),(5,1,'_line_subtotal','468'),(6,1,'_line_total','468'),(7,1,'_line_subtotal_tax','0'),(8,1,'_line_tax','0'),(9,1,'_line_tax_data','a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),(10,2,'_qty','1'),(11,2,'_tax_class',''),(12,2,'_product_id','272'),(13,2,'_variation_id','0'),(14,2,'_line_subtotal','468'),(15,2,'_line_total','468'),(16,2,'_line_subtotal_tax','0'),(17,2,'_line_tax','0'),(18,2,'_line_tax_data','a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),(19,3,'_qty','1'),(20,3,'_tax_class',''),(21,3,'_product_id','272'),(22,3,'_variation_id','0'),(23,3,'_line_subtotal','468'),(24,3,'_line_total','468'),(25,3,'_line_subtotal_tax','0'),(26,3,'_line_tax','0'),(27,3,'_line_tax_data','a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),(28,4,'_qty','1'),(29,4,'_tax_class',''),(30,4,'_product_id','272'),(31,4,'_variation_id','0'),(32,4,'_line_subtotal','468'),(33,4,'_line_total','468'),(34,4,'_line_subtotal_tax','0'),(35,4,'_line_tax','0'),(36,4,'_line_tax_data','a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),(37,5,'_qty','1'),(38,5,'_tax_class',''),(39,5,'_product_id','271'),(40,5,'_variation_id','0'),(41,5,'_line_subtotal','45'),(42,5,'_line_total','45'),(43,5,'_line_subtotal_tax','0'),(44,5,'_line_tax','0'),(45,5,'_line_tax_data','a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),(62,7,'_line_tax','0'),(61,7,'_line_subtotal_tax','0'),(60,7,'_line_total','468'),(59,7,'_line_subtotal','468'),(58,7,'_variation_id','0'),(57,7,'_product_id','272'),(55,7,'_qty','1'),(56,7,'_tax_class',''),(63,7,'_line_tax_data','a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),(64,8,'_qty','1'),(65,8,'_tax_class',''),(66,8,'_product_id','272'),(67,8,'_variation_id','0'),(68,8,'_line_subtotal','468'),(69,8,'_line_total','468'),(70,8,'_line_subtotal_tax','0'),(71,8,'_line_tax','0'),(72,8,'_line_tax_data','a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),(73,9,'_qty','1'),(74,9,'_tax_class',''),(75,9,'_product_id','272'),(76,9,'_variation_id','0'),(77,9,'_line_subtotal','468'),(78,9,'_line_total','468'),(79,9,'_line_subtotal_tax','0'),(80,9,'_line_tax','0'),(81,9,'_line_tax_data','a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),(82,10,'_qty','1'),(83,10,'_tax_class',''),(84,10,'_product_id','272'),(85,10,'_variation_id','0'),(86,10,'_line_subtotal','468'),(87,10,'_line_total','468'),(88,10,'_line_subtotal_tax','0'),(89,10,'_line_tax','0'),(90,10,'_line_tax_data','a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),(91,11,'_qty','1'),(92,11,'_tax_class',''),(93,11,'_product_id','273'),(94,11,'_variation_id','0'),(95,11,'_line_subtotal','59'),(96,11,'_line_total','59'),(97,11,'_line_subtotal_tax','0'),(98,11,'_line_tax','0'),(99,11,'_line_tax_data','a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),(100,12,'_qty','1'),(101,12,'_tax_class',''),(102,12,'_product_id','272'),(103,12,'_variation_id','0'),(104,12,'_line_subtotal','468'),(105,12,'_line_total','468'),(106,12,'_line_subtotal_tax','0'),(107,12,'_line_tax','0'),(108,12,'_line_tax_data','a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}');
/*!40000 ALTER TABLE `wp_woocommerce_order_itemmeta` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:47:55

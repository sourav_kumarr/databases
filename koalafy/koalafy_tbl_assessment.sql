-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: koalafy
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_assessment`
--

DROP TABLE IF EXISTS `tbl_assessment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_assessment` (
  `assessment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `assessment_account_id` bigint(20) NOT NULL,
  `assessment_name` varchar(255) NOT NULL DEFAULT 'New Assessment',
  `assessment_assessment_type_id` bigint(20) NOT NULL,
  `assessment_result_type_id` bigint(20) NOT NULL,
  `assessment_x_category` varchar(255) NOT NULL DEFAULT '',
  `assessment_y_category` varchar(255) NOT NULL DEFAULT '',
  `assessment_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `assessment_modified_at` varchar(200) NOT NULL,
  PRIMARY KEY (`assessment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_assessment`
--

LOCK TABLES `tbl_assessment` WRITE;
/*!40000 ALTER TABLE `tbl_assessment` DISABLE KEYS */;
INSERT INTO `tbl_assessment` VALUES (2,19,'Testooooo',5,1,'Authority','Timeline','2016-09-19 15:05:47',''),(3,19,'A',5,1,'Motivation','Specific Knowledge','2016-09-19 20:37:55',''),(6,19,'Title Here',5,2,'','','2016-09-25 02:00:18',''),(7,19,'Title Here',5,2,'Dog Lover','Animal Lover','2016-09-25 02:03:02',''),(8,19,'',5,2,'','','2016-09-25 03:21:29',''),(9,19,'',5,2,'','','2016-09-25 03:28:46',''),(10,19,'Full Info',5,2,'Authority','Timeline','2016-09-26 18:49:41',''),(11,19,'Full Info',5,2,'','','2016-09-26 18:53:47',''),(12,19,'Full Info',5,2,'Authority','Need','2016-09-27 17:15:56',''),(13,19,'Full Info2',5,2,'Timeline','Authority','2016-09-28 21:43:24',''),(14,19,'Full Info',5,2,'Authority','Timeline','2016-09-29 07:12:53',''),(15,13,'av',1,2,'Experience','Motivation','2016-09-29 09:39:52',''),(16,19,'Full Info',5,2,'Authority','Timeline','2016-09-29 20:19:01',''),(17,19,'Full Info',5,2,'','','2016-09-30 06:15:21',''),(18,19,'Full Info',5,2,'Timeline','Authority','2016-09-30 17:52:08',''),(19,13,'A Copy',1,1,'Motivation','Experience','2016-10-03 13:16:33',''),(21,19,'Full Info',5,2,'Timeline','Authority','2016-10-05 05:27:43',''),(22,19,'Full Info',5,2,'Authority','Need','2016-10-06 19:25:15',''),(23,19,'New Assessment',5,2,'','','2016-10-06 19:29:00',''),(24,19,'New Assessment',5,2,'Timeline','Authority','2016-10-07 03:59:13',''),(25,19,'Ultimate',5,2,'Timeline','Authority','2016-10-07 04:00:25',''),(26,19,'Ultimate',5,2,'Authority','Authority','2016-10-07 18:24:33',''),(27,22,'test',1,2,'Motivation','Motivation','2016-10-24 13:47:50',''),(28,22,'what is your age ?',4,2,'Motivation','Motivation','2016-10-24 13:48:03',''),(29,22,'New Assessment',1,2,'Motivation','Motivation','2016-10-27 10:09:00',''),(31,4,'test',5,2,'Authority','Timeline','2016-12-19 16:33:18',''),(32,4,'Name',1,2,'as','asd','2016-12-20 01:17:06',''),(33,4,'Testing',5,2,'Timeline','Authority','2016-12-21 22:39:10',''),(34,4,'new',1,2,'Timeline','Authority','2016-12-23 02:51:59',''),(36,13,'first',1,2,'Timeline','Authority','2016-12-24 11:18:43',''),(37,3,'ddd',1,2,'Timeline','Authority','2016-12-24 13:30:31',''),(38,4,'ssdsd',1,2,'Timeline','Authority','2016-12-24 13:56:28',''),(39,5,'New Assessments',1,2,'Timeline','Authority','2016-12-24 14:11:35',''),(42,4,'sdsd',1,2,'Timeline','Authority','2016-12-26 07:39:07',''),(43,4,'ddd',1,2,'Timeline','Authority','2016-12-26 07:58:27',''),(47,13,'New Assessment',1,2,'Authority','Timeline','2016-12-27 00:25:34',''),(52,4,'new ass',1,2,'Timeline','Authority','2016-12-29 07:31:14',''),(60,5,'New User Assessmentsyy',1,2,'Timeline','Authority','2017-01-09 11:48:39',''),(63,5,'dd',1,2,'Timeline','Authority','2017-03-25 13:17:51','03/25/2017'),(64,5,'ddddddd',1,2,'','','2017-03-25 13:21:38',''),(65,5,'dddddddsssss',1,2,'Timeline','Authority','2017-03-25 13:21:38','25/03/17'),(66,5,'new test found',1,2,'Timeline','Authority','2017-03-25 14:36:53','03/28/2017');
/*!40000 ALTER TABLE `tbl_assessment` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:47:58

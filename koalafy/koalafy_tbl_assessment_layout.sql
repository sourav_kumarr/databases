-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: koalafy
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_assessment_layout`
--

DROP TABLE IF EXISTS `tbl_assessment_layout`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_assessment_layout` (
  `layout_id` bigint(100) NOT NULL AUTO_INCREMENT,
  `layout_selected` varchar(200) NOT NULL,
  `layout_content_width` varchar(200) NOT NULL,
  `layout_page_width` varchar(200) NOT NULL,
  `layout_assessment_id` varchar(200) NOT NULL,
  PRIMARY KEY (`layout_id`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_assessment_layout`
--

LOCK TABLES `tbl_assessment_layout` WRITE;
/*!40000 ALTER TABLE `tbl_assessment_layout` DISABLE KEYS */;
INSERT INTO `tbl_assessment_layout` VALUES (16,'desktop','80','80','68'),(17,'desktop','70','70','39'),(18,'desktop','80','80','75'),(19,'desktop','80','80','71'),(20,'desktop','80','80','70'),(21,'smartphone','70','80','69'),(22,'desktop','50','80','73'),(23,'desktop','50','80','74'),(24,'desktop','80','80','72'),(25,'desktop','80','80','77'),(26,'desktop','50','70','60'),(27,'','','','80'),(28,'','','','81'),(29,'desktop','80','80','82'),(30,'','','','83'),(31,'desktop','80','80','84'),(32,'','','','85'),(33,'','','','83'),(34,'desktop','80','80','84'),(35,'','','','85'),(36,'','','','86'),(37,'','','','87'),(38,'desktop','80','80','88'),(39,'','','','89'),(40,'','','','90'),(41,'desktop','80','80','91'),(42,'desktop','70','70','92'),(43,'desktop','80','80','47'),(44,'desktop','80','80','93'),(45,'desktop','80','80','94'),(46,'desktop','80','80','95'),(47,'desktop','70','78','96'),(48,'desktop','70','78','97'),(49,'desktop','70','78','98'),(50,'desktop','70','78','99'),(51,'desktop','70','78','100'),(52,'desktop','80','80','101'),(53,'desktop','80','80','102'),(54,'desktop','70','78','103'),(55,'desktop','70','78','104'),(56,'','','','105'),(57,'','','','106'),(58,'','','','107'),(59,'desktop','50','70','108'),(60,'','','','109'),(61,'','','','110'),(62,'','','','111'),(63,'','','','112'),(64,'','','','113'),(65,'','','','114'),(66,'','','','115'),(67,'desktop','80','80','116'),(68,'','','','117'),(69,'','80','80','61'),(70,'','','','62'),(71,'desktop','80','80','63'),(72,'desktop','80','80','64'),(73,'desktop','80','80','65'),(74,'desktop','80','80','66');
/*!40000 ALTER TABLE `tbl_assessment_layout` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:13

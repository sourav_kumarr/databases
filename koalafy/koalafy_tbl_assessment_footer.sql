-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: koalafy
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_assessment_footer`
--

DROP TABLE IF EXISTS `tbl_assessment_footer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_assessment_footer` (
  `footer_id` bigint(200) NOT NULL AUTO_INCREMENT,
  `footer_is_font` varchar(200) NOT NULL,
  `footer_font` varchar(200) NOT NULL,
  `footer_text_size` varchar(200) NOT NULL,
  `footer_text_color` varchar(200) NOT NULL,
  `footer_text_align` varchar(200) NOT NULL,
  `footer_back_color` varchar(200) NOT NULL,
  `footer_height` varchar(200) NOT NULL,
  `footer_text` text NOT NULL,
  `footer_assessment_id` varchar(200) NOT NULL,
  PRIMARY KEY (`footer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_assessment_footer`
--

LOCK TABLES `tbl_assessment_footer` WRITE;
/*!40000 ALTER TABLE `tbl_assessment_footer` DISABLE KEYS */;
INSERT INTO `tbl_assessment_footer` VALUES (10,'A','Gentium','24','#000000','left','#ff7f50','130.00','','75'),(11,'A','Arial Rounded MT Bold','28','#ffffff','center','#ff0000','150','dsdf','39'),(12,'','','','','','','130.00','','78'),(13,'','','','','','','130.00','','79'),(14,'A','monospace','10','#000000','center','#9464b8','102.30','','60'),(15,'A','monospace','10','#ffffff','center','#000000','150','','68'),(16,'A','monospace','10','#000000','center','','150','','71'),(17,'A','Century','26','#ffffff','center','#ff7f50','100.00','','70'),(18,'A','default','24','#ffffff','right','#ff7f50','100.00','','69'),(19,'A','monospace','10','#000000','center','','150','','73'),(20,'A','monospace','10','#000000','center','','150','','74'),(21,'A','Gentium','48','#000000','right','#db8a8a','150','','72'),(22,'A','monospace','10','#000000','center','','150','','77'),(23,'','','','','','','130.00','','80'),(24,'','','','','','','130.00','','81'),(25,'A','monospace','10','#ffffff','center','#a15858','150','','82'),(26,'','','','','','','130.00','','83'),(27,'A','monospace','10','#000000','center','','150','','84'),(28,'','','','','','','130.00','','85'),(29,'','','','','','','130.00','','83'),(30,'A','monospace','10','#000000','center','','150','','84'),(31,'','','','','','','130.00','','85'),(32,'','','','','','','130.00','','86'),(33,'','','','','','','130.00','','87'),(34,'A','monospace','10','#000000','center','','150','','88'),(35,'','','','','','','130.00','','89'),(36,'','','','','','','130.00','','90'),(37,'A','monospace','10','#ffffff','center','#ff0000','150','','91'),(38,'A','monospace','10','#000000','center','','150','','92'),(39,'A','monospace','10','#000000','center','','150','','47'),(40,'A','monospace','10','#000000','center','','150','','93'),(41,'A','monospace','10','#000000','center','','150','','94'),(42,'A','monospace','10','#000000','center','#9464b8','102.30','','95'),(43,'A','monospace','10','#000000','center','#9464b8','102.30','','96'),(44,'A','monospace','10','#000000','center','#9464b8','102.30','','97'),(45,'A','monospace','10','#000000','center','#9464b8','102.30','','98'),(46,'A','monospace','10','#000000','center','#9464b8','102.30','','99'),(47,'A','monospace','10','#000000','center','#9464b8','102.30','','100'),(48,'A','monospace','10','#000000','center','','150','','101'),(49,'A','monospace','10','#000000','center','','150','','102'),(50,'A','monospace','10','#000000','center','#9464b8','102.30','','103'),(51,'A','monospace','10','#000000','center','#9464b8','102.30','','104'),(52,'','','','','','','130.00','','105'),(53,'','','','','','','130.00','','106'),(54,'','','','','','','130.00','','107'),(55,'A','monospace','10','#000000','center','#9464b8','102.30','','108'),(56,'','','','','','','130.00','','109'),(57,'','','','','','','130.00','','110'),(58,'','','','','','','130.00','','111'),(59,'','','','','','','130.00','','112'),(60,'','','','','','','130.00','','113'),(61,'','','','','','','130.00','','114'),(62,'','','','','','','130.00','','115'),(63,'A','monospace','10','#000000','center','','150','','116'),(64,'','','','','','','130.00','','117'),(65,'','','','','','','130.00','','61'),(66,'','','','','','','130.00','','62'),(67,'A','monospace','10','#000000','center','','150','','63'),(68,'A','monospace','10','#000000','center','','130.00','Page Footer','64'),(69,'A','monospace','10','#000000','center','','130.00','Page Footer','65'),(70,'A','monospace','10','#000000','center','','130.00','Page Footer','66');
/*!40000 ALTER TABLE `tbl_assessment_footer` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:17

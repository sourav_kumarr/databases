-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: koalafy
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wp_pmpro_membership_orders`
--

DROP TABLE IF EXISTS `wp_pmpro_membership_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_pmpro_membership_orders` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(32) COLLATE utf32_unicode_ci NOT NULL,
  `session_id` varchar(64) COLLATE utf32_unicode_ci NOT NULL DEFAULT '',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0',
  `membership_id` int(11) unsigned NOT NULL DEFAULT '0',
  `paypal_token` varchar(64) COLLATE utf32_unicode_ci NOT NULL DEFAULT '',
  `billing_name` varchar(128) COLLATE utf32_unicode_ci NOT NULL DEFAULT '',
  `billing_street` varchar(128) COLLATE utf32_unicode_ci NOT NULL DEFAULT '',
  `billing_city` varchar(128) COLLATE utf32_unicode_ci NOT NULL DEFAULT '',
  `billing_state` varchar(32) COLLATE utf32_unicode_ci NOT NULL DEFAULT '',
  `billing_zip` varchar(16) COLLATE utf32_unicode_ci NOT NULL DEFAULT '',
  `billing_country` varchar(128) COLLATE utf32_unicode_ci NOT NULL,
  `billing_phone` varchar(32) COLLATE utf32_unicode_ci NOT NULL,
  `subtotal` varchar(16) COLLATE utf32_unicode_ci NOT NULL DEFAULT '',
  `tax` varchar(16) COLLATE utf32_unicode_ci NOT NULL DEFAULT '',
  `couponamount` varchar(16) COLLATE utf32_unicode_ci NOT NULL DEFAULT '',
  `checkout_id` int(11) NOT NULL DEFAULT '0',
  `certificate_id` int(11) NOT NULL DEFAULT '0',
  `certificateamount` varchar(16) COLLATE utf32_unicode_ci NOT NULL DEFAULT '',
  `total` varchar(16) COLLATE utf32_unicode_ci NOT NULL DEFAULT '',
  `payment_type` varchar(64) COLLATE utf32_unicode_ci NOT NULL DEFAULT '',
  `cardtype` varchar(32) COLLATE utf32_unicode_ci NOT NULL DEFAULT '',
  `accountnumber` varchar(32) COLLATE utf32_unicode_ci NOT NULL DEFAULT '',
  `expirationmonth` char(2) COLLATE utf32_unicode_ci NOT NULL DEFAULT '',
  `expirationyear` varchar(4) COLLATE utf32_unicode_ci NOT NULL DEFAULT '',
  `status` varchar(32) COLLATE utf32_unicode_ci NOT NULL DEFAULT '',
  `gateway` varchar(64) COLLATE utf32_unicode_ci NOT NULL,
  `gateway_environment` varchar(64) COLLATE utf32_unicode_ci NOT NULL,
  `payment_transaction_id` varchar(64) COLLATE utf32_unicode_ci NOT NULL,
  `subscription_transaction_id` varchar(32) COLLATE utf32_unicode_ci NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `affiliate_id` varchar(32) COLLATE utf32_unicode_ci NOT NULL,
  `affiliate_subid` varchar(32) COLLATE utf32_unicode_ci NOT NULL,
  `notes` text COLLATE utf32_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `session_id` (`session_id`),
  KEY `user_id` (`user_id`),
  KEY `membership_id` (`membership_id`),
  KEY `status` (`status`),
  KEY `timestamp` (`timestamp`),
  KEY `gateway` (`gateway`),
  KEY `gateway_environment` (`gateway_environment`),
  KEY `payment_transaction_id` (`payment_transaction_id`),
  KEY `subscription_transaction_id` (`subscription_transaction_id`),
  KEY `affiliate_id` (`affiliate_id`),
  KEY `affiliate_subid` (`affiliate_subid`),
  KEY `checkout_id` (`checkout_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_pmpro_membership_orders`
--

LOCK TABLES `wp_pmpro_membership_orders` WRITE;
/*!40000 ALTER TABLE `wp_pmpro_membership_orders` DISABLE KEYS */;
INSERT INTO `wp_pmpro_membership_orders` VALUES (1,'5F541780AA','fd421355604b92d079a761b696161c96',1,1,'','','','','','','','','0','0','',1,0,'','0','','','','','','','free','sandbox','','','2016-12-01 14:21:11','','','');
/*!40000 ALTER TABLE `wp_pmpro_membership_orders` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:03

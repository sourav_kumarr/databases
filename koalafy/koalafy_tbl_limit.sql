-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: koalafy
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_limit`
--

DROP TABLE IF EXISTS `tbl_limit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_limit` (
  `limit_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `limit_max_assessments` tinyint(4) DEFAULT NULL,
  `limit_max_questions_per_assessment` tinyint(4) DEFAULT NULL,
  `limit_max_answers_per_question` tinyint(4) DEFAULT NULL,
  `limit_lifecycle_stages` tinyint(4) DEFAULT NULL,
  `limit_weighting` tinyint(1) NOT NULL DEFAULT '0',
  `limit_logo_branding` tinyint(1) NOT NULL DEFAULT '0',
  `limit_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`limit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_limit`
--

LOCK TABLES `tbl_limit` WRITE;
/*!40000 ALTER TABLE `tbl_limit` DISABLE KEYS */;
INSERT INTO `tbl_limit` VALUES (1,3,5,3,2,0,0,'2016-09-28 20:34:54'),(2,10,10,5,3,0,0,'2016-09-28 20:34:54'),(3,25,NULL,NULL,5,1,1,'2016-09-28 20:34:54'),(4,50,NULL,NULL,6,1,1,'2016-09-28 20:34:54');
/*!40000 ALTER TABLE `tbl_limit` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:47:58

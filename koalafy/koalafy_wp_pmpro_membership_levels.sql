-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: koalafy
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wp_pmpro_membership_levels`
--

DROP TABLE IF EXISTS `wp_pmpro_membership_levels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_pmpro_membership_levels` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf32_unicode_ci NOT NULL,
  `description` longtext COLLATE utf32_unicode_ci NOT NULL,
  `confirmation` longtext COLLATE utf32_unicode_ci NOT NULL,
  `initial_payment` decimal(10,2) NOT NULL DEFAULT '0.00',
  `billing_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `cycle_number` int(11) NOT NULL DEFAULT '0',
  `cycle_period` enum('','','','') COLLATE utf32_unicode_ci DEFAULT '',
  `billing_limit` int(11) NOT NULL COMMENT 'After how many cycles should billing stop?',
  `trial_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `trial_limit` int(11) NOT NULL DEFAULT '0',
  `allow_signups` tinyint(4) NOT NULL DEFAULT '1',
  `expiration_number` int(10) unsigned NOT NULL,
  `expiration_period` enum('','','','') COLLATE utf32_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `allow_signups` (`allow_signups`),
  KEY `initial_payment` (`initial_payment`),
  KEY `name` (`name`(250))
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_pmpro_membership_levels`
--

LOCK TABLES `wp_pmpro_membership_levels` WRITE;
/*!40000 ALTER TABLE `wp_pmpro_membership_levels` DISABLE KEYS */;
INSERT INTO `wp_pmpro_membership_levels` VALUES (1,'Free Koalafy','Up to 3 Assessments\r\nUp to 5 Questions Per Assessment\r\nUp to 3 Answers Per Question\r\n2 Lifecycle Stages\r\nAdjustable Points Per Question (Weighting)\r\nUtilize Your Own Logo/Branding','Register Free Trail',0.00,10.00,1,'',0,0.00,0,1,10,''),(2,'Koalafy Joey','Up to 10 Assessments\r\nUp to 10 Questions Per Assessment\r\nUp to 5 Answers Per Question\r\n3 Lifecycle Stages\r\nAdjustable Points Per Question (Weighting)\r\nUtilize Your Own Logo/Branding','Buy Now',45.00,0.00,0,'',0,0.00,0,1,1,''),(3,'Koalafy Eucalyptus','Up to 25 Assessments\r\nUnlimited Questions Per Assessment\r\nUnlimited Answers Per Question\r\n5 Lifecycle Stages\r\nAdjustable Points Per Question (Weighting)\r\nUtilize Your Own Logo/Branding','Buy Now',39.00,468.00,1,'',468,0.00,0,1,1,''),(4,'Koalafy King','Up to 50 Assessments\r\nUnlimited Questions Per Assessment\r\nUnliimited Answers Per Question\r\n6 Lifecycle Stages\r\nAdjustable Points Per Question (Weighting)\r\nUtilize Your Own Logo/Branding','Buy Now',59.00,59.00,1,'',0,0.00,0,1,1,'');
/*!40000 ALTER TABLE `wp_pmpro_membership_levels` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:47:58

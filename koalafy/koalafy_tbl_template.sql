-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: koalafy
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_template`
--

DROP TABLE IF EXISTS `tbl_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_template` (
  `temp_id` bigint(100) NOT NULL AUTO_INCREMENT,
  `temp_name` varchar(255) NOT NULL,
  `temp_logo_align` varchar(255) NOT NULL,
  `temp_titleback_color` varchar(255) NOT NULL,
  `temp_titletext_color` varchar(255) NOT NULL,
  `temp_back_align` varchar(255) NOT NULL,
  `temp_footer_color` varchar(255) NOT NULL,
  `temp_footerback_color` varchar(255) NOT NULL,
  `temp_pageback_color` varchar(255) NOT NULL,
  `temp_pagetext_color` text NOT NULL,
  `temp_desc_align` varchar(255) NOT NULL,
  `temp_back_color` varchar(255) NOT NULL,
  `temp_status` text NOT NULL,
  `temp_path` varchar(200) NOT NULL,
  PRIMARY KEY (`temp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_template`
--

LOCK TABLES `tbl_template` WRITE;
/*!40000 ALTER TABLE `tbl_template` DISABLE KEYS */;
INSERT INTO `tbl_template` VALUES (1,'Soothing','left','#6495ed','#000000','10px','#ffffff','#ff7f50','#d3d3d3','#000000','center','#a9a9a9','A','soothing.jpg'),(2,'Red Hot','left','#ff0000','#000000','10px','#ffffff','#ff0000','#d3d3d3','#000000','center','#a9a9a9','A','Red_hot.jpg'),(3,'Green Machine','left','#70ad47','#000000','10px','#ffffff','#70ad47','#ffffff','#ffffff','center','#000000','A','green_machine.jpg'),(4,'Monochrome','left','#767171','#000000','10px','#ffffff','#767171','#ffffff','#ffffff','center','#000000','A','monochrome.jpg'),(5,'Bumble Bee','right','#000000','#ffffff','10px','#ffffff','#000000','#ffc000','#000000','center','#ffffff','A','black_white.jpg'),(6,'Royal','right','#ffc000','#ffffff','10px','#000000','#9464b8','#270656','#000000','center','#ffffff','A','royal.jpg'),(7,'Fall','right','#ffc000','#000000','10px','#ffffff','#ffc000','#ed7d31','#000000','center','#ffffff','A','fall.jpg'),(8,'Sun & Rain','right','#ffc000','#000000','10px','#ffffff','#a4a4a4','#a4a4a4','#000000','center','#a4a4a4','A','sun_and_rain.jpg');
/*!40000 ALTER TABLE `tbl_template` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:00

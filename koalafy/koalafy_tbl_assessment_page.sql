-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: koalafy
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_assessment_page`
--

DROP TABLE IF EXISTS `tbl_assessment_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_assessment_page` (
  `page_id` int(80) NOT NULL AUTO_INCREMENT,
  `page_is_font` varchar(200) NOT NULL,
  `page_is_progress` varchar(200) NOT NULL,
  `page_is_top` varchar(200) NOT NULL,
  `page_assessment_id` bigint(100) NOT NULL,
  `page_back_color` varchar(200) NOT NULL,
  `page_text_color` varchar(200) NOT NULL,
  PRIMARY KEY (`page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_assessment_page`
--

LOCK TABLES `tbl_assessment_page` WRITE;
/*!40000 ALTER TABLE `tbl_assessment_page` DISABLE KEYS */;
INSERT INTO `tbl_assessment_page` VALUES (10,'A','A','D',75,'#b85454','#663333'),(11,'D','A','D',39,'#a9a9a9','#000000'),(12,'','','',78,'',''),(13,'','','',79,'',''),(14,'A','A','D',60,'#ffffff','#000000'),(15,'A','A','D',68,'#ffc000','#ffffff'),(16,'A','A','D',71,'','#000000'),(17,'A','A','D',70,'#d3d3d3','#000000'),(18,'D','A','D',69,'#d3d3d3','#000000'),(19,'A','A','D',73,'','#000000'),(20,'A','A','D',74,'','#000000'),(21,'A','A','D',72,'','#000000'),(22,'A','A','D',77,'','#000000'),(23,'','','',80,'',''),(24,'','','',81,'',''),(25,'D','A','D',82,'#805d5d','#8f1010'),(26,'','','',83,'',''),(27,'A','A','D',84,'','#000000'),(28,'','','',85,'',''),(29,'','','',83,'',''),(30,'A','A','D',84,'','#000000'),(31,'','','',85,'',''),(32,'','','',86,'',''),(33,'','','',87,'',''),(34,'A','A','D',88,'','#000000'),(35,'','','',89,'',''),(36,'','','',90,'',''),(37,'A','A','D',91,'#a9a9a9','#000000'),(38,'A','A','D',92,'','#000000'),(39,'A','A','D',47,'','#000000'),(40,'A','A','D',93,'','#000000'),(41,'A','A','D',94,'','#000000'),(42,'A','A','D',95,'#ffffff','#000000'),(43,'A','A','D',96,'#ffffff','#000000'),(44,'A','A','D',97,'#ffffff','#000000'),(45,'A','A','D',98,'#ffffff','#000000'),(46,'A','A','D',99,'#ffffff','#000000'),(47,'A','A','D',100,'#ffffff','#000000'),(48,'A','A','D',101,'','#000000'),(49,'A','A','D',102,'','#000000'),(50,'A','A','D',103,'#ffffff','#000000'),(51,'A','A','D',104,'#ffffff','#000000'),(52,'','','',105,'',''),(53,'','','',106,'',''),(54,'','','',107,'',''),(55,'A','A','D',108,'#ffffff','#000000'),(56,'','','',109,'',''),(57,'','','',110,'',''),(58,'','','',111,'',''),(59,'','','',112,'',''),(60,'','','',113,'',''),(61,'','','',114,'',''),(62,'','','',115,'',''),(63,'A','A','D',116,'','#000000'),(64,'','','',117,'',''),(65,'','','D',61,'',''),(66,'','','D',62,'',''),(67,'A','A','D',63,'','#000000'),(68,'A','A','D',64,'#000000',''),(69,'A','A','D',65,'#000000','#9e2525'),(70,'A','A','D',66,'#000000','#fffff');
/*!40000 ALTER TABLE `tbl_assessment_page` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:07

-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: koalafy
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_contact`
--

DROP TABLE IF EXISTS `tbl_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_contact` (
  `contact_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `contact_account_id` bigint(20) NOT NULL,
  `contact_work_email` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`contact_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_contact`
--

LOCK TABLES `tbl_contact` WRITE;
/*!40000 ALTER TABLE `tbl_contact` DISABLE KEYS */;
INSERT INTO `tbl_contact` VALUES (1,1,'vistasoftsolution.baljeetin581@gmail.com'),(2,2,'vistasoftsolution.baljeetin581@gmail.com'),(3,3,'vistasoftsolution.baljeetin581@gmail.com'),(4,4,'vistasoftsolution.baljeetin581@gmail.com'),(5,5,'vistasoftsolution.baljeetin581@gmail.com'),(6,6,'vistasoftsolution.baljeetin581@gmail.com'),(8,8,'vistasoftsolution.baljeetin581@gmail.com'),(9,9,'vistasoftsolution.baljeetin581@gmail.com'),(10,10,'vistasoftsolution.baljeetin581@gmail.com'),(11,11,'vistasoftsolution.baljeetin581@gmail.com'),(13,15,'vistasoftsolution.baljeetin581@gmail.com'),(14,16,'vistasoftsolution.baljeetin581@gmail.com'),(15,17,'vistasoftsolution.baljeetin581@gmail.com'),(16,19,'test@koalafy.me'),(19,22,'vikas.vistasoftsolutions@gmail.com'),(20,23,'vistasoftsolutions@gmail.com'),(21,24,'wairsingh@gmail.com'),(22,25,'lvidvi.vistasoft@gmail.com'),(23,26,'wazirsingh@gmail.con'),(24,27,'wazirsingh@gmail.com');
/*!40000 ALTER TABLE `tbl_contact` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:47:54

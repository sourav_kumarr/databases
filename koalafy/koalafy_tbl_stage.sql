-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: koalafy
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_stage`
--

DROP TABLE IF EXISTS `tbl_stage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_stage` (
  `stage_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `stage_assessment_id` bigint(20) NOT NULL,
  `stage_abbreviation` varchar(10) NOT NULL DEFAULT '',
  `stage_description` text NOT NULL,
  PRIMARY KEY (`stage_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1555 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_stage`
--

LOCK TABLES `tbl_stage` WRITE;
/*!40000 ALTER TABLE `tbl_stage` DISABLE KEYS */;
INSERT INTO `tbl_stage` VALUES (1,1,'5',''),(2,20,'',''),(3,25,'UNQ','Uniqualified'),(4,13,'ABC','Tests'),(5,27,'tes1','testtest'),(6,28,'',''),(7,27,'tes2','test'),(8,29,'',''),(9,6,'',''),(10,2,'ABC','Long Text'),(11,30,'abc','abc'),(12,31,'abc','abc'),(13,31,'bcd','bcd'),(14,30,'cde','cde'),(15,30,'hig','hig'),(16,33,'abc','abc'),(17,33,'dcd','cdad'),(18,34,'abc','abc'),(19,34,'ccd','ccd'),(20,35,'dd','ss'),(21,35,'dd','sss'),(22,36,'dd','ddd'),(23,36,'ee','fff'),(24,37,'dd','sss'),(25,37,'ee','ddd'),(26,38,'dd','sssss'),(27,39,'dd','dd'),(28,40,'dd','dd'),(29,40,'dd','ddd'),(31,41,'UNQ','Unqualified'),(32,41,'MQL','Marketing Qualified'),(33,41,'SQL','Sales Qualified'),(34,41,'IS','Inside Sales'),(35,42,'sdsd','ddd'),(36,43,'sdsd','rrrrr'),(37,43,'ffff','rrrr'),(38,44,'dd','dddd'),(39,45,'dd','ddd'),(40,46,'',''),(41,47,'ssss','ddddddddd'),(42,48,'ee','ee'),(43,48,'ere','fff'),(44,49,'abc','abc'),(45,49,'cde','cde'),(46,49,'lmn','lmn'),(47,49,'oh','oh'),(48,50,'',''),(49,51,'UNQ','Unqualified Lead'),(50,51,'MQL','Marketing Qualified Lead'),(51,51,'TM','Telemarketing Lead'),(52,51,'SQL','Sales Qualified Lead'),(53,52,'dd','rrrr'),(54,52,'ere','gggg'),(55,53,'',''),(56,54,'',''),(57,55,'abc','abc'),(58,55,'adfd','adfd'),(59,55,'ffd','dfdfd'),(60,55,'adfd','adfddf'),(61,55,'dfdf','dfdfadfddfdf'),(62,55,'bbbb','bbbb'),(63,57,'UNQ','Unqualified'),(64,57,'MQL','Marketing Qualified'),(65,57,'SQL','Sales Qualified'),(66,58,'abc','abc'),(67,59,'abc','abc'),(68,59,'xss','adfddfdf'),(69,59,'bcvd','asdfdfdfd'),(70,59,'bbbb','cdfddfdd'),(71,60,'TOP','These are the best'),(72,60,'MID','These are alright'),(78,64,'abc','abc'),(83,67,'',''),(85,68,'UNQ','Unqualified'),(86,68,'MQL','Marketing Qualified'),(87,69,'UNQ','Unqualified'),(88,69,'MQL','Marketing Qualified'),(89,69,'SQL','Sales Qualified'),(90,70,'ABC','ABC Stuff'),(91,70,'CDE','CDE stuff'),(1472,77,'',''),(1493,75,'',''),(1494,71,'',''),(1501,73,'',''),(1502,74,'',''),(1508,72,'',''),(1510,68,'SQL','Sales Qualified'),(1511,39,'abc','abc'),(1512,60,'BAD','Disregard these as they are junk'),(1513,80,'',''),(1514,81,'',''),(1515,82,'dddd','ddd'),(1521,83,'',''),(1522,84,'',''),(1523,85,'',''),(1524,86,'',''),(1525,87,'',''),(1526,88,'',''),(1527,89,'',''),(1528,90,'',''),(1529,91,'sd','ff'),(1530,92,'',''),(1531,82,'',''),(1532,82,'',''),(1533,82,'',''),(1534,82,'',''),(1535,82,'',''),(1536,93,'',''),(1537,94,'',''),(1538,95,'',''),(1539,104,'',''),(1540,109,'',''),(1541,110,'',''),(1542,111,'',''),(1543,112,'',''),(1544,113,'',''),(1545,114,'',''),(1546,115,'',''),(1547,116,'','f'),(1548,117,'',''),(1549,61,'','ff'),(1550,62,'',''),(1551,63,'','ddddddd'),(1552,64,'',''),(1553,65,'',''),(1554,66,'cccc','cccccccc');
/*!40000 ALTER TABLE `tbl_stage` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:03

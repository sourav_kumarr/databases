-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: koalafy
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_account`
--

DROP TABLE IF EXISTS `tbl_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_account` (
  `account_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_first_name` varchar(255) NOT NULL,
  `account_last_name` varchar(255) NOT NULL,
  `account_company_name` varchar(255) NOT NULL,
  `account_login_name` varchar(255) NOT NULL,
  `account_email` varchar(255) NOT NULL,
  `account_password` varchar(255) NOT NULL,
  `account_confirm_password` varchar(255) NOT NULL,
  `account_type` varchar(255) NOT NULL,
  `account_active_date` varchar(255) NOT NULL,
  `account_valid_upto` varchar(255) NOT NULL,
  `account_how_id` int(11) NOT NULL,
  `account_activation_flag` tinyint(1) NOT NULL DEFAULT '0',
  `account_auth_token` varchar(255) NOT NULL,
  `login_status` varchar(10) NOT NULL,
  `account_right_id` bigint(100) NOT NULL,
  `account_is_video` varchar(255) NOT NULL,
  `account_card_type` varchar(255) NOT NULL,
  `account_cardholder_name` varchar(255) NOT NULL,
  `account_card_expdate` varchar(255) NOT NULL,
  `account_billing_firstname` varchar(255) NOT NULL,
  `account_billing_lastname` varchar(255) NOT NULL,
  `account_billing_email` varchar(255) NOT NULL,
  `account_billing_alter_email` varchar(255) NOT NULL,
  `account_billing_receipt` varchar(255) NOT NULL,
  `account_billing_address` varchar(255) NOT NULL,
  `account_billing_mobile` text NOT NULL,
  `account_card_number` text NOT NULL,
  `account_alerts` int(100) NOT NULL,
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_account`
--

LOCK TABLES `tbl_account` WRITE;
/*!40000 ALTER TABLE `tbl_account` DISABLE KEYS */;
INSERT INTO `tbl_account` VALUES (3,'Kamal ','Kumar','vistasoft','koalafy.test','kshayne@gmail.com','joeyKoalafy','joeyKoalafy','Koalafy Joey','11-12-2016','15-01-2017',0,1,'7436464bd3a61c02cd6403b3c','1',3,'0','MasterCard','kshyne','09/21','Kevin','Shyne','kshayne@gmail.comn','kshayne@gmail.comn','Yes','241,Boston road,West ','6032474494','xxxxxxxxxxx12345',0),(4,'rajan','sharma','vistasoft','koalafy.test','kshayne@gmail.com','euca_Koalafy','euca_Koalafy','Koalafy Eucalyptus','07-12-2016','24-03-2017',0,1,'237badad699a572a4071d58e1','1',4,'1','MasterCard','Kevin shyne','09/21','Kevin','Shyne','kshayne@gmail.comn','kshayne@gmail.comn','Yes','241,Boston Road,west','6032474494','xxxxxxxxxxx12345',0),(5,'King','Koalafy-User','vistasoft','koalafy.testy','kshayne@gmail.com','kingKoalafy','kingKoalafy','Koalafy King','08-12-2016','07-12-2017',0,1,'2927483f3cede4441e34d957b','1',5,'1','American Express','Kevin shyne','01/18','Kevin','Shayne','kshayne@gmail.com','kshayne@gmail.com','Yes','241,Boston Road,west new york','6032474494','3434 343434 3434',1),(13,'wazir','singh','vistasoft','koalafy.test','nk3527@gmail.com','koalafy_912934','freeKoalafy','free','09-12-2016','19-12-2016',1,1,'88eb1a323b2605217ca67189a','1',2,'1','MasterCard','Kevin shyne','09/21','Kevin','Shyne','kshayne@gmail.comn','kshayne@gmail.comn','Yes','241,Boston Road,west','6032474494','xxxxxxxxxxx12345',0);
/*!40000 ALTER TABLE `tbl_account` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:14

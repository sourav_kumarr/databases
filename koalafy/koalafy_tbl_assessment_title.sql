-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: koalafy
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_assessment_title`
--

DROP TABLE IF EXISTS `tbl_assessment_title`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_assessment_title` (
  `title_id` bigint(200) NOT NULL AUTO_INCREMENT,
  `title_font` varchar(200) NOT NULL,
  `title_size` varchar(200) NOT NULL,
  `title_color` varchar(200) NOT NULL,
  `title_alignment` varchar(200) NOT NULL,
  `title_background` varchar(200) NOT NULL,
  `title_height` varchar(200) NOT NULL,
  `title_assessment_id` bigint(200) NOT NULL,
  PRIMARY KEY (`title_id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_assessment_title`
--

LOCK TABLES `tbl_assessment_title` WRITE;
/*!40000 ALTER TABLE `tbl_assessment_title` DISABLE KEYS */;
INSERT INTO `tbl_assessment_title` VALUES (1,'Gentium','26','#000000','center','#ff0000','170.00',39),(2,'Comic Sans MS','26','#000000','right','#d13d3d','170.00',75),(3,'monospace','10','#000000','right','','170.00',71),(4,'Century','16','#000000','right','#6495ed','170.00',70),(5,'Arial Narrow','28','#000000','right','#6495ed','170.00',69),(6,'monospace','10','#000000','right','','170.00',73),(7,'monospace','10','#000000','left','','170.00',74),(8,'Gentium','36','#000000','right','#cf8383','170.00',72),(9,'monospace','10','#000000','left','','168',77),(10,'monospace','32','#ffffff','left','#ffc000','170.00',60),(11,'monospace','10','#000000','left','#000000','170.00',68),(12,'','','','','','170.00',80),(13,'','','','','','170.00',81),(14,'monospace','10','#000000','left','#ffc000','170.00',82),(15,'','','','','','170.00',83),(16,'monospace','10','#000000','left','','170.00',84),(17,'','','','','','170.00',85),(18,'','','','','','170.00',83),(19,'monospace','10','#000000','left','','170.00',84),(20,'','','','','','170.00',85),(21,'','','','','','170.00',86),(22,'','','','','','170.00',87),(23,'monospace','10','#000000','left','','170.00',88),(24,'','','','','','170.00',89),(25,'','','','','','170.00',90),(26,'monospace','10','#000000','right','#ff0000','170.00',91),(27,'monospace','32','#000000','left','','170.00',92),(28,'monospace','10','#000000','left','','170.00',47),(29,'monospace','10','#000000','left','','170.00',93),(30,'monospace','10','#000000','left','','170.00',94),(31,'default','32','#ffffff','center','#ffc000','170.00',95),(32,'monospace','32','#ffffff','left','#ffc000','170.00',96),(33,'monospace','32','#ffffff','left','#ffc000','170.00',97),(34,'monospace','32','#ffffff','left','#ffc000','170.00',98),(35,'monospace','32','#ffffff','left','#ffc000','170.00',99),(36,'monospace','32','#ffffff','left','#ffc000','170.00',100),(37,'monospace','10','#000000','left','','170.00',101),(38,'monospace','32','#000000','center','','170.00',102),(39,'monospace','32','#ffffff','left','#ffc000','170.00',103),(40,'monospace','32','#ffffff','left','#ffc000','170.00',104),(41,'','','','','','170.00',105),(42,'','','','','','170.00',106),(43,'','','','','','170.00',107),(44,'monospace','32','#ffffff','left','#ffc000','170.00',108),(45,'','','','','','170.00',109),(46,'','','','','','170.00',110),(47,'','','','','','170.00',111),(48,'','','','','','170.00',112),(49,'','','','','','170.00',113),(50,'','','','','','170.00',114),(51,'','','','','','170.00',115),(52,'monospace','10','#000000','left','','170.00',116),(53,'','','','','','170.00',117),(54,'','','','','','170.00',61),(55,'','','','','','170.00',62),(56,'monospace','32','#000000','center','','170.00',63),(57,'monospace','10','#000000','left','','170.00',64),(58,'monospace','10','#000000','left','','170.00',65),(59,'monospace','10','#000000','left','','170.00',66);
/*!40000 ALTER TABLE `tbl_assessment_title` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:47:55

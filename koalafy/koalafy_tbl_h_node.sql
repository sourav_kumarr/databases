-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: koalafy
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_h_node`
--

DROP TABLE IF EXISTS `tbl_h_node`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_h_node` (
  `h_node_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `h_node_account_id` bigint(20) NOT NULL,
  `h_node_h_node_id` bigint(20) DEFAULT NULL,
  `h_node_h_node_type_id` bigint(20) NOT NULL DEFAULT '1',
  `h_node_name` varchar(255) NOT NULL DEFAULT '',
  `h_node_h_node_level_id` bigint(20) NOT NULL,
  PRIMARY KEY (`h_node_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_h_node`
--

LOCK TABLES `tbl_h_node` WRITE;
/*!40000 ALTER TABLE `tbl_h_node` DISABLE KEYS */;
INSERT INTO `tbl_h_node` VALUES (1,0,NULL,1,'Human Resources',1),(2,0,1,1,'HR General',1),(4,0,2,2,'Specific Knowledge',1),(5,0,2,2,'Personality',1),(6,0,2,2,'Motivation',1),(7,0,2,2,'Experience',1),(8,0,0,1,'Marketing',1),(9,0,8,1,'BANT',1),(10,0,9,2,'Budget',1),(11,0,9,2,'Authority',1),(12,0,9,2,'Timeline',1),(13,0,9,2,'Need',1),(15,19,NULL,1,'BANT',2),(17,19,15,2,'Authority',2),(18,19,15,2,'Timeline',2),(22,5,NULL,1,'Special One',2),(23,5,22,2,'Timeline',2),(24,5,22,2,'Funding',2),(25,5,NULL,1,'Marketing',2),(26,5,NULL,1,'Sales',2),(27,5,26,1,'BANT',2);
/*!40000 ALTER TABLE `tbl_h_node` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:01

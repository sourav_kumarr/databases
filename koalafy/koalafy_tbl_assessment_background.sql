-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: koalafy
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_assessment_background`
--

DROP TABLE IF EXISTS `tbl_assessment_background`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_assessment_background` (
  `back_id` bigint(100) NOT NULL AUTO_INCREMENT,
  `back_color` varchar(200) NOT NULL,
  `back_assessment_id` bigint(200) NOT NULL,
  PRIMARY KEY (`back_id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_assessment_background`
--

LOCK TABLES `tbl_assessment_background` WRITE;
/*!40000 ALTER TABLE `tbl_assessment_background` DISABLE KEYS */;
INSERT INTO `tbl_assessment_background` VALUES (30,'#d3d3d3',75),(31,'#a9a9a9',39),(32,'',78),(33,'',79),(34,'#ffffff',60),(35,'#ffffff',68),(36,'#d3d3d3',71),(37,'#a9a9a9',70),(38,'#ffffff',69),(39,'#d3d3d3',73),(40,'#d3d3d3',74),(41,'#d3d3d3',72),(42,'#d3d3d3',77),(43,'',80),(44,'',81),(45,'#a4a4a4',82),(46,'',83),(47,'#d3d3d3',84),(48,'',85),(49,'',83),(50,'#d3d3d3',84),(51,'',85),(52,'',86),(53,'',87),(54,'#d3d3d3',88),(55,'',89),(56,'',90),(57,'#964c4c',91),(58,'#d3d3d3',92),(59,'#d3d3d3',47),(60,'#d3d3d3',93),(61,'#d3d3d3',94),(62,'#ffffff',95),(63,'#ffffff',96),(64,'#ffffff',97),(65,'#ffffff',98),(66,'#ffffff',99),(67,'#ffffff',100),(68,'#d3d3d3',101),(69,'#d3d3d3',102),(70,'#ffffff',103),(71,'#ffffff',104),(72,'',105),(73,'',106),(74,'',107),(75,'#ffffff',108),(76,'',109),(77,'',110),(78,'',111),(79,'',112),(80,'',113),(81,'',114),(82,'',115),(83,'#d3d3d3',116),(84,'',117),(85,'',61),(86,'',62),(87,'#d3d3d3',63),(88,'#d3d3d3',64),(89,'#d3d3d3',65),(90,'#d3d3d3',66);
/*!40000 ALTER TABLE `tbl_assessment_background` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:06

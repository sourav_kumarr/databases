-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: koalafy
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wp_comments`
--

DROP TABLE IF EXISTS `wp_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext NOT NULL,
  `comment_author_email` varchar(100) NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) NOT NULL DEFAULT '',
  `comment_type` varchar(20) NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_comments`
--

LOCK TABLES `wp_comments` WRITE;
/*!40000 ALTER TABLE `wp_comments` DISABLE KEYS */;
INSERT INTO `wp_comments` VALUES (13,28992,'WooCommerce','woocommerce@koalafy.me','','','2016-12-07 06:48:21','2016-12-07 06:48:21','Order status changed from Pending Payment to Processing.',0,'1','WooCommerce','order_note',0,0),(11,28986,'WooCommerce','woocommerce@koalafy.me','','','2016-12-03 18:22:55','2016-12-03 18:22:55','Unpaid order cancelled - time limit reached. Order status changed from Pending Payment to Cancelled.',0,'1','WooCommerce','order_note',0,0),(10,28985,'WooCommerce','woocommerce@koalafy.me','','','2016-12-03 18:22:54','2016-12-03 18:22:54','Unpaid order cancelled - time limit reached. Order status changed from Pending Payment to Cancelled.',0,'1','WooCommerce','order_note',0,0),(5,3357,'WooCommerce','woocommerce@koalafy.me','','','2016-11-02 11:39:00','2016-11-02 11:39:00','Order cancelled by customer. Order status changed from Pending Payment to Cancelled.',0,'1','WooCommerce','order_note',0,0),(6,3358,'WooCommerce','woocommerce@koalafy.me','','','2016-11-02 12:00:38','2016-11-02 12:00:38','Order cancelled by customer. Order status changed from Pending Payment to Cancelled.',0,'1','WooCommerce','order_note',0,0),(7,3359,'WooCommerce','woocommerce@koalafy.me','','','2016-11-02 16:41:24','2016-11-02 16:41:24','Order cancelled by customer. Order status changed from Pending Payment to Cancelled.',0,'1','WooCommerce','order_note',0,0),(12,28992,'WooCommerce','woocommerce@koalafy.me','','','2016-12-07 06:48:21','2016-12-07 06:48:21','IPN payment completed',0,'1','WooCommerce','order_note',0,0),(8,28801,'WooCommerce','woocommerce@koalafy.me','','','2016-11-07 06:11:29','2016-11-07 06:11:29','Order cancelled by customer. Order status changed from Pending Payment to Cancelled.',0,'1','WooCommerce','order_note',0,0),(9,28859,'WooCommerce','woocommerce@koalafy.me','','','2016-11-11 10:25:00','2016-11-11 10:25:00','Order cancelled by customer. Order status changed from Pending Payment to Cancelled.',0,'1','WooCommerce','order_note',0,0);
/*!40000 ALTER TABLE `wp_comments` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:47:55

-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: koalafy
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_assessment_setting`
--

DROP TABLE IF EXISTS `tbl_assessment_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_assessment_setting` (
  `setting_id` bigint(200) NOT NULL AUTO_INCREMENT,
  `setting_is_back` varchar(200) NOT NULL,
  `setting_back_text` varchar(200) NOT NULL,
  `setting_next_text` varchar(200) NOT NULL,
  `setting_is_question` varchar(200) NOT NULL,
  `setting_is_header` varchar(200) NOT NULL,
  `setting_is_desc` varchar(200) NOT NULL,
  `setting_desc_text` varchar(200) NOT NULL,
  `setting_font` varchar(200) NOT NULL,
  `setting_text_size` varchar(200) NOT NULL,
  `setting_color` varchar(200) NOT NULL,
  `setting_alignment` varchar(200) NOT NULL,
  `setting_question_limit` varchar(50) NOT NULL,
  `setting_assessment_id` varchar(200) NOT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_assessment_setting`
--

LOCK TABLES `tbl_assessment_setting` WRITE;
/*!40000 ALTER TABLE `tbl_assessment_setting` DISABLE KEYS */;
INSERT INTO `tbl_assessment_setting` VALUES (16,'A','Back','Next','A','A','A','','monospace','10','#000000','center','3','75'),(17,'A','Back','Next','A','A','A','Description goes here','monospace','10','#000000','center','3','39'),(18,'','','','','','','','','','','','',''),(19,'','','','','','','','','','','','',''),(20,'A','Back','Next','A','A','A','Description goes here','monospace','10','#000000','center','3','60'),(21,'A','Back','Next','A','A','A','Description goes here','monospace','10','#000000','center','3','68'),(22,'A','Back','Next','A','A','A','Description goes here','monospace','10','#000000','center','','71'),(23,'A','Back','Next','A','A','A','Header Text Goes Here','Gentium','16','#ff0808','left','3','70'),(24,'A','Back','Next','D','A','A','Description goes here','Gentium','24','#000000','left','','69'),(25,'D','Back','Forward','A','A','A','Description goes here','monospace','10','#000000','center','','73'),(26,'A','Back','Next','A','A','A','Description goes here','monospace','10','#000000','center','','74'),(27,'A','Back','Next','A','A','A','Description goes here','monospace','10','#000000','center','','72'),(28,'A','Back','Next','A','A','A','Description goes here','monospace','10','#000000','center','','77'),(29,'','','','','','','','','','','','',''),(30,'','','','','','','','','','','','','80'),(31,'','','','','','','','','','','','',''),(32,'','','','','','','','','','','','','81'),(33,'','','','','','','','','','','','',''),(34,'A','Back','Next','A','A','A','Description goes here','monospace','10','#000000','center','5','82'),(35,'','','','','','','','','','','','',''),(36,'','','','','','','','','','','','','83'),(37,'','','','','','','','','','','','',''),(38,'A','Back','Next','A','A','A','Description goes here','monospace','10','#000000','center','5','84'),(39,'','','','','','','','','','','','',''),(40,'','','','','','','','','','','','','85'),(41,'','','','','','','','','','','','',''),(42,'','','','','','','','','','','','',''),(43,'','','','','','','','','','','','',''),(44,'','','','','','','','','','','','',''),(45,'','','','','','','','','','','','','86'),(46,'','','','','','','','','','','','',''),(47,'','','','','','','','','','','','',''),(48,'A','Back','Next','A','A','A','Description goes here','monospace','10','#000000','center','5','88'),(49,'','','','','','','','','','','','',''),(50,'','','','','','','','','','','','','89'),(51,'','','','','','','','','','','','',''),(52,'','','','','','','','','','','','','90'),(53,'','','','','','','','','','','','',''),(54,'','','','','','','','','','','','',''),(55,'A','Back','Next','A','A','A','Description goes here','monospace','10','#000000','center','5','47'),(56,'A','Back','Next','A','A','A','Description goes here','monospace','10','#000000','center','5','92'),(57,'','','','','','','','','','','','',''),(58,'A','Back','Next','A','A','A','Description goes here','monospace','10','#000000','center','5','93'),(59,'','','','','','','','','','','','',''),(60,'A','Back','Next','A','A','A','Description goes here','monospace','10','#000000','center','5','94'),(61,'','','','','','','','','','','','',''),(62,'A','Back','Next','A','A','A','Description goes here','monospace','10','#000000','center','3','95'),(63,'','','','','','','','','','','','',''),(64,'A','Back','Next','A','A','A','Description goes here','monospace','10','#000000','center','3','96'),(65,'','','','','','','','','','','','',''),(66,'A','Back','Next','A','A','A','Description goes here','monospace','10','#000000','center','3','97'),(67,'','','','','','','','','','','','',''),(68,'A','Back','Next','A','A','A','Description goes here','monospace','10','#000000','center','3','98'),(69,'','','','','','','','','','','','',''),(70,'A','Back','Next','A','A','A','Description goes here','monospace','10','#000000','center','3','99'),(71,'','','','','','','','','','','','',''),(72,'A','Back','Next','A','A','A','Description goes here','monospace','10','#000000','center','3','100'),(73,'','','','','','','','','','','','',''),(74,'A','Back','Next','A','A','A','Description goes here','monospace','10','#000000','center','5','101'),(75,'','','','','','','','','','','','',''),(76,'A','Back','Next','A','A','A','Description goes here','monospace','10','#000000','center','5','102'),(77,'','','','','','','','','','','','',''),(78,'A','Back','Next','A','A','A','Description goes here','monospace','10','#000000','center','3','103'),(79,'','','','','','','','','','','','',''),(80,'A','Back','Next','A','A','A','Description goes here','monospace','10','#000000','center','3','104'),(81,'A','Back','Next','A','A','A','Description goes here','monospace','10','#000000','center','5','91'),(82,'','','','','','','','','','','','',''),(83,'','','','','','','','','','','','',''),(84,'','','','','','','','','','','','',''),(85,'','','','','','','','','','','','',''),(86,'A','Back','Next','A','A','A','Description goes here','monospace','10','#000000','center','3','108'),(87,'','','','','','','','','','','','',''),(88,'','','','','','','','','','','','',''),(89,'','','','','','','','','','','','','110'),(90,'','','','','','','','','','','','',''),(91,'','','','','','','','','','','','','111'),(92,'','','','','','','','','','','','',''),(93,'','','','','','','','','','','','',''),(94,'','','','','','','','','','','','',''),(95,'','','','','','','','','','','','','114'),(96,'','','','','','','','','','','','',''),(97,'','','','','','','','','','','','','115'),(98,'','','','','','','','','','','','',''),(99,'A','Back','Next','A','A','A','Description goes here','monospace','10','#000000','center','5','116'),(100,'','','','','','','','','','','','',''),(101,'','','','','','','','','','','','',''),(102,'','','','A','A','','','','','','','','61'),(103,'','','','','','','','','','','','',''),(104,'','','','','','','','','','','','','62'),(105,'','','','','','','','','','','','',''),(106,'A','Back','Next','A','A','A','Description goes here','monospace','10','#000000','center','5','63'),(107,'A','Back','Next','A','A','A','Description goes here','monospace','10','#000000','center','5',''),(108,'A','Back','Next','A','A','A','Description goes here','monospace','10','#000000','center','5',''),(109,'A','Back','Next','A','A','A','Description goes here','monospace','10','#000000','center','5','65'),(110,'A','Back','Next','A','A','A','Description goes here','monospace','10','#000000','center','5',''),(111,'A','Back','Next','A','A','A','Description goes here','monospace','10','#000000','center','3','66');
/*!40000 ALTER TABLE `tbl_assessment_setting` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:00

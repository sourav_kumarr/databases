-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: ctesting
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_bug`
--

DROP TABLE IF EXISTS `tbl_bug`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_bug` (
  `bug_id` int(100) NOT NULL AUTO_INCREMENT,
  `bug_unique_id` varchar(1000) NOT NULL,
  `bug_name` varchar(1000) NOT NULL,
  `bug_desc` varchar(1000) NOT NULL,
  `bug_timestamp` varchar(100) NOT NULL,
  `bug_userid` varchar(1000) NOT NULL,
  `bug_revision` varchar(3) NOT NULL,
  `status` varchar(100) NOT NULL,
  PRIMARY KEY (`bug_id`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_bug`
--

LOCK TABLES `tbl_bug` WRITE;
/*!40000 ALTER TABLE `tbl_bug` DISABLE KEYS */;
INSERT INTO `tbl_bug` VALUES (87,'BUG_72892','layout issue','layout issue','2016-02-03 01:15:05','1','1','A'),(88,'BUG_80110','wrong component value','wrong component value','2016-02-03 01:15:05','1','1','A'),(89,'BUG_38434','wrong footprint','wrong footprint','2016-02-03 01:15:05','1','1','D'),(90,'BUG_53602','underrated or over rated parts','underrated or over rated parts','2016-02-03 01:15:05','1','1','D'),(91,'BUG_50842','software issues','software issues','2016-02-03 01:15:05','1','1','D'),(92,'BUG_32203','layout issuessss','layout issuesssss','2016-02-05 01:46:39','1','1','D'),(93,'BUG_83353','ffffff','fffff','2016-03-10 02:01:30','1','1','D'),(94,'BUG_86764','layout helllooooo','layout issuesssss','2016-03-11 10:09:56','7','1','P');
/*!40000 ALTER TABLE `tbl_bug` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:49

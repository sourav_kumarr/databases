-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: ctesting
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_unit`
--

DROP TABLE IF EXISTS `tbl_unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_unit` (
  `unit_uid` int(100) NOT NULL AUTO_INCREMENT,
  `unit_unique_id` varchar(100) NOT NULL,
  `unit_revid` float NOT NULL,
  `unit_name` varchar(1000) NOT NULL,
  `unit_description` text NOT NULL,
  `unit_partno` varchar(1000) NOT NULL,
  `unit_type` varchar(1000) NOT NULL,
  `unit_timestamp` varchar(1000) NOT NULL,
  `unit_status` varchar(100) NOT NULL,
  `unit_userid` varchar(100) NOT NULL,
  PRIMARY KEY (`unit_uid`)
) ENGINE=InnoDB AUTO_INCREMENT=226 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_unit`
--

LOCK TABLES `tbl_unit` WRITE;
/*!40000 ALTER TABLE `tbl_unit` DISABLE KEYS */;
INSERT INTO `tbl_unit` VALUES (112,'UNT_38882',0,'Switch','4 Port Switch','1.101.11','Active','2016-02-20 05:41:14','D','1'),(113,'UNT_66489',0,'UCC','universal Control Card','203.20017.02','Active','2016-02-20 05:41:14','D','1'),(114,'UNT_57878',0,'backplane','R3 backplane','203.20024.03','passive','2016-02-20 05:41:14','D','1'),(115,'UNT_94779',0,'ADP power','Alarm Display power','203.20031.01','Active','2016-02-20 05:41:14','D','1'),(116,'UNT_94077',0,'MPPT','Max Peak power tracking','2','Active','2016-02-20 05:41:14','D','1'),(124,'UNT_65101',1,'ggg','gggggggggggg','1','yyy','2016-03-11 08:40:39','P','1'),(125,'UNT_29734',1,'CMU','DC-DC SUPPLY(48V to 56V)<p></p>','401.20022.03','Active','2016-03-15 12:48:12','P','10'),(175,'UNT_34732',0,'CMU','<p>DC-DC SUPPLY(48V to 56V)<p></p></p>','401.20022.03','Active','2016-03-16 06:09:12','P','10'),(176,'UNT_31712',2,'CMU','<p>DC-DC SUPPLY(48V to 56V)<p></p></p>','401.20022.03','Active','2016-03-16 06:09:25','P','10'),(177,'UNT_36782',3,'CMU','<p>DC-DC SUPPLY(48V to 56V)<p></p></p>','401.20022.03','Active','2016-03-16 06:09:38','P','10'),(178,'UNT_1329',4,'CMU','<p>DC-DC SUPPLY(48V to 56V)<p></p></p>','401.20022.03','Active','2016-03-16 06:09:51','P','10'),(179,'UNT_39233',5,'CMU','<p>DC-DC SUPPLY(48V to 56V)<p></p></p>','401.20022.03','Active','2016-03-16 06:10:16','P','10'),(180,'UNT_3163',1,'ECU','<p>AC-dc 1.5KW</p>','203.20023.03','Active','2016-03-16 06:11:06','P','10'),(181,'UNT_66821',2,'ECU','<p>AC-dc 1.5KW</p>','203.20023.03','Active','2016-03-16 06:11:20','P','10'),(182,'UNT_40836',3,'ECU','<p>AC-dc 1.5KW</p>','203.20023.03','Active','2016-03-16 06:11:34','P','10'),(183,'UNT_33836',4,'ECU','<p>AC-dc 1.5KW</p>','203.20023.03','Active','2016-03-16 06:11:50','P','10'),(184,'UNT_97167',5,'ECU','<p>AC-dc 1.5KW</p>','203.20023.03','Active','2016-03-16 06:12:03','P','10'),(185,'UNT_60182',5,'L module','<p><p>ADC section on this </p></p>','401.20035.03','Active','2016-03-16 07:38:38','P','10'),(186,'UNT_34835',4,'L module','<p><p>ADC section on this </p></p>','401.20035.03','Active','2016-03-16 07:39:38','P','10'),(187,'UNT_62333',3,'L module','<p><p>ADC section on this </p></p>','401.20035.03','Active','2016-03-16 07:40:17','P','10'),(188,'UNT_13855',2,'L module','<p><p>ADC section on this </p></p>','401.20035.03','Active','2016-03-16 07:41:00','P','10'),(189,'UNT_48873',1,'L module','<p>ADC section on this </p>','401.20035.03','Active','2016-03-16 07:42:03','P','10'),(190,'UNT_24265',5,'K module','<p><p>Solar control section on this module<p></p></p></p>','401.20034.03','Active','2016-03-16 07:43:18','P','10'),(191,'UNT_30738',4,'K module','<p><p>Solar control section on this module<p></p></p></p>','401.20034.03','Active','2016-03-16 07:44:16','P','10'),(192,'UNT_67229',3,'K module','<p><p>Solar control section on this module<p></p></p></p>','401.20034.03','Active','2016-03-16 07:44:45','P','10'),(193,'UNT_82714',2,'K module','<p><p>Solar control section on this module<p></p></p></p>','401.20034.03','Active','2016-03-16 07:45:10','P','10'),(194,'UNT_25537',1,'K module','<p><p>Solar control section on this module<p></p></p></p>','401.20034.03','Active','2016-03-16 07:45:32','P','10'),(195,'UNT_47592',1,'G module','<p><p><p>DSP module<p></p></p></p></p>','401.20040.03','Active','2016-03-16 07:47:35','P','10'),(196,'UNT_97881',2,'G module','<p><p><p>DSP module<p></p></p></p></p>','401.20040.03','Active','2016-03-16 07:47:54','P','10'),(197,'UNT_84244',3,'G module','<p><p><p>DSP module<p></p></p></p></p>','401.20040.03','Active','2016-03-16 07:48:15','P','10'),(198,'UNT_3152',4,'G module','<p><p><p>DSP module<p></p></p></p></p>','401.20040.03','Active','2016-03-16 07:48:37','P','10'),(199,'UNT_26566',5,'G module','<p><p><p>DSP module<p></p></p></p></p>','401.20040.03','Active','2016-03-16 07:48:58','P','10'),(200,'UNT_25935',5,'F module','<p><p>Fan module<p></p></p></p>','401.20039.03','Active','2016-03-16 07:50:35','P','10'),(201,'UNT_44505',4,'F module','<p><p>Fan module<p></p></p></p>','401.20039.03','Active','2016-03-16 07:50:54','P','10'),(202,'UNT_54434',3,'F module','<p><p>Fan module<p></p></p></p>','401.20039.03','Active','2016-03-16 07:51:15','P','10'),(203,'UNT_58743',2,'F module','<p><p>Fan module<p></p></p></p>','401.20039.03','Active','2016-03-16 07:51:36','P','10'),(204,'UNT_33252',1,'F module','<p><p>Fan module<p></p></p></p>','401.20039.03','Active','2016-03-16 07:51:57','P','10'),(205,'UNT_55196',5,'D module','<p><p>Converter board.<p></p></p></p>','401.20038.03','Active','2016-03-16 07:53:02','P','10'),(206,'UNT_70958',4,'D module','<p><p>Converter board.<p></p></p></p>','401.20038.03','Active','2016-03-16 07:53:24','P','10'),(207,'UNT_47628',3,'D module','<p><p>Converter board.<p></p></p></p>','401.20038.03','Active','2016-03-16 07:53:43','P','10'),(208,'UNT_92712',2,'D module','<p><p>Converter board.<p></p></p></p>','401.20038.03','Active','2016-03-16 07:54:11','P','10'),(209,'UNT_96161',1,'D module','<p><p>Converter board.<p></p></p></p>','401.20038.03','Active','2016-03-16 07:54:32','P','10'),(210,'UNT_1357',5,'C module','<p><p>PFC control board.<p></p></p></p>','401.20037.03','Active','2016-03-16 07:55:43','P','10'),(211,'UNT_61756',4,'C module','<p><p>PFC control board.<p></p></p></p>','401.20037.03','Active','2016-03-16 07:56:08','P','10'),(212,'UNT_88651',3,'C module','<p><p>PFC control board.<p></p></p></p>','401.20037.03','Active','2016-03-16 07:56:28','P','10'),(213,'UNT_86347',2,'C module','<p><p>PFC control board.<p></p></p></p>','401.20037.03','Active','2016-03-16 07:56:51','P','10'),(214,'UNT_41189',1,'C module','<p><p>PFC control board.<p></p></p></p>','401.20037.03','Active','2016-03-16 07:57:11','P','10'),(215,'UNT_99895',5,'B module','<p><p>Supply power to housekeeping section.<p></p></p></p>','401.20036.03','Active','2016-03-16 07:58:52','P','10'),(216,'UNT_71171',4,'B module','<p><p>Supply power to housekeeping section.<p></p></p></p>','401.20036.03','Active','2016-03-16 07:59:10','P','10'),(217,'UNT_64561',3,'B module','<p><p>Supply power to housekeeping section.<p></p></p></p>','401.20036.03','Active','2016-03-16 07:59:33','P','10'),(218,'UNT_44016',2,'B module','<p><p>Supply power to housekeeping section.<p></p></p></p>','401.20036.03','Active','2016-03-16 07:59:53','P','10'),(219,'UNT_18922',1,'B module','<p><p>Supply power to housekeeping section.<p></p></p></p>','401.20036.03','Active','2016-03-16 08:00:13','P','10'),(220,'UNT_50567',5,'A module','<p><p>AC high-low cut module<p></p></p></p>','401.20043.03','Active','2016-03-16 08:01:26','P','10'),(221,'UNT_33769',4,'A module','<p><p>AC high-low cut module<p></p></p></p>','401.20043.03','Active','2016-03-16 08:01:46','P','10'),(222,'UNT_96702',3,'A module','<p><p>AC high-low cut module<p></p></p></p>','401.20043.03','Active','2016-03-16 08:02:06','P','10'),(223,'UNT_52889',2,'A module','<p><p>AC high-low cut module<p></p></p></p>','401.20043.03','Active','2016-03-16 08:02:23','P','10'),(224,'UNT_68556',1,'A module','<p><p>AC high-low cut module<p></p></p></p>','401.20043.03','Active','2016-03-16 08:02:46','P','10'),(225,'UNT_14933',1,'unit test','this unit is test<p></p>','101.1.1.22','ff','2016-08-01 12:12:17','P','1');
/*!40000 ALTER TABLE `tbl_unit` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:57

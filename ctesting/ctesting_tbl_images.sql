-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: ctesting
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_images`
--

DROP TABLE IF EXISTS `tbl_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_images` (
  `image_id` int(50) NOT NULL AUTO_INCREMENT,
  `image_path` text NOT NULL,
  `image_object_id` int(50) NOT NULL,
  `image_object_type` varchar(100) NOT NULL,
  `image_object_rev` varchar(100) NOT NULL,
  `image_timestamp` datetime NOT NULL,
  `image_desc` text NOT NULL,
  `image_name` varchar(70) NOT NULL,
  PRIMARY KEY (`image_id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_images`
--

LOCK TABLES `tbl_images` WRITE;
/*!40000 ALTER TABLE `tbl_images` DISABLE KEYS */;
INSERT INTO `tbl_images` VALUES (3,'images/PTS test requirement_4th sept.xlsx',2,'solution','','2015-09-25 12:55:58','this is all about solution2','solution2'),(4,'images/example1.xls',2,'solution','','2015-09-25 12:55:04','this is all about solution3','solution11'),(5,'aa.jpg',1,'bug','','2015-09-08 00:00:00','great image','hello'),(6,'images/example.xls',1,'solution','','2015-10-30 01:57:20','dfdf','dfd'),(7,'images/12180094_1063132073731165_1665847105_n.jpg',23,'unit','','2015-11-05 05:44:48','Helllo how r uuu','Unit12'),(8,'images/image_preview.png',23,'unit','','2015-11-05 05:45:43','cndnxzhnj','hiiiii'),(9,'images/ctesting.sql',15,'unit','','2015-11-05 05:52:17','vvvvvvvvvvvvv',''),(10,'images/ctesting.sql',15,'unit','','2015-11-05 05:56:25','aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa','aaaaaaaa'),(11,'images/unit_excel-1.xls',3,'test','','2015-11-05 06:11:00','ddddddddddddddddddddddddddd','d'),(12,'images/12180094_1063132073731165_1665847105_n.jpg',35,'unit','','2015-11-05 06:24:35','ncjxzncxnxzjjk','hlo'),(13,'images/unit_excel-.xls',8,'fault','','2015-11-05 06:34:24','aaaaaaaaaaaaaaaaaa','aaaaaaaaaaaaa'),(14,'images/unit_excel-.xls',1,'solution','','2015-11-05 06:40:04','ggggggg','gggggggggggggggg'),(16,'images/unit_excel-1.xls',1,'bug','','2015-11-05 06:44:49',';;;;;;;;;;;;;',';;;;;;;;;;;;;;;;;;'),(17,'images/unit_excel-.xls',7,'ecn','','2015-11-05 06:54:29','aaaaaaaaaa','aaaaaaaaaaa'),(18,'images/Mr. Prem Sindhu.psd',6,'test','','2015-11-09 05:21:23','dfdsfdfdsf','aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'),(19,'images/Mr. Prem Sindhu.psd',36,'unit','','2015-11-09 05:28:02','bbbbbbbbbbbbbbbb','bbbbbbbbbbbbsssssssssssssssssssssssssssssssssss'),(20,'images/sachtech.ai',6,'test','','2015-11-09 05:44:10','llllllllll','bbbbbbbbbbbb'),(21,'images/firefox.exe',15,'test','','2015-11-09 05:50:18','fffffffffffffffffffffffffffffffffffffff','ffffffffffffffffff'),(22,'images/firefox.exe',12,'test','','2015-11-09 06:16:17','ggggggggggggggggggggl','ggggggggggggggllllllllllllllllllllllllllllllllllllllllllllllllllllllll'),(23,'images/firefox.exe',17,'test','','2015-11-09 06:24:21','kkkkkkkkkk','kkkkkkk'),(24,'images/firefox.exe',17,'test','','2015-11-09 06:24:32','kkkkkkkkkk','kkkkkkk'),(25,'images/Unit.docx',9,'fault','','2015-11-09 11:25:52','gggggg','gggggggdddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd'),(26,'images/logo.png',9,'ecn','','2015-11-09 07:14:18','llllllllll','bbbbbbbbbbbb'),(27,'images/Unit.docx',9,'fault','','2015-11-09 08:03:57','aSAADAD','aaaa'),(28,'images/WinHTTrack.exe',9,'ecn','','2015-11-09 08:08:45','ertgererterte','rrrhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhfg'),(29,'images/logo.png',40,'unit','','2015-11-09 12:23:12','kkkkkkkkff','kkkkkkkkkff'),(30,'images/logo.png',21,'test','','2015-11-09 01:04:28','sssssjjj','ssssjj'),(31,'images/httrack.exe',8,'test','','2015-11-14 05:46:07','fdf','dfd'),(32,'images/httrack-doc.html',8,'test','','2015-11-09 01:17:01','sd','sd'),(33,'images/file_id.diz',8,'test','','2015-11-09 01:17:45','sds','sdsdsd'),(34,'images/file_id.diz',8,'test','','2015-11-09 01:20:29','dfdf','dfdf'),(35,'images/example.xls',12,'test','','2015-11-09 01:22:35','sdsd','dsdsdsdsdssds'),(36,'images/example.xls',40,'unit','','2015-11-09 01:26:15','sds','sd'),(37,'images/example.xls',40,'unit','','2015-11-09 01:29:32','sds','dsdsds'),(38,'images/example.xls',40,'unit','','2015-11-09 01:30:40','sdsd','sds'),(39,'images/example1.xls',8,'test','','2015-11-09 01:41:16','sds','sds'),(40,'images/example1.xls',8,'test','','2015-11-09 01:41:23','sds','sds'),(41,'images/example1.xls',8,'test','','2015-11-09 01:41:57','sds','sds'),(42,'images/DesktopWallpkkaper-2.png',42,'unit','','2015-11-10 06:07:46','ddddddddddddd','ddddddddddd'),(43,'images/DesktopWallpkkaper-2.png',14,'unit','','2015-11-16 04:38:59','ssssssssssss','kakka'),(44,'images/logo.png',5,'solution','','2015-11-10 06:37:27','sssssssss','sssssssss'),(46,'images/jass3.jpg',14,'unit','','2015-11-16 04:29:37','aaaaaaaaaaa','aaaaaaaaaaa'),(48,'images/jass3.jpg',12,'test','','2015-11-16 04:30:14','assdd','sssssssa'),(49,'images/jass2.jpg',14,'test','','2015-11-16 04:30:56','aaaaaaaa','aaaaaa'),(51,'images/jass3.jpg',4,'solution','','2015-11-16 04:31:43','sads','saaaaaaa'),(52,'images/jass1.jpg',6,'ecn','','2015-11-16 11:35:29','sssssssss','sssssss'),(53,'images/jass3.jpg',6,'unit','','2015-11-16 04:42:01','lll','lllll'),(54,'images/download_excal_file_Reoprt.xls',4,'solution','','2015-11-16 10:52:26','sadsa','tyt'),(55,'images/android-free-wallpapers.jpg',14,'unit','','2015-11-16 11:14:51','ffffffff','ffffffff'),(56,'images/netbeans.exe',7,'unit','','2015-11-16 11:17:31','eeee','lll'),(57,'images/Tulips.jpg',4,'bug','','2015-11-16 11:30:24','fffffff','ffffff'),(58,'images/Tulips.jpg',4,'bug','','2015-11-16 11:30:24','fffffff','ffffff'),(59,'images/Tulips.jpg',5,'bug','','2015-11-16 11:31:37','sssssssssssss','sssssssssssssssssssssssssss'),(60,'images/netbeans.exe',24,'fault','','2015-11-23 06:06:19','dgtrffg','aaaasadsadasdasdadadasbfcttttttttttttttttttttttttttttttttttttttttttttt'),(62,'images/Backup of 600W DC R4 System.PDF',62,'unit','','2015-11-24 04:20:21','FILE','DATASHEET'),(64,'images/netbeans.exe',63,'unit','','2015-11-26 07:51:35','beans','net'),(65,'images/example1.xls',62,'unit','','2015-11-26 12:42:30','sdsdsdsdsd','dsdsdsdsdssds'),(66,'images/example.xls',63,'unit','','2015-11-26 01:00:28','sdsd','dsdsdsdsdssds'),(68,'images/example1.xls',12,'design','','2015-11-27 11:36:03','sdsds','dsdsdsdsdssds'),(69,'images/netbeans',12,'design','','2015-11-27 11:42:40','fff','ff'),(70,'images/PTS test requirement_4th sept.xlsx',12,'design','1','2015-12-02 01:22:08','sdsdsd','dsdsdsdsdssds'),(71,'images/example1.xls',12,'design','1','2015-12-02 01:22:30','sdssdsd','ssddsd'),(73,'images/lang-1032.dll',78,'test','','2015-12-30 10:44:50',' qwe','1'),(74,'images/lang-1036.dll',78,'test','','2015-12-30 10:52:20','ASDASD','a'),(75,'images/lang-1026.dll',93,'unit','0','2015-12-30 10:54:28','s','sa'),(76,'images/lang-1030.dll',78,'test','1','2015-12-30 10:56:32','wer','qw'),(77,'images/web bug status.xlsx',14,'ecn','1','2016-01-06 09:28:43','hjkj','f'),(78,'images/ipmsg.log',224,'unit','1','2016-07-28 11:56:15','dfdfd','fdd'),(79,'images/Chrysanthemum.jpg',152,'test','1','2016-08-01 09:09:17','HS1 heat sink bend','HS1 heat'),(80,'images/Koala.jpg',66,'solution','1','2016-08-01 01:52:38','solution attachment added','solutions attachment');
/*!40000 ALTER TABLE `tbl_images` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:57

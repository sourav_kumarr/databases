-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: ctesting
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_comments`
--

DROP TABLE IF EXISTS `tbl_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_comments` (
  `comm_id` int(100) NOT NULL AUTO_INCREMENT,
  `comm_desc` text NOT NULL,
  `comm_item_type` varchar(100) NOT NULL,
  `comm_item_id` varchar(100) NOT NULL,
  `comm_userid` varchar(100) NOT NULL,
  `comm_status` varchar(100) NOT NULL,
  `comm_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`comm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_comments`
--

LOCK TABLES `tbl_comments` WRITE;
/*!40000 ALTER TABLE `tbl_comments` DISABLE KEYS */;
INSERT INTO `tbl_comments` VALUES (3,'ssssssss','unit','109','8','A','2016-03-09 14:24:24'),(4,'ffffffffff','unit','109','8','A','2016-03-09 14:53:52'),(5,'ddddddd','unit','110','8','A','2016-03-10 10:05:15'),(6,'gggggggggggg','test','108','8','A','2016-03-10 10:13:03'),(7,'fffffff','bug','87','8','A','2016-03-10 10:15:26'),(8,'fffff','solution','45','8','A','2016-03-10 10:16:47'),(9,'yyyyyyy','ecn','15','8','A','2016-03-10 10:17:21'),(10,'vvvvv','format','5','8','A','2016-03-10 10:17:46'),(11,'hhhhhhhhh','design','1','8','A','2016-03-10 10:18:19'),(12,'yyyyyy','design','1','8','A','2016-03-10 10:18:44'),(13,'gggggggggg','design','1','8','A','2016-03-10 10:18:54'),(14,'yyyyyyyyy','design','1','8','A','2016-03-10 10:20:37'),(15,'fffff','design','1','8','A','2016-03-10 10:25:48'),(16,'gggg','fault','48','8','A','2016-03-10 11:00:24'),(17,'jhhhhhhhhhh','fault','48','8','A','2016-03-10 11:05:10'),(18,'gggggggg','fault','48','8','A','2016-03-10 11:05:59'),(19,'yyyyyyyyyyyyyyy','solution','45','8','A','2016-03-10 11:17:33'),(20,'yyyyyyyyyrrrrrrrrrrr','solution','50','8','A','2016-03-10 11:17:56'),(21,'ddddddddd','ecn','15','8','A','2016-03-10 11:32:59'),(22,'fffffffffff','format','5','8','A','2016-03-10 11:35:40'),(23,'ffffffffffffff','process','31','8','A','2016-03-10 11:40:53'),(24,'ggggggggg','unit','110','1','A','2016-03-10 11:50:04'),(25,'ggggggggggg','unit','110','1','A','2016-03-11 14:04:04'),(26,'yyyyyyyy','unit','114','1','A','2016-03-11 14:04:50'),(27,'ggggggg','unit','110','1','A','2016-03-11 14:05:46');
/*!40000 ALTER TABLE `tbl_comments` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:55

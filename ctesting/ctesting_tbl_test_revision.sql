-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: ctesting
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_test_revision`
--

DROP TABLE IF EXISTS `tbl_test_revision`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_test_revision` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `test_id` varchar(100) NOT NULL,
  `test_uniqueid` varchar(100) NOT NULL,
  `test_name` varchar(1000) NOT NULL,
  `test_category` varchar(1000) NOT NULL,
  `test_description` varchar(1000) NOT NULL,
  `test_precondition` varchar(1000) NOT NULL,
  `test_revision` varchar(1000) NOT NULL,
  `test_ideal` varchar(1000) NOT NULL,
  `test_min_level` varchar(1000) NOT NULL,
  `test_max_level` varchar(1000) NOT NULL,
  `test_type` varchar(100) NOT NULL,
  `test_userid` varchar(1000) NOT NULL,
  `test_timestamp` datetime DEFAULT NULL,
  `test_status` enum('A','D','DELETE') NOT NULL,
  `test_data_type` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_test_revision`
--

LOCK TABLES `tbl_test_revision` WRITE;
/*!40000 ALTER TABLE `tbl_test_revision` DISABLE KEYS */;
INSERT INTO `tbl_test_revision` VALUES (1,'160','TST_20423','Pts_5','1','<p></p>','test','1','40000','41001','41005','','1','2016-02-10 06:39:48','D','Short'),(2,'161','TST_42014','Pts_6','1','<p></p>','test','1','50000','51001','51005','','1','2016-02-10 06:40:31','D','Short'),(3,'157','TST_35320','Pts_2','1','<p></p>','test','1','10000','11001','11005','','1','2016-02-10 06:37:16','D','Short'),(4,'158','TST_73812','Pts_3','1','<p></p>','test','1','20000','21001','21005','','1','2016-02-10 06:38:05','D','Short'),(5,'159','TST_84908','Pts_4','1','<p></p>','test','1','30000','31001','31005','','1','2016-02-10 06:39:01','D','Short'),(6,'160','TST_20423','Pts_5','1','<p><p></p></p>','test','1.1','31000','31002','31007','','1','2016-02-10 08:23:16','D','Short'),(7,'161','TST_42014','Pts_6','1','<p><p></p></p>','test','1.1','32000','32001','32005','','1','2016-02-10 08:24:02','D','Short');
/*!40000 ALTER TABLE `tbl_test_revision` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:56

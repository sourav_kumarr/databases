-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: ctesting
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_product_child`
--

DROP TABLE IF EXISTS `tbl_product_child`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_child` (
  `pchild_id` int(100) NOT NULL AUTO_INCREMENT,
  `pchild_parent_id` varchar(100) NOT NULL,
  `pchild_child_id` varchar(100) NOT NULL,
  `pchild_child_revision` varchar(100) NOT NULL,
  `pchild_child_name` varchar(100) NOT NULL,
  `pchild_child_partno` text NOT NULL,
  `pchild_parent_partno` text NOT NULL,
  `pchild_tree_id` varchar(100) NOT NULL,
  `pchild_quantity` varchar(100) NOT NULL,
  `pchild_timestamp` date NOT NULL,
  `pchild_tree_revision` varchar(100) NOT NULL,
  PRIMARY KEY (`pchild_id`)
) ENGINE=MyISAM AUTO_INCREMENT=297 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_child`
--

LOCK TABLES `tbl_product_child` WRITE;
/*!40000 ALTER TABLE `tbl_product_child` DISABLE KEYS */;
INSERT INTO `tbl_product_child` VALUES (171,'UNT_94779','UNT_57878','1','backplane','','','70','1','2016-03-11','1'),(170,'UNT_65101','UNT_94779','1','ADP power','','','70','1','2016-03-11','1'),(169,'UNT_65101','UNT_94077','1','MPPT','','','70','1','2016-03-11','1'),(168,'UNT_65101','UNT_65101','1','ggg','','','70','12','2016-03-11','1'),(167,'0','UNT_65101','1','ggg','','','70','','2016-03-11','1'),(166,'UNT_94779',' UNT_66489','1','UCC','','','69','1','2016-03-11','1'),(165,'UNT_94779','UNT_57878','1','backplane','','','69','1','2016-03-11','1'),(164,'UNT_65101','UNT_94779','1','ADP power','','','69','1','2016-03-11','1'),(163,'UNT_65101','UNT_94077','1','MPPT','','','69','1','2016-03-11','1'),(162,'UNT_65101','UNT_65101','1','ggg','','','69','12','2016-03-11','1'),(161,'0','UNT_65101','1','ggg','','','69','','2016-03-11','1'),(160,'UNT_16699','UNT_66489','1','UCC','','','67','1','2016-02-20','1'),(159,'UNT_16699','UNT_21151','1','Vsat power','','','67','1','2016-02-20','1'),(158,'UNT_38882','UNT_38882','1','Switch','','','67','2','2016-02-20','1'),(157,'UNT_65010','UNT_53072','1','ECU','','','67','1','2016-02-20','1'),(156,'UNT_94779','UNT_16699','1','CMU','','','67','1','2016-02-20','1'),(155,'UNT_94779','UNT_38882','1','Switch','','','67','1','2016-02-20','1'),(154,'UNT_94779','UNT_65010','1','SPD','','','67','12','2016-02-20','1'),(153,'0','UNT_94779','1','ADP power','','','67','','2016-02-20','1'),(144,'UNT_54331','UNT_82185','1','UCC3','','','62','1','2016-02-08','1'),(143,'UNT_54331','UNT_95456','1','backplane3','','','62','1','2016-02-08','1'),(142,'UNT_40978','UNT_54331','1','ADP power3','','','62','1','2016-02-08','1'),(141,'UNT_40978','UNT_89953','1','MPPT3','','','62','1','2016-02-08','1'),(140,'UNT_40978','UNT_88272','1','Backhaul3','','','62','12','2016-02-08','1'),(139,'0','UNT_40978','1','SPD3','','','62','','2016-02-08','1'),(172,'UNT_94779',' UNT_66489','1','UCC','','','70','1','2016-03-11','1'),(268,'UNT_66821','UNT_52889','2','A module','','','102','1','2016-03-16','2'),(267,'0','UNT_66821','2','ECU','','','102','','2016-03-16','2'),(266,'UNT_3163','UNT_47592','1','G module','','','101','1','2016-03-16','1'),(245,'0','UNT_29734','1','CMU','','','96','','2016-03-16','1'),(246,'UNT_29734','UNT_25537','1','K module','','','96','1','2016-03-16','1'),(247,'UNT_29734','UNT_48873','1','L module','','','96','1','2016-03-16','1'),(248,'0','UNT_31712','2','CMU','','','97','','2016-03-16','2'),(249,'UNT_31712','UNT_82714','2','K module','','','97','1','2016-03-16','2'),(250,'UNT_31712','UNT_13855','2','L module','','','97','1','2016-03-16','2'),(251,'0','UNT_36782','3','CMU','','','98','','2016-03-16','3'),(252,'UNT_36782','UNT_67229','3','K module','','','98','1','2016-03-16','3'),(253,'UNT_36782','UNT_62333','3','L module','','','98','1','2016-03-16','3'),(254,'0','UNT_1329','4','CMU','','','99','','2016-03-16','4'),(255,'UNT_1329','UNT_30738','4','K module','','','99','1','2016-03-16','4'),(256,'UNT_1329','UNT_34835','4','L module','','','99','1','2016-03-16','4'),(257,'0','UNT_39233','5','CMU','','','100','','2016-03-16','5'),(258,'UNT_39233','UNT_24265','5','K module','','','100','1','2016-03-16','5'),(259,'UNT_39233','UNT_60182','5','L module','','','100','1','2016-03-16','5'),(260,'0','UNT_3163','1','ECU','','','101','','2016-03-16','1'),(261,'UNT_3163','UNT_68556','1','A module','','','101','1','2016-03-16','1'),(262,'UNT_3163','UNT_18922','1','B module','','','101','1','2016-03-16','1'),(263,'UNT_3163','UNT_41189','1','C module','','','101','1','2016-03-16','1'),(264,'UNT_3163','UNT_96161','1','D module','','','101','1','2016-03-16','1'),(265,'UNT_3163','UNT_33252','1','F module','','','101','1','2016-03-16','1'),(269,'UNT_66821','UNT_44016','2','B module','','','102','1','2016-03-16','2'),(270,'UNT_66821','UNT_86347','2','C module','','','102','1','2016-03-16','2'),(271,'UNT_66821','UNT_92712','2','D module','','','102','1','2016-03-16','2'),(272,'UNT_66821','UNT_58743','2','F module','','','102','1','2016-03-16','2'),(273,'UNT_66821','UNT_97881','2','G module','','','102','1','2016-03-16','2'),(274,'0','UNT_40836','3','ECU','','','103','','2016-03-16','3'),(275,'UNT_40836','UNT_96702','3','A module','','','103','1','2016-03-16','3'),(276,'UNT_40836','UNT_64561','3','B module','','','103','1','2016-03-16','3'),(277,'UNT_40836','UNT_88651','3','C module','','','103','1','2016-03-16','3'),(278,'UNT_40836','UNT_47628','3','D module','','','103','1','2016-03-16','3'),(279,'UNT_40836','UNT_54434','3','F module','','','103','1','2016-03-16','3'),(280,'UNT_40836','UNT_84244','3','G module','','','103','1','2016-03-16','3'),(281,'0','UNT_33836','4','ECU','','','104','','2016-03-16','4'),(282,'UNT_33836','UNT_33769','4','A module','','','104','1','2016-03-16','4'),(283,'UNT_33836','UNT_71171','4','B module','','','104','1','2016-03-16','4'),(284,'UNT_33836','UNT_61756','4','C module','','','104','1','2016-03-16','4'),(285,'UNT_33836','UNT_70958','4','D module','','','104','1','2016-03-16','4'),(286,'UNT_33836','UNT_44505','4','F module','','','104','1','2016-03-16','4'),(287,'UNT_33836','UNT_3152','4','G module','','','104','1','2016-03-16','4'),(288,'0','UNT_97167','5','ECU','','','105','','2016-03-16','5'),(289,'UNT_97167','UNT_50567','5','A module','','','105','1','2016-03-16','5'),(290,'UNT_97167','UNT_99895','5','B module','','','105','1','2016-03-16','5'),(291,'UNT_97167','UNT_1357','5','C module','','','105','1','2016-03-16','5'),(292,'UNT_97167','UNT_55196','5','D module','','','105','1','2016-03-16','5'),(293,'UNT_97167','UNT_25935','5','F module','','','105','1','2016-03-16','5'),(294,'UNT_97167','UNT_26566','5','G module','','','105','1','2016-03-16','5'),(295,'0','UNT_38882','0','Switch','1.101.11','p','106','','2016-08-02','1'),(296,'UNT_38882','UNT_66489','0','UCC','203.20017.02','1.101.11','106','11','2016-08-02','1');
/*!40000 ALTER TABLE `tbl_product_child` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:54

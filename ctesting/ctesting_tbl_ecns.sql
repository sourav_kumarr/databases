-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: ctesting
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_ecns`
--

DROP TABLE IF EXISTS `tbl_ecns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_ecns` (
  `ecn_id` int(100) NOT NULL AUTO_INCREMENT,
  `ecn_uid` varchar(100) NOT NULL,
  `ecn_name` varchar(1000) NOT NULL,
  `ecn_userid` varchar(1000) NOT NULL,
  `ecn_timestamp` varchar(1000) NOT NULL,
  `ecn_desc` varchar(100) NOT NULL,
  `ecn_status` enum('A','D','DELETE') NOT NULL,
  `ecn_rev` varchar(3) NOT NULL,
  PRIMARY KEY (`ecn_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_ecns`
--

LOCK TABLES `tbl_ecns` WRITE;
/*!40000 ALTER TABLE `tbl_ecns` DISABLE KEYS */;
INSERT INTO `tbl_ecns` VALUES (15,'ECN13393','layout correction in CMU','1','2016-02-03 01:16:14','layout correction in CMU','D','1'),(16,'ECN64717','layout correction in ECU','1','2016-02-03 01:16:14','layout correction in ECU','D','1'),(17,'ECN84134','layout correction in ADP','1','2016-02-03 01:16:14','layout correction in ADP','D','1'),(18,'ECN67280','layout correction in backhaul','1','2016-02-03 01:16:14','layout correction in backhaul','D','1'),(19,'ECN40718','layout correction in backplane','1','2016-02-03 01:16:14','layout correction in backplane','D','1'),(20,'ECN60505','layout correction in SPD','1','2016-02-03 01:16:14','layout correction in SPD','D','1'),(21,'ECN35843','layout correction in Vsat','1','2016-02-03 01:16:14','layout correction in Vsat','D','1'),(22,'ECN72473','software change in ADP','1','2016-02-03 01:16:14','software change in ADP','D','1'),(23,'ECN34046','software change in ECU','1','2016-02-03 01:16:14','software change in ECU','D','1'),(24,'ECN25475','software change in UCC','1','2016-02-03 01:16:14','software change in UCC','D','1'),(25,'ECN98039','mechanical change of backhaul','1','2016-02-03 01:16:14','mechanical change of backhaul','D','1'),(26,'ECN81943','layout correction in CMUuuuuuuu','1','2016-02-05 02:18:41','layout correction in CMUiiiiii','D','1'),(27,'ECN27436','layout ','7','2016-03-11 10:11:28','layout correction','','1');
/*!40000 ALTER TABLE `tbl_ecns` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:56

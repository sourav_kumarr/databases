-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 22, 2016 at 11:18 PM
-- Server version: 5.5.45-cll-lve
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ctesting`
--

-- --------------------------------------------------------

--
-- Table structure for table `phpjobscheduler`
--

CREATE TABLE IF NOT EXISTS `phpjobscheduler` (
  `phpjobscheduler` varchar(1000) NOT NULL,
  `fire_time` varchar(1000) NOT NULL,
  `paused` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `phpjobscheduler_logs`
--

CREATE TABLE IF NOT EXISTS `phpjobscheduler_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_added` int(11) DEFAULT NULL,
  `script` varchar(128) DEFAULT NULL,
  `output` text,
  `execution_time` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sync_detail`
--

CREATE TABLE IF NOT EXISTS `sync_detail` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `table_name` varchar(100) NOT NULL,
  `sync_time` varchar(100) NOT NULL,
  `sync_date` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_backup`
--

CREATE TABLE IF NOT EXISTS `tbl_backup` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `db_name` varchar(100) NOT NULL,
  `db_user` varchar(100) NOT NULL,
  `db_pass` varchar(1000) NOT NULL,
  `backup_name` varchar(100) NOT NULL,
  `timestamp` varchar(700) NOT NULL,
  `status` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=139 ;

--
-- Dumping data for table `tbl_backup`
--

INSERT INTO `tbl_backup` (`id`, `db_name`, `db_user`, `db_pass`, `backup_name`, `timestamp`, `status`) VALUES
(36, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-10-28 @ 01-12-51.sql', '2015-10-28 @ 01-12-51', 'A'),
(37, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-10-29 @ 09-24-29.sql', '2015-10-29 @ 09-24-29', 'A'),
(38, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-10-30 @ 01-12-07.sql', '2015-10-30 @ 01-12-07', 'A'),
(39, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-02 @ 01-12-16.sql', '2015-11-02 @ 01-12-16', 'A'),
(40, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-02 @ 01-12-50.sql', '2015-11-02 @ 01-12-50', 'A'),
(41, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-03 @ 01-12-04.sql', '2015-11-03 @ 01-12-04', 'A'),
(42, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-03 @ 01-12-36.sql', '2015-11-03 @ 01-12-36', 'A'),
(43, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-04 @ 01-12-03.sql', '2015-11-04 @ 01-12-03', 'A'),
(44, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-04 @ 01-12-26.sql', '2015-11-04 @ 01-12-26', 'A'),
(45, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-04 @ 01-12-33.sql', '2015-11-04 @ 01-12-33', 'A'),
(46, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-05 @ 01-12-26.sql', '2015-11-05 @ 01-12-26', 'A'),
(47, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-05 @ 01-12-56.sql', '2015-11-05 @ 01-12-56', 'A'),
(48, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-06 @ 01-12-16.sql', '2015-11-06 @ 01-12-16', 'A'),
(49, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-06 @ 01-12-25.sql', '2015-11-06 @ 01-12-25', 'A'),
(50, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-06 @ 01-12-52.sql', '2015-11-06 @ 01-12-52', 'A'),
(51, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-07 @ 01-12-49.sql', '2015-11-07 @ 01-12-49', 'A'),
(52, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-07 @ 01-12-58.sql', '2015-11-07 @ 01-12-58', 'A'),
(53, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-09 @ 12-06-56.sql', '2015-11-09 @ 12-06-56', 'A'),
(54, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-09 @ 01-12-02.sql', '2015-11-09 @ 01-12-02', 'A'),
(55, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-09 @ 01-12-48.sql', '2015-11-09 @ 01-12-48', 'A'),
(56, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-13 @ 01-12-33.sql', '2015-11-13 @ 01-12-33', 'A'),
(57, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-16 @ 01-12-27.sql', '2015-11-16 @ 01-12-27', 'A'),
(58, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-16 @ 01-12-34.sql', '2015-11-16 @ 01-12-34', 'A'),
(59, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-16 @ 01-12-43.sql', '2015-11-16 @ 01-12-43', 'A'),
(60, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-16 @ 01-44-21.sql', '2015-11-16 @ 01-44-21', 'A'),
(61, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-16 @ 01-44-21.sql', '2015-11-16 @ 01-44-21', 'A'),
(62, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-16 @ 01-44-21.sql', '2015-11-16 @ 01-44-21', 'A'),
(63, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-16 @ 01-45-26.sql', '2015-11-16 @ 01-45-26', 'A'),
(64, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-16 @ 01-45-26.sql', '2015-11-16 @ 01-45-26', 'A'),
(65, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-19 @ 01-12-57.sql', '2015-11-19 @ 01-12-57', 'A'),
(66, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-23 @ 01-11-58.sql', '2015-11-23 @ 01-11-58', 'A'),
(67, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-23 @ 01-12-27.sql', '2015-11-23 @ 01-12-27', 'A'),
(68, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-23 @ 01-12-48.sql', '2015-11-23 @ 01-12-48', 'A'),
(69, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-24 @ 01-12-41.sql', '2015-11-24 @ 01-12-41', 'A'),
(70, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-25 @ 01-12-16.sql', '2015-11-25 @ 01-12-16', 'A'),
(71, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-26 @ 01-12-35.sql', '2015-11-26 @ 01-12-35', 'A'),
(72, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-26 @ 01-12-41.sql', '2015-11-26 @ 01-12-41', 'A'),
(73, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-26 @ 01-12-43.sql', '2015-11-26 @ 01-12-43', 'A'),
(74, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-27 @ 01-12-25.sql', '2015-11-27 @ 01-12-25', 'A'),
(75, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-27 @ 01-12-45.sql', '2015-11-27 @ 01-12-45', 'A'),
(76, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-28 @ 01-12-21.sql', '2015-11-28 @ 01-12-21', 'A'),
(77, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-30 @ 01-12-18.sql', '2015-11-30 @ 01-12-18', 'A'),
(78, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-30 @ 01-12-22.sql', '2015-11-30 @ 01-12-22', 'A'),
(79, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-11-30 @ 01-12-29.sql', '2015-11-30 @ 01-12-29', 'A'),
(80, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-12-01 @ 01-12-21.sql', '2015-12-01 @ 01-12-21', 'A'),
(81, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-12-02 @ 01-12-20.sql', '2015-12-02 @ 01-12-20', 'A'),
(82, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-12-02 @ 01-12-24.sql', '2015-12-02 @ 01-12-24', 'A'),
(83, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-12-02 @ 01-12-27.sql', '2015-12-02 @ 01-12-27', 'A'),
(84, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-12-03 @ 01-12-20.sql', '2015-12-03 @ 01-12-20', 'A'),
(85, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-12-04 @ 01-12-09.sql', '2015-12-04 @ 01-12-09', 'A'),
(86, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-12-07 @ 01-12-05.sql', '2015-12-07 @ 01-12-05', 'A'),
(87, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-12-08 @ 01-12-07.sql', '2015-12-08 @ 01-12-07', 'A'),
(88, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-12-08 @ 01-12-18.sql', '2015-12-08 @ 01-12-18', 'A'),
(89, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-12-09 @ 01-12-26.sql', '2015-12-09 @ 01-12-26', 'A'),
(90, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-12-12 @ 06-40-04.sql', '2015-12-12 @ 06-40-04', 'A'),
(91, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-12-16 @ 06-40-23.sql', '2015-12-16 @ 06-40-23', 'A'),
(92, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-12-18 @ 06-40-36.sql', '2015-12-18 @ 06-40-36', 'A'),
(93, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-12-21 @ 04-51-26.sql', '2015-12-21 @ 04-51-26', 'A'),
(94, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-12-22 @ 06-40-12.sql', '2015-12-22 @ 06-40-12', 'A'),
(95, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-12-23 @ 06-40-48.sql', '2015-12-23 @ 06-40-48', 'A'),
(96, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-12-26 @ 01-12-04.sql', '2015-12-26 @ 01-12-04', 'A'),
(97, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-12-28 @ 01-12-38.sql', '2015-12-28 @ 01-12-38', 'A'),
(98, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-12-29 @ 01-12-43.sql', '2015-12-29 @ 01-12-43', 'A'),
(99, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-12-29 @ 01-12-57.sql', '2015-12-29 @ 01-12-57', 'A'),
(100, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-12-30 @ 01-12-27.sql', '2015-12-30 @ 01-12-27', 'A'),
(101, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-12-30 @ 01-12-40.sql', '2015-12-30 @ 01-12-40', 'A'),
(102, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2015-12-30 @ 01-12-53.sql', '2015-12-30 @ 01-12-53', 'A'),
(103, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2016-01-02 @ 01-12-36.sql', '2016-01-02 @ 01-12-36', 'A'),
(104, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2016-01-04 @ 01-12-52.sql', '2016-01-04 @ 01-12-52', 'A'),
(105, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2016-01-05 @ 01-12-49.sql', '2016-01-05 @ 01-12-49', 'A'),
(106, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2016-01-06 @ 01-12-22.sql', '2016-01-06 @ 01-12-22', 'A'),
(107, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2016-01-11 @ 01-12-20.sql', '2016-01-11 @ 01-12-20', 'A'),
(108, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2016-01-12 @ 01-12-32.sql', '2016-01-12 @ 01-12-32', 'A'),
(109, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2016-01-13 @ 01-12-34.sql', '2016-01-13 @ 01-12-34', 'A'),
(110, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2016-01-13 @ 01-12-40.sql', '2016-01-13 @ 01-12-40', 'A'),
(111, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2016-01-13 @ 01-12-58.sql', '2016-01-13 @ 01-12-58', 'A'),
(112, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2016-01-14 @ 01-12-27.sql', '2016-01-14 @ 01-12-27', 'A'),
(113, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2016-01-19 @ 01-12-37.sql', '2016-01-19 @ 01-12-37', 'A'),
(114, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2016-01-19 @ 01-12-42.sql', '2016-01-19 @ 01-12-42', 'A'),
(115, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2016-01-20 @ 01-12-54.sql', '2016-01-20 @ 01-12-54', 'A'),
(116, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2016-01-21 @ 01-12-16.sql', '2016-01-21 @ 01-12-16', 'A'),
(117, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2016-01-21 @ 01-12-19.sql', '2016-01-21 @ 01-12-19', 'A'),
(118, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2016-01-21 @ 01-12-44.sql', '2016-01-21 @ 01-12-44', 'A'),
(119, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2016-01-21 @ 01-12-45.sql', '2016-01-21 @ 01-12-45', 'A'),
(120, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2016-01-22 @ 01-12-16.sql', '2016-01-22 @ 01-12-16', 'A'),
(121, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2016-01-27 @ 01-12-03.sql', '2016-01-27 @ 01-12-03', 'A'),
(122, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2016-01-28 @ 01-12-49.sql', '2016-01-28 @ 01-12-49', 'A'),
(123, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2016-01-30 @ 01-12-10.sql', '2016-01-30 @ 01-12-10', 'A'),
(124, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2016-01-30 @ 01-12-55.sql', '2016-01-30 @ 01-12-55', 'A'),
(125, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2016-02-01 @ 01-12-06.sql', '2016-02-01 @ 01-12-06', 'A'),
(126, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2016-02-01 @ 01-12-13.sql', '2016-02-01 @ 01-12-13', 'A'),
(127, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2016-02-01 @ 01-12-27.sql', '2016-02-01 @ 01-12-27', 'A'),
(128, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2016-02-01 @ 01-12-40.sql', '2016-02-01 @ 01-12-40', 'A'),
(129, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2016-02-01 @ 01-12-51.sql', '2016-02-01 @ 01-12-51', 'A'),
(130, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2016-02-02 @ 03-53-30.sql', '2016-02-02 @ 03-53-30', 'A'),
(131, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2016-02-02 @ 01-12-46.sql', '2016-02-02 @ 01-12-46', 'A'),
(132, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2016-02-03 @ 01-12-48.sql', '2016-02-03 @ 01-12-48', 'A'),
(133, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2016-02-10 @ 01-12-13.sql', '2016-02-10 @ 01-12-13', 'A'),
(134, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2016-02-12 @ 01-12-15.sql', '2016-02-12 @ 01-12-15', 'A'),
(135, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2016-02-13 @ 01-12-09.sql', '2016-02-13 @ 01-12-09', 'A'),
(136, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2016-02-18 @ 01-12-18.sql', '2016-02-18 @ 01-12-18', 'A'),
(137, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2016-02-20 @ 01-13-00.sql', '2016-02-20 @ 01-13-00', 'A'),
(138, 'ctesting', 'sourav', 'sourav', 'backup_dump/ctesting-Backup-2016-02-20 @ 01-13-13.sql', '2016-02-20 @ 01-13-13', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bug`
--

CREATE TABLE IF NOT EXISTS `tbl_bug` (
  `bug_id` int(100) NOT NULL AUTO_INCREMENT,
  `bug_unique_id` varchar(1000) NOT NULL,
  `bug_name` varchar(1000) NOT NULL,
  `bug_desc` varchar(1000) NOT NULL,
  `bug_timestamp` varchar(100) NOT NULL,
  `bug_userid` varchar(1000) NOT NULL,
  `bug_revision` varchar(3) NOT NULL,
  `status` varchar(100) NOT NULL,
  PRIMARY KEY (`bug_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=93 ;

--
-- Dumping data for table `tbl_bug`
--

INSERT INTO `tbl_bug` (`bug_id`, `bug_unique_id`, `bug_name`, `bug_desc`, `bug_timestamp`, `bug_userid`, `bug_revision`, `status`) VALUES
(87, 'BUG_72892', 'layout issue', 'layout issue', '2016-02-03 01:15:05', '1', '1', 'D'),
(88, 'BUG_80110', 'wrong component value', 'wrong component value', '2016-02-03 01:15:05', '1', '1', 'D'),
(89, 'BUG_38434', 'wrong footprint', 'wrong footprint', '2016-02-03 01:15:05', '1', '1', 'D'),
(90, 'BUG_53602', 'underrated or over rated parts', 'underrated or over rated parts', '2016-02-03 01:15:05', '1', '1', 'D'),
(91, 'BUG_50842', 'software issues', 'software issues', '2016-02-03 01:15:05', '1', '1', 'D'),
(92, 'BUG_32203', 'layout issuessss', 'layout issuesssss', '2016-02-05 01:46:39', '1', '1', 'D');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bug_count`
--

CREATE TABLE IF NOT EXISTS `tbl_bug_count` (
  `bug_count_id` int(10) NOT NULL AUTO_INCREMENT,
  `bug_id` varchar(100) NOT NULL,
  `bug_count` varchar(100) NOT NULL,
  PRIMARY KEY (`bug_count_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bug_revision`
--

CREATE TABLE IF NOT EXISTS `tbl_bug_revision` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `bug_id` varchar(100) NOT NULL,
  `bug_unique_id` varchar(1000) NOT NULL,
  `bug_name` varchar(1000) NOT NULL,
  `bug_revision` varchar(100) NOT NULL,
  `bug_desc` varchar(1000) NOT NULL,
  `bug_timestamp` varchar(100) NOT NULL,
  `bug_userid` varchar(1000) NOT NULL,
  `status` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE IF NOT EXISTS `tbl_category` (
  `cat_id` int(20) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(100) NOT NULL,
  `cat_description` varchar(100) NOT NULL,
  `cat_timestamp` datetime NOT NULL,
  `cat_status` enum('Active','Disabled') NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`cat_id`, `cat_name`, `cat_description`, `cat_timestamp`, `cat_status`) VALUES
(1, 'Measurment', 'It tests measurment for electronic parts', '2015-09-04 00:00:00', 'Active'),
(2, 'Visual', 'Visulal is used for display type tests', '2015-09-04 00:00:00', 'Active'),
(3, 'Generic', 'Generic cateory is used to test generic type tests', '2015-09-04 00:00:00', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category_links`
--

CREATE TABLE IF NOT EXISTS `tbl_category_links` (
  `category_id` int(100) NOT NULL AUTO_INCREMENT,
  `category_testid` int(100) NOT NULL,
  `category_unitid` int(100) NOT NULL,
  `category_parentid` int(100) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_company`
--

CREATE TABLE IF NOT EXISTS `tbl_company` (
  `company_id` int(100) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(100) NOT NULL,
  `company_email` varchar(100) NOT NULL,
  `company_address` varchar(100) NOT NULL,
  `company_address_2` varchar(100) NOT NULL,
  `company_city` varchar(100) NOT NULL,
  `company_state` varchar(100) NOT NULL,
  `company_pincode` varchar(100) NOT NULL,
  `company_timestamp` varchar(100) DEFAULT NULL,
  `company_status` enum('y','n') NOT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_design`
--

CREATE TABLE IF NOT EXISTS `tbl_design` (
  `design_id` int(100) NOT NULL AUTO_INCREMENT,
  `design_unique_id` varchar(100) NOT NULL,
  `design_name` varchar(100) NOT NULL,
  `design_version` float NOT NULL,
  `design_unit` varchar(100) NOT NULL,
  `design_data` text NOT NULL,
  `design_units` text NOT NULL,
  `design_status` varchar(100) NOT NULL,
  `design_timestamp` datetime NOT NULL,
  `design_userid` varchar(100) NOT NULL,
  PRIMARY KEY (`design_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `tbl_design`
--

INSERT INTO `tbl_design` (`design_id`, `design_unique_id`, `design_name`, `design_version`, `design_unit`, `design_data`, `design_units`, `design_status`, `design_timestamp`, `design_userid`) VALUES
(23, 'design_64724', 'testing design here', 1, 'UNT_66244', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_40978 Revision : 1","unitRevision":1,"unitName":"SPD3","unitId":"UNT_40978","parentid":"","level":0,"state":"open","children":[{"attributes":{"id":1,"rel":"folder"},"data":"BUG_50842 Revision : 1","buggRevision":1,"buggName":"software issues","buggId":"BUG_50842","parentid":0,"level":1,"state":"open","children":[{"attributes":{"id":5,"rel":"folder"},"data":"ECN98039 Revision : 1","ecnRevision":1,"ecnName":"mechanical change of backhaul","ecnId":"ECN98039","parentid":1,"level":2,"state":"open","children":[]},{"attributes":{"id":6,"rel":"folder"},"data":"ECN25475 Revision : 1","ecnRevision":1,"ecnName":"software change in UCC","ecnId":"ECN25475","parentid":1,"level":2,"state":"open","children":[]},{"attributes":{"id":7,"rel":"folder"},"data":"ECN34046 Revision : 1","ecnRevision":1,"ecnName":"software change in ECU","ecnId":"ECN34046","parentid":1,"level":2,"state":"open","children":[]},{"attributes":{"id":8,"rel":"folder"},"data":"ECN72473 Revision : 1","ecnRevision":1,"ecnName":"software change in ADP","ecnId":"ECN72473","parentid":1,"level":2,"state":"open","children":[]}]},{"attributes":{"id":2,"rel":"folder"},"data":"BUG_53602 Revision : 1","buggRevision":1,"buggName":"underrated or over rated parts","buggId":"BUG_53602","parentid":0,"level":1,"state":"open","children":[]},{"attributes":{"id":3,"rel":"folder"},"data":"BUG_38434 Revision : 1","buggRevision":1,"buggName":"wrong footprint","buggId":"BUG_38434","parentid":0,"level":1,"state":"open","children":[]},{"attributes":{"id":4,"rel":"folder"},"data":"BUG_80110 Revision : 1","buggRevision":1,"buggName":"wrong component value","buggId":"BUG_80110","parentid":0,"level":1,"state":"open","children":[]}]}]', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_40978 Revision : 1","unitRevision":1,"unitName":"SPD3","unitId":"UNT_40978","parentid":"","level":0,"state":"open"},{"attributes":{"id":1,"rel":"folder"},"data":"BUG_50842 Revision : 1","buggRevision":1,"buggName":"software issues","buggId":"BUG_50842","parentid":0,"level":1,"state":"open"},{"attributes":{"id":2,"rel":"folder"},"data":"BUG_53602 Revision : 1","buggRevision":1,"buggName":"underrated or over rated parts","buggId":"BUG_53602","parentid":0,"level":1,"state":"open"},{"attributes":{"id":3,"rel":"folder"},"data":"BUG_38434 Revision : 1","buggRevision":1,"buggName":"wrong footprint","buggId":"BUG_38434","parentid":0,"level":1,"state":"open"},{"attributes":{"id":4,"rel":"folder"},"data":"BUG_80110 Revision : 1","buggRevision":1,"buggName":"wrong component value","buggId":"BUG_80110","parentid":0,"level":1,"state":"open"},{"attributes":{"id":5,"rel":"folder"},"data":"ECN98039 Revision : 1","ecnRevision":1,"ecnName":"mechanical change of backhaul","ecnId":"ECN98039","parentid":1,"level":2,"state":"open"},{"attributes":{"id":6,"rel":"folder"},"data":"ECN25475 Revision : 1","ecnRevision":1,"ecnName":"software change in UCC","ecnId":"ECN25475","parentid":1,"level":2,"state":"open"},{"attributes":{"id":7,"rel":"folder"},"data":"ECN34046 Revision : 1","ecnRevision":1,"ecnName":"software change in ECU","ecnId":"ECN34046","parentid":1,"level":2,"state":"open"},{"attributes":{"id":8,"rel":"folder"},"data":"ECN72473 Revision : 1","ecnRevision":1,"ecnName":"software change in ADP","ecnId":"ECN72473","parentid":1,"level":2,"state":"open"}]', 'D', '2016-02-08 06:30:17', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_design_child`
--

CREATE TABLE IF NOT EXISTS `tbl_design_child` (
  `dchild_id` int(100) NOT NULL AUTO_INCREMENT,
  `dchild_child_id` varchar(100) NOT NULL,
  `dchild_child_revision` varchar(100) NOT NULL,
  `dchild_parent_id` varchar(100) NOT NULL,
  `dchild_design_id` varchar(100) NOT NULL,
  `dchild_revision` varchar(100) NOT NULL,
  `dchild_level` varchar(100) NOT NULL,
  `dchild_timestamp` datetime NOT NULL,
  PRIMARY KEY (`dchild_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=55 ;

--
-- Dumping data for table `tbl_design_child`
--

INSERT INTO `tbl_design_child` (`dchild_id`, `dchild_child_id`, `dchild_child_revision`, `dchild_parent_id`, `dchild_design_id`, `dchild_revision`, `dchild_level`, `dchild_timestamp`) VALUES
(46, 'UNT_40978', '1', 'UNT_40978', 'design_64724', '1', '0', '2016-02-08 06:30:17'),
(47, 'BUG_50842', '1', 'UNT_40978', 'design_64724', '1', '1', '2016-02-08 06:30:17'),
(48, 'ECN98039', '1', 'BUG_50842', 'design_64724', '1', '2', '2016-02-08 06:30:17'),
(49, 'ECN25475', '1', 'BUG_50842', 'design_64724', '1', '2', '2016-02-08 06:30:17'),
(50, 'ECN34046', '1', 'BUG_50842', 'design_64724', '1', '2', '2016-02-08 06:30:17'),
(51, 'ECN72473', '1', 'BUG_50842', 'design_64724', '1', '2', '2016-02-08 06:30:17'),
(52, 'BUG_53602', '1', 'UNT_40978', 'design_64724', '1', '1', '2016-02-08 06:30:17'),
(53, 'BUG_38434', '1', 'UNT_40978', 'design_64724', '1', '1', '2016-02-08 06:30:17'),
(54, 'BUG_80110', '1', 'UNT_40978', 'design_64724', '1', '1', '2016-02-08 06:30:17');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_design_revision`
--

CREATE TABLE IF NOT EXISTS `tbl_design_revision` (
  `rev_id` int(100) NOT NULL AUTO_INCREMENT,
  `rev_design_no` varchar(100) NOT NULL,
  `rev_design_id` varchar(100) NOT NULL,
  `rev_design_name` varchar(100) NOT NULL,
  `rev_design_unique` varchar(100) NOT NULL,
  `rev_unique_id` varchar(100) NOT NULL,
  `rev_design_unit` varchar(100) NOT NULL,
  `rev_design_data` text NOT NULL,
  `rev_design_units` text NOT NULL,
  `rev_design_status` varchar(100) NOT NULL,
  `rev_design_timestamp` varchar(100) NOT NULL,
  `rev_design_userid` varchar(100) NOT NULL,
  PRIMARY KEY (`rev_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=56 ;

--
-- Dumping data for table `tbl_design_revision`
--

INSERT INTO `tbl_design_revision` (`rev_id`, `rev_design_no`, `rev_design_id`, `rev_design_name`, `rev_design_unique`, `rev_unique_id`, `rev_design_unit`, `rev_design_data`, `rev_design_units`, `rev_design_status`, `rev_design_timestamp`, `rev_design_userid`) VALUES
(55, '1', '23', 'testing design here', 'design_64724', 'revision_43219', 'UNT_66244', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_40978 Revision : 1","unitRevision":1,"unitName":"SPD3","unitId":"UNT_40978","parentid":"","level":0,"state":"open","children":[{"attributes":{"id":1,"rel":"folder"},"data":"BUG_50842 Revision : 1","buggRevision":1,"buggName":"software issues","buggId":"BUG_50842","parentid":0,"level":1,"state":"open","children":[{"attributes":{"id":5,"rel":"folder"},"data":"ECN98039 Revision : 1","ecnRevision":1,"ecnName":"mechanical change of backhaul","ecnId":"ECN98039","parentid":1,"level":2,"state":"open","children":[]},{"attributes":{"id":6,"rel":"folder"},"data":"ECN25475 Revision : 1","ecnRevision":1,"ecnName":"software change in UCC","ecnId":"ECN25475","parentid":1,"level":2,"state":"open","children":[]},{"attributes":{"id":7,"rel":"folder"},"data":"ECN34046 Revision : 1","ecnRevision":1,"ecnName":"software change in ECU","ecnId":"ECN34046","parentid":1,"level":2,"state":"open","children":[]},{"attributes":{"id":8,"rel":"folder"},"data":"ECN72473 Revision : 1","ecnRevision":1,"ecnName":"software change in ADP","ecnId":"ECN72473","parentid":1,"level":2,"state":"open","children":[]}]},{"attributes":{"id":2,"rel":"folder"},"data":"BUG_53602 Revision : 1","buggRevision":1,"buggName":"underrated or over rated parts","buggId":"BUG_53602","parentid":0,"level":1,"state":"open","children":[]},{"attributes":{"id":3,"rel":"folder"},"data":"BUG_38434 Revision : 1","buggRevision":1,"buggName":"wrong footprint","buggId":"BUG_38434","parentid":0,"level":1,"state":"open","children":[]},{"attributes":{"id":4,"rel":"folder"},"data":"BUG_80110 Revision : 1","buggRevision":1,"buggName":"wrong component value","buggId":"BUG_80110","parentid":0,"level":1,"state":"open","children":[]}]}]', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_40978 Revision : 1","unitRevision":1,"unitName":"SPD3","unitId":"UNT_40978","parentid":"","level":0,"state":"open"},{"attributes":{"id":1,"rel":"folder"},"data":"BUG_50842 Revision : 1","buggRevision":1,"buggName":"software issues","buggId":"BUG_50842","parentid":0,"level":1,"state":"open"},{"attributes":{"id":2,"rel":"folder"},"data":"BUG_53602 Revision : 1","buggRevision":1,"buggName":"underrated or over rated parts","buggId":"BUG_53602","parentid":0,"level":1,"state":"open"},{"attributes":{"id":3,"rel":"folder"},"data":"BUG_38434 Revision : 1","buggRevision":1,"buggName":"wrong footprint","buggId":"BUG_38434","parentid":0,"level":1,"state":"open"},{"attributes":{"id":4,"rel":"folder"},"data":"BUG_80110 Revision : 1","buggRevision":1,"buggName":"wrong component value","buggId":"BUG_80110","parentid":0,"level":1,"state":"open"},{"attributes":{"id":5,"rel":"folder"},"data":"ECN98039 Revision : 1","ecnRevision":1,"ecnName":"mechanical change of backhaul","ecnId":"ECN98039","parentid":1,"level":2,"state":"open"},{"attributes":{"id":6,"rel":"folder"},"data":"ECN25475 Revision : 1","ecnRevision":1,"ecnName":"software change in UCC","ecnId":"ECN25475","parentid":1,"level":2,"state":"open"},{"attributes":{"id":7,"rel":"folder"},"data":"ECN34046 Revision : 1","ecnRevision":1,"ecnName":"software change in ECU","ecnId":"ECN34046","parentid":1,"level":2,"state":"open"},{"attributes":{"id":8,"rel":"folder"},"data":"ECN72473 Revision : 1","ecnRevision":1,"ecnName":"software change in ADP","ecnId":"ECN72473","parentid":1,"level":2,"state":"open"}]', 'D', '2016-02-08 06:30:17', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ecns`
--

CREATE TABLE IF NOT EXISTS `tbl_ecns` (
  `ecn_id` int(100) NOT NULL AUTO_INCREMENT,
  `ecn_uid` varchar(100) NOT NULL,
  `ecn_name` varchar(1000) NOT NULL,
  `ecn_userid` varchar(1000) NOT NULL,
  `ecn_timestamp` varchar(1000) NOT NULL,
  `ecn_desc` varchar(100) NOT NULL,
  `ecn_status` enum('A','D','DELETE') NOT NULL,
  `ecn_rev` varchar(3) NOT NULL,
  PRIMARY KEY (`ecn_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `tbl_ecns`
--

INSERT INTO `tbl_ecns` (`ecn_id`, `ecn_uid`, `ecn_name`, `ecn_userid`, `ecn_timestamp`, `ecn_desc`, `ecn_status`, `ecn_rev`) VALUES
(15, 'ECN13393', 'layout correction in CMU', '1', '2016-02-03 01:16:14', 'layout correction in CMU', 'D', '1'),
(16, 'ECN64717', 'layout correction in ECU', '1', '2016-02-03 01:16:14', 'layout correction in ECU', 'D', '1'),
(17, 'ECN84134', 'layout correction in ADP', '1', '2016-02-03 01:16:14', 'layout correction in ADP', 'D', '1'),
(18, 'ECN67280', 'layout correction in backhaul', '1', '2016-02-03 01:16:14', 'layout correction in backhaul', 'D', '1'),
(19, 'ECN40718', 'layout correction in backplane', '1', '2016-02-03 01:16:14', 'layout correction in backplane', 'D', '1'),
(20, 'ECN60505', 'layout correction in SPD', '1', '2016-02-03 01:16:14', 'layout correction in SPD', 'D', '1'),
(21, 'ECN35843', 'layout correction in Vsat', '1', '2016-02-03 01:16:14', 'layout correction in Vsat', 'D', '1'),
(22, 'ECN72473', 'software change in ADP', '1', '2016-02-03 01:16:14', 'software change in ADP', 'D', '1'),
(23, 'ECN34046', 'software change in ECU', '1', '2016-02-03 01:16:14', 'software change in ECU', 'D', '1'),
(24, 'ECN25475', 'software change in UCC', '1', '2016-02-03 01:16:14', 'software change in UCC', 'D', '1'),
(25, 'ECN98039', 'mechanical change of backhaul', '1', '2016-02-03 01:16:14', 'mechanical change of backhaul', 'D', '1'),
(26, 'ECN81943', 'layout correction in CMUuuuuuuu', '1', '2016-02-05 02:18:41', 'layout correction in CMUiiiiii', 'D', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ecns_revision`
--

CREATE TABLE IF NOT EXISTS `tbl_ecns_revision` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `ecn_id` varchar(100) NOT NULL,
  `ecn_uid` varchar(100) NOT NULL,
  `ecn_name` varchar(1000) NOT NULL,
  `ecn_userid` varchar(1000) NOT NULL,
  `ecn_timestamp` varchar(1000) NOT NULL,
  `ecn_desc` varchar(100) NOT NULL,
  `ecn_status` enum('A','D','DELETE') NOT NULL,
  `ecn_rev` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_fault`
--

CREATE TABLE IF NOT EXISTS `tbl_fault` (
  `fault_id` int(20) NOT NULL AUTO_INCREMENT,
  `fault_unique_id` varchar(20) NOT NULL,
  `fault_name` varchar(1000) NOT NULL,
  `fault_description` varchar(1000) NOT NULL,
  `fault_userid` varchar(1000) NOT NULL,
  `fault_status` varchar(100) NOT NULL,
  `fault_revision` varchar(1000) NOT NULL,
  `fault_timestamp` varchar(1000) NOT NULL,
  PRIMARY KEY (`fault_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=72 ;

--
-- Dumping data for table `tbl_fault`
--

INSERT INTO `tbl_fault` (`fault_id`, `fault_unique_id`, `fault_name`, `fault_description`, `fault_userid`, `fault_status`, `fault_revision`, `fault_timestamp`) VALUES
(48, 'FLT_45039', 'polarity reversed', 'polarity reversed', '1', 'D', '1', '2016-02-03 01:13:26'),
(49, 'FLT_49711', 'Dry solder', 'Dry solder', '1', 'D', '1', '2016-02-03 01:13:26'),
(50, 'FLT_96452', 'track shorting', 'track shorting', '1', 'D', '1', '2016-02-03 01:13:26'),
(51, 'FLT_89776', 'impedence low', 'impedence low', '1', 'D', '1', '2016-02-03 01:13:26'),
(52, 'FLT_22802', 'powered up but housekeeping not up', 'powered up but housekeeping not up', '1', 'D', '1', '2016-02-03 01:13:26'),
(53, 'FLT_72471', 'led status not ok', 'led status not ok', '1', 'D', '1', '2016-02-03 01:13:26'),
(54, 'FLT_14145', 'buzzer not working', 'buzzer not working', '1', 'D', '1', '2016-02-03 01:13:26'),
(55, 'FLT_64952', 'ADC count not working', 'ADC count not working', '1', 'D', '1', '2016-02-03 01:13:26'),
(56, 'FLT_49404', 'RS485 comm. Fail', 'RS485 comm. Fail', '1', 'D', '1', '2016-02-03 01:13:26'),
(57, 'FLT_50895', 'Calibration fail', 'Calibration fail', '1', 'D', '1', '2016-02-03 01:13:26'),
(58, 'FLT_99505', 'output voltage zero ', 'output voltage zero ', '1', 'D', '1', '2016-02-03 01:13:26'),
(59, 'FLT_76388', 'fuse blown', 'fuse blown', '1', 'D', '1', '2016-02-03 01:13:26'),
(60, 'FLT_67681', 'reverse protection failure', 'reverse protection failure', '1', 'D', '1', '2016-02-03 01:13:26'),
(61, 'FLT_18653', 'Diode blown', 'Diode blown', '1', 'D', '1', '2016-02-03 01:13:26'),
(62, 'FLT_63580', 'AC input voltage low', 'AC input voltage low', '1', 'D', '1', '2016-02-03 01:13:26'),
(63, 'FLT_56272', 'AC input voltage high', 'AC input voltage high', '1', 'D', '1', '2016-02-03 01:13:26'),
(64, 'FLT_79466', 'FAN fault', 'FAN fault', '1', 'D', '1', '2016-02-03 01:13:26'),
(65, 'FLT_80710', 'Bridge shorted', 'Bridge shorted', '1', 'D', '1', '2016-02-03 01:13:26'),
(66, 'FLT_70732', 'PFC mosfet blown', 'PFC mosfet blown', '1', 'D', '1', '2016-02-03 01:13:26'),
(67, 'FLT_74959', 'Converter mosfet blown', 'Converter mosfet blown', '1', 'D', '1', '2016-02-03 01:13:26'),
(68, 'FLT_22226', 'SPI communication fail between MSP & AC board', 'SPI communication fail between MSP & AC board', '1', 'D', '1', '2016-02-03 01:13:26'),
(69, 'FLT_44403', 'watchdog failure', 'watchdog failure', '1', 'D', '1', '2016-02-03 01:13:26'),
(70, 'FLT_64997', 'reset not working', 'reset not working', '1', 'D', '1', '2016-02-03 01:13:26'),
(71, 'FLT_67238', 'polarity reversedggggggggggg', 'polarity reversed', '1', 'D', '1', '2016-02-05 12:55:21');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_fault_revision`
--

CREATE TABLE IF NOT EXISTS `tbl_fault_revision` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `fault_id` int(20) NOT NULL,
  `fault_unique_id` varchar(20) NOT NULL,
  `fault_name` varchar(1000) NOT NULL,
  `fault_description` varchar(1000) NOT NULL,
  `fault_userid` varchar(1000) NOT NULL,
  `fault_status` varchar(100) NOT NULL,
  `fault_revision` varchar(1000) NOT NULL,
  `fault_timestamp` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_format`
--

CREATE TABLE IF NOT EXISTS `tbl_format` (
  `format_id` int(50) NOT NULL AUTO_INCREMENT,
  `format_name` varchar(100) NOT NULL,
  `format_revision` varchar(3) NOT NULL,
  `format_unique_id` varchar(150) NOT NULL,
  `format_unit` varchar(150) NOT NULL,
  `format_jigid` varchar(100) NOT NULL,
  `format_data` text NOT NULL,
  `format_units` text NOT NULL,
  `format_status` varchar(100) NOT NULL,
  `format_timestamp` datetime NOT NULL,
  `format_userid` int(50) NOT NULL,
  PRIMARY KEY (`format_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tbl_format`
--

INSERT INTO `tbl_format` (`format_id`, `format_name`, `format_revision`, `format_unique_id`, `format_unit`, `format_jigid`, `format_data`, `format_units`, `format_status`, `format_timestamp`, `format_userid`) VALUES
(3, 'testing format here', '1', 'format_46570', 'UNT_30644', '910-326-R4-zxcvb', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_30644 Revision : 1","unitRevision":1,"unitName":"hello","unitId":"UNT_30644","parentid":"","level":0,"state":"open","children":[{"attributes":{"id":1,"rel":"folder"},"data":"TST_47326 Revision : 1","testRevision":1,"testName":"hello","testId":"TST_47326","parentid":0,"level":1,"tag_id":11,"maxtag_id":111,"mintag_id":112,"resulttag_id":113,"state":"open","children":[{"attributes":{"id":5,"rel":"folder"},"data":"FLT_67238 Revision : 1","faultRevision":1,"faultName":"faulty","faultId":"FLT_67238","parentid":1,"level":2,"state":"open","children":[{"attributes":{"id":9,"rel":"folder"},"data":"SLN11824 Revision : 1","solRevision":1,"solName":"hhhhh","solId":"SLN11824","parentid":5,"level":3,"state":"open","children":[]},{"attributes":{"id":10,"rel":"folder"},"data":"SLN58968 Revision : 1","solRevision":1,"solName":"hhhhh","solId":"SLN58968","parentid":5,"level":3,"state":"open","children":[]},{"attributes":{"id":11,"rel":"folder"},"data":"SLN34948 Revision : 1","solRevision":1,"solName":"hhhhh","solId":"SLN34948","parentid":5,"level":3,"state":"open","children":[]},{"attributes":{"id":12,"rel":"folder"},"data":"SLN20423 Revision : 1","solRevision":1,"solName":"hhhhh","solId":"SLN20423","parentid":5,"level":3,"state":"open","children":[]}]},{"attributes":{"id":6,"rel":"folder"},"data":"FLT_64997 Revision : 1","faultRevision":1,"faultName":"faulty","faultId":"FLT_64997","parentid":1,"level":2,"state":"open","children":[]},{"attributes":{"id":7,"rel":"folder"},"data":"FLT_44403 Revision : 1","faultRevision":1,"faultName":"faulty","faultId":"FLT_44403","parentid":1,"level":2,"state":"open","children":[]},{"attributes":{"id":8,"rel":"folder"},"data":"FLT_22226 Revision : 1","faultRevision":1,"faultName":"faulty","faultId":"FLT_22226","parentid":1,"level":2,"state":"open","children":[]}]},{"attributes":{"id":2,"rel":"folder"},"data":"TST_73529 Revision : 1","testRevision":1,"testName":"hello","testId":"TST_73529","parentid":0,"level":1,"tag_id":12,"maxtag_id":116,"mintag_id":117,"resulttag_id":118,"state":"open","children":[]},{"attributes":{"id":3,"rel":"folder"},"data":"TST_18977 Revision : 1","testRevision":1,"testName":"hello","testId":"TST_18977","parentid":0,"level":1,"tag_id":21,"maxtag_id":211,"mintag_id":212,"resulttag_id":213,"state":"open","children":[]},{"attributes":{"id":4,"rel":"folder"},"data":"TST_80750 Revision : 1","testRevision":1,"testName":"hello","testId":"TST_80750","parentid":0,"level":1,"tag_id":32,"maxtag_id":321,"mintag_id":322,"resulttag_id":323,"state":"open","children":[]}]}]', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_30644 Revision : 1","unitRevision":1,"unitName":"hello","unitId":"UNT_30644","parentid":"","level":0,"state":"open"},{"attributes":{"id":1,"rel":"folder"},"data":"TST_47326 Revision : 1","testRevision":1,"testName":"hello","testId":"TST_47326","parentid":0,"level":1,"tag_id":11,"maxtag_id":111,"mintag_id":112,"resulttag_id":113,"state":"open"},{"attributes":{"id":2,"rel":"folder"},"data":"TST_73529 Revision : 1","testRevision":1,"testName":"hello","testId":"TST_73529","parentid":0,"level":1,"tag_id":12,"maxtag_id":116,"mintag_id":117,"resulttag_id":118,"state":"open"},{"attributes":{"id":3,"rel":"folder"},"data":"TST_18977 Revision : 1","testRevision":1,"testName":"hello","testId":"TST_18977","parentid":0,"level":1,"tag_id":21,"maxtag_id":211,"mintag_id":212,"resulttag_id":213,"state":"open"},{"attributes":{"id":4,"rel":"folder"},"data":"TST_80750 Revision : 1","testRevision":1,"testName":"hello","testId":"TST_80750","parentid":0,"level":1,"tag_id":32,"maxtag_id":321,"mintag_id":322,"resulttag_id":323,"state":"open"},{"attributes":{"id":5,"rel":"folder"},"data":"FLT_67238 Revision : 1","faultRevision":1,"faultName":"faulty","faultId":"FLT_67238","parentid":1,"level":2,"state":"open"},{"attributes":{"id":6,"rel":"folder"},"data":"FLT_64997 Revision : 1","faultRevision":1,"faultName":"faulty","faultId":"FLT_64997","parentid":1,"level":2,"state":"open"},{"attributes":{"id":7,"rel":"folder"},"data":"FLT_44403 Revision : 1","faultRevision":1,"faultName":"faulty","faultId":"FLT_44403","parentid":1,"level":2,"state":"open"},{"attributes":{"id":8,"rel":"folder"},"data":"FLT_22226 Revision : 1","faultRevision":1,"faultName":"faulty","faultId":"FLT_22226","parentid":1,"level":2,"state":"open"},{"attributes":{"id":9,"rel":"folder"},"data":"SLN11824 Revision : 1","solRevision":1,"solName":"hhhhh","solId":"SLN11824","parentid":5,"level":3,"state":"open"},{"attributes":{"id":10,"rel":"folder"},"data":"SLN58968 Revision : 1","solRevision":1,"solName":"hhhhh","solId":"SLN58968","parentid":5,"level":3,"state":"open"},{"attributes":{"id":11,"rel":"folder"},"data":"SLN34948 Revision : 1","solRevision":1,"solName":"hhhhh","solId":"SLN34948","parentid":5,"level":3,"state":"open"},{"attributes":{"id":12,"rel":"folder"},"data":"SLN20423 Revision : 1","solRevision":1,"solName":"hhhhh","solId":"SLN20423","parentid":5,"level":3,"state":"open"}]', 'DELETE', '2016-02-08 06:25:05', 1),
(4, 'testing format here', '1', 'format_59624', 'UNT_30644', '910-326-R4-zxcvb', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_30644 Revision : 1","unitRevision":1,"unitName":"hello","unitId":"UNT_30644","parentid":"","level":0,"state":"open","children":[{"attributes":{"id":1,"rel":"folder"},"data":"TST_47326 Revision : 1","testRevision":1,"testName":"hello","testId":"TST_47326","parentid":0,"level":1,"tag_id":11,"maxtag_id":111,"mintag_id":112,"resulttag_id":113,"state":"open","children":[{"attributes":{"id":5,"rel":"folder"},"data":"FLT_67238 Revision : 1","faultRevision":1,"faultName":"faulty","faultId":"FLT_67238","parentid":1,"level":2,"state":"open","children":[{"attributes":{"id":9,"rel":"folder"},"data":"SLN11824 Revision : 1","solRevision":1,"solName":"hhhhh","solId":"SLN11824","parentid":5,"level":3,"state":"open","children":[]},{"attributes":{"id":10,"rel":"folder"},"data":"SLN58968 Revision : 1","solRevision":1,"solName":"hhhhh","solId":"SLN58968","parentid":5,"level":3,"state":"open","children":[]},{"attributes":{"id":11,"rel":"folder"},"data":"SLN34948 Revision : 1","solRevision":1,"solName":"hhhhh","solId":"SLN34948","parentid":5,"level":3,"state":"open","children":[]},{"attributes":{"id":12,"rel":"folder"},"data":"SLN20423 Revision : 1","solRevision":1,"solName":"hhhhh","solId":"SLN20423","parentid":5,"level":3,"state":"open","children":[]}]},{"attributes":{"id":6,"rel":"folder"},"data":"FLT_64997 Revision : 1","faultRevision":1,"faultName":"faulty","faultId":"FLT_64997","parentid":1,"level":2,"state":"open","children":[]},{"attributes":{"id":7,"rel":"folder"},"data":"FLT_44403 Revision : 1","faultRevision":1,"faultName":"faulty","faultId":"FLT_44403","parentid":1,"level":2,"state":"open","children":[]},{"attributes":{"id":8,"rel":"folder"},"data":"FLT_22226 Revision : 1","faultRevision":1,"faultName":"faulty","faultId":"FLT_22226","parentid":1,"level":2,"state":"open","children":[]}]},{"attributes":{"id":2,"rel":"folder"},"data":"TST_73529 Revision : 1","testRevision":1,"testName":"hello","testId":"TST_73529","parentid":0,"level":1,"tag_id":12,"maxtag_id":116,"mintag_id":117,"resulttag_id":118,"state":"open","children":[]},{"attributes":{"id":3,"rel":"folder"},"data":"TST_18977 Revision : 1","testRevision":1,"testName":"hello","testId":"TST_18977","parentid":0,"level":1,"tag_id":21,"maxtag_id":211,"mintag_id":212,"resulttag_id":213,"state":"open","children":[]},{"attributes":{"id":4,"rel":"folder"},"data":"TST_80750 Revision : 1","testRevision":1,"testName":"hello","testId":"TST_80750","parentid":0,"level":1,"tag_id":32,"maxtag_id":321,"mintag_id":322,"resulttag_id":323,"state":"open","children":[]}]}]', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_30644 Revision : 1","unitRevision":1,"unitName":"hello","unitId":"UNT_30644","parentid":"","level":0,"state":"open"},{"attributes":{"id":1,"rel":"folder"},"data":"TST_47326 Revision : 1","testRevision":1,"testName":"hello","testId":"TST_47326","parentid":0,"level":1,"tag_id":11,"maxtag_id":111,"mintag_id":112,"resulttag_id":113,"state":"open"},{"attributes":{"id":2,"rel":"folder"},"data":"TST_73529 Revision : 1","testRevision":1,"testName":"hello","testId":"TST_73529","parentid":0,"level":1,"tag_id":12,"maxtag_id":116,"mintag_id":117,"resulttag_id":118,"state":"open"},{"attributes":{"id":3,"rel":"folder"},"data":"TST_18977 Revision : 1","testRevision":1,"testName":"hello","testId":"TST_18977","parentid":0,"level":1,"tag_id":21,"maxtag_id":211,"mintag_id":212,"resulttag_id":213,"state":"open"},{"attributes":{"id":4,"rel":"folder"},"data":"TST_80750 Revision : 1","testRevision":1,"testName":"hello","testId":"TST_80750","parentid":0,"level":1,"tag_id":32,"maxtag_id":321,"mintag_id":322,"resulttag_id":323,"state":"open"},{"attributes":{"id":5,"rel":"folder"},"data":"FLT_67238 Revision : 1","faultRevision":1,"faultName":"faulty","faultId":"FLT_67238","parentid":1,"level":2,"state":"open"},{"attributes":{"id":6,"rel":"folder"},"data":"FLT_64997 Revision : 1","faultRevision":1,"faultName":"faulty","faultId":"FLT_64997","parentid":1,"level":2,"state":"open"},{"attributes":{"id":7,"rel":"folder"},"data":"FLT_44403 Revision : 1","faultRevision":1,"faultName":"faulty","faultId":"FLT_44403","parentid":1,"level":2,"state":"open"},{"attributes":{"id":8,"rel":"folder"},"data":"FLT_22226 Revision : 1","faultRevision":1,"faultName":"faulty","faultId":"FLT_22226","parentid":1,"level":2,"state":"open"},{"attributes":{"id":9,"rel":"folder"},"data":"SLN11824 Revision : 1","solRevision":1,"solName":"hhhhh","solId":"SLN11824","parentid":5,"level":3,"state":"open"},{"attributes":{"id":10,"rel":"folder"},"data":"SLN58968 Revision : 1","solRevision":1,"solName":"hhhhh","solId":"SLN58968","parentid":5,"level":3,"state":"open"},{"attributes":{"id":11,"rel":"folder"},"data":"SLN34948 Revision : 1","solRevision":1,"solName":"hhhhh","solId":"SLN34948","parentid":5,"level":3,"state":"open"},{"attributes":{"id":12,"rel":"folder"},"data":"SLN20423 Revision : 1","solRevision":1,"solName":"hhhhh","solId":"SLN20423","parentid":5,"level":3,"state":"open"}]', 'D', '2016-02-08 06:52:44', 1),
(5, 'format_testing1', '1', 'format_84294', 'UNT_75237', '12345', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_75237 Revision : 0","unitRevision":"0","unitName":"CMU","unitId":"UNT_75237","state":"open","parentid":0,"level":"0","children":[{"attributes":{"id":1,"rel":"folder"},"data":"TST_47326 Revision : 1","tag_id":"1100","maxtag_id":"1200","mintag_id":"1300","resulttag_id":"1400","testName":"Visual Inspecyyyy","testId":"TST_47326","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":2,"rel":"folder"},"data":"FLT_50895 Revision : 1","faultName":"Calibration fail","faultId":"FLT_50895","faultRevision":"1","state":"open","parentid":"1","level":"2","children":[{"attributes":{"id":3,"rel":"folder"},"data":"SLN76370 Revision : 1","solId":"SLN76370","solRevision":"1","state":"open","parentid":"2","level":"3","children":[]}]}]}]}]', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_75237 Revision : 0","unitRevision":"0","unitName":"CMU","unitId":"UNT_75237","state":"open","parentid":0,"level":"0","children":[{"attributes":{"id":1,"rel":"folder"},"data":"TST_47326 Revision : 1","tag_id":"1100","maxtag_id":"1200","mintag_id":"1300","resulttag_id":"1400","testName":"Visual Inspecyyyy","testId":"TST_47326","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":2,"rel":"folder"},"data":"FLT_50895 Revision : 1","faultName":"Calibration fail","faultId":"FLT_50895","faultRevision":"1","state":"open","parentid":"1","level":"2","children":[{"attributes":{"id":3,"rel":"folder"},"data":"SLN76370 Revision : 1","solId":"SLN76370","solRevision":"1","state":"open","parentid":"2","level":"3","children":[]}]}]}]},{"attributes":{"id":1,"rel":"folder"},"data":"TST_47326 Revision : 1","tag_id":"1100","maxtag_id":"1200","mintag_id":"1300","resulttag_id":"1400","testName":"Visual Inspecyyyy","testId":"TST_47326","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":2,"rel":"folder"},"data":"FLT_50895 Revision : 1","faultName":"Calibration fail","faultId":"FLT_50895","faultRevision":"1","state":"open","parentid":"1","level":"2","children":[{"attributes":{"id":3,"rel":"folder"},"data":"SLN76370 Revision : 1","solId":"SLN76370","solRevision":"1","state":"open","parentid":"2","level":"3","children":[]}]}]},{"attributes":{"id":2,"rel":"folder"},"data":"FLT_50895 Revision : 1","faultName":"Calibration fail","faultId":"FLT_50895","faultRevision":"1","state":"open","parentid":"1","level":"2","children":[{"attributes":{"id":3,"rel":"folder"},"data":"SLN76370 Revision : 1","solId":"SLN76370","solRevision":"1","state":"open","parentid":"2","level":"3","children":[]}]},{"attributes":{"id":3,"rel":"folder"},"data":"SLN76370 Revision : 1","solId":"SLN76370","solRevision":"1","state":"open","parentid":"2","level":"3","children":[]}]', 'D', '2016-02-10 05:29:50', 1),
(6, 'Pts_app_format', '1.1', 'format_25555', 'UNT_63374', 'PTS-910-326 R4', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_63374 Revision : 1","unitRevision":"1","unitName":"Pts_app_unit","unitId":"UNT_63374","state":"open","parentid":0,"level":"0","children":[{"attributes":{"id":1,"rel":"folder"},"data":"TST_69490 Revision : 1","tag_id":"101","maxtag_id":"201","mintag_id":"301","resulttag_id":"401","testName":"Pts_1","testId":"TST_69490","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":2,"rel":"folder"},"data":"FLT_49404 Revision : 1","faultName":"RS485 comm. Fail","faultId":"FLT_49404","faultRevision":"1","state":"open","parentid":"1","level":"2","children":[{"attributes":{"id":3,"rel":"folder"},"data":"SLN76370 Revision : 1","solId":"SLN76370","solRevision":"1","state":"open","parentid":"2","level":"3","children":[]}]}]},{"attributes":{"id":4,"rel":"folder"},"tag_id":"102","maxtag_id":"202","mintag_id":"302","resulttag_id":"402","data":"TST_35320 Revision : 1","testName":"Pts_2","testId":"TST_35320","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":5,"rel":"folder"},"data":"FLT_45039 Revision : 1","faultName":"polarity reversed","faultId":"FLT_45039","state":"open","parentid":"4","level":"2","faultRevision":"1","children":[{"attributes":{"id":6,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"5","level":"3","children":[]}]}]},{"attributes":{"id":7,"rel":"folder"},"tag_id":"103","maxtag_id":"203","mintag_id":"303","resulttag_id":"403","data":"TST_73812 Revision : 1","testName":"Pts_3","testId":"TST_73812","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":8,"rel":"folder"},"data":"FLT_49404 Revision : 1","faultName":"RS485 comm. Fail","faultId":"FLT_49404","state":"open","parentid":"7","level":"2","faultRevision":"1","children":[{"attributes":{"id":9,"rel":"folder"},"data":"SLN76370 Revision : 1","solName":"correct soldering","solId":"SLN76370","solRevision":"1","state":"open","parentid":"8","level":"3","children":[]}]}]},{"attributes":{"id":10,"rel":"folder"},"tag_id":"104","maxtag_id":"204","mintag_id":"304","resulttag_id":"404","data":"TST_84908 Revision : 1","testName":"Pts_4","testId":"TST_84908","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":11,"rel":"folder"},"data":"FLT_49404 Revision : 1","faultName":"RS485 comm. Fail","faultId":"FLT_49404","state":"open","parentid":"10","level":"2","faultRevision":"1","children":[{"attributes":{"id":12,"rel":"folder"},"data":"SLN76370 Revision : 1","solName":"correct soldering","solId":"SLN76370","solRevision":"1","state":"open","parentid":"11","level":"3","children":[]}]}]},{"attributes":{"id":13,"rel":"folder"},"tag_id":"105","maxtag_id":"205","mintag_id":"305","resulttag_id":"405","data":"TST_20423 Revision : 1","testName":"Pts_5","testId":"TST_20423","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":14,"rel":"folder"},"data":"FLT_50895 Revision : 1","faultName":"Calibration fail","faultId":"FLT_50895","state":"open","parentid":"13","level":"2","faultRevision":"1","children":[{"attributes":{"id":15,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"14","level":"3","children":[]}]}]},{"attributes":{"id":16,"rel":"folder"},"tag_id":"106","maxtag_id":"206","mintag_id":"306","resulttag_id":"406","data":"TST_42014 Revision : 1","testName":"Pts_6","testId":"TST_42014","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":17,"rel":"folder"},"data":"FLT_49404 Revision : 1","faultName":"RS485 comm. Fail","faultId":"FLT_49404","state":"open","parentid":"16","level":"2","faultRevision":"1","children":[{"attributes":{"id":18,"rel":"folder"},"data":"SLN33270 Revision : 1","solName":"check shorting & load connection","solId":"SLN33270","solRevision":"1","state":"open","parentid":"17","level":"3","children":[]}]}]},{"attributes":{"id":19,"rel":"folder"},"tag_id":"107","maxtag_id":"207","mintag_id":"307","resulttag_id":"407","data":"TST_78837 Revision : 1","testName":"Pts_7","testId":"TST_78837","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":20,"rel":"folder"},"data":"FLT_67681 Revision : 1","faultName":"reverse protection failure","faultId":"FLT_67681","state":"open","parentid":"19","level":"2","faultRevision":"1","children":[{"attributes":{"id":21,"rel":"folder"},"data":"SLN76370 Revision : 1","solName":"correct soldering","solId":"SLN76370","solRevision":"1","state":"open","parentid":"20","level":"3","children":[]}]}]},{"attributes":{"id":22,"rel":"folder"},"tag_id":"108","maxtag_id":"208","mintag_id":"308","resulttag_id":"408","data":"TST_18043 Revision : 1","testName":"Pts_8","testId":"TST_18043","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":23,"rel":"folder"},"data":"FLT_50895 Revision : 1","faultName":"Calibration fail","faultId":"FLT_50895","state":"open","parentid":"22","level":"2","faultRevision":"1","children":[{"attributes":{"id":24,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"23","level":"3","children":[]}]}]},{"attributes":{"id":25,"rel":"folder"},"tag_id":"109","maxtag_id":"209","mintag_id":"309","resulttag_id":"409","data":"TST_49743 Revision : 1","testName":"Pts_9","testId":"TST_49743","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":26,"rel":"folder"},"data":"FLT_49711 Revision : 1","faultName":"Dry solder","faultId":"FLT_49711","state":"open","parentid":"25","level":"2","faultRevision":"1","children":[{"attributes":{"id":27,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"26","level":"3","children":[]}]}]},{"attributes":{"id":28,"rel":"folder"},"tag_id":"110","maxtag_id":"210","mintag_id":"310","resulttag_id":"410","data":"TST_33502 Revision : 1","testName":"Pts_10","testId":"TST_33502","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":29,"rel":"folder"},"data":"FLT_45039 Revision : 1","faultName":"polarity reversed","faultId":"FLT_45039","state":"open","parentid":"28","level":"2","faultRevision":"1","children":[{"attributes":{"id":30,"rel":"folder"},"data":"SLN68563 Revision : 1","solName":"remove track shorting","solId":"SLN68563","solRevision":"1","state":"open","parentid":"29","level":"3","children":[]}]}]}]}]', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_63374 Revision : 1","unitRevision":"1","unitName":"Pts_app_unit","unitId":"UNT_63374","state":"open","parentid":0,"level":"0","children":[{"attributes":{"id":1,"rel":"folder"},"data":"TST_69490 Revision : 1","tag_id":"101","maxtag_id":"201","mintag_id":"301","resulttag_id":"401","testName":"Pts_1","testId":"TST_69490","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":2,"rel":"folder"},"data":"FLT_49404 Revision : 1","faultName":"RS485 comm. Fail","faultId":"FLT_49404","faultRevision":"1","state":"open","parentid":"1","level":"2","children":[{"attributes":{"id":3,"rel":"folder"},"data":"SLN76370 Revision : 1","solId":"SLN76370","solRevision":"1","state":"open","parentid":"2","level":"3","children":[]}]}]},{"attributes":{"id":4,"rel":"folder"},"tag_id":"102","maxtag_id":"202","mintag_id":"302","resulttag_id":"402","data":"TST_35320 Revision : 1","testName":"Pts_2","testId":"TST_35320","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":5,"rel":"folder"},"data":"FLT_45039 Revision : 1","faultName":"polarity reversed","faultId":"FLT_45039","state":"open","parentid":"4","level":"2","faultRevision":"1","children":[{"attributes":{"id":6,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"5","level":"3","children":[]}]}]},{"attributes":{"id":7,"rel":"folder"},"tag_id":"103","maxtag_id":"203","mintag_id":"303","resulttag_id":"403","data":"TST_73812 Revision : 1","testName":"Pts_3","testId":"TST_73812","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":8,"rel":"folder"},"data":"FLT_49404 Revision : 1","faultName":"RS485 comm. Fail","faultId":"FLT_49404","state":"open","parentid":"7","level":"2","faultRevision":"1","children":[{"attributes":{"id":9,"rel":"folder"},"data":"SLN76370 Revision : 1","solName":"correct soldering","solId":"SLN76370","solRevision":"1","state":"open","parentid":"8","level":"3","children":[]}]}]},{"attributes":{"id":10,"rel":"folder"},"tag_id":"104","maxtag_id":"204","mintag_id":"304","resulttag_id":"404","data":"TST_84908 Revision : 1","testName":"Pts_4","testId":"TST_84908","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":11,"rel":"folder"},"data":"FLT_49404 Revision : 1","faultName":"RS485 comm. Fail","faultId":"FLT_49404","state":"open","parentid":"10","level":"2","faultRevision":"1","children":[{"attributes":{"id":12,"rel":"folder"},"data":"SLN76370 Revision : 1","solName":"correct soldering","solId":"SLN76370","solRevision":"1","state":"open","parentid":"11","level":"3","children":[]}]}]},{"attributes":{"id":13,"rel":"folder"},"tag_id":"105","maxtag_id":"205","mintag_id":"305","resulttag_id":"405","data":"TST_20423 Revision : 1","testName":"Pts_5","testId":"TST_20423","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":14,"rel":"folder"},"data":"FLT_50895 Revision : 1","faultName":"Calibration fail","faultId":"FLT_50895","state":"open","parentid":"13","level":"2","faultRevision":"1","children":[{"attributes":{"id":15,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"14","level":"3","children":[]}]}]},{"attributes":{"id":16,"rel":"folder"},"tag_id":"106","maxtag_id":"206","mintag_id":"306","resulttag_id":"406","data":"TST_42014 Revision : 1","testName":"Pts_6","testId":"TST_42014","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":17,"rel":"folder"},"data":"FLT_49404 Revision : 1","faultName":"RS485 comm. Fail","faultId":"FLT_49404","state":"open","parentid":"16","level":"2","faultRevision":"1","children":[{"attributes":{"id":18,"rel":"folder"},"data":"SLN33270 Revision : 1","solName":"check shorting & load connection","solId":"SLN33270","solRevision":"1","state":"open","parentid":"17","level":"3","children":[]}]}]},{"attributes":{"id":19,"rel":"folder"},"tag_id":"107","maxtag_id":"207","mintag_id":"307","resulttag_id":"407","data":"TST_78837 Revision : 1","testName":"Pts_7","testId":"TST_78837","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":20,"rel":"folder"},"data":"FLT_67681 Revision : 1","faultName":"reverse protection failure","faultId":"FLT_67681","state":"open","parentid":"19","level":"2","faultRevision":"1","children":[{"attributes":{"id":21,"rel":"folder"},"data":"SLN76370 Revision : 1","solName":"correct soldering","solId":"SLN76370","solRevision":"1","state":"open","parentid":"20","level":"3","children":[]}]}]},{"attributes":{"id":22,"rel":"folder"},"tag_id":"108","maxtag_id":"208","mintag_id":"308","resulttag_id":"408","data":"TST_18043 Revision : 1","testName":"Pts_8","testId":"TST_18043","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":23,"rel":"folder"},"data":"FLT_50895 Revision : 1","faultName":"Calibration fail","faultId":"FLT_50895","state":"open","parentid":"22","level":"2","faultRevision":"1","children":[{"attributes":{"id":24,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"23","level":"3","children":[]}]}]},{"attributes":{"id":25,"rel":"folder"},"tag_id":"109","maxtag_id":"209","mintag_id":"309","resulttag_id":"409","data":"TST_49743 Revision : 1","testName":"Pts_9","testId":"TST_49743","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":26,"rel":"folder"},"data":"FLT_49711 Revision : 1","faultName":"Dry solder","faultId":"FLT_49711","state":"open","parentid":"25","level":"2","faultRevision":"1","children":[{"attributes":{"id":27,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"26","level":"3","children":[]}]}]},{"attributes":{"id":28,"rel":"folder"},"tag_id":"110","maxtag_id":"210","mintag_id":"310","resulttag_id":"410","data":"TST_33502 Revision : 1","testName":"Pts_10","testId":"TST_33502","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":29,"rel":"folder"},"data":"FLT_45039 Revision : 1","faultName":"polarity reversed","faultId":"FLT_45039","state":"open","parentid":"28","level":"2","faultRevision":"1","children":[{"attributes":{"id":30,"rel":"folder"},"data":"SLN68563 Revision : 1","solName":"remove track shorting","solId":"SLN68563","solRevision":"1","state":"open","parentid":"29","level":"3","children":[]}]}]}]},{"attributes":{"id":1,"rel":"folder"},"data":"TST_69490 Revision : 1","tag_id":"101","maxtag_id":"201","mintag_id":"301","resulttag_id":"401","testName":"Pts_1","testId":"TST_69490","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":2,"rel":"folder"},"data":"FLT_49404 Revision : 1","faultName":"RS485 comm. Fail","faultId":"FLT_49404","faultRevision":"1","state":"open","parentid":"1","level":"2","children":[{"attributes":{"id":3,"rel":"folder"},"data":"SLN76370 Revision : 1","solId":"SLN76370","solRevision":"1","state":"open","parentid":"2","level":"3","children":[]}]}]},{"attributes":{"id":2,"rel":"folder"},"data":"FLT_49404 Revision : 1","faultName":"RS485 comm. Fail","faultId":"FLT_49404","faultRevision":"1","state":"open","parentid":"1","level":"2","children":[{"attributes":{"id":3,"rel":"folder"},"data":"SLN76370 Revision : 1","solId":"SLN76370","solRevision":"1","state":"open","parentid":"2","level":"3","children":[]}]},{"attributes":{"id":3,"rel":"folder"},"data":"SLN76370 Revision : 1","solId":"SLN76370","solRevision":"1","state":"open","parentid":"2","level":"3","children":[]},{"attributes":{"id":4,"rel":"folder"},"tag_id":"102","maxtag_id":"202","mintag_id":"302","resulttag_id":"402","data":"TST_35320 Revision : 1","testName":"Pts_2","testId":"TST_35320","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":5,"rel":"folder"},"data":"FLT_45039 Revision : 1","faultName":"polarity reversed","faultId":"FLT_45039","state":"open","parentid":"4","level":"2","faultRevision":"1","children":[{"attributes":{"id":6,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"5","level":"3","children":[]}]}]},{"attributes":{"id":5,"rel":"folder"},"data":"FLT_45039 Revision : 1","faultName":"polarity reversed","faultId":"FLT_45039","state":"open","parentid":"4","level":"2","faultRevision":"1","children":[{"attributes":{"id":6,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"5","level":"3","children":[]}]},{"attributes":{"id":6,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"5","level":"3","children":[]},{"attributes":{"id":7,"rel":"folder"},"tag_id":"103","maxtag_id":"203","mintag_id":"303","resulttag_id":"403","data":"TST_73812 Revision : 1","testName":"Pts_3","testId":"TST_73812","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":8,"rel":"folder"},"data":"FLT_49404 Revision : 1","faultName":"RS485 comm. Fail","faultId":"FLT_49404","state":"open","parentid":"7","level":"2","faultRevision":"1","children":[{"attributes":{"id":9,"rel":"folder"},"data":"SLN76370 Revision : 1","solName":"correct soldering","solId":"SLN76370","solRevision":"1","state":"open","parentid":"8","level":"3","children":[]}]}]},{"attributes":{"id":8,"rel":"folder"},"data":"FLT_49404 Revision : 1","faultName":"RS485 comm. Fail","faultId":"FLT_49404","state":"open","parentid":"7","level":"2","faultRevision":"1","children":[{"attributes":{"id":9,"rel":"folder"},"data":"SLN76370 Revision : 1","solName":"correct soldering","solId":"SLN76370","solRevision":"1","state":"open","parentid":"8","level":"3","children":[]}]},{"attributes":{"id":9,"rel":"folder"},"data":"SLN76370 Revision : 1","solName":"correct soldering","solId":"SLN76370","solRevision":"1","state":"open","parentid":"8","level":"3","children":[]},{"attributes":{"id":10,"rel":"folder"},"tag_id":"104","maxtag_id":"204","mintag_id":"304","resulttag_id":"404","data":"TST_84908 Revision : 1","testName":"Pts_4","testId":"TST_84908","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":11,"rel":"folder"},"data":"FLT_49404 Revision : 1","faultName":"RS485 comm. Fail","faultId":"FLT_49404","state":"open","parentid":"10","level":"2","faultRevision":"1","children":[{"attributes":{"id":12,"rel":"folder"},"data":"SLN76370 Revision : 1","solName":"correct soldering","solId":"SLN76370","solRevision":"1","state":"open","parentid":"11","level":"3","children":[]}]}]},{"attributes":{"id":11,"rel":"folder"},"data":"FLT_49404 Revision : 1","faultName":"RS485 comm. Fail","faultId":"FLT_49404","state":"open","parentid":"10","level":"2","faultRevision":"1","children":[{"attributes":{"id":12,"rel":"folder"},"data":"SLN76370 Revision : 1","solName":"correct soldering","solId":"SLN76370","solRevision":"1","state":"open","parentid":"11","level":"3","children":[]}]},{"attributes":{"id":12,"rel":"folder"},"data":"SLN76370 Revision : 1","solName":"correct soldering","solId":"SLN76370","solRevision":"1","state":"open","parentid":"11","level":"3","children":[]},{"attributes":{"id":13,"rel":"folder"},"tag_id":"105","maxtag_id":"205","mintag_id":"305","resulttag_id":"405","data":"TST_20423 Revision : 1","testName":"Pts_5","testId":"TST_20423","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":14,"rel":"folder"},"data":"FLT_50895 Revision : 1","faultName":"Calibration fail","faultId":"FLT_50895","state":"open","parentid":"13","level":"2","faultRevision":"1","children":[{"attributes":{"id":15,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"14","level":"3","children":[]}]}]},{"attributes":{"id":14,"rel":"folder"},"data":"FLT_50895 Revision : 1","faultName":"Calibration fail","faultId":"FLT_50895","state":"open","parentid":"13","level":"2","faultRevision":"1","children":[{"attributes":{"id":15,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"14","level":"3","children":[]}]},{"attributes":{"id":15,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"14","level":"3","children":[]},{"attributes":{"id":16,"rel":"folder"},"tag_id":"106","maxtag_id":"206","mintag_id":"306","resulttag_id":"406","data":"TST_42014 Revision : 1","testName":"Pts_6","testId":"TST_42014","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":17,"rel":"folder"},"data":"FLT_49404 Revision : 1","faultName":"RS485 comm. Fail","faultId":"FLT_49404","state":"open","parentid":"16","level":"2","faultRevision":"1","children":[{"attributes":{"id":18,"rel":"folder"},"data":"SLN33270 Revision : 1","solName":"check shorting & load connection","solId":"SLN33270","solRevision":"1","state":"open","parentid":"17","level":"3","children":[]}]}]},{"attributes":{"id":17,"rel":"folder"},"data":"FLT_49404 Revision : 1","faultName":"RS485 comm. Fail","faultId":"FLT_49404","state":"open","parentid":"16","level":"2","faultRevision":"1","children":[{"attributes":{"id":18,"rel":"folder"},"data":"SLN33270 Revision : 1","solName":"check shorting & load connection","solId":"SLN33270","solRevision":"1","state":"open","parentid":"17","level":"3","children":[]}]},{"attributes":{"id":18,"rel":"folder"},"data":"SLN33270 Revision : 1","solName":"check shorting & load connection","solId":"SLN33270","solRevision":"1","state":"open","parentid":"17","level":"3","children":[]},{"attributes":{"id":19,"rel":"folder"},"tag_id":"107","maxtag_id":"207","mintag_id":"307","resulttag_id":"407","data":"TST_78837 Revision : 1","testName":"Pts_7","testId":"TST_78837","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":20,"rel":"folder"},"data":"FLT_67681 Revision : 1","faultName":"reverse protection failure","faultId":"FLT_67681","state":"open","parentid":"19","level":"2","faultRevision":"1","children":[{"attributes":{"id":21,"rel":"folder"},"data":"SLN76370 Revision : 1","solName":"correct soldering","solId":"SLN76370","solRevision":"1","state":"open","parentid":"20","level":"3","children":[]}]}]},{"attributes":{"id":20,"rel":"folder"},"data":"FLT_67681 Revision : 1","faultName":"reverse protection failure","faultId":"FLT_67681","state":"open","parentid":"19","level":"2","faultRevision":"1","children":[{"attributes":{"id":21,"rel":"folder"},"data":"SLN76370 Revision : 1","solName":"correct soldering","solId":"SLN76370","solRevision":"1","state":"open","parentid":"20","level":"3","children":[]}]},{"attributes":{"id":21,"rel":"folder"},"data":"SLN76370 Revision : 1","solName":"correct soldering","solId":"SLN76370","solRevision":"1","state":"open","parentid":"20","level":"3","children":[]},{"attributes":{"id":22,"rel":"folder"},"tag_id":"108","maxtag_id":"208","mintag_id":"308","resulttag_id":"408","data":"TST_18043 Revision : 1","testName":"Pts_8","testId":"TST_18043","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":23,"rel":"folder"},"data":"FLT_50895 Revision : 1","faultName":"Calibration fail","faultId":"FLT_50895","state":"open","parentid":"22","level":"2","faultRevision":"1","children":[{"attributes":{"id":24,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"23","level":"3","children":[]}]}]},{"attributes":{"id":23,"rel":"folder"},"data":"FLT_50895 Revision : 1","faultName":"Calibration fail","faultId":"FLT_50895","state":"open","parentid":"22","level":"2","faultRevision":"1","children":[{"attributes":{"id":24,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"23","level":"3","children":[]}]},{"attributes":{"id":24,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"23","level":"3","children":[]},{"attributes":{"id":25,"rel":"folder"},"tag_id":"109","maxtag_id":"209","mintag_id":"309","resulttag_id":"409","data":"TST_49743 Revision : 1","testName":"Pts_9","testId":"TST_49743","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":26,"rel":"folder"},"data":"FLT_49711 Revision : 1","faultName":"Dry solder","faultId":"FLT_49711","state":"open","parentid":"25","level":"2","faultRevision":"1","children":[{"attributes":{"id":27,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"26","level":"3","children":[]}]}]},{"attributes":{"id":26,"rel":"folder"},"data":"FLT_49711 Revision : 1","faultName":"Dry solder","faultId":"FLT_49711","state":"open","parentid":"25","level":"2","faultRevision":"1","children":[{"attributes":{"id":27,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"26","level":"3","children":[]}]},{"attributes":{"id":27,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"26","level":"3","children":[]},{"attributes":{"id":28,"rel":"folder"},"tag_id":"110","maxtag_id":"210","mintag_id":"310","resulttag_id":"410","data":"TST_33502 Revision : 1","testName":"Pts_10","testId":"TST_33502","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":29,"rel":"folder"},"data":"FLT_45039 Revision : 1","faultName":"polarity reversed","faultId":"FLT_45039","state":"open","parentid":"28","level":"2","faultRevision":"1","children":[{"attributes":{"id":30,"rel":"folder"},"data":"SLN68563 Revision : 1","solName":"remove track shorting","solId":"SLN68563","solRevision":"1","state":"open","parentid":"29","level":"3","children":[]}]}]},{"attributes":{"id":29,"rel":"folder"},"data":"FLT_45039 Revision : 1","faultName":"polarity reversed","faultId":"FLT_45039","state":"open","parentid":"28","level":"2","faultRevision":"1","children":[{"attributes":{"id":30,"rel":"folder"},"data":"SLN68563 Revision : 1","solName":"remove track shorting","solId":"SLN68563","solRevision":"1","state":"open","parentid":"29","level":"3","children":[]}]},{"attributes":{"id":30,"rel":"folder"},"data":"SLN68563 Revision : 1","solName":"remove track shorting","solId":"SLN68563","solRevision":"1","state":"open","parentid":"29","level":"3","children":[]}]', 'D', '2016-02-10 06:59:07', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_format_child`
--

CREATE TABLE IF NOT EXISTS `tbl_format_child` (
  `fchild_id` int(50) NOT NULL AUTO_INCREMENT,
  `fchild_child_id` varchar(150) NOT NULL,
  `fchild_child_revision` varchar(100) NOT NULL,
  `fchild_parent` varchar(100) NOT NULL,
  `fchild_format_id` varchar(100) NOT NULL,
  `fchild_level` varchar(100) NOT NULL,
  `fchild_tag_id` varchar(100) NOT NULL,
  `fchild_maxtag_id` varchar(100) NOT NULL,
  `fchild_mintag_id` varchar(100) NOT NULL,
  `fchild_resulttag_id` varchar(100) NOT NULL,
  `fchild_revision` varchar(3) NOT NULL,
  `fchild_serial` text NOT NULL,
  `fchild_timestamp` datetime NOT NULL,
  PRIMARY KEY (`fchild_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=92 ;

--
-- Dumping data for table `tbl_format_child`
--

INSERT INTO `tbl_format_child` (`fchild_id`, `fchild_child_id`, `fchild_child_revision`, `fchild_parent`, `fchild_format_id`, `fchild_level`, `fchild_tag_id`, `fchild_maxtag_id`, `fchild_mintag_id`, `fchild_resulttag_id`, `fchild_revision`, `fchild_serial`, `fchild_timestamp`) VALUES
(40, 'UNT_30644', '1', 'UNT_30644', 'format_59624', '0', '', '', '', '', '1', '', '2016-02-08 06:52:44'),
(41, 'TST_47326', '1', 'UNT_30644', 'format_59624', '1', '11', '111', '112', '113', '1', '', '2016-02-08 06:52:44'),
(42, 'FLT_67238', '1', 'TST_47326', 'format_59624', '2', '', '', '', '', '1', '', '2016-02-08 06:52:44'),
(43, 'SLN11824', '1', 'FLT_67238', 'format_59624', '3', '', '', '', '', '1', '', '2016-02-08 06:52:44'),
(44, 'SLN58968', '1', 'FLT_67238', 'format_59624', '3', '', '', '', '', '1', '', '2016-02-08 06:52:44'),
(45, 'SLN34948', '1', 'FLT_67238', 'format_59624', '3', '', '', '', '', '1', '', '2016-02-08 06:52:44'),
(46, 'SLN20423', '1', 'FLT_67238', 'format_59624', '3', '', '', '', '', '1', '', '2016-02-08 06:52:44'),
(47, 'FLT_64997', '1', 'TST_47326', 'format_59624', '2', '', '', '', '', '1', '', '2016-02-08 06:52:44'),
(48, 'FLT_44403', '1', 'TST_47326', 'format_59624', '2', '', '', '', '', '1', '', '2016-02-08 06:52:44'),
(49, 'FLT_22226', '1', 'TST_47326', 'format_59624', '2', '', '', '', '', '1', '', '2016-02-08 06:52:44'),
(50, 'TST_73529', '1', 'UNT_30644', 'format_59624', '1', '12', '116', '117', '118', '1', '', '2016-02-08 06:52:44'),
(51, 'TST_18977', '1', 'UNT_30644', 'format_59624', '1', '21', '211', '212', '213', '1', '', '2016-02-08 06:52:44'),
(52, 'TST_80750', '1', 'UNT_30644', 'format_59624', '1', '32', '321', '322', '323', '1', '', '2016-02-08 06:52:44'),
(53, 'UNT_75237', '0', 'UNT_75237', 'format_84294', '0', '', '', '', '', '1', '', '2016-02-10 05:29:50'),
(54, 'TST_47326', '1', 'UNT_75237', 'format_84294', '1', '1100', '1200', '1300', '1400', '1', '', '2016-02-10 05:29:50'),
(55, 'FLT_50895', '1', 'TST_47326', 'format_84294', '2', '', '', '', '', '1', '', '2016-02-10 05:29:50'),
(56, 'SLN76370', '1', 'FLT_50895', 'format_84294', '3', '', '', '', '', '1', '', '2016-02-10 05:29:50'),
(57, 'UNT_63374', '1', 'UNT_63374', 'format_25555', '0', '', '', '', '', '1', '', '2016-02-10 06:49:32'),
(58, 'TST_69490', '1', 'UNT_63374', 'format_25555', '1', '101', '201', '301', '401', '1', '', '2016-02-10 06:49:32'),
(59, 'FLT_49404', '1', 'TST_69490', 'format_25555', '2', '', '', '', '', '1', '', '2016-02-10 06:49:32'),
(60, 'SLN76370', '1', 'FLT_49404', 'format_25555', '3', '', '', '', '', '1', '', '2016-02-10 06:49:32'),
(61, 'UNT_63374', '1', 'UNT_63374', 'format_25555', '0', '', '', '', '', '1.1', '', '2016-02-10 06:59:07'),
(62, 'TST_69490', '1', 'UNT_63374', 'format_25555', '1', '101', '201', '301', '401', '1.1', '', '2016-02-10 06:59:07'),
(63, 'FLT_49404', '1', 'TST_69490', 'format_25555', '2', '', '', '', '', '1.1', '', '2016-02-10 06:59:07'),
(64, 'SLN76370', '1', 'FLT_49404', 'format_25555', '3', '', '', '', '', '1.1', '', '2016-02-10 06:59:07'),
(65, 'TST_35320', '1', 'UNT_63374', 'format_25555', '1', '102', '202', '302', '402', '1.1', '', '2016-02-10 06:59:07'),
(66, 'FLT_45039', '1', 'TST_35320', 'format_25555', '2', '', '', '', '', '1.1', '', '2016-02-10 06:59:07'),
(67, 'SLN76000', '1', 'FLT_45039', 'format_25555', '3', '', '', '', '', '1.1', '', '2016-02-10 06:59:07'),
(68, 'TST_73812', '1', 'UNT_63374', 'format_25555', '1', '103', '203', '303', '403', '1.1', '', '2016-02-10 06:59:07'),
(69, 'FLT_49404', '1', 'TST_73812', 'format_25555', '2', '', '', '', '', '1.1', '', '2016-02-10 06:59:07'),
(70, 'SLN76370', '1', 'FLT_49404', 'format_25555', '3', '', '', '', '', '1.1', '', '2016-02-10 06:59:07'),
(71, 'TST_84908', '1', 'UNT_63374', 'format_25555', '1', '104', '204', '304', '404', '1.1', '', '2016-02-10 06:59:07'),
(72, 'FLT_49404', '1', 'TST_84908', 'format_25555', '2', '', '', '', '', '1.1', '', '2016-02-10 06:59:07'),
(73, 'SLN76370', '1', 'FLT_49404', 'format_25555', '3', '', '', '', '', '1.1', '', '2016-02-10 06:59:07'),
(74, 'TST_20423', '1', 'UNT_63374', 'format_25555', '1', '105', '205', '305', '405', '1.1', '', '2016-02-10 06:59:07'),
(75, 'FLT_50895', '1', 'TST_20423', 'format_25555', '2', '', '', '', '', '1.1', '', '2016-02-10 06:59:07'),
(76, 'SLN76000', '1', 'FLT_50895', 'format_25555', '3', '', '', '', '', '1.1', '', '2016-02-10 06:59:07'),
(77, 'TST_42014', '1', 'UNT_63374', 'format_25555', '1', '106', '206', '306', '406', '1.1', '', '2016-02-10 06:59:07'),
(78, 'FLT_49404', '1', 'TST_42014', 'format_25555', '2', '', '', '', '', '1.1', '', '2016-02-10 06:59:07'),
(79, 'SLN33270', '1', 'FLT_49404', 'format_25555', '3', '', '', '', '', '1.1', '', '2016-02-10 06:59:07'),
(80, 'TST_78837', '1', 'UNT_63374', 'format_25555', '1', '107', '207', '307', '407', '1.1', '', '2016-02-10 06:59:07'),
(81, 'FLT_67681', '1', 'TST_78837', 'format_25555', '2', '', '', '', '', '1.1', '', '2016-02-10 06:59:07'),
(82, 'SLN76370', '1', 'FLT_67681', 'format_25555', '3', '', '', '', '', '1.1', '', '2016-02-10 06:59:07'),
(83, 'TST_18043', '1', 'UNT_63374', 'format_25555', '1', '108', '208', '308', '408', '1.1', '', '2016-02-10 06:59:07'),
(84, 'FLT_50895', '1', 'TST_18043', 'format_25555', '2', '', '', '', '', '1.1', '', '2016-02-10 06:59:07'),
(85, 'SLN76000', '1', 'FLT_50895', 'format_25555', '3', '', '', '', '', '1.1', '', '2016-02-10 06:59:07'),
(86, 'TST_49743', '1', 'UNT_63374', 'format_25555', '1', '109', '209', '309', '409', '1.1', '', '2016-02-10 06:59:07'),
(87, 'FLT_49711', '1', 'TST_49743', 'format_25555', '2', '', '', '', '', '1.1', '', '2016-02-10 06:59:07'),
(88, 'SLN76000', '1', 'FLT_49711', 'format_25555', '3', '', '', '', '', '1.1', '', '2016-02-10 06:59:07'),
(89, 'TST_33502', '1', 'UNT_63374', 'format_25555', '1', '110', '210', '310', '410', '1.1', '', '2016-02-10 06:59:07'),
(90, 'FLT_45039', '1', 'TST_33502', 'format_25555', '2', '', '', '', '', '1.1', '', '2016-02-10 06:59:07'),
(91, 'SLN68563', '1', 'FLT_45039', 'format_25555', '3', '', '', '', '', '1.1', '', '2016-02-10 06:59:07');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_format_revision`
--

CREATE TABLE IF NOT EXISTS `tbl_format_revision` (
  `rev_id` int(100) NOT NULL AUTO_INCREMENT,
  `rev_format_id` varchar(100) NOT NULL,
  `rev_format_unique` varchar(100) NOT NULL,
  `rev_format_name` varchar(100) NOT NULL,
  `rev_format_revision` varchar(3) NOT NULL,
  `rev_format_unit` varchar(100) NOT NULL,
  `rev_format_jigid` varchar(100) NOT NULL,
  `rev_format_data` text NOT NULL,
  `rev_format_units` text NOT NULL,
  `rev_format_status` varchar(100) NOT NULL,
  `rev_format_timestamp` varchar(100) NOT NULL,
  `rev_format_userid` varchar(100) NOT NULL,
  `rev_unique_id` varchar(100) NOT NULL,
  PRIMARY KEY (`rev_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tbl_format_revision`
--

INSERT INTO `tbl_format_revision` (`rev_id`, `rev_format_id`, `rev_format_unique`, `rev_format_name`, `rev_format_revision`, `rev_format_unit`, `rev_format_jigid`, `rev_format_data`, `rev_format_units`, `rev_format_status`, `rev_format_timestamp`, `rev_format_userid`, `rev_unique_id`) VALUES
(3, '3', 'format_46570', 'testing format here', '1', 'UNT_30644', '910-326-R4-zxcvb', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_30644 Revision : 1","unitRevision":1,"unitName":"hello","unitId":"UNT_30644","parentid":"","level":0,"state":"open","children":[{"attributes":{"id":1,"rel":"folder"},"data":"TST_47326 Revision : 1","testRevision":1,"testName":"hello","testId":"TST_47326","parentid":0,"level":1,"tag_id":11,"maxtag_id":111,"mintag_id":112,"resulttag_id":113,"state":"open","children":[{"attributes":{"id":5,"rel":"folder"},"data":"FLT_67238 Revision : 1","faultRevision":1,"faultName":"faulty","faultId":"FLT_67238","parentid":1,"level":2,"state":"open","children":[{"attributes":{"id":9,"rel":"folder"},"data":"SLN11824 Revision : 1","solRevision":1,"solName":"hhhhh","solId":"SLN11824","parentid":5,"level":3,"state":"open","children":[]},{"attributes":{"id":10,"rel":"folder"},"data":"SLN58968 Revision : 1","solRevision":1,"solName":"hhhhh","solId":"SLN58968","parentid":5,"level":3,"state":"open","children":[]},{"attributes":{"id":11,"rel":"folder"},"data":"SLN34948 Revision : 1","solRevision":1,"solName":"hhhhh","solId":"SLN34948","parentid":5,"level":3,"state":"open","children":[]},{"attributes":{"id":12,"rel":"folder"},"data":"SLN20423 Revision : 1","solRevision":1,"solName":"hhhhh","solId":"SLN20423","parentid":5,"level":3,"state":"open","children":[]}]},{"attributes":{"id":6,"rel":"folder"},"data":"FLT_64997 Revision : 1","faultRevision":1,"faultName":"faulty","faultId":"FLT_64997","parentid":1,"level":2,"state":"open","children":[]},{"attributes":{"id":7,"rel":"folder"},"data":"FLT_44403 Revision : 1","faultRevision":1,"faultName":"faulty","faultId":"FLT_44403","parentid":1,"level":2,"state":"open","children":[]},{"attributes":{"id":8,"rel":"folder"},"data":"FLT_22226 Revision : 1","faultRevision":1,"faultName":"faulty","faultId":"FLT_22226","parentid":1,"level":2,"state":"open","children":[]}]},{"attributes":{"id":2,"rel":"folder"},"data":"TST_73529 Revision : 1","testRevision":1,"testName":"hello","testId":"TST_73529","parentid":0,"level":1,"tag_id":12,"maxtag_id":116,"mintag_id":117,"resulttag_id":118,"state":"open","children":[]},{"attributes":{"id":3,"rel":"folder"},"data":"TST_18977 Revision : 1","testRevision":1,"testName":"hello","testId":"TST_18977","parentid":0,"level":1,"tag_id":21,"maxtag_id":211,"mintag_id":212,"resulttag_id":213,"state":"open","children":[]},{"attributes":{"id":4,"rel":"folder"},"data":"TST_80750 Revision : 1","testRevision":1,"testName":"hello","testId":"TST_80750","parentid":0,"level":1,"tag_id":32,"maxtag_id":321,"mintag_id":322,"resulttag_id":323,"state":"open","children":[]}]}]', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_30644 Revision : 1","unitRevision":1,"unitName":"hello","unitId":"UNT_30644","parentid":"","level":0,"state":"open"},{"attributes":{"id":1,"rel":"folder"},"data":"TST_47326 Revision : 1","testRevision":1,"testName":"hello","testId":"TST_47326","parentid":0,"level":1,"tag_id":11,"maxtag_id":111,"mintag_id":112,"resulttag_id":113,"state":"open"},{"attributes":{"id":2,"rel":"folder"},"data":"TST_73529 Revision : 1","testRevision":1,"testName":"hello","testId":"TST_73529","parentid":0,"level":1,"tag_id":12,"maxtag_id":116,"mintag_id":117,"resulttag_id":118,"state":"open"},{"attributes":{"id":3,"rel":"folder"},"data":"TST_18977 Revision : 1","testRevision":1,"testName":"hello","testId":"TST_18977","parentid":0,"level":1,"tag_id":21,"maxtag_id":211,"mintag_id":212,"resulttag_id":213,"state":"open"},{"attributes":{"id":4,"rel":"folder"},"data":"TST_80750 Revision : 1","testRevision":1,"testName":"hello","testId":"TST_80750","parentid":0,"level":1,"tag_id":32,"maxtag_id":321,"mintag_id":322,"resulttag_id":323,"state":"open"},{"attributes":{"id":5,"rel":"folder"},"data":"FLT_67238 Revision : 1","faultRevision":1,"faultName":"faulty","faultId":"FLT_67238","parentid":1,"level":2,"state":"open"},{"attributes":{"id":6,"rel":"folder"},"data":"FLT_64997 Revision : 1","faultRevision":1,"faultName":"faulty","faultId":"FLT_64997","parentid":1,"level":2,"state":"open"},{"attributes":{"id":7,"rel":"folder"},"data":"FLT_44403 Revision : 1","faultRevision":1,"faultName":"faulty","faultId":"FLT_44403","parentid":1,"level":2,"state":"open"},{"attributes":{"id":8,"rel":"folder"},"data":"FLT_22226 Revision : 1","faultRevision":1,"faultName":"faulty","faultId":"FLT_22226","parentid":1,"level":2,"state":"open"},{"attributes":{"id":9,"rel":"folder"},"data":"SLN11824 Revision : 1","solRevision":1,"solName":"hhhhh","solId":"SLN11824","parentid":5,"level":3,"state":"open"},{"attributes":{"id":10,"rel":"folder"},"data":"SLN58968 Revision : 1","solRevision":1,"solName":"hhhhh","solId":"SLN58968","parentid":5,"level":3,"state":"open"},{"attributes":{"id":11,"rel":"folder"},"data":"SLN34948 Revision : 1","solRevision":1,"solName":"hhhhh","solId":"SLN34948","parentid":5,"level":3,"state":"open"},{"attributes":{"id":12,"rel":"folder"},"data":"SLN20423 Revision : 1","solRevision":1,"solName":"hhhhh","solId":"SLN20423","parentid":5,"level":3,"state":"open"}]', 'DELETE', '2016-02-08 06:25:05', '1', 'revision_75833'),
(4, '4', 'format_59624', 'testing format here', '1', 'UNT_30644', '910-326-R4-zxcvb', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_30644 Revision : 1","unitRevision":1,"unitName":"hello","unitId":"UNT_30644","parentid":"","level":0,"state":"open","children":[{"attributes":{"id":1,"rel":"folder"},"data":"TST_47326 Revision : 1","testRevision":1,"testName":"hello","testId":"TST_47326","parentid":0,"level":1,"tag_id":11,"maxtag_id":111,"mintag_id":112,"resulttag_id":113,"state":"open","children":[{"attributes":{"id":5,"rel":"folder"},"data":"FLT_67238 Revision : 1","faultRevision":1,"faultName":"faulty","faultId":"FLT_67238","parentid":1,"level":2,"state":"open","children":[{"attributes":{"id":9,"rel":"folder"},"data":"SLN11824 Revision : 1","solRevision":1,"solName":"hhhhh","solId":"SLN11824","parentid":5,"level":3,"state":"open","children":[]},{"attributes":{"id":10,"rel":"folder"},"data":"SLN58968 Revision : 1","solRevision":1,"solName":"hhhhh","solId":"SLN58968","parentid":5,"level":3,"state":"open","children":[]},{"attributes":{"id":11,"rel":"folder"},"data":"SLN34948 Revision : 1","solRevision":1,"solName":"hhhhh","solId":"SLN34948","parentid":5,"level":3,"state":"open","children":[]},{"attributes":{"id":12,"rel":"folder"},"data":"SLN20423 Revision : 1","solRevision":1,"solName":"hhhhh","solId":"SLN20423","parentid":5,"level":3,"state":"open","children":[]}]},{"attributes":{"id":6,"rel":"folder"},"data":"FLT_64997 Revision : 1","faultRevision":1,"faultName":"faulty","faultId":"FLT_64997","parentid":1,"level":2,"state":"open","children":[]},{"attributes":{"id":7,"rel":"folder"},"data":"FLT_44403 Revision : 1","faultRevision":1,"faultName":"faulty","faultId":"FLT_44403","parentid":1,"level":2,"state":"open","children":[]},{"attributes":{"id":8,"rel":"folder"},"data":"FLT_22226 Revision : 1","faultRevision":1,"faultName":"faulty","faultId":"FLT_22226","parentid":1,"level":2,"state":"open","children":[]}]},{"attributes":{"id":2,"rel":"folder"},"data":"TST_73529 Revision : 1","testRevision":1,"testName":"hello","testId":"TST_73529","parentid":0,"level":1,"tag_id":12,"maxtag_id":116,"mintag_id":117,"resulttag_id":118,"state":"open","children":[]},{"attributes":{"id":3,"rel":"folder"},"data":"TST_18977 Revision : 1","testRevision":1,"testName":"hello","testId":"TST_18977","parentid":0,"level":1,"tag_id":21,"maxtag_id":211,"mintag_id":212,"resulttag_id":213,"state":"open","children":[]},{"attributes":{"id":4,"rel":"folder"},"data":"TST_80750 Revision : 1","testRevision":1,"testName":"hello","testId":"TST_80750","parentid":0,"level":1,"tag_id":32,"maxtag_id":321,"mintag_id":322,"resulttag_id":323,"state":"open","children":[]}]}]', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_30644 Revision : 1","unitRevision":1,"unitName":"hello","unitId":"UNT_30644","parentid":"","level":0,"state":"open"},{"attributes":{"id":1,"rel":"folder"},"data":"TST_47326 Revision : 1","testRevision":1,"testName":"hello","testId":"TST_47326","parentid":0,"level":1,"tag_id":11,"maxtag_id":111,"mintag_id":112,"resulttag_id":113,"state":"open"},{"attributes":{"id":2,"rel":"folder"},"data":"TST_73529 Revision : 1","testRevision":1,"testName":"hello","testId":"TST_73529","parentid":0,"level":1,"tag_id":12,"maxtag_id":116,"mintag_id":117,"resulttag_id":118,"state":"open"},{"attributes":{"id":3,"rel":"folder"},"data":"TST_18977 Revision : 1","testRevision":1,"testName":"hello","testId":"TST_18977","parentid":0,"level":1,"tag_id":21,"maxtag_id":211,"mintag_id":212,"resulttag_id":213,"state":"open"},{"attributes":{"id":4,"rel":"folder"},"data":"TST_80750 Revision : 1","testRevision":1,"testName":"hello","testId":"TST_80750","parentid":0,"level":1,"tag_id":32,"maxtag_id":321,"mintag_id":322,"resulttag_id":323,"state":"open"},{"attributes":{"id":5,"rel":"folder"},"data":"FLT_67238 Revision : 1","faultRevision":1,"faultName":"faulty","faultId":"FLT_67238","parentid":1,"level":2,"state":"open"},{"attributes":{"id":6,"rel":"folder"},"data":"FLT_64997 Revision : 1","faultRevision":1,"faultName":"faulty","faultId":"FLT_64997","parentid":1,"level":2,"state":"open"},{"attributes":{"id":7,"rel":"folder"},"data":"FLT_44403 Revision : 1","faultRevision":1,"faultName":"faulty","faultId":"FLT_44403","parentid":1,"level":2,"state":"open"},{"attributes":{"id":8,"rel":"folder"},"data":"FLT_22226 Revision : 1","faultRevision":1,"faultName":"faulty","faultId":"FLT_22226","parentid":1,"level":2,"state":"open"},{"attributes":{"id":9,"rel":"folder"},"data":"SLN11824 Revision : 1","solRevision":1,"solName":"hhhhh","solId":"SLN11824","parentid":5,"level":3,"state":"open"},{"attributes":{"id":10,"rel":"folder"},"data":"SLN58968 Revision : 1","solRevision":1,"solName":"hhhhh","solId":"SLN58968","parentid":5,"level":3,"state":"open"},{"attributes":{"id":11,"rel":"folder"},"data":"SLN34948 Revision : 1","solRevision":1,"solName":"hhhhh","solId":"SLN34948","parentid":5,"level":3,"state":"open"},{"attributes":{"id":12,"rel":"folder"},"data":"SLN20423 Revision : 1","solRevision":1,"solName":"hhhhh","solId":"SLN20423","parentid":5,"level":3,"state":"open"}]', 'D', '2016-02-08 06:52:44', '1', 'revision_64803'),
(5, '5', 'format_84294', 'format_testing1', '1', 'UNT_75237', '12345', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_75237 Revision : 0","unitRevision":"0","unitName":"CMU","unitId":"UNT_75237","state":"open","parentid":0,"level":"0","children":[{"attributes":{"id":1,"rel":"folder"},"data":"TST_47326 Revision : 1","tag_id":"1100","maxtag_id":"1200","mintag_id":"1300","resulttag_id":"1400","testName":"Visual Inspecyyyy","testId":"TST_47326","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":2,"rel":"folder"},"data":"FLT_50895 Revision : 1","faultName":"Calibration fail","faultId":"FLT_50895","faultRevision":"1","state":"open","parentid":"1","level":"2","children":[{"attributes":{"id":3,"rel":"folder"},"data":"SLN76370 Revision : 1","solId":"SLN76370","solRevision":"1","state":"open","parentid":"2","level":"3","children":[]}]}]}]}]', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_75237 Revision : 0","unitRevision":"0","unitName":"CMU","unitId":"UNT_75237","state":"open","parentid":0,"level":"0","children":[{"attributes":{"id":1,"rel":"folder"},"data":"TST_47326 Revision : 1","tag_id":"1100","maxtag_id":"1200","mintag_id":"1300","resulttag_id":"1400","testName":"Visual Inspecyyyy","testId":"TST_47326","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":2,"rel":"folder"},"data":"FLT_50895 Revision : 1","faultName":"Calibration fail","faultId":"FLT_50895","faultRevision":"1","state":"open","parentid":"1","level":"2","children":[{"attributes":{"id":3,"rel":"folder"},"data":"SLN76370 Revision : 1","solId":"SLN76370","solRevision":"1","state":"open","parentid":"2","level":"3","children":[]}]}]}]},{"attributes":{"id":1,"rel":"folder"},"data":"TST_47326 Revision : 1","tag_id":"1100","maxtag_id":"1200","mintag_id":"1300","resulttag_id":"1400","testName":"Visual Inspecyyyy","testId":"TST_47326","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":2,"rel":"folder"},"data":"FLT_50895 Revision : 1","faultName":"Calibration fail","faultId":"FLT_50895","faultRevision":"1","state":"open","parentid":"1","level":"2","children":[{"attributes":{"id":3,"rel":"folder"},"data":"SLN76370 Revision : 1","solId":"SLN76370","solRevision":"1","state":"open","parentid":"2","level":"3","children":[]}]}]},{"attributes":{"id":2,"rel":"folder"},"data":"FLT_50895 Revision : 1","faultName":"Calibration fail","faultId":"FLT_50895","faultRevision":"1","state":"open","parentid":"1","level":"2","children":[{"attributes":{"id":3,"rel":"folder"},"data":"SLN76370 Revision : 1","solId":"SLN76370","solRevision":"1","state":"open","parentid":"2","level":"3","children":[]}]},{"attributes":{"id":3,"rel":"folder"},"data":"SLN76370 Revision : 1","solId":"SLN76370","solRevision":"1","state":"open","parentid":"2","level":"3","children":[]}]', 'D', '2016-02-10 05:29:50', '1', 'revision_22073'),
(6, '6', 'format_25555', 'Pts_app_format', '1', 'UNT_63374', 'PTS-910-326 R4', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_63374 Revision : 1","unitRevision":"1","unitName":"Pts_app_unit","unitId":"UNT_63374","state":"open","parentid":0,"level":"0","children":[{"attributes":{"id":1,"rel":"folder"},"data":"TST_69490 Revision : 1","tag_id":"101","maxtag_id":"201","mintag_id":"301","resulttag_id":"401","testName":"Pts_1","testId":"TST_69490","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":2,"rel":"folder"},"data":"FLT_49404 Revision : 1","faultName":"RS485 comm. Fail","faultId":"FLT_49404","faultRevision":"1","state":"open","parentid":"1","level":"2","children":[{"attributes":{"id":3,"rel":"folder"},"data":"SLN76370 Revision : 1","solId":"SLN76370","solRevision":"1","state":"open","parentid":"2","level":"3","children":[]}]}]}]}]', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_63374 Revision : 1","unitRevision":"1","unitName":"Pts_app_unit","unitId":"UNT_63374","state":"open","parentid":0,"level":"0","children":[{"attributes":{"id":1,"rel":"folder"},"data":"TST_69490 Revision : 1","tag_id":"101","maxtag_id":"201","mintag_id":"301","resulttag_id":"401","testName":"Pts_1","testId":"TST_69490","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":2,"rel":"folder"},"data":"FLT_49404 Revision : 1","faultName":"RS485 comm. Fail","faultId":"FLT_49404","faultRevision":"1","state":"open","parentid":"1","level":"2","children":[{"attributes":{"id":3,"rel":"folder"},"data":"SLN76370 Revision : 1","solId":"SLN76370","solRevision":"1","state":"open","parentid":"2","level":"3","children":[]}]}]}]},{"attributes":{"id":1,"rel":"folder"},"data":"TST_69490 Revision : 1","tag_id":"101","maxtag_id":"201","mintag_id":"301","resulttag_id":"401","testName":"Pts_1","testId":"TST_69490","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":2,"rel":"folder"},"data":"FLT_49404 Revision : 1","faultName":"RS485 comm. Fail","faultId":"FLT_49404","faultRevision":"1","state":"open","parentid":"1","level":"2","children":[{"attributes":{"id":3,"rel":"folder"},"data":"SLN76370 Revision : 1","solId":"SLN76370","solRevision":"1","state":"open","parentid":"2","level":"3","children":[]}]}]},{"attributes":{"id":2,"rel":"folder"},"data":"FLT_49404 Revision : 1","faultName":"RS485 comm. Fail","faultId":"FLT_49404","faultRevision":"1","state":"open","parentid":"1","level":"2","children":[{"attributes":{"id":3,"rel":"folder"},"data":"SLN76370 Revision : 1","solId":"SLN76370","solRevision":"1","state":"open","parentid":"2","level":"3","children":[]}]},{"attributes":{"id":3,"rel":"folder"},"data":"SLN76370 Revision : 1","solId":"SLN76370","solRevision":"1","state":"open","parentid":"2","level":"3","children":[]}]', 'D', '2016-02-10 06:49:32', '1', 'revision_93529'),
(7, '6', 'format_25555', 'Pts_app_format', '1.1', 'UNT_63374', 'PTS-910-326 R4', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_63374 Revision : 1","unitRevision":"1","unitName":"Pts_app_unit","unitId":"UNT_63374","state":"open","parentid":0,"level":"0","children":[{"attributes":{"id":1,"rel":"folder"},"data":"TST_69490 Revision : 1","tag_id":"101","maxtag_id":"201","mintag_id":"301","resulttag_id":"401","testName":"Pts_1","testId":"TST_69490","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":2,"rel":"folder"},"data":"FLT_49404 Revision : 1","faultName":"RS485 comm. Fail","faultId":"FLT_49404","faultRevision":"1","state":"open","parentid":"1","level":"2","children":[{"attributes":{"id":3,"rel":"folder"},"data":"SLN76370 Revision : 1","solId":"SLN76370","solRevision":"1","state":"open","parentid":"2","level":"3","children":[]}]}]},{"attributes":{"id":4,"rel":"folder"},"tag_id":"102","maxtag_id":"202","mintag_id":"302","resulttag_id":"402","data":"TST_35320 Revision : 1","testName":"Pts_2","testId":"TST_35320","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":5,"rel":"folder"},"data":"FLT_45039 Revision : 1","faultName":"polarity reversed","faultId":"FLT_45039","state":"open","parentid":"4","level":"2","faultRevision":"1","children":[{"attributes":{"id":6,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"5","level":"3","children":[]}]}]},{"attributes":{"id":7,"rel":"folder"},"tag_id":"103","maxtag_id":"203","mintag_id":"303","resulttag_id":"403","data":"TST_73812 Revision : 1","testName":"Pts_3","testId":"TST_73812","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":8,"rel":"folder"},"data":"FLT_49404 Revision : 1","faultName":"RS485 comm. Fail","faultId":"FLT_49404","state":"open","parentid":"7","level":"2","faultRevision":"1","children":[{"attributes":{"id":9,"rel":"folder"},"data":"SLN76370 Revision : 1","solName":"correct soldering","solId":"SLN76370","solRevision":"1","state":"open","parentid":"8","level":"3","children":[]}]}]},{"attributes":{"id":10,"rel":"folder"},"tag_id":"104","maxtag_id":"204","mintag_id":"304","resulttag_id":"404","data":"TST_84908 Revision : 1","testName":"Pts_4","testId":"TST_84908","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":11,"rel":"folder"},"data":"FLT_49404 Revision : 1","faultName":"RS485 comm. Fail","faultId":"FLT_49404","state":"open","parentid":"10","level":"2","faultRevision":"1","children":[{"attributes":{"id":12,"rel":"folder"},"data":"SLN76370 Revision : 1","solName":"correct soldering","solId":"SLN76370","solRevision":"1","state":"open","parentid":"11","level":"3","children":[]}]}]},{"attributes":{"id":13,"rel":"folder"},"tag_id":"105","maxtag_id":"205","mintag_id":"305","resulttag_id":"405","data":"TST_20423 Revision : 1","testName":"Pts_5","testId":"TST_20423","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":14,"rel":"folder"},"data":"FLT_50895 Revision : 1","faultName":"Calibration fail","faultId":"FLT_50895","state":"open","parentid":"13","level":"2","faultRevision":"1","children":[{"attributes":{"id":15,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"14","level":"3","children":[]}]}]},{"attributes":{"id":16,"rel":"folder"},"tag_id":"106","maxtag_id":"206","mintag_id":"306","resulttag_id":"406","data":"TST_42014 Revision : 1","testName":"Pts_6","testId":"TST_42014","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":17,"rel":"folder"},"data":"FLT_49404 Revision : 1","faultName":"RS485 comm. Fail","faultId":"FLT_49404","state":"open","parentid":"16","level":"2","faultRevision":"1","children":[{"attributes":{"id":18,"rel":"folder"},"data":"SLN33270 Revision : 1","solName":"check shorting & load connection","solId":"SLN33270","solRevision":"1","state":"open","parentid":"17","level":"3","children":[]}]}]},{"attributes":{"id":19,"rel":"folder"},"tag_id":"107","maxtag_id":"207","mintag_id":"307","resulttag_id":"407","data":"TST_78837 Revision : 1","testName":"Pts_7","testId":"TST_78837","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":20,"rel":"folder"},"data":"FLT_67681 Revision : 1","faultName":"reverse protection failure","faultId":"FLT_67681","state":"open","parentid":"19","level":"2","faultRevision":"1","children":[{"attributes":{"id":21,"rel":"folder"},"data":"SLN76370 Revision : 1","solName":"correct soldering","solId":"SLN76370","solRevision":"1","state":"open","parentid":"20","level":"3","children":[]}]}]},{"attributes":{"id":22,"rel":"folder"},"tag_id":"108","maxtag_id":"208","mintag_id":"308","resulttag_id":"408","data":"TST_18043 Revision : 1","testName":"Pts_8","testId":"TST_18043","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":23,"rel":"folder"},"data":"FLT_50895 Revision : 1","faultName":"Calibration fail","faultId":"FLT_50895","state":"open","parentid":"22","level":"2","faultRevision":"1","children":[{"attributes":{"id":24,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"23","level":"3","children":[]}]}]},{"attributes":{"id":25,"rel":"folder"},"tag_id":"109","maxtag_id":"209","mintag_id":"309","resulttag_id":"409","data":"TST_49743 Revision : 1","testName":"Pts_9","testId":"TST_49743","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":26,"rel":"folder"},"data":"FLT_49711 Revision : 1","faultName":"Dry solder","faultId":"FLT_49711","state":"open","parentid":"25","level":"2","faultRevision":"1","children":[{"attributes":{"id":27,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"26","level":"3","children":[]}]}]},{"attributes":{"id":28,"rel":"folder"},"tag_id":"110","maxtag_id":"210","mintag_id":"310","resulttag_id":"410","data":"TST_33502 Revision : 1","testName":"Pts_10","testId":"TST_33502","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":29,"rel":"folder"},"data":"FLT_45039 Revision : 1","faultName":"polarity reversed","faultId":"FLT_45039","state":"open","parentid":"28","level":"2","faultRevision":"1","children":[{"attributes":{"id":30,"rel":"folder"},"data":"SLN68563 Revision : 1","solName":"remove track shorting","solId":"SLN68563","solRevision":"1","state":"open","parentid":"29","level":"3","children":[]}]}]}]}]', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_63374 Revision : 1","unitRevision":"1","unitName":"Pts_app_unit","unitId":"UNT_63374","state":"open","parentid":0,"level":"0","children":[{"attributes":{"id":1,"rel":"folder"},"data":"TST_69490 Revision : 1","tag_id":"101","maxtag_id":"201","mintag_id":"301","resulttag_id":"401","testName":"Pts_1","testId":"TST_69490","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":2,"rel":"folder"},"data":"FLT_49404 Revision : 1","faultName":"RS485 comm. Fail","faultId":"FLT_49404","faultRevision":"1","state":"open","parentid":"1","level":"2","children":[{"attributes":{"id":3,"rel":"folder"},"data":"SLN76370 Revision : 1","solId":"SLN76370","solRevision":"1","state":"open","parentid":"2","level":"3","children":[]}]}]},{"attributes":{"id":4,"rel":"folder"},"tag_id":"102","maxtag_id":"202","mintag_id":"302","resulttag_id":"402","data":"TST_35320 Revision : 1","testName":"Pts_2","testId":"TST_35320","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":5,"rel":"folder"},"data":"FLT_45039 Revision : 1","faultName":"polarity reversed","faultId":"FLT_45039","state":"open","parentid":"4","level":"2","faultRevision":"1","children":[{"attributes":{"id":6,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"5","level":"3","children":[]}]}]},{"attributes":{"id":7,"rel":"folder"},"tag_id":"103","maxtag_id":"203","mintag_id":"303","resulttag_id":"403","data":"TST_73812 Revision : 1","testName":"Pts_3","testId":"TST_73812","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":8,"rel":"folder"},"data":"FLT_49404 Revision : 1","faultName":"RS485 comm. Fail","faultId":"FLT_49404","state":"open","parentid":"7","level":"2","faultRevision":"1","children":[{"attributes":{"id":9,"rel":"folder"},"data":"SLN76370 Revision : 1","solName":"correct soldering","solId":"SLN76370","solRevision":"1","state":"open","parentid":"8","level":"3","children":[]}]}]},{"attributes":{"id":10,"rel":"folder"},"tag_id":"104","maxtag_id":"204","mintag_id":"304","resulttag_id":"404","data":"TST_84908 Revision : 1","testName":"Pts_4","testId":"TST_84908","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":11,"rel":"folder"},"data":"FLT_49404 Revision : 1","faultName":"RS485 comm. Fail","faultId":"FLT_49404","state":"open","parentid":"10","level":"2","faultRevision":"1","children":[{"attributes":{"id":12,"rel":"folder"},"data":"SLN76370 Revision : 1","solName":"correct soldering","solId":"SLN76370","solRevision":"1","state":"open","parentid":"11","level":"3","children":[]}]}]},{"attributes":{"id":13,"rel":"folder"},"tag_id":"105","maxtag_id":"205","mintag_id":"305","resulttag_id":"405","data":"TST_20423 Revision : 1","testName":"Pts_5","testId":"TST_20423","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":14,"rel":"folder"},"data":"FLT_50895 Revision : 1","faultName":"Calibration fail","faultId":"FLT_50895","state":"open","parentid":"13","level":"2","faultRevision":"1","children":[{"attributes":{"id":15,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"14","level":"3","children":[]}]}]},{"attributes":{"id":16,"rel":"folder"},"tag_id":"106","maxtag_id":"206","mintag_id":"306","resulttag_id":"406","data":"TST_42014 Revision : 1","testName":"Pts_6","testId":"TST_42014","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":17,"rel":"folder"},"data":"FLT_49404 Revision : 1","faultName":"RS485 comm. Fail","faultId":"FLT_49404","state":"open","parentid":"16","level":"2","faultRevision":"1","children":[{"attributes":{"id":18,"rel":"folder"},"data":"SLN33270 Revision : 1","solName":"check shorting & load connection","solId":"SLN33270","solRevision":"1","state":"open","parentid":"17","level":"3","children":[]}]}]},{"attributes":{"id":19,"rel":"folder"},"tag_id":"107","maxtag_id":"207","mintag_id":"307","resulttag_id":"407","data":"TST_78837 Revision : 1","testName":"Pts_7","testId":"TST_78837","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":20,"rel":"folder"},"data":"FLT_67681 Revision : 1","faultName":"reverse protection failure","faultId":"FLT_67681","state":"open","parentid":"19","level":"2","faultRevision":"1","children":[{"attributes":{"id":21,"rel":"folder"},"data":"SLN76370 Revision : 1","solName":"correct soldering","solId":"SLN76370","solRevision":"1","state":"open","parentid":"20","level":"3","children":[]}]}]},{"attributes":{"id":22,"rel":"folder"},"tag_id":"108","maxtag_id":"208","mintag_id":"308","resulttag_id":"408","data":"TST_18043 Revision : 1","testName":"Pts_8","testId":"TST_18043","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":23,"rel":"folder"},"data":"FLT_50895 Revision : 1","faultName":"Calibration fail","faultId":"FLT_50895","state":"open","parentid":"22","level":"2","faultRevision":"1","children":[{"attributes":{"id":24,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"23","level":"3","children":[]}]}]},{"attributes":{"id":25,"rel":"folder"},"tag_id":"109","maxtag_id":"209","mintag_id":"309","resulttag_id":"409","data":"TST_49743 Revision : 1","testName":"Pts_9","testId":"TST_49743","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":26,"rel":"folder"},"data":"FLT_49711 Revision : 1","faultName":"Dry solder","faultId":"FLT_49711","state":"open","parentid":"25","level":"2","faultRevision":"1","children":[{"attributes":{"id":27,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"26","level":"3","children":[]}]}]},{"attributes":{"id":28,"rel":"folder"},"tag_id":"110","maxtag_id":"210","mintag_id":"310","resulttag_id":"410","data":"TST_33502 Revision : 1","testName":"Pts_10","testId":"TST_33502","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":29,"rel":"folder"},"data":"FLT_45039 Revision : 1","faultName":"polarity reversed","faultId":"FLT_45039","state":"open","parentid":"28","level":"2","faultRevision":"1","children":[{"attributes":{"id":30,"rel":"folder"},"data":"SLN68563 Revision : 1","solName":"remove track shorting","solId":"SLN68563","solRevision":"1","state":"open","parentid":"29","level":"3","children":[]}]}]}]},{"attributes":{"id":1,"rel":"folder"},"data":"TST_69490 Revision : 1","tag_id":"101","maxtag_id":"201","mintag_id":"301","resulttag_id":"401","testName":"Pts_1","testId":"TST_69490","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":2,"rel":"folder"},"data":"FLT_49404 Revision : 1","faultName":"RS485 comm. Fail","faultId":"FLT_49404","faultRevision":"1","state":"open","parentid":"1","level":"2","children":[{"attributes":{"id":3,"rel":"folder"},"data":"SLN76370 Revision : 1","solId":"SLN76370","solRevision":"1","state":"open","parentid":"2","level":"3","children":[]}]}]},{"attributes":{"id":2,"rel":"folder"},"data":"FLT_49404 Revision : 1","faultName":"RS485 comm. Fail","faultId":"FLT_49404","faultRevision":"1","state":"open","parentid":"1","level":"2","children":[{"attributes":{"id":3,"rel":"folder"},"data":"SLN76370 Revision : 1","solId":"SLN76370","solRevision":"1","state":"open","parentid":"2","level":"3","children":[]}]},{"attributes":{"id":3,"rel":"folder"},"data":"SLN76370 Revision : 1","solId":"SLN76370","solRevision":"1","state":"open","parentid":"2","level":"3","children":[]},{"attributes":{"id":4,"rel":"folder"},"tag_id":"102","maxtag_id":"202","mintag_id":"302","resulttag_id":"402","data":"TST_35320 Revision : 1","testName":"Pts_2","testId":"TST_35320","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":5,"rel":"folder"},"data":"FLT_45039 Revision : 1","faultName":"polarity reversed","faultId":"FLT_45039","state":"open","parentid":"4","level":"2","faultRevision":"1","children":[{"attributes":{"id":6,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"5","level":"3","children":[]}]}]},{"attributes":{"id":5,"rel":"folder"},"data":"FLT_45039 Revision : 1","faultName":"polarity reversed","faultId":"FLT_45039","state":"open","parentid":"4","level":"2","faultRevision":"1","children":[{"attributes":{"id":6,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"5","level":"3","children":[]}]},{"attributes":{"id":6,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"5","level":"3","children":[]},{"attributes":{"id":7,"rel":"folder"},"tag_id":"103","maxtag_id":"203","mintag_id":"303","resulttag_id":"403","data":"TST_73812 Revision : 1","testName":"Pts_3","testId":"TST_73812","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":8,"rel":"folder"},"data":"FLT_49404 Revision : 1","faultName":"RS485 comm. Fail","faultId":"FLT_49404","state":"open","parentid":"7","level":"2","faultRevision":"1","children":[{"attributes":{"id":9,"rel":"folder"},"data":"SLN76370 Revision : 1","solName":"correct soldering","solId":"SLN76370","solRevision":"1","state":"open","parentid":"8","level":"3","children":[]}]}]},{"attributes":{"id":8,"rel":"folder"},"data":"FLT_49404 Revision : 1","faultName":"RS485 comm. Fail","faultId":"FLT_49404","state":"open","parentid":"7","level":"2","faultRevision":"1","children":[{"attributes":{"id":9,"rel":"folder"},"data":"SLN76370 Revision : 1","solName":"correct soldering","solId":"SLN76370","solRevision":"1","state":"open","parentid":"8","level":"3","children":[]}]},{"attributes":{"id":9,"rel":"folder"},"data":"SLN76370 Revision : 1","solName":"correct soldering","solId":"SLN76370","solRevision":"1","state":"open","parentid":"8","level":"3","children":[]},{"attributes":{"id":10,"rel":"folder"},"tag_id":"104","maxtag_id":"204","mintag_id":"304","resulttag_id":"404","data":"TST_84908 Revision : 1","testName":"Pts_4","testId":"TST_84908","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":11,"rel":"folder"},"data":"FLT_49404 Revision : 1","faultName":"RS485 comm. Fail","faultId":"FLT_49404","state":"open","parentid":"10","level":"2","faultRevision":"1","children":[{"attributes":{"id":12,"rel":"folder"},"data":"SLN76370 Revision : 1","solName":"correct soldering","solId":"SLN76370","solRevision":"1","state":"open","parentid":"11","level":"3","children":[]}]}]},{"attributes":{"id":11,"rel":"folder"},"data":"FLT_49404 Revision : 1","faultName":"RS485 comm. Fail","faultId":"FLT_49404","state":"open","parentid":"10","level":"2","faultRevision":"1","children":[{"attributes":{"id":12,"rel":"folder"},"data":"SLN76370 Revision : 1","solName":"correct soldering","solId":"SLN76370","solRevision":"1","state":"open","parentid":"11","level":"3","children":[]}]},{"attributes":{"id":12,"rel":"folder"},"data":"SLN76370 Revision : 1","solName":"correct soldering","solId":"SLN76370","solRevision":"1","state":"open","parentid":"11","level":"3","children":[]},{"attributes":{"id":13,"rel":"folder"},"tag_id":"105","maxtag_id":"205","mintag_id":"305","resulttag_id":"405","data":"TST_20423 Revision : 1","testName":"Pts_5","testId":"TST_20423","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":14,"rel":"folder"},"data":"FLT_50895 Revision : 1","faultName":"Calibration fail","faultId":"FLT_50895","state":"open","parentid":"13","level":"2","faultRevision":"1","children":[{"attributes":{"id":15,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"14","level":"3","children":[]}]}]},{"attributes":{"id":14,"rel":"folder"},"data":"FLT_50895 Revision : 1","faultName":"Calibration fail","faultId":"FLT_50895","state":"open","parentid":"13","level":"2","faultRevision":"1","children":[{"attributes":{"id":15,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"14","level":"3","children":[]}]},{"attributes":{"id":15,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"14","level":"3","children":[]},{"attributes":{"id":16,"rel":"folder"},"tag_id":"106","maxtag_id":"206","mintag_id":"306","resulttag_id":"406","data":"TST_42014 Revision : 1","testName":"Pts_6","testId":"TST_42014","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":17,"rel":"folder"},"data":"FLT_49404 Revision : 1","faultName":"RS485 comm. Fail","faultId":"FLT_49404","state":"open","parentid":"16","level":"2","faultRevision":"1","children":[{"attributes":{"id":18,"rel":"folder"},"data":"SLN33270 Revision : 1","solName":"check shorting & load connection","solId":"SLN33270","solRevision":"1","state":"open","parentid":"17","level":"3","children":[]}]}]},{"attributes":{"id":17,"rel":"folder"},"data":"FLT_49404 Revision : 1","faultName":"RS485 comm. Fail","faultId":"FLT_49404","state":"open","parentid":"16","level":"2","faultRevision":"1","children":[{"attributes":{"id":18,"rel":"folder"},"data":"SLN33270 Revision : 1","solName":"check shorting & load connection","solId":"SLN33270","solRevision":"1","state":"open","parentid":"17","level":"3","children":[]}]},{"attributes":{"id":18,"rel":"folder"},"data":"SLN33270 Revision : 1","solName":"check shorting & load connection","solId":"SLN33270","solRevision":"1","state":"open","parentid":"17","level":"3","children":[]},{"attributes":{"id":19,"rel":"folder"},"tag_id":"107","maxtag_id":"207","mintag_id":"307","resulttag_id":"407","data":"TST_78837 Revision : 1","testName":"Pts_7","testId":"TST_78837","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":20,"rel":"folder"},"data":"FLT_67681 Revision : 1","faultName":"reverse protection failure","faultId":"FLT_67681","state":"open","parentid":"19","level":"2","faultRevision":"1","children":[{"attributes":{"id":21,"rel":"folder"},"data":"SLN76370 Revision : 1","solName":"correct soldering","solId":"SLN76370","solRevision":"1","state":"open","parentid":"20","level":"3","children":[]}]}]},{"attributes":{"id":20,"rel":"folder"},"data":"FLT_67681 Revision : 1","faultName":"reverse protection failure","faultId":"FLT_67681","state":"open","parentid":"19","level":"2","faultRevision":"1","children":[{"attributes":{"id":21,"rel":"folder"},"data":"SLN76370 Revision : 1","solName":"correct soldering","solId":"SLN76370","solRevision":"1","state":"open","parentid":"20","level":"3","children":[]}]},{"attributes":{"id":21,"rel":"folder"},"data":"SLN76370 Revision : 1","solName":"correct soldering","solId":"SLN76370","solRevision":"1","state":"open","parentid":"20","level":"3","children":[]},{"attributes":{"id":22,"rel":"folder"},"tag_id":"108","maxtag_id":"208","mintag_id":"308","resulttag_id":"408","data":"TST_18043 Revision : 1","testName":"Pts_8","testId":"TST_18043","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":23,"rel":"folder"},"data":"FLT_50895 Revision : 1","faultName":"Calibration fail","faultId":"FLT_50895","state":"open","parentid":"22","level":"2","faultRevision":"1","children":[{"attributes":{"id":24,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"23","level":"3","children":[]}]}]},{"attributes":{"id":23,"rel":"folder"},"data":"FLT_50895 Revision : 1","faultName":"Calibration fail","faultId":"FLT_50895","state":"open","parentid":"22","level":"2","faultRevision":"1","children":[{"attributes":{"id":24,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"23","level":"3","children":[]}]},{"attributes":{"id":24,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"23","level":"3","children":[]},{"attributes":{"id":25,"rel":"folder"},"tag_id":"109","maxtag_id":"209","mintag_id":"309","resulttag_id":"409","data":"TST_49743 Revision : 1","testName":"Pts_9","testId":"TST_49743","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":26,"rel":"folder"},"data":"FLT_49711 Revision : 1","faultName":"Dry solder","faultId":"FLT_49711","state":"open","parentid":"25","level":"2","faultRevision":"1","children":[{"attributes":{"id":27,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"26","level":"3","children":[]}]}]},{"attributes":{"id":26,"rel":"folder"},"data":"FLT_49711 Revision : 1","faultName":"Dry solder","faultId":"FLT_49711","state":"open","parentid":"25","level":"2","faultRevision":"1","children":[{"attributes":{"id":27,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"26","level":"3","children":[]}]},{"attributes":{"id":27,"rel":"folder"},"data":"SLN76000 Revision : 1","solName":"correct the polarity ","solId":"SLN76000","solRevision":"1","state":"open","parentid":"26","level":"3","children":[]},{"attributes":{"id":28,"rel":"folder"},"tag_id":"110","maxtag_id":"210","mintag_id":"310","resulttag_id":"410","data":"TST_33502 Revision : 1","testName":"Pts_10","testId":"TST_33502","testRevision":"1","state":"open","parentid":"0","level":"1","children":[{"attributes":{"id":29,"rel":"folder"},"data":"FLT_45039 Revision : 1","faultName":"polarity reversed","faultId":"FLT_45039","state":"open","parentid":"28","level":"2","faultRevision":"1","children":[{"attributes":{"id":30,"rel":"folder"},"data":"SLN68563 Revision : 1","solName":"remove track shorting","solId":"SLN68563","solRevision":"1","state":"open","parentid":"29","level":"3","children":[]}]}]},{"attributes":{"id":29,"rel":"folder"},"data":"FLT_45039 Revision : 1","faultName":"polarity reversed","faultId":"FLT_45039","state":"open","parentid":"28","level":"2","faultRevision":"1","children":[{"attributes":{"id":30,"rel":"folder"},"data":"SLN68563 Revision : 1","solName":"remove track shorting","solId":"SLN68563","solRevision":"1","state":"open","parentid":"29","level":"3","children":[]}]},{"attributes":{"id":30,"rel":"folder"},"data":"SLN68563 Revision : 1","solName":"remove track shorting","solId":"SLN68563","solRevision":"1","state":"open","parentid":"29","level":"3","children":[]}]', 'D', '2016-02-10 06:59:07', '1', 'revision_77547');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_images`
--

CREATE TABLE IF NOT EXISTS `tbl_images` (
  `image_id` int(50) NOT NULL AUTO_INCREMENT,
  `image_path` text NOT NULL,
  `image_object_id` int(50) NOT NULL,
  `image_object_type` varchar(100) NOT NULL,
  `image_object_rev` varchar(100) NOT NULL,
  `image_timestamp` datetime NOT NULL,
  `image_desc` text NOT NULL,
  `image_name` varchar(70) NOT NULL,
  PRIMARY KEY (`image_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=78 ;

--
-- Dumping data for table `tbl_images`
--

INSERT INTO `tbl_images` (`image_id`, `image_path`, `image_object_id`, `image_object_type`, `image_object_rev`, `image_timestamp`, `image_desc`, `image_name`) VALUES
(3, 'images/PTS test requirement_4th sept.xlsx', 2, 'solution', '', '2015-09-25 12:55:58', 'this is all about solution2', 'solution2'),
(4, 'images/example1.xls', 2, 'solution', '', '2015-09-25 12:55:04', 'this is all about solution3', 'solution11'),
(5, 'aa.jpg', 1, 'bug', '', '2015-09-08 00:00:00', 'great image', 'hello'),
(6, 'images/example.xls', 1, 'solution', '', '2015-10-30 01:57:20', 'dfdf', 'dfd'),
(7, 'images/12180094_1063132073731165_1665847105_n.jpg', 23, 'unit', '', '2015-11-05 05:44:48', 'Helllo how r uuu', 'Unit12'),
(8, 'images/image_preview.png', 23, 'unit', '', '2015-11-05 05:45:43', 'cndnxzhnj', 'hiiiii'),
(9, 'images/ctesting.sql', 15, 'unit', '', '2015-11-05 05:52:17', 'vvvvvvvvvvvvv', ''),
(10, 'images/ctesting.sql', 15, 'unit', '', '2015-11-05 05:56:25', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 'aaaaaaaa'),
(11, 'images/unit_excel-1.xls', 3, 'test', '', '2015-11-05 06:11:00', 'ddddddddddddddddddddddddddd', 'd'),
(12, 'images/12180094_1063132073731165_1665847105_n.jpg', 35, 'unit', '', '2015-11-05 06:24:35', 'ncjxzncxnxzjjk', 'hlo'),
(13, 'images/unit_excel-.xls', 8, 'fault', '', '2015-11-05 06:34:24', 'aaaaaaaaaaaaaaaaaa', 'aaaaaaaaaaaaa'),
(14, 'images/unit_excel-.xls', 1, 'solution', '', '2015-11-05 06:40:04', 'ggggggg', 'gggggggggggggggg'),
(16, 'images/unit_excel-1.xls', 1, 'bug', '', '2015-11-05 06:44:49', ';;;;;;;;;;;;;', ';;;;;;;;;;;;;;;;;;'),
(17, 'images/unit_excel-.xls', 7, 'ecn', '', '2015-11-05 06:54:29', 'aaaaaaaaaa', 'aaaaaaaaaaa'),
(18, 'images/Mr. Prem Sindhu.psd', 6, 'test', '', '2015-11-09 05:21:23', 'dfdsfdfdsf', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'),
(19, 'images/Mr. Prem Sindhu.psd', 36, 'unit', '', '2015-11-09 05:28:02', 'bbbbbbbbbbbbbbbb', 'bbbbbbbbbbbbsssssssssssssssssssssssssssssssssss'),
(20, 'images/sachtech.ai', 6, 'test', '', '2015-11-09 05:44:10', 'llllllllll', 'bbbbbbbbbbbb'),
(21, 'images/firefox.exe', 15, 'test', '', '2015-11-09 05:50:18', 'fffffffffffffffffffffffffffffffffffffff', 'ffffffffffffffffff'),
(22, 'images/firefox.exe', 12, 'test', '', '2015-11-09 06:16:17', 'ggggggggggggggggggggl', 'ggggggggggggggllllllllllllllllllllllllllllllllllllllllllllllllllllllll'),
(23, 'images/firefox.exe', 17, 'test', '', '2015-11-09 06:24:21', 'kkkkkkkkkk', 'kkkkkkk'),
(24, 'images/firefox.exe', 17, 'test', '', '2015-11-09 06:24:32', 'kkkkkkkkkk', 'kkkkkkk'),
(25, 'images/Unit.docx', 9, 'fault', '', '2015-11-09 11:25:52', 'gggggg', 'gggggggdddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd'),
(26, 'images/logo.png', 9, 'ecn', '', '2015-11-09 07:14:18', 'llllllllll', 'bbbbbbbbbbbb'),
(27, 'images/Unit.docx', 9, 'fault', '', '2015-11-09 08:03:57', 'aSAADAD', 'aaaa'),
(28, 'images/WinHTTrack.exe', 9, 'ecn', '', '2015-11-09 08:08:45', 'ertgererterte', 'rrrhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhfg'),
(29, 'images/logo.png', 40, 'unit', '', '2015-11-09 12:23:12', 'kkkkkkkkff', 'kkkkkkkkkff'),
(30, 'images/logo.png', 21, 'test', '', '2015-11-09 01:04:28', 'sssssjjj', 'ssssjj'),
(31, 'images/httrack.exe', 8, 'test', '', '2015-11-14 05:46:07', 'fdf', 'dfd'),
(32, 'images/httrack-doc.html', 8, 'test', '', '2015-11-09 01:17:01', 'sd', 'sd'),
(33, 'images/file_id.diz', 8, 'test', '', '2015-11-09 01:17:45', 'sds', 'sdsdsd'),
(34, 'images/file_id.diz', 8, 'test', '', '2015-11-09 01:20:29', 'dfdf', 'dfdf'),
(35, 'images/example.xls', 12, 'test', '', '2015-11-09 01:22:35', 'sdsd', 'dsdsdsdsdssds'),
(36, 'images/example.xls', 40, 'unit', '', '2015-11-09 01:26:15', 'sds', 'sd'),
(37, 'images/example.xls', 40, 'unit', '', '2015-11-09 01:29:32', 'sds', 'dsdsds'),
(38, 'images/example.xls', 40, 'unit', '', '2015-11-09 01:30:40', 'sdsd', 'sds'),
(39, 'images/example1.xls', 8, 'test', '', '2015-11-09 01:41:16', 'sds', 'sds'),
(40, 'images/example1.xls', 8, 'test', '', '2015-11-09 01:41:23', 'sds', 'sds'),
(41, 'images/example1.xls', 8, 'test', '', '2015-11-09 01:41:57', 'sds', 'sds'),
(42, 'images/DesktopWallpkkaper-2.png', 42, 'unit', '', '2015-11-10 06:07:46', 'ddddddddddddd', 'ddddddddddd'),
(43, 'images/DesktopWallpkkaper-2.png', 14, 'unit', '', '2015-11-16 04:38:59', 'ssssssssssss', 'kakka'),
(44, 'images/logo.png', 5, 'solution', '', '2015-11-10 06:37:27', 'sssssssss', 'sssssssss'),
(46, 'images/jass3.jpg', 14, 'unit', '', '2015-11-16 04:29:37', 'aaaaaaaaaaa', 'aaaaaaaaaaa'),
(48, 'images/jass3.jpg', 12, 'test', '', '2015-11-16 04:30:14', 'assdd', 'sssssssa'),
(49, 'images/jass2.jpg', 14, 'test', '', '2015-11-16 04:30:56', 'aaaaaaaa', 'aaaaaa'),
(51, 'images/jass3.jpg', 4, 'solution', '', '2015-11-16 04:31:43', 'sads', 'saaaaaaa'),
(52, 'images/jass1.jpg', 6, 'ecn', '', '2015-11-16 11:35:29', 'sssssssss', 'sssssss'),
(53, 'images/jass3.jpg', 6, 'unit', '', '2015-11-16 04:42:01', 'lll', 'lllll'),
(54, 'images/download_excal_file_Reoprt.xls', 4, 'solution', '', '2015-11-16 10:52:26', 'sadsa', 'tyt'),
(55, 'images/android-free-wallpapers.jpg', 14, 'unit', '', '2015-11-16 11:14:51', 'ffffffff', 'ffffffff'),
(56, 'images/netbeans.exe', 7, 'unit', '', '2015-11-16 11:17:31', 'eeee', 'lll'),
(57, 'images/Tulips.jpg', 4, 'bug', '', '2015-11-16 11:30:24', 'fffffff', 'ffffff'),
(58, 'images/Tulips.jpg', 4, 'bug', '', '2015-11-16 11:30:24', 'fffffff', 'ffffff'),
(59, 'images/Tulips.jpg', 5, 'bug', '', '2015-11-16 11:31:37', 'sssssssssssss', 'sssssssssssssssssssssssssss'),
(60, 'images/netbeans.exe', 24, 'fault', '', '2015-11-23 06:06:19', 'dgtrffg', 'aaaasadsadasdasdadadasbfcttttttttttttttttttttttttttttttttttttttttttttt'),
(62, 'images/Backup of 600W DC R4 System.PDF', 62, 'unit', '', '2015-11-24 04:20:21', 'FILE', 'DATASHEET'),
(64, 'images/netbeans.exe', 63, 'unit', '', '2015-11-26 07:51:35', 'beans', 'net'),
(65, 'images/example1.xls', 62, 'unit', '', '2015-11-26 12:42:30', 'sdsdsdsdsd', 'dsdsdsdsdssds'),
(66, 'images/example.xls', 63, 'unit', '', '2015-11-26 01:00:28', 'sdsd', 'dsdsdsdsdssds'),
(68, 'images/example1.xls', 12, 'design', '', '2015-11-27 11:36:03', 'sdsds', 'dsdsdsdsdssds'),
(69, 'images/netbeans', 12, 'design', '', '2015-11-27 11:42:40', 'fff', 'ff'),
(70, 'images/PTS test requirement_4th sept.xlsx', 12, 'design', '1', '2015-12-02 01:22:08', 'sdsdsd', 'dsdsdsdsdssds'),
(71, 'images/example1.xls', 12, 'design', '1', '2015-12-02 01:22:30', 'sdssdsd', 'ssddsd'),
(73, 'images/lang-1032.dll', 78, 'test', '', '2015-12-30 10:44:50', ' qwe', '1'),
(74, 'images/lang-1036.dll', 78, 'test', '', '2015-12-30 10:52:20', 'ASDASD', 'a'),
(75, 'images/lang-1026.dll', 93, 'unit', '0', '2015-12-30 10:54:28', 's', 'sa'),
(76, 'images/lang-1030.dll', 78, 'test', '1', '2015-12-30 10:56:32', 'wer', 'qw'),
(77, 'images/web bug status.xlsx', 14, 'ecn', '1', '2016-01-06 09:28:43', 'hjkj', 'f');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_process`
--

CREATE TABLE IF NOT EXISTS `tbl_process` (
  `process_id` int(50) NOT NULL AUTO_INCREMENT,
  `process_unique` varchar(100) NOT NULL,
  `process_name` varchar(100) NOT NULL,
  `process_desc` text NOT NULL,
  `process_unit` varchar(100) NOT NULL,
  `process_status` varchar(100) NOT NULL,
  `process_revision` varchar(3) NOT NULL,
  `process_user` varchar(100) NOT NULL,
  `process_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`process_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `tbl_process`
--

INSERT INTO `tbl_process` (`process_id`, `process_unique`, `process_name`, `process_desc`, `process_unit`, `process_status`, `process_revision`, `process_user`, `process_timestamp`) VALUES
(20, 'process_13588', 'fffff', 'uuuuuuuuuuyyyyy', 'UNT_16699', 'D', '1', '1', '2016-02-20 08:38:28');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_process_item`
--

CREATE TABLE IF NOT EXISTS `tbl_process_item` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_unique` varchar(100) NOT NULL,
  `item_phase` varchar(100) NOT NULL,
  `item_format_unique` varchar(100) NOT NULL,
  `item_process_id` varchar(100) NOT NULL,
  `item_process_revision` varchar(100) NOT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=60 ;

--
-- Dumping data for table `tbl_process_item`
--

INSERT INTO `tbl_process_item` (`item_id`, `item_unique`, `item_phase`, `item_format_unique`, `item_process_id`, `item_process_revision`) VALUES
(57, 'item_83886', 'Phase 1', 'format_59624', '20', '1'),
(58, 'item_83886', 'Phase 2', 'format_84294', '20', '1'),
(59, 'item_83886', 'Phase 3', 'format_25555', '20', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_process_revision`
--

CREATE TABLE IF NOT EXISTS `tbl_process_revision` (
  `rev_id` int(100) NOT NULL AUTO_INCREMENT,
  `rev_unique` varchar(100) NOT NULL,
  `rev_process_id` varchar(100) NOT NULL,
  `rev_process_unique` varchar(100) NOT NULL,
  `rev_process_name` varchar(100) NOT NULL,
  `rev_process_desc` text NOT NULL,
  `rev_process_unit` varchar(100) NOT NULL,
  `rev_process_status` varchar(100) NOT NULL,
  `rev_process_revision` varchar(3) NOT NULL,
  `rev_process_user` varchar(100) NOT NULL,
  `rev_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`rev_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `tbl_process_revision`
--

INSERT INTO `tbl_process_revision` (`rev_id`, `rev_unique`, `rev_process_id`, `rev_process_unique`, `rev_process_name`, `rev_process_desc`, `rev_process_unit`, `rev_process_status`, `rev_process_revision`, `rev_process_user`, `rev_timestamp`) VALUES
(32, 'revision_61606', '20', 'process_13588', 'fffff', 'uuuuuuuuuu', 'UNT_16699', 'D', '1', '1', '2016-02-20 08:38:02');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_child`
--

CREATE TABLE IF NOT EXISTS `tbl_product_child` (
  `pchild_id` int(100) NOT NULL AUTO_INCREMENT,
  `pchild_parent_id` varchar(100) NOT NULL,
  `pchild_child_id` varchar(100) NOT NULL,
  `pchild_child_revision` varchar(100) NOT NULL,
  `pchild_child_name` varchar(100) NOT NULL,
  `pchild_tree_id` varchar(100) NOT NULL,
  `pchild_quantity` varchar(100) NOT NULL,
  `pchild_timestamp` date NOT NULL,
  `pchild_tree_revision` varchar(100) NOT NULL,
  PRIMARY KEY (`pchild_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=161 ;

--
-- Dumping data for table `tbl_product_child`
--

INSERT INTO `tbl_product_child` (`pchild_id`, `pchild_parent_id`, `pchild_child_id`, `pchild_child_revision`, `pchild_child_name`, `pchild_tree_id`, `pchild_quantity`, `pchild_timestamp`, `pchild_tree_revision`) VALUES
(160, 'UNT_16699', 'UNT_66489', '1', 'UCC', '67', '1', '2016-02-20', '1'),
(159, 'UNT_16699', 'UNT_21151', '1', 'Vsat power', '67', '1', '2016-02-20', '1'),
(158, 'UNT_38882', 'UNT_38882', '1', 'Switch', '67', '2', '2016-02-20', '1'),
(157, 'UNT_65010', 'UNT_53072', '1', 'ECU', '67', '1', '2016-02-20', '1'),
(156, 'UNT_94779', 'UNT_16699', '1', 'CMU', '67', '1', '2016-02-20', '1'),
(155, 'UNT_94779', 'UNT_38882', '1', 'Switch', '67', '1', '2016-02-20', '1'),
(154, 'UNT_94779', 'UNT_65010', '1', 'SPD', '67', '12', '2016-02-20', '1'),
(153, '0', 'UNT_94779', '1', 'ADP power', '67', '', '2016-02-20', '1'),
(144, 'UNT_54331', 'UNT_82185', '1', 'UCC3', '62', '1', '2016-02-08', '1'),
(143, 'UNT_54331', 'UNT_95456', '1', 'backplane3', '62', '1', '2016-02-08', '1'),
(142, 'UNT_40978', 'UNT_54331', '1', 'ADP power3', '62', '1', '2016-02-08', '1'),
(141, 'UNT_40978', 'UNT_89953', '1', 'MPPT3', '62', '1', '2016-02-08', '1'),
(140, 'UNT_40978', 'UNT_88272', '1', 'Backhaul3', '62', '12', '2016-02-08', '1'),
(139, '0', 'UNT_40978', '1', 'SPD3', '62', '', '2016-02-08', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_revision`
--

CREATE TABLE IF NOT EXISTS `tbl_product_revision` (
  `rev_id` int(100) NOT NULL AUTO_INCREMENT,
  `rev_unique_id` varchar(100) NOT NULL,
  `rev_tree_id` varchar(100) NOT NULL,
  `rev_tree_unique` varchar(100) NOT NULL,
  `rev_tree_unit` varchar(100) NOT NULL,
  `rev_tree_name` varchar(100) NOT NULL,
  `rev_tree_revision` varchar(100) NOT NULL,
  `rev_tree_data` text NOT NULL,
  `rev_tree_units` text NOT NULL,
  `rev_tree_timestamp` datetime NOT NULL,
  `rev_tree_status` varchar(100) NOT NULL,
  `rev_tree_userid` varchar(100) NOT NULL,
  PRIMARY KEY (`rev_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=64 ;

--
-- Dumping data for table `tbl_product_revision`
--

INSERT INTO `tbl_product_revision` (`rev_id`, `rev_unique_id`, `rev_tree_id`, `rev_tree_unique`, `rev_tree_unit`, `rev_tree_name`, `rev_tree_revision`, `rev_tree_data`, `rev_tree_units`, `rev_tree_timestamp`, `rev_tree_status`, `rev_tree_userid`) VALUES
(58, 'revision_27542', '62', 'tree_52911', 'UNT_40978', 'testing tree here', '1', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_40978 Revision : 1","unitRevision":1,"unitName":"SPD3","unitId":"UNT_40978","parentid":null,"quantity":2,"state":"open","children":[{"attributes":{"id":1,"rel":"folder"},"data":"UNT_88272 Revision : 1","unitRevision":1,"unitName":"Backhaul3","unitId":"UNT_88272","parentid":0,"quantity":12,"state":"open","children":[]},{"attributes":{"id":2,"rel":"folder"},"data":"UNT_89953 Revision : 1","unitRevision":1,"unitName":"MPPT3","unitId":"UNT_89953","parentid":0,"quantity":1,"state":"open","children":[]},{"attributes":{"id":3,"rel":"folder"},"data":"UNT_54331 Revision : 1","unitRevision":1,"unitName":"ADP power3","unitId":"UNT_54331","parentid":0,"quantity":1,"state":"open","children":[{"attributes":{"id":4,"rel":"folder"},"data":"UNT_95456 Revision : 1","unitRevision":1,"unitName":"backplane3","unitId":"UNT_95456","parentid":3,"quantity":1,"state":"open","children":[]},{"attributes":{"id":5,"rel":"folder"},"data":"UNT_82185 Revision : 1","unitRevision":1,"unitName":"UCC3","unitId":"UNT_82185","parentid":3,"quantity":1,"state":"open","children":[]}]}]}]', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_40978 Revision : 1","unitRevision":1,"unitName":"SPD3","unitId":"UNT_40978","parentid":null,"quantity":2,"state":"open"},{"attributes":{"id":1,"rel":"folder"},"data":"UNT_88272 Revision : 1","unitRevision":1,"unitName":"Backhaul3","unitId":"UNT_88272","parentid":0,"quantity":12,"state":"open"},{"attributes":{"id":2,"rel":"folder"},"data":"UNT_89953 Revision : 1","unitRevision":1,"unitName":"MPPT3","unitId":"UNT_89953","parentid":0,"quantity":1,"state":"open"},{"attributes":{"id":3,"rel":"folder"},"data":"UNT_54331 Revision : 1","unitRevision":1,"unitName":"ADP power3","unitId":"UNT_54331","parentid":0,"quantity":1,"state":"open"},{"attributes":{"id":4,"rel":"folder"},"data":"UNT_95456 Revision : 1","unitRevision":1,"unitName":"backplane3","unitId":"UNT_95456","parentid":3,"quantity":1,"state":"open"},{"attributes":{"id":5,"rel":"folder"},"data":"UNT_82185 Revision : 1","unitRevision":1,"unitName":"UCC3","unitId":"UNT_82185","parentid":3,"quantity":1,"state":"open"}]', '2016-02-08 06:32:24', 'D', '1'),
(59, 'revision_28884', '63', 'tree_33053', 'UNT_65010', 'eeeeeeeeeee', '1', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_59840 Revision : 1","unitRevision":1,"unitName":"Backhaul","unitId":"UNT_59840","parentid":null,"quantity":2,"state":"open"}]', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_59840 Revision : 1","unitRevision":1,"unitName":"Backhaul","unitId":"UNT_59840","parentid":null,"quantity":2,"state":"open"}]', '2016-02-20 06:14:25', 'DELETE', '1'),
(60, 'revision_69394', '64', 'tree_48781', 'UNT_65010', 'AAA', '1', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_59840 Revision : 1","unitRevision":1,"unitName":"Backhaul","unitId":"UNT_59840","parentid":null,"quantity":2,"state":"open","children":[{"attributes":{"id":1,"rel":"folder"},"data":"UNT_94077 Revision : 1","unitRevision":1,"unitName":"MPPT","unitId":"UNT_94077","parentid":0,"quantity":12,"state":"open","children":[]},{"attributes":{"id":2,"rel":"folder"},"data":"UNT_94779 Revision : 1","unitRevision":1,"unitName":"ADP power","unitId":"UNT_94779","parentid":0,"quantity":1,"state":"open","children":[]},{"attributes":{"id":3,"rel":"folder"},"data":"UNT_57878 Revision : 1","unitRevision":1,"unitName":"backplane","unitId":"UNT_57878","parentid":0,"quantity":1,"state":"open","children":[{"attributes":{"id":4,"rel":"folder"},"data":"UNT_66489 Revision : 1","unitRevision":1,"unitName":"UCC","unitId":"UNT_66489","parentid":3,"quantity":1,"state":"open","children":[]},{"attributes":{"id":5,"rel":"folder"},"data":"UNT_38882 Revision : 1","unitRevision":1,"unitName":"Switch","unitId":"UNT_38882","parentid":3,"quantity":1,"state":"open","children":[]}]}]}]', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_59840 Revision : 1","unitRevision":1,"unitName":"Backhaul","unitId":"UNT_59840","parentid":null,"quantity":2,"state":"open"},{"attributes":{"id":1,"rel":"folder"},"data":"UNT_94077 Revision : 1","unitRevision":1,"unitName":"MPPT","unitId":"UNT_94077","parentid":0,"quantity":12,"state":"open"},{"attributes":{"id":2,"rel":"folder"},"data":"UNT_94779 Revision : 1","unitRevision":1,"unitName":"ADP power","unitId":"UNT_94779","parentid":0,"quantity":1,"state":"open"},{"attributes":{"id":3,"rel":"folder"},"data":"UNT_57878 Revision : 1","unitRevision":1,"unitName":"backplane","unitId":"UNT_57878","parentid":0,"quantity":1,"state":"open"},{"attributes":{"id":4,"rel":"folder"},"data":"UNT_66489 Revision : 1","unitRevision":1,"unitName":"UCC","unitId":"UNT_66489","parentid":3,"quantity":1,"state":"open"},{"attributes":{"id":5,"rel":"folder"},"data":"UNT_38882 Revision : 1","unitRevision":1,"unitName":"Switch","unitId":"UNT_38882","parentid":3,"quantity":1,"state":"open"}]', '2016-02-20 06:16:30', 'DELETE', '1'),
(61, 'revision_84776', '65', 'tree_69512', 'UNT_65010', 'AAA', '1', '[]', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_59840 Revision : 1","unitRevision":1,"unitName":"Backhaul","unitId":"UNT_59840","parentid":0,"quantity":2,"state":"open"},{"attributes":{"id":1,"rel":"folder"},"data":"UNT_94077 Revision : 1","unitRevision":1,"unitName":"MPPT","unitId":"UNT_94077","parentid":1,"quantity":12,"state":"open"},{"attributes":{"id":2,"rel":"folder"},"data":"UNT_94779 Revision : 1","unitRevision":1,"unitName":"ADP power","unitId":"UNT_94779","parentid":1,"quantity":1,"state":"open"},{"attributes":{"id":3,"rel":"folder"},"data":"UNT_57878 Revision : 1","unitRevision":1,"unitName":"backplane","unitId":"UNT_57878","parentid":2,"quantity":1,"state":"open"},{"attributes":{"id":4,"rel":"folder"},"data":"UNT_66489 Revision : 1","unitRevision":1,"unitName":"UCC","unitId":"UNT_66489","parentid":2,"quantity":1,"state":"open"},{"attributes":{"id":5,"rel":"folder"},"data":"UNT_38882 Revision : 1","unitRevision":1,"unitName":"Switch","unitId":"UNT_38882","parentid":3,"quantity":1,"state":"open"}]', '2016-02-20 06:17:53', 'DELETE', '1'),
(62, 'revision_92091', '66', 'tree_94773', 'UNT_65010', 'AAA', '1', '[]', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_59840 Revision : 1","unitRevision":1,"unitName":"Backhaul","unitId":"UNT_59840","parentid":0,"quantity":2,"state":"open"},{"attributes":{"id":1,"rel":"folder"},"data":"UNT_94077 Revision : 1","unitRevision":1,"unitName":"MPPT","unitId":"UNT_94077","parentid":1,"quantity":12,"state":"open"},{"attributes":{"id":2,"rel":"folder"},"data":"UNT_94779 Revision : 1","unitRevision":1,"unitName":"ADP power","unitId":"UNT_94779","parentid":1,"quantity":1,"state":"open"},{"attributes":{"id":3,"rel":"folder"},"data":"UNT_57878 Revision : 1","unitRevision":1,"unitName":"backplane","unitId":"UNT_57878","parentid":2,"quantity":1,"state":"open"},{"attributes":{"id":4,"rel":"folder"},"data":"UNT_66489 Revision : 1","unitRevision":1,"unitName":"UCC","unitId":"UNT_66489","parentid":2,"quantity":1,"state":"open"},{"attributes":{"id":5,"rel":"folder"},"data":"UNT_38882 Revision : 1","unitRevision":1,"unitName":"Switch","unitId":"UNT_38882","parentid":3,"quantity":1,"state":"open"}]', '2016-02-20 06:24:10', 'DELETE', '1'),
(63, 'revision_37654', '67', 'tree_59258', 'UNT_94077', 'power system R3', '1', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_94779 Revision : 1","unitRevision":1,"unitName":"ADP power","unitId":"UNT_94779","parentid":null,"quantity":2,"state":"open","children":[{"attributes":{"id":1,"rel":"folder"},"data":"UNT_65010 Revision : 1","unitRevision":1,"unitName":"SPD","unitId":"UNT_65010","parentid":0,"quantity":12,"state":"open","children":[{"attributes":{"id":4,"rel":"folder"},"data":"UNT_53072 Revision : 1","unitRevision":1,"unitName":"ECU","unitId":"UNT_53072","parentid":1,"quantity":1,"state":"open","children":[]}]},{"attributes":{"id":2,"rel":"folder"},"data":"UNT_38882 Revision : 1","unitRevision":1,"unitName":"Switch","unitId":"UNT_38882","parentid":0,"quantity":1,"state":"open","children":[{"attributes":{"id":6,"rel":"folder"},"data":"UNT_38882 Revision : 1","unitRevision":1,"unitName":"Switch","unitId":"UNT_38882","parentid":2,"quantity":2,"state":"open","children":[]}]},{"attributes":{"id":3,"rel":"folder"},"data":"UNT_16699 Revision : 1","unitRevision":1,"unitName":"CMU","unitId":"UNT_16699","parentid":0,"quantity":1,"state":"open","children":[{"attributes":{"id":5,"rel":"folder"},"data":"UNT_21151 Revision : 1","unitRevision":1,"unitName":"Vsat power","unitId":"UNT_21151","parentid":3,"quantity":1,"state":"open","children":[]},{"attributes":{"id":7,"rel":"folder"},"data":"UNT_66489 Revision : 1","unitRevision":1,"unitName":"UCC","unitId":"UNT_66489","parentid":3,"quantity":1,"state":"open","children":[]}]}]}]', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_94779 Revision : 1","unitRevision":1,"unitName":"ADP power","unitId":"UNT_94779","parentid":null,"quantity":2,"state":"open"},{"attributes":{"id":1,"rel":"folder"},"data":"UNT_65010 Revision : 1","unitRevision":1,"unitName":"SPD","unitId":"UNT_65010","parentid":0,"quantity":12,"state":"open"},{"attributes":{"id":2,"rel":"folder"},"data":"UNT_38882 Revision : 1","unitRevision":1,"unitName":"Switch","unitId":"UNT_38882","parentid":0,"quantity":1,"state":"open"},{"attributes":{"id":3,"rel":"folder"},"data":"UNT_16699 Revision : 1","unitRevision":1,"unitName":"CMU","unitId":"UNT_16699","parentid":0,"quantity":1,"state":"open"},{"attributes":{"id":4,"rel":"folder"},"data":"UNT_53072 Revision : 1","unitRevision":1,"unitName":"ECU","unitId":"UNT_53072","parentid":1,"quantity":1,"state":"open"},{"attributes":{"id":5,"rel":"folder"},"data":"UNT_21151 Revision : 1","unitRevision":1,"unitName":"Vsat power","unitId":"UNT_21151","parentid":3,"quantity":1,"state":"open"},{"attributes":{"id":6,"rel":"folder"},"data":"UNT_38882 Revision : 1","unitRevision":1,"unitName":"Switch","unitId":"UNT_38882","parentid":2,"quantity":2,"state":"open"},{"attributes":{"id":7,"rel":"folder"},"data":"UNT_66489 Revision : 1","unitRevision":1,"unitName":"UCC","unitId":"UNT_66489","parentid":3,"quantity":1,"state":"open"}]', '2016-02-20 06:28:21', 'D', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_tree`
--

CREATE TABLE IF NOT EXISTS `tbl_product_tree` (
  `tree_id` int(50) NOT NULL AUTO_INCREMENT,
  `tree_name` varchar(100) NOT NULL,
  `tree_unique` varchar(100) NOT NULL,
  `tree_revision` varchar(3) NOT NULL,
  `tree_unit` varchar(100) NOT NULL,
  `tree_data` text NOT NULL,
  `tree_units` text NOT NULL,
  `tree_status` varchar(50) NOT NULL,
  `tree_timestamp` datetime NOT NULL,
  `tree_userid` varchar(100) NOT NULL,
  PRIMARY KEY (`tree_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=68 ;

--
-- Dumping data for table `tbl_product_tree`
--

INSERT INTO `tbl_product_tree` (`tree_id`, `tree_name`, `tree_unique`, `tree_revision`, `tree_unit`, `tree_data`, `tree_units`, `tree_status`, `tree_timestamp`, `tree_userid`) VALUES
(62, 'testing tree here', 'tree_52911', '1', 'UNT_40978', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_40978 Revision : 1","unitRevision":1,"unitName":"SPD3","unitId":"UNT_40978","parentid":null,"quantity":2,"state":"open","children":[{"attributes":{"id":1,"rel":"folder"},"data":"UNT_88272 Revision : 1","unitRevision":1,"unitName":"Backhaul3","unitId":"UNT_88272","parentid":0,"quantity":12,"state":"open","children":[]},{"attributes":{"id":2,"rel":"folder"},"data":"UNT_89953 Revision : 1","unitRevision":1,"unitName":"MPPT3","unitId":"UNT_89953","parentid":0,"quantity":1,"state":"open","children":[]},{"attributes":{"id":3,"rel":"folder"},"data":"UNT_54331 Revision : 1","unitRevision":1,"unitName":"ADP power3","unitId":"UNT_54331","parentid":0,"quantity":1,"state":"open","children":[{"attributes":{"id":4,"rel":"folder"},"data":"UNT_95456 Revision : 1","unitRevision":1,"unitName":"backplane3","unitId":"UNT_95456","parentid":3,"quantity":1,"state":"open","children":[]},{"attributes":{"id":5,"rel":"folder"},"data":"UNT_82185 Revision : 1","unitRevision":1,"unitName":"UCC3","unitId":"UNT_82185","parentid":3,"quantity":1,"state":"open","children":[]}]}]}]', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_40978 Revision : 1","unitRevision":1,"unitName":"SPD3","unitId":"UNT_40978","parentid":null,"quantity":2,"state":"open"},{"attributes":{"id":1,"rel":"folder"},"data":"UNT_88272 Revision : 1","unitRevision":1,"unitName":"Backhaul3","unitId":"UNT_88272","parentid":0,"quantity":12,"state":"open"},{"attributes":{"id":2,"rel":"folder"},"data":"UNT_89953 Revision : 1","unitRevision":1,"unitName":"MPPT3","unitId":"UNT_89953","parentid":0,"quantity":1,"state":"open"},{"attributes":{"id":3,"rel":"folder"},"data":"UNT_54331 Revision : 1","unitRevision":1,"unitName":"ADP power3","unitId":"UNT_54331","parentid":0,"quantity":1,"state":"open"},{"attributes":{"id":4,"rel":"folder"},"data":"UNT_95456 Revision : 1","unitRevision":1,"unitName":"backplane3","unitId":"UNT_95456","parentid":3,"quantity":1,"state":"open"},{"attributes":{"id":5,"rel":"folder"},"data":"UNT_82185 Revision : 1","unitRevision":1,"unitName":"UCC3","unitId":"UNT_82185","parentid":3,"quantity":1,"state":"open"}]', 'D', '2016-02-08 06:32:24', '1'),
(63, 'eeeeeeeeeee', 'tree_33053', '1', 'UNT_65010', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_59840 Revision : 1","unitRevision":1,"unitName":"Backhaul","unitId":"UNT_59840","parentid":null,"quantity":2,"state":"open"}]', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_59840 Revision : 1","unitRevision":1,"unitName":"Backhaul","unitId":"UNT_59840","parentid":null,"quantity":2,"state":"open"}]', 'DELETE', '2016-02-20 06:14:25', '1'),
(64, 'AAA', 'tree_48781', '1', 'UNT_65010', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_59840 Revision : 1","unitRevision":1,"unitName":"Backhaul","unitId":"UNT_59840","parentid":null,"quantity":2,"state":"open","children":[{"attributes":{"id":1,"rel":"folder"},"data":"UNT_94077 Revision : 1","unitRevision":1,"unitName":"MPPT","unitId":"UNT_94077","parentid":0,"quantity":12,"state":"open","children":[]},{"attributes":{"id":2,"rel":"folder"},"data":"UNT_94779 Revision : 1","unitRevision":1,"unitName":"ADP power","unitId":"UNT_94779","parentid":0,"quantity":1,"state":"open","children":[]},{"attributes":{"id":3,"rel":"folder"},"data":"UNT_57878 Revision : 1","unitRevision":1,"unitName":"backplane","unitId":"UNT_57878","parentid":0,"quantity":1,"state":"open","children":[{"attributes":{"id":4,"rel":"folder"},"data":"UNT_66489 Revision : 1","unitRevision":1,"unitName":"UCC","unitId":"UNT_66489","parentid":3,"quantity":1,"state":"open","children":[]},{"attributes":{"id":5,"rel":"folder"},"data":"UNT_38882 Revision : 1","unitRevision":1,"unitName":"Switch","unitId":"UNT_38882","parentid":3,"quantity":1,"state":"open","children":[]}]}]}]', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_59840 Revision : 1","unitRevision":1,"unitName":"Backhaul","unitId":"UNT_59840","parentid":null,"quantity":2,"state":"open"},{"attributes":{"id":1,"rel":"folder"},"data":"UNT_94077 Revision : 1","unitRevision":1,"unitName":"MPPT","unitId":"UNT_94077","parentid":0,"quantity":12,"state":"open"},{"attributes":{"id":2,"rel":"folder"},"data":"UNT_94779 Revision : 1","unitRevision":1,"unitName":"ADP power","unitId":"UNT_94779","parentid":0,"quantity":1,"state":"open"},{"attributes":{"id":3,"rel":"folder"},"data":"UNT_57878 Revision : 1","unitRevision":1,"unitName":"backplane","unitId":"UNT_57878","parentid":0,"quantity":1,"state":"open"},{"attributes":{"id":4,"rel":"folder"},"data":"UNT_66489 Revision : 1","unitRevision":1,"unitName":"UCC","unitId":"UNT_66489","parentid":3,"quantity":1,"state":"open"},{"attributes":{"id":5,"rel":"folder"},"data":"UNT_38882 Revision : 1","unitRevision":1,"unitName":"Switch","unitId":"UNT_38882","parentid":3,"quantity":1,"state":"open"}]', 'DELETE', '2016-02-20 06:16:30', '1'),
(65, 'AAA', 'tree_69512', '1', 'UNT_65010', '[]', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_59840 Revision : 1","unitRevision":1,"unitName":"Backhaul","unitId":"UNT_59840","parentid":0,"quantity":2,"state":"open"},{"attributes":{"id":1,"rel":"folder"},"data":"UNT_94077 Revision : 1","unitRevision":1,"unitName":"MPPT","unitId":"UNT_94077","parentid":1,"quantity":12,"state":"open"},{"attributes":{"id":2,"rel":"folder"},"data":"UNT_94779 Revision : 1","unitRevision":1,"unitName":"ADP power","unitId":"UNT_94779","parentid":1,"quantity":1,"state":"open"},{"attributes":{"id":3,"rel":"folder"},"data":"UNT_57878 Revision : 1","unitRevision":1,"unitName":"backplane","unitId":"UNT_57878","parentid":2,"quantity":1,"state":"open"},{"attributes":{"id":4,"rel":"folder"},"data":"UNT_66489 Revision : 1","unitRevision":1,"unitName":"UCC","unitId":"UNT_66489","parentid":2,"quantity":1,"state":"open"},{"attributes":{"id":5,"rel":"folder"},"data":"UNT_38882 Revision : 1","unitRevision":1,"unitName":"Switch","unitId":"UNT_38882","parentid":3,"quantity":1,"state":"open"}]', 'DELETE', '2016-02-20 06:17:53', '1'),
(66, 'AAA', 'tree_94773', '1', 'UNT_65010', '[]', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_59840 Revision : 1","unitRevision":1,"unitName":"Backhaul","unitId":"UNT_59840","parentid":0,"quantity":2,"state":"open"},{"attributes":{"id":1,"rel":"folder"},"data":"UNT_94077 Revision : 1","unitRevision":1,"unitName":"MPPT","unitId":"UNT_94077","parentid":1,"quantity":12,"state":"open"},{"attributes":{"id":2,"rel":"folder"},"data":"UNT_94779 Revision : 1","unitRevision":1,"unitName":"ADP power","unitId":"UNT_94779","parentid":1,"quantity":1,"state":"open"},{"attributes":{"id":3,"rel":"folder"},"data":"UNT_57878 Revision : 1","unitRevision":1,"unitName":"backplane","unitId":"UNT_57878","parentid":2,"quantity":1,"state":"open"},{"attributes":{"id":4,"rel":"folder"},"data":"UNT_66489 Revision : 1","unitRevision":1,"unitName":"UCC","unitId":"UNT_66489","parentid":2,"quantity":1,"state":"open"},{"attributes":{"id":5,"rel":"folder"},"data":"UNT_38882 Revision : 1","unitRevision":1,"unitName":"Switch","unitId":"UNT_38882","parentid":3,"quantity":1,"state":"open"}]', 'DELETE', '2016-02-20 06:24:10', '1'),
(67, 'power system R3', 'tree_59258', '1', 'UNT_94077', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_94779 Revision : 1","unitRevision":1,"unitName":"ADP power","unitId":"UNT_94779","parentid":null,"quantity":2,"state":"open","children":[{"attributes":{"id":1,"rel":"folder"},"data":"UNT_65010 Revision : 1","unitRevision":1,"unitName":"SPD","unitId":"UNT_65010","parentid":0,"quantity":12,"state":"open","children":[{"attributes":{"id":4,"rel":"folder"},"data":"UNT_53072 Revision : 1","unitRevision":1,"unitName":"ECU","unitId":"UNT_53072","parentid":1,"quantity":1,"state":"open","children":[]}]},{"attributes":{"id":2,"rel":"folder"},"data":"UNT_38882 Revision : 1","unitRevision":1,"unitName":"Switch","unitId":"UNT_38882","parentid":0,"quantity":1,"state":"open","children":[{"attributes":{"id":6,"rel":"folder"},"data":"UNT_38882 Revision : 1","unitRevision":1,"unitName":"Switch","unitId":"UNT_38882","parentid":2,"quantity":2,"state":"open","children":[]}]},{"attributes":{"id":3,"rel":"folder"},"data":"UNT_16699 Revision : 1","unitRevision":1,"unitName":"CMU","unitId":"UNT_16699","parentid":0,"quantity":1,"state":"open","children":[{"attributes":{"id":5,"rel":"folder"},"data":"UNT_21151 Revision : 1","unitRevision":1,"unitName":"Vsat power","unitId":"UNT_21151","parentid":3,"quantity":1,"state":"open","children":[]},{"attributes":{"id":7,"rel":"folder"},"data":"UNT_66489 Revision : 1","unitRevision":1,"unitName":"UCC","unitId":"UNT_66489","parentid":3,"quantity":1,"state":"open","children":[]}]}]}]', '[{"attributes":{"id":0,"rel":"folder"},"data":"UNT_94779 Revision : 1","unitRevision":1,"unitName":"ADP power","unitId":"UNT_94779","parentid":null,"quantity":2,"state":"open"},{"attributes":{"id":1,"rel":"folder"},"data":"UNT_65010 Revision : 1","unitRevision":1,"unitName":"SPD","unitId":"UNT_65010","parentid":0,"quantity":12,"state":"open"},{"attributes":{"id":2,"rel":"folder"},"data":"UNT_38882 Revision : 1","unitRevision":1,"unitName":"Switch","unitId":"UNT_38882","parentid":0,"quantity":1,"state":"open"},{"attributes":{"id":3,"rel":"folder"},"data":"UNT_16699 Revision : 1","unitRevision":1,"unitName":"CMU","unitId":"UNT_16699","parentid":0,"quantity":1,"state":"open"},{"attributes":{"id":4,"rel":"folder"},"data":"UNT_53072 Revision : 1","unitRevision":1,"unitName":"ECU","unitId":"UNT_53072","parentid":1,"quantity":1,"state":"open"},{"attributes":{"id":5,"rel":"folder"},"data":"UNT_21151 Revision : 1","unitRevision":1,"unitName":"Vsat power","unitId":"UNT_21151","parentid":3,"quantity":1,"state":"open"},{"attributes":{"id":6,"rel":"folder"},"data":"UNT_38882 Revision : 1","unitRevision":1,"unitName":"Switch","unitId":"UNT_38882","parentid":2,"quantity":2,"state":"open"},{"attributes":{"id":7,"rel":"folder"},"data":"UNT_66489 Revision : 1","unitRevision":1,"unitName":"UCC","unitId":"UNT_66489","parentid":3,"quantity":1,"state":"open"}]', 'D', '2016-02-20 06:28:21', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_revision`
--

CREATE TABLE IF NOT EXISTS `tbl_revision` (
  `rev_id` int(20) NOT NULL AUTO_INCREMENT,
  `rev_version` int(20) NOT NULL,
  `rev_faultid` int(10) NOT NULL,
  `rev_solid` int(10) NOT NULL,
  `rev_object_id` int(20) NOT NULL,
  `rev_object_type` varchar(100) NOT NULL,
  `rev_description` varchar(100) NOT NULL,
  `rev_timestamp` varchar(100) NOT NULL,
  `rev_status` enum('y','n') NOT NULL,
  PRIMARY KEY (`rev_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_solution`
--

CREATE TABLE IF NOT EXISTS `tbl_solution` (
  `sol_id` int(10) NOT NULL AUTO_INCREMENT,
  `sol_unique_id` varchar(100) NOT NULL,
  `sol_name` varchar(1000) NOT NULL,
  `sol_description` varchar(1000) NOT NULL,
  `sol_revision` varchar(1000) NOT NULL,
  `sol_userid` varchar(1000) NOT NULL,
  `sol_status` varchar(100) NOT NULL DEFAULT 'D',
  `sol_timestamp` varchar(1000) NOT NULL,
  PRIMARY KEY (`sol_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=68 ;

--
-- Dumping data for table `tbl_solution`
--

INSERT INTO `tbl_solution` (`sol_id`, `sol_unique_id`, `sol_name`, `sol_description`, `sol_revision`, `sol_userid`, `sol_status`, `sol_timestamp`) VALUES
(45, 'SLN76000', 'correct the polarity ', 'correct the polarity ', '1', '1', 'D', '2016-02-03 01:14:04'),
(46, 'SLN76370', 'correct soldering', 'correct soldering', '1', '1', 'D', '2016-02-03 01:14:04'),
(47, 'SLN68563', 'remove track shorting', 'remove track shorting', '1', '1', 'D', '2016-02-03 01:14:04'),
(48, 'SLN42866', 'check component values', 'check component values', '1', '1', 'D', '2016-02-03 01:14:04'),
(49, 'SLN73541', 'PWM IC pin values to check', 'PWM IC pin values to check', '1', '1', 'D', '2016-02-03 01:14:04'),
(50, 'SLN89631', 'Switch off  & correct the test setup ', 'Switch off  & correct the test setup ', '1', '1', 'D', '2016-02-03 01:14:04'),
(51, 'SLN36676', 'check voltage & buzzer also', 'check voltage & buzzer also', '1', '1', 'D', '2016-02-03 01:14:04'),
(52, 'SLN36818', 'check one by one circuit', 'check one by one circuit', '1', '1', 'D', '2016-02-03 01:14:04'),
(53, 'SLN34671', 'check noise ', 'check noise ', '1', '1', 'D', '2016-02-03 01:14:04'),
(54, 'SLN75277', 'repeat & if fail again check IC', 'repeat & if fail again check IC', '1', '1', 'D', '2016-02-03 01:14:04'),
(55, 'SLN33270', 'check shorting & load connection', 'check shorting & load connection', '1', '1', 'D', '2016-02-03 01:14:04'),
(56, 'SLN24447', 'replace fuse ', 'replace fuse ', '1', '1', 'D', '2016-02-03 01:14:04'),
(57, 'SLN86968', 'replace the componets & correct the setup', 'replace the componets & correct the setup', '1', '1', 'D', '2016-02-03 01:14:04'),
(58, 'SLN94630', 'replace diode', 'replace diode', '1', '1', 'D', '2016-02-03 01:14:04'),
(59, 'SLN27482', 'set required range input volatge', 'set required range input volatge', '1', '1', 'D', '2016-02-03 01:14:04'),
(60, 'SLN51920', 'check reasons & correct them', 'check reasons & correct them', '1', '1', 'D', '2016-02-03 01:14:04'),
(61, 'SLN44034', 'replace bridge ', 'replace bridge ', '1', '1', 'D', '2016-02-03 01:14:04'),
(62, 'SLN67267', 'replace Mosfet & related components ', 'replace Mosfet & related components ', '1', '1', 'D', '2016-02-03 01:14:04'),
(63, 'SLN51425', 'replace mosfets & circuitary', 'replace mosfets & circuitary', '1', '1', 'D', '2016-02-03 01:14:04'),
(64, 'SLN20423', 'track the signal & IC also', 'track the signal & IC also', '1', '1', 'D', '2016-02-03 01:14:04'),
(65, 'SLN34948', 'check correct values of capacitor & R', 'check correct values of capacitor & R', '1', '1', 'D', '2016-02-03 01:14:04'),
(66, 'SLN58968', 'replace reset switch ', 'replace reset switch ', '1', '1', 'D', '2016-02-03 01:14:04'),
(67, 'SLN11824', 'first solution', 'first solution is added', '1', '1', 'D', '2016-02-05 01:40:47');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_solution_revision`
--

CREATE TABLE IF NOT EXISTS `tbl_solution_revision` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sol_id` varchar(100) NOT NULL,
  `sol_unique_id` varchar(100) NOT NULL,
  `sol_name` varchar(1000) NOT NULL,
  `sol_description` varchar(1000) NOT NULL,
  `sol_revision` varchar(1000) NOT NULL,
  `sol_userid` varchar(1000) NOT NULL,
  `sol_status` varchar(100) NOT NULL DEFAULT 'D',
  `sol_timestamp` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_test`
--

CREATE TABLE IF NOT EXISTS `tbl_test` (
  `test_id` int(10) NOT NULL AUTO_INCREMENT,
  `test_uniqueid` varchar(100) NOT NULL,
  `test_name` varchar(1000) NOT NULL,
  `test_category` varchar(1000) NOT NULL,
  `test_description` varchar(1000) NOT NULL,
  `test_precondition` varchar(1000) NOT NULL,
  `test_revision` varchar(1000) NOT NULL,
  `test_ideal` varchar(1000) NOT NULL,
  `test_min_level` varchar(1000) NOT NULL,
  `test_max_level` varchar(1000) NOT NULL,
  `test_userid` varchar(1000) NOT NULL,
  `test_timestamp` datetime DEFAULT NULL,
  `test_status` enum('A','D','DELETE') NOT NULL,
  `test_data_type` varchar(100) NOT NULL,
  PRIMARY KEY (`test_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=166 ;

--
-- Dumping data for table `tbl_test`
--

INSERT INTO `tbl_test` (`test_id`, `test_uniqueid`, `test_name`, `test_category`, `test_description`, `test_precondition`, `test_revision`, `test_ideal`, `test_min_level`, `test_max_level`, `test_userid`, `test_timestamp`, `test_status`, `test_data_type`) VALUES
(108, 'TST_56225', 'VCCA_12V', '1', 'TP8/C46', 'Main Board CMU', '1', '12.5V', '11.5', '13.5', '1', '2016-02-03 01:09:00', 'A', 'ok'),
(109, 'TST_41095', 'VCCA_AUX', '1', 'TP9/C43', 'Main Board CMU', '1', '12.5V', '11.5', '13.5', '1', '2016-02-03 01:09:00', 'A', 'ok'),
(110, 'TST_71075', 'VCC-12V SOLAR_A', '1', 'TP3/C12', 'Main Board CMU', '1', '12.5V', '11.5', '13.5', '1', '2016-02-03 01:09:00', 'D', 'ok'),
(111, 'TST_9438', 'VCC-12V SOLAR_B', '1', 'TP2/C11', 'Main Board CMU', '1', '12.5V', '11.5', '13.5', '1', '2016-02-03 01:09:00', 'D', 'ok'),
(112, 'TST_10905', 'VCC_12V_Control_Card', '1', 'TP4/C14', 'Main Board CMU', '1', '12.5V', '11.5', '13.5', '1', '2016-02-03 01:09:00', 'D', 'ok'),
(113, 'TST_26371', 'VCC_4V_BACKPLANE', '1', 'TP5/C48', 'Main Board CMU', '1', '6.5V', '5.5', '7.5', '1', '2016-02-03 01:09:00', 'D', 'ok'),
(114, 'TST_33459', 'VCC_3V3_RS485_A', '1', 'TP6/CK11', 'Module Cmu', '1', '3.3V', '3.28', '3.31', '1', '2016-02-03 01:09:00', 'D', 'ok'),
(115, 'TST_29477', 'VCCA_6V', '1', 'C64', 'Main Board CMU', '1', '6.2V', '6', '6.7', '1', '2016-02-03 01:09:00', 'D', 'ok'),
(116, 'TST_61778', 'VCCA_3V3_A', '1', 'TP10/CL23', 'Module', '1', '3.3V', '3.28', '3.31', '1', '2016-02-03 01:09:00', 'D', 'ok'),
(117, 'TST_53984', 'VREF 2.5V', '1', 'TP13/C25', 'Main Board CMU', '1', '2.5V', '2.48', '2.51', '1', '2016-02-03 01:09:00', 'D', 'ok'),
(118, 'TST_72186', 'BAT_V_SENSE(I/P DC=50V)', '1', 'CL2', 'Module', '1', '1.7@50v', '1.69', '1.71', '1', '2016-02-03 01:09:00', 'D', 'ok'),
(119, 'TST_20793', 'BAT_I_SENSE', '1', 'CL5', 'Module', '1', '1.645V', '1.63', '1.65', '1', '2016-02-03 01:09:00', 'D', 'ok'),
(120, 'TST_41029', 'EXT DC_I_SENSE', '1', 'CL3', 'Module', '1', '1.645V', '1.63', '1.65', '1', '2016-02-03 01:09:00', 'D', 'ok'),
(121, 'TST_57690', 'SOLAR_I_SENSE', '1', 'CL4', 'Module', '1', '1.645V', '1.63', '1.65', '1', '2016-02-03 01:09:00', 'D', 'ok'),
(122, 'TST_35163', 'LOAD_I_SENSE', '1', 'TP52/CL10,DL2', 'Module', '1', '0', '0', '10', '1', '2016-02-03 01:09:00', 'D', 'ok'),
(123, 'TST_6972', 'Temperature Sense Line', '1', 'TP51', 'Module', '1', '750mV@25C', '', '', '1', '2016-02-03 01:09:00', 'D', 'ok'),
(124, 'TST_90329', 'BAT_TMP_SNS', '1', 'TP50/CL9', 'Module', '1', '.55-1V', '', '', '1', '2016-02-03 01:09:00', 'D', 'ok'),
(125, 'TST_76467', 'Sys. low voltage turnoff', '1', 'C71, LED-D66 OFF', 'Main Board CMU', '1', '30V', '29', '31', '1', '2016-02-03 01:09:00', 'D', 'ok'),
(126, 'TST_93951', 'Sys. low voltage turnon', '1', 'C71, LED-D66 ON', 'Main Board CMU', '1', '35V', '34', '36', '1', '2016-02-03 01:09:00', 'D', 'ok'),
(127, 'TST_46677', 'HARDWARE LVD', '1', 'TP12=LOW', 'Main Board CMU', '1', '44V', '44', '44.4', '1', '2016-02-03 01:09:00', 'D', 'ok'),
(128, 'TST_52483', 'HARDWARE LVR', '1', 'TP12=HIGH', 'Main Board CMU', '1', '50V', '50', '51', '1', '2016-02-03 01:09:00', 'D', 'ok'),
(129, 'TST_98561', 'SOLAR FAILSAIF CUTOFF', '1', 'TP18=LOW', 'Main Board CMU', '1', '52V', '51.5', '52', '1', '2016-02-03 01:09:00', 'D', 'ok'),
(130, 'TST_93779', 'SOLAR FAILSAIF CUTIN', '1', 'TP18=HIGH', 'Main Board CMU', '1', '48V', '47.5', '48.5', '1', '2016-02-03 01:09:00', 'D', 'ok'),
(131, 'TST_37483', 'Solar overvoltage discon.', '1', 'TP15=LOW', 'Main Board CMU', '1', '58.5V', '58', '59', '1', '2016-02-03 01:09:00', 'D', 'ok'),
(132, 'TST_20990', 'Solar overvoltage reconn.', '1', 'TP15=HIGH', 'Main Board CMU', '1', '53V', '52.5', '53.5', '1', '2016-02-03 01:09:00', 'D', 'ok'),
(133, 'TST_91060', 'Hot swap low line cutoff', '1', 'TP23/J21-P12', 'Main Board CMU', '1', '39.6V', '38.6', '39.6', '1', '2016-02-03 01:09:00', 'D', 'ok'),
(134, 'TST_74707', 'Hot swap low line cutin', '1', 'TP23/J21-P12', 'Main Board CMU', '1', '43.4V', '42.4', '43.4', '1', '2016-02-03 01:09:00', 'D', 'ok'),
(135, 'TST_90313', 'Hot swap high line cutoff', '1', 'TP23/J21-P12', 'Main Board CMU', '1', '62V', '61', '62', '1', '2016-02-03 01:09:00', 'D', 'ok'),
(136, 'TST_53312', 'Hot swap high line cutin', '1', 'TP23/J21-P12', 'Main Board CMU', '1', '60V', '59', '60', '1', '2016-02-03 01:09:00', 'D', 'ok'),
(137, 'TST_21528', 'Power Good signal', '1', 'JL1-P22', '', '1', '57.5V', '57', '58', '1', '2016-02-03 01:09:00', 'D', 'ok'),
(138, 'TST_9674', 'VCCS_3V3(monostable)', '1', 'TP21/C53 (SolarV=50V)', 'Main Board', '1', '3.3V', '3', '3.5', '1', '2016-02-03 01:09:00', 'D', 'ok'),
(139, 'TST_9538', '10 SEC TIMER', '1', 'TP19[Apply 3.3V externally]', 'Main Board', '1', '10 sec', '8.5', '11', '1', '2016-02-03 01:09:00', 'D', 'ok'),
(140, 'TST_62624', 'Q7,Q9,Q11,Q13', '1', 'GND-D37,D38 Anode', '', '1', '12.25', '12', '12.5', '1', '2016-02-03 01:09:00', 'D', 'ok'),
(141, 'TST_80750', 'Q8,Q10,Q12,Q14', '1', '', '', '1', '0', '0', '0', '1', '2016-02-03 01:09:00', 'D', 'ok'),
(142, 'TST_18977', 'Q6', '1', '', '', '1', '3.05', '3', '3.3', '1', '2016-02-03 01:09:00', 'D', 'ok'),
(143, 'TST_73529', 'Q3,Q4,Q5', '1', '', '', '1', '12.7', '12', '13', '1', '2016-02-03 01:09:00', 'D', 'ok'),
(144, 'TST_35549', 'Visual Inspection', '2', 'V3,V4,V5', '', '1', 'S20K95', '', '', '1', '2016-02-03 01:11:37', 'D', 'ok'),
(145, 'TST_47915', 'V1,V2,V3,V4,V5', '2', 'V1,V2', '', '1', 'S20K96', '', '', '1', '2016-02-03 01:11:37', 'D', 'ok'),
(146, 'TST_33420', 'SOLAR 1 Fuse Rating', '2', 'F4', '', '1', '40A', '', '', '1', '2016-02-03 01:11:37', 'D', 'ok'),
(147, 'TST_57410', 'SOLAR 2 Fuse Rating', '2', 'F5', '', '1', '40A', '', '', '1', '2016-02-03 01:11:37', 'D', 'ok'),
(148, 'TST_50058', 'LOAD Fuse Rating', '2', 'F7', '', '1', '20A', '', '', '1', '2016-02-03 01:11:37', 'D', 'ok'),
(149, 'TST_28203', 'BATTERY Fuse Rating', '2', 'F8', '', '1', '80A', '', '', '1', '2016-02-03 01:11:37', 'D', 'ok'),
(150, 'TST_34539', 'External DC Fuse rating', '2', 'F6', '', '1', '80A', '', '', '1', '2016-02-03 01:11:37', 'D', 'ok'),
(151, 'TST_17660', 'Polymide tape under', '2', 'HS1,HS2,HS3,HS4', '', '1', '', '', '', '1', '2016-02-03 01:11:37', 'D', 'ok'),
(152, 'TST_57256', 'HS1 Heat Sink Bend', '2', 'HS1,HS2,HS3,HS4', '', '1', '', '', '', '1', '2016-02-03 01:11:37', 'D', 'ok'),
(153, 'TST_21966', 'RS-485 comm', '3', 'comm ok', 'ckt ok', '1', '', '', '', '1', '2016-02-03 01:12:14', 'D', ''),
(154, 'TST_27842', 'ADC', '3', 'counts ok', 'ckt ok', '1', '', '', '', '1', '2016-02-03 01:12:14', 'D', ''),
(155, 'TST_47326', 'Visual Inspecyyyy', '1', 'V3,V4,V5', '', '1', 'S20K95', '', '', '1', '2016-02-05 12:38:53', 'D', 'ok'),
(156, 'TST_69490', 'Pts_1', '1', '<p></p>', 'test', '1', '110000', '110001', '110005', '1', '2016-02-10 06:36:18', 'D', 'Integer'),
(157, 'TST_35320', 'Pts_2', '1', '<p><p></p></p>', 'test', '2', '11000', '12001', '11007', '1', '2016-02-10 10:56:39', 'D', 'Integer'),
(158, 'TST_73812', 'Pts_3', '1', '<p><p></p></p>', 'test', '1.1', '21000', '21003', '21007', '1', '2016-02-10 10:57:07', 'D', 'Integer'),
(159, 'TST_84908', 'Pts_4', '1', '<p><p></p></p>', 'test', '1.1', '30001', '31002', '31007', '1', '2016-02-10 10:57:31', 'D', 'Integer'),
(160, 'TST_20423', 'Pts_5', '1', '<p><p><p></p></p></p>', 'test', '1.2', '32000', '31003', '31009', '1', '2016-02-10 10:57:59', 'D', 'Integer'),
(161, 'TST_42014', 'Pts_6', '1', '<p><p><p></p></p></p>', 'test', '1.2', '32001', '32003', '32008', '1', '2016-02-10 10:58:24', 'D', 'Integer'),
(162, 'TST_78837', 'Pts_7', '1', '<p></p>', 'test', '1', '200000', '210001', '210005', '1', '2016-02-10 06:41:30', 'D', 'Integer'),
(163, 'TST_18043', 'Pts_8', '1', '<p></p>', 'test', '1', '300000', '310001', '310005', '1', '2016-02-10 06:42:17', 'D', 'Integer'),
(164, 'TST_49743', 'Pts_9', '1', '<p></p>', 'test', '1', '400000', '410001', '410005', '1', '2016-02-10 06:43:00', 'D', 'Integer'),
(165, 'TST_33502', 'Pts_10', '1', '<p></p>', 'test', '1', '500000', '510001', '510005', '1', '2016-02-10 06:43:33', 'D', 'Integer');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_test_result`
--

CREATE TABLE IF NOT EXISTS `tbl_test_result` (
  `result_id` int(50) NOT NULL AUTO_INCREMENT,
  `result_format_unique` varchar(100) NOT NULL,
  `result_format_revision` varchar(100) NOT NULL,
  `result_format_tagid` varchar(100) NOT NULL,
  `result_unit_serialno` varchar(100) NOT NULL,
  `test_result_unique` varchar(100) NOT NULL,
  `test_resutl_unique_rev` varchar(1000) NOT NULL,
  `result_status` varchar(100) NOT NULL,
  `result_observation` varchar(100) NOT NULL,
  `result_timestamp` datetime NOT NULL,
  `result_userid` varchar(100) NOT NULL,
  PRIMARY KEY (`result_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=109 ;

--
-- Dumping data for table `tbl_test_result`
--

INSERT INTO `tbl_test_result` (`result_id`, `result_format_unique`, `result_format_revision`, `result_format_tagid`, `result_unit_serialno`, `test_result_unique`, `test_resutl_unique_rev`, `result_status`, `result_observation`, `result_timestamp`, `result_userid`) VALUES
(19, 'format_25555', '1.1', '101', 'hhh', 'UNT_63374', '1', 'Pass', '10000', '2016-02-12 18:03:28', '1'),
(20, 'format_25555', '1.1', '102', 'hhh', 'UNT_63374', '1', 'Fail', '12000', '2016-02-12 18:03:28', '1'),
(21, 'format_25555', '1.1', '103', 'hhh', 'UNT_63374', '1', 'Pass', '13000', '2016-02-12 18:03:28', '1'),
(22, 'format_25555', '1.1', '104', 'hhh', 'UNT_63374', '1', 'Fail', '14000', '2016-02-12 18:03:28', '1'),
(23, 'format_25555', '1.1', '105', 'hhh', 'UNT_63374', '1', 'Fail', '14000', '2016-02-12 18:03:28', '1'),
(24, 'format_25555', '1.1', '106', 'hhh', 'UNT_63374', '1', 'Fail', '14000', '2016-02-12 18:03:28', '1'),
(25, 'format_25555', '1.1', '107', 'hhh', 'UNT_63374', '1', 'Fail', '14000', '2016-02-12 18:03:28', '1'),
(26, 'format_25555', '1.1', '108', 'hhh', 'UNT_63374', '1', 'Fail', '14000', '2016-02-12 18:03:28', '1'),
(27, 'format_25555', '1.1', '109', 'hhh', 'UNT_63374', '1', 'Fail', '14000', '2016-02-12 18:03:28', '1'),
(28, 'format_25555', '1.1', '110', 'hhh', 'UNT_63374', '1', 'Fail', '14000', '2016-02-12 18:03:28', '1'),
(29, 'format_25555', '1.1', '101', 'hhh', 'UNT_63374', '1', 'Pass', '10000', '2016-02-12 18:52:25', '1'),
(30, 'format_25555', '1.1', '102', 'hhh', 'UNT_63374', '1', 'Fail', '12000', '2016-02-12 18:52:25', '1'),
(31, 'format_25555', '1.1', '103', 'hhh', 'UNT_63374', '1', 'Pass', '13000', '2016-02-12 18:52:25', '1'),
(32, 'format_25555', '1.1', '104', 'hhh', 'UNT_63374', '1', 'Fail', '14000', '2016-02-12 18:52:25', '1'),
(33, 'format_25555', '1.1', '105', 'hhh', 'UNT_63374', '1', 'Fail', '14000', '2016-02-12 18:52:25', '1'),
(34, 'format_25555', '1.1', '106', 'hhh', 'UNT_63374', '1', 'Fail', '14000', '2016-02-12 18:52:25', '1'),
(35, 'format_25555', '1.1', '107', 'hhh', 'UNT_63374', '1', 'Fail', '14000', '2016-02-12 18:52:25', '1'),
(36, 'format_25555', '1.1', '108', 'hhh', 'UNT_63374', '1', 'Fail', '14000', '2016-02-12 18:52:25', '1'),
(37, 'format_25555', '1.1', '109', 'hhh', 'UNT_63374', '1', 'Fail', '14000', '2016-02-12 18:52:25', '1'),
(38, 'format_25555', '1.1', '110', 'hhh', 'UNT_63374', '1', 'Fail', '14000', '2016-02-12 18:52:25', '1'),
(39, 'format_25555', '1.1', '101', 'hj', 'UNT_63374', '1', 'Pass', '10000', '2016-02-13 12:18:39', '1'),
(40, 'format_25555', '1.1', '102', 'hj', 'UNT_63374', '1', 'Fail', '12000', '2016-02-13 12:18:39', '1'),
(41, 'format_25555', '1.1', '103', 'hj', 'UNT_63374', '1', 'Pass', '13000', '2016-02-13 12:18:39', '1'),
(42, 'format_25555', '1.1', '104', 'hj', 'UNT_63374', '1', 'Fail', '14000', '2016-02-13 12:18:39', '1'),
(43, 'format_25555', '1.1', '105', 'hj', 'UNT_63374', '1', 'pending ', '', '2016-02-13 12:18:39', '1'),
(44, 'format_25555', '1.1', '106', 'hj', 'UNT_63374', '1', 'pending ', '', '2016-02-13 12:18:39', '1'),
(45, 'format_25555', '1.1', '107', 'hj', 'UNT_63374', '1', 'pending ', '', '2016-02-13 12:18:39', '1'),
(46, 'format_25555', '1.1', '108', 'hj', 'UNT_63374', '1', 'pending ', '', '2016-02-13 12:18:39', '1'),
(47, 'format_25555', '1.1', '109', 'hj', 'UNT_63374', '1', 'pending ', '', '2016-02-13 12:18:39', '1'),
(48, 'format_25555', '1.1', '110', 'hj', 'UNT_63374', '1', 'pending ', '', '2016-02-13 12:18:39', '1'),
(49, 'format_25555', '1.1', '101', 'hj', 'UNT_63374', '1', 'Pass', '10000', '2016-02-13 12:19:46', '1'),
(50, 'format_25555', '1.1', '102', 'hj', 'UNT_63374', '1', 'Fail', '12000', '2016-02-13 12:19:46', '1'),
(51, 'format_25555', '1.1', '103', 'hj', 'UNT_63374', '1', 'Pass', '13000', '2016-02-13 12:19:46', '1'),
(52, 'format_25555', '1.1', '104', 'hj', 'UNT_63374', '1', 'Fail', '14000', '2016-02-13 12:19:46', '1'),
(53, 'format_25555', '1.1', '105', 'hj', 'UNT_63374', '1', 'pending ', '', '2016-02-13 12:19:46', '1'),
(54, 'format_25555', '1.1', '106', 'hj', 'UNT_63374', '1', 'pending ', '', '2016-02-13 12:19:46', '1'),
(55, 'format_25555', '1.1', '107', 'hj', 'UNT_63374', '1', 'pending ', '', '2016-02-13 12:19:46', '1'),
(56, 'format_25555', '1.1', '108', 'hj', 'UNT_63374', '1', 'pending ', '', '2016-02-13 12:19:46', '1'),
(57, 'format_25555', '1.1', '109', 'hj', 'UNT_63374', '1', 'pending ', '', '2016-02-13 12:19:46', '1'),
(58, 'format_25555', '1.1', '110', 'hj', 'UNT_63374', '1', 'pending ', '', '2016-02-13 12:19:46', '1'),
(59, 'format_25555', '1.1', '101', 'hj', 'UNT_63374', '1', 'Pass', '10000', '2016-02-13 12:19:53', '1'),
(60, 'format_25555', '1.1', '102', 'hj', 'UNT_63374', '1', 'Fail', '12000', '2016-02-13 12:19:53', '1'),
(61, 'format_25555', '1.1', '103', 'hj', 'UNT_63374', '1', 'Pass', '13000', '2016-02-13 12:19:53', '1'),
(62, 'format_25555', '1.1', '104', 'hj', 'UNT_63374', '1', 'Fail', '14000', '2016-02-13 12:19:53', '1'),
(63, 'format_25555', '1.1', '105', 'hj', 'UNT_63374', '1', 'pending ', '', '2016-02-13 12:19:53', '1'),
(64, 'format_25555', '1.1', '106', 'hj', 'UNT_63374', '1', 'pending ', '', '2016-02-13 12:19:53', '1'),
(65, 'format_25555', '1.1', '107', 'hj', 'UNT_63374', '1', 'pending ', '', '2016-02-13 12:19:53', '1'),
(66, 'format_25555', '1.1', '108', 'hj', 'UNT_63374', '1', 'pending ', '', '2016-02-13 12:19:53', '1'),
(67, 'format_25555', '1.1', '109', 'hj', 'UNT_63374', '1', 'pending ', '', '2016-02-13 12:19:53', '1'),
(68, 'format_25555', '1.1', '110', 'hj', 'UNT_63374', '1', 'pending ', '', '2016-02-13 12:19:53', '1'),
(69, 'format_25555', '1.1', '101', 'bh', 'UNT_63374', '1', 'pending ', '', '2016-02-15 12:59:37', '1'),
(70, 'format_25555', '1.1', '102', 'bh', 'UNT_63374', '1', 'pending ', '', '2016-02-15 12:59:37', '1'),
(71, 'format_25555', '1.1', '103', 'bh', 'UNT_63374', '1', 'pending ', '', '2016-02-15 12:59:37', '1'),
(72, 'format_25555', '1.1', '104', 'bh', 'UNT_63374', '1', 'pending ', '', '2016-02-15 12:59:37', '1'),
(73, 'format_25555', '1.1', '105', 'bh', 'UNT_63374', '1', 'pending ', '', '2016-02-15 12:59:37', '1'),
(74, 'format_25555', '1.1', '106', 'bh', 'UNT_63374', '1', 'pending ', '', '2016-02-15 12:59:37', '1'),
(75, 'format_25555', '1.1', '107', 'bh', 'UNT_63374', '1', 'pending ', '', '2016-02-15 12:59:37', '1'),
(76, 'format_25555', '1.1', '108', 'bh', 'UNT_63374', '1', 'pending ', '', '2016-02-15 12:59:37', '1'),
(77, 'format_25555', '1.1', '109', 'bh', 'UNT_63374', '1', 'pending ', '', '2016-02-15 12:59:37', '1'),
(78, 'format_25555', '1.1', '110', 'bh', 'UNT_63374', '1', 'pending ', '', '2016-02-15 12:59:37', '1'),
(79, 'format_25555', '1.1', '101', 'jnh', 'UNT_63374', '1', 'Fail', '1000', '2016-02-15 18:46:01', '1'),
(80, 'format_25555', '1.1', '102', 'jnh', 'UNT_63374', '1', 'Fail', '2000', '2016-02-15 18:46:01', '1'),
(81, 'format_25555', '1.1', '103', 'jnh', 'UNT_63374', '1', 'Fail', '3000', '2016-02-15 18:46:01', '1'),
(82, 'format_25555', '1.1', '104', 'jnh', 'UNT_63374', '1', 'Fail', '4000', '2016-02-15 18:46:01', '1'),
(83, 'format_25555', '1.1', '105', 'jnh', 'UNT_63374', '1', 'Fail', '5000', '2016-02-15 18:46:01', '1'),
(84, 'format_25555', '1.1', '106', 'jnh', 'UNT_63374', '1', 'Fail', '6000', '2016-02-15 18:46:01', '1'),
(85, 'format_25555', '1.1', '107', 'jnh', 'UNT_63374', '1', 'Fail', '7000', '2016-02-15 18:46:01', '1'),
(86, 'format_25555', '1.1', '108', 'jnh', 'UNT_63374', '1', 'Fail', '1000', '2016-02-15 18:46:01', '1'),
(87, 'format_25555', '1.1', '109', 'jnh', 'UNT_63374', '1', 'Fail', '1000', '2016-02-15 18:46:01', '1'),
(88, 'format_25555', '1.1', '110', 'jnh', 'UNT_63374', '1', 'Fail', '1000', '2016-02-15 18:46:01', '1'),
(89, 'format_25555', '1.1', '101', 'pts1', 'UNT_63374', '1', 'Pass', '110002', '2016-02-15 18:52:10', '1'),
(90, 'format_25555', '1.1', '102', 'pts1', 'UNT_63374', '1', 'Fail', '2000', '2016-02-15 18:52:10', '1'),
(91, 'format_25555', '1.1', '103', 'pts1', 'UNT_63374', '1', 'Pass', '21004', '2016-02-15 18:52:10', '1'),
(92, 'format_25555', '1.1', '104', 'pts1', 'UNT_63374', '1', 'Pass', '31004', '2016-02-15 18:52:10', '1'),
(93, 'format_25555', '1.1', '105', 'pts1', 'UNT_63374', '1', 'Pass', '31005', '2016-02-15 18:52:10', '1'),
(94, 'format_25555', '1.1', '106', 'pts1', 'UNT_63374', '1', 'Pass', '32004', '2016-02-15 18:52:10', '1'),
(95, 'format_25555', '1.1', '107', 'pts1', 'UNT_63374', '1', 'Pass', '210002', '2016-02-15 18:52:10', '1'),
(96, 'format_25555', '1.1', '108', 'pts1', 'UNT_63374', '1', 'Pass', '310002', '2016-02-15 18:52:10', '1'),
(97, 'format_25555', '1.1', '109', 'pts1', 'UNT_63374', '1', 'Pass', '410004', '2016-02-15 18:52:10', '1'),
(98, 'format_25555', '1.1', '110', 'pts1', 'UNT_63374', '1', 'Pass', '510002', '2016-02-15 18:52:10', '1'),
(99, 'format_25555', '1.1', '101', 'pts1', 'UNT_63374', '1', 'Pass', '110002', '2016-02-15 18:57:07', '1'),
(100, 'format_25555', '1.1', '102', 'pts1', 'UNT_63374', '1', 'Fail', '2000', '2016-02-15 18:57:07', '1'),
(101, 'format_25555', '1.1', '103', 'pts1', 'UNT_63374', '1', 'Pass', '21004', '2016-02-15 18:57:07', '1'),
(102, 'format_25555', '1.1', '104', 'pts1', 'UNT_63374', '1', 'Pass', '31004', '2016-02-15 18:57:07', '1'),
(103, 'format_25555', '1.1', '105', 'pts1', 'UNT_63374', '1', 'Pass', '31005', '2016-02-15 18:57:07', '1'),
(104, 'format_25555', '1.1', '106', 'pts1', 'UNT_63374', '1', 'Pass', '32004', '2016-02-15 18:57:07', '1'),
(105, 'format_25555', '1.1', '107', 'pts1', 'UNT_63374', '1', 'Pass', '210002', '2016-02-15 18:57:07', '1'),
(106, 'format_25555', '1.1', '108', 'pts1', 'UNT_63374', '1', 'Pass', '310002', '2016-02-15 18:57:07', '1'),
(107, 'format_25555', '1.1', '109', 'pts1', 'UNT_63374', '1', 'Pass', '410004', '2016-02-15 18:57:07', '1'),
(108, 'format_25555', '1.1', '110', 'pts1', 'UNT_63374', '1', 'Pass', '510002', '2016-02-15 18:57:07', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_test_revision`
--

CREATE TABLE IF NOT EXISTS `tbl_test_revision` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `test_id` varchar(100) NOT NULL,
  `test_uniqueid` varchar(100) NOT NULL,
  `test_name` varchar(1000) NOT NULL,
  `test_category` varchar(1000) NOT NULL,
  `test_description` varchar(1000) NOT NULL,
  `test_precondition` varchar(1000) NOT NULL,
  `test_revision` varchar(1000) NOT NULL,
  `test_ideal` varchar(1000) NOT NULL,
  `test_min_level` varchar(1000) NOT NULL,
  `test_max_level` varchar(1000) NOT NULL,
  `test_userid` varchar(1000) NOT NULL,
  `test_timestamp` datetime DEFAULT NULL,
  `test_status` enum('A','D','DELETE') NOT NULL,
  `test_data_type` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tbl_test_revision`
--

INSERT INTO `tbl_test_revision` (`id`, `test_id`, `test_uniqueid`, `test_name`, `test_category`, `test_description`, `test_precondition`, `test_revision`, `test_ideal`, `test_min_level`, `test_max_level`, `test_userid`, `test_timestamp`, `test_status`, `test_data_type`) VALUES
(1, '160', 'TST_20423', 'Pts_5', '1', '<p></p>', 'test', '1', '40000', '41001', '41005', '1', '2016-02-10 06:39:48', 'D', 'Short'),
(2, '161', 'TST_42014', 'Pts_6', '1', '<p></p>', 'test', '1', '50000', '51001', '51005', '1', '2016-02-10 06:40:31', 'D', 'Short'),
(3, '157', 'TST_35320', 'Pts_2', '1', '<p></p>', 'test', '1', '10000', '11001', '11005', '1', '2016-02-10 06:37:16', 'D', 'Short'),
(4, '158', 'TST_73812', 'Pts_3', '1', '<p></p>', 'test', '1', '20000', '21001', '21005', '1', '2016-02-10 06:38:05', 'D', 'Short'),
(5, '159', 'TST_84908', 'Pts_4', '1', '<p></p>', 'test', '1', '30000', '31001', '31005', '1', '2016-02-10 06:39:01', 'D', 'Short'),
(6, '160', 'TST_20423', 'Pts_5', '1', '<p><p></p></p>', 'test', '1.1', '31000', '31002', '31007', '1', '2016-02-10 08:23:16', 'D', 'Short'),
(7, '161', 'TST_42014', 'Pts_6', '1', '<p><p></p></p>', 'test', '1.1', '32000', '32001', '32005', '1', '2016-02-10 08:24:02', 'D', 'Short');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_unit`
--

CREATE TABLE IF NOT EXISTS `tbl_unit` (
  `unit_uid` int(100) NOT NULL AUTO_INCREMENT,
  `unit_unique_id` varchar(100) NOT NULL,
  `unit_revid` float NOT NULL,
  `unit_name` varchar(1000) NOT NULL,
  `unit_description` text NOT NULL,
  `unit_partno` varchar(1000) NOT NULL,
  `unit_type` varchar(1000) NOT NULL,
  `unit_timestamp` varchar(1000) NOT NULL,
  `unit_status` varchar(100) NOT NULL,
  `unit_userid` varchar(100) NOT NULL,
  PRIMARY KEY (`unit_uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=119 ;

--
-- Dumping data for table `tbl_unit`
--

INSERT INTO `tbl_unit` (`unit_uid`, `unit_unique_id`, `unit_revid`, `unit_name`, `unit_description`, `unit_partno`, `unit_type`, `unit_timestamp`, `unit_status`, `unit_userid`) VALUES
(109, 'UNT_16699', 0, 'CMU', 'dc-dc converter 500W', '203.20022.03', 'Active', '2016-02-20 05:41:14', 'D', '1'),
(110, 'UNT_53072', 0, 'ECU', 'AC-dc 1.5KW', '203.20023.03', 'Active', '2016-02-20 05:41:14', 'D', '1'),
(111, 'UNT_21151', 0, 'Vsat power', 'vsat power 24v,8.5v', '203.00694.00', 'Active', '2016-02-20 05:41:14', 'D', '1'),
(112, 'UNT_38882', 0, 'Switch', '4 Port Switch', '1', 'Active', '2016-02-20 05:41:14', 'D', '1'),
(113, 'UNT_66489', 0, 'UCC', 'universal Control Card', '203.20017.02', 'Active', '2016-02-20 05:41:14', 'D', '1'),
(114, 'UNT_57878', 0, 'backplane', 'R3 backplane', '203.20024.03', 'passive', '2016-02-20 05:41:14', 'D', '1'),
(115, 'UNT_94779', 0, 'ADP power', 'Alarm Display power', '203.20031.01', 'Active', '2016-02-20 05:41:14', 'D', '1'),
(116, 'UNT_94077', 0, 'MPPT', 'Max Peak power tracking', '2', 'Active', '2016-02-20 05:41:14', 'D', '1'),
(117, 'UNT_59840', 0, 'Backhaul', 'backhaul module', '203.20033.03', 'Active', '2016-02-20 05:41:14', 'D', '1'),
(118, 'UNT_65010', 0, 'SPD', 'Surge protection ', '203.20018.03', 'Active', '2016-02-20 05:41:14', 'D', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_unit_revision`
--

CREATE TABLE IF NOT EXISTS `tbl_unit_revision` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `unit_uid` varchar(1000) NOT NULL,
  `unit_unique_id` varchar(100) NOT NULL,
  `unit_revid` float NOT NULL,
  `unit_name` varchar(1000) NOT NULL,
  `unit_description` text NOT NULL,
  `unit_partno` varchar(1000) NOT NULL,
  `unit_type` varchar(1000) NOT NULL,
  `unit_timestamp` varchar(1000) NOT NULL,
  `unit_status` varchar(100) NOT NULL,
  `unit_userid` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `user_id` int(20) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(100) NOT NULL,
  `user_password` varchar(100) NOT NULL,
  `user_mobile` varchar(100) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_type` varchar(100) NOT NULL,
  `user_status` varchar(100) NOT NULL,
  `user_timestamp` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `user_name`, `user_password`, `user_mobile`, `user_email`, `user_type`, `user_status`, `user_timestamp`) VALUES
(1, 'admin', 'admin', '7788778877', 'admin@gmail.com', 'admin', 'A', '2015-12-01 09:31:14'),
(7, 'sunil', 'sunil', '9780951560', 'sbsunilbhatia9@gmail.com', 'user', 'A', '2015-12-01 10:25:15'),
(8, 'super', 'super', '7788994455', 'super@super.com', 'super', 'A', '2015-12-01 10:26:08'),
(9, 'Test', 'NjhjZDQ2ZjhhNmU1YTRjYzU2ZWE5Mjk3MjhlZDQ0NzA', '1230000000', 'hjhj@gmail.com', 'user', 'A', '2015-12-15 07:09:57');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_preveilege`
--

CREATE TABLE IF NOT EXISTS `tbl_user_preveilege` (
  `prev_id` int(11) NOT NULL AUTO_INCREMENT,
  `prev_name` varchar(100) NOT NULL,
  `prev_desc` text NOT NULL,
  `prev_timestamp` datetime NOT NULL,
  `prev_status` varchar(100) NOT NULL,
  PRIMARY KEY (`prev_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `tbl_user_preveilege`
--

INSERT INTO `tbl_user_preveilege` (`prev_id`, `prev_name`, `prev_desc`, `prev_timestamp`, `prev_status`) VALUES
(1, 'Create Unit', 'user can create unit by this previage', '2015-09-17 00:00:00', 'A'),
(2, 'Edit Unit', 'user can edit unit change its value', '2015-09-17 00:00:00', 'A'),
(3, 'Delete Unit', 'user can remove units via this preveilage', '2015-09-17 00:00:00', 'A'),
(4, 'Create Test', 'user can create test via this previlage', '2015-09-17 00:00:00', 'A'),
(5, 'Edit Test', 'user can edit test via this previliage', '2015-09-17 00:00:00', 'A'),
(6, 'Delete Test', 'user can delete test via this prevelige.', '2015-09-17 00:00:00', 'A'),
(7, 'Create Fault', 'user can create faults', '2015-09-17 00:00:00', 'A'),
(8, 'Edit Fault', 'user can edit faults', '2015-09-17 00:00:00', 'A'),
(9, 'Delete Fault', 'user can delete faults', '2015-09-17 00:00:00', 'A'),
(10, 'Create Bugg', 'user can create bugg', '2015-09-17 00:00:00', 'A'),
(11, 'Edit Bugg', 'user can edit bugg', '2015-09-17 00:00:00', 'A'),
(12, 'Delete Bugg', 'user can delete bugg', '2015-09-17 00:00:00', 'A'),
(14, 'All Privilege', 'user can do anythin with databas', '2015-09-17 00:00:00', 'A'),
(15, 'Create Sol', '', '2015-10-21 00:00:00', 'A'),
(16, 'Edit Sol', '', '2015-10-21 00:00:00', 'A'),
(17, 'Delete Sol', '', '2015-10-21 00:00:00', 'A'),
(18, 'Create Ecn', '', '2015-10-21 00:00:00', 'A'),
(19, 'Edit Ecn', '', '2015-10-21 00:00:00', 'A'),
(20, 'Delete Ecn', '', '2015-10-21 00:00:00', 'A'),
(21, 'Edit User', '', '2015-10-21 00:00:00', 'A'),
(22, 'Create User', '', '2015-10-21 00:00:00', 'A'),
(23, 'Delete User', '', '2015-10-14 00:00:00', 'A'),
(24, 'Create Product', 'user can product', '2015-11-04 00:00:00', 'A'),
(25, 'Edit Product', 'user can edit product', '2015-11-04 00:00:00', 'A'),
(26, 'Delete Product    ', 'user can delete product', '2015-11-04 00:00:00', 'A'),
(27, 'Create Format', 'user can create format here by', '2015-11-04 00:00:00', 'A'),
(28, 'Edit Format', 'user can edit format', '2015-11-04 00:00:00', 'A'),
(29, 'Delete Format', 'user can delete format ', '2015-11-04 00:00:00', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_preveilege_links`
--

CREATE TABLE IF NOT EXISTS `tbl_user_preveilege_links` (
  `link_id` int(50) NOT NULL AUTO_INCREMENT,
  `user_id` int(50) NOT NULL,
  `prev_id` int(50) NOT NULL,
  `link_status` varchar(100) NOT NULL,
  PRIMARY KEY (`link_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=335 ;

--
-- Dumping data for table `tbl_user_preveilege_links`
--

INSERT INTO `tbl_user_preveilege_links` (`link_id`, `user_id`, `prev_id`, `link_status`) VALUES
(249, 6, 1, 'A'),
(250, 6, 2, 'A'),
(251, 6, 3, 'A'),
(252, 6, 4, 'A'),
(253, 6, 5, 'A'),
(254, 6, 6, 'A'),
(255, 6, 7, 'A'),
(256, 6, 8, 'A'),
(257, 6, 9, 'A'),
(258, 6, 10, 'A'),
(259, 6, 11, 'A'),
(260, 6, 12, 'A'),
(261, 6, 15, 'A'),
(262, 6, 16, 'A'),
(263, 6, 17, 'A'),
(264, 6, 18, 'A'),
(265, 6, 19, 'A'),
(266, 6, 20, 'A'),
(267, 6, 21, 'A'),
(268, 6, 22, 'A'),
(269, 6, 23, 'A'),
(270, 6, 24, 'A'),
(271, 6, 25, 'A'),
(272, 6, 26, 'A'),
(273, 6, 27, 'A'),
(274, 6, 28, 'A'),
(275, 6, 29, 'A'),
(277, 8, 1, 'A'),
(278, 8, 2, 'A'),
(279, 8, 3, 'A'),
(280, 8, 4, 'A'),
(281, 8, 5, 'A'),
(282, 8, 6, 'A'),
(283, 8, 7, 'A'),
(284, 8, 8, 'A'),
(285, 8, 9, 'A'),
(286, 8, 10, 'A'),
(287, 8, 11, 'A'),
(288, 8, 12, 'A'),
(289, 8, 15, 'A'),
(290, 8, 16, 'A'),
(291, 8, 17, 'A'),
(292, 8, 18, 'A'),
(293, 8, 19, 'A'),
(294, 8, 20, 'A'),
(295, 8, 21, 'A'),
(296, 8, 22, 'A'),
(297, 8, 23, 'A'),
(298, 8, 24, 'A'),
(299, 8, 26, 'A'),
(300, 8, 25, 'A'),
(301, 8, 27, 'A'),
(302, 8, 28, 'A'),
(303, 8, 29, 'A'),
(304, 9, 7, 'A'),
(305, 9, 10, 'A'),
(306, 9, 15, 'A'),
(307, 9, 18, 'A'),
(308, 1, 1, 'A'),
(309, 1, 2, 'A'),
(310, 1, 3, 'A'),
(311, 1, 4, 'A'),
(312, 1, 5, 'A'),
(313, 1, 6, 'A'),
(314, 1, 7, 'A'),
(315, 1, 8, 'A'),
(316, 1, 9, 'A'),
(317, 1, 10, 'A'),
(318, 1, 11, 'A'),
(319, 1, 12, 'A'),
(320, 1, 15, 'A'),
(321, 1, 16, 'A'),
(322, 1, 17, 'A'),
(323, 1, 18, 'A'),
(324, 1, 19, 'A'),
(325, 1, 20, 'A'),
(326, 1, 21, 'A'),
(327, 1, 22, 'A'),
(328, 1, 23, 'A'),
(329, 1, 24, 'A'),
(330, 1, 25, 'A'),
(331, 1, 26, 'A'),
(332, 1, 27, 'A'),
(333, 1, 28, 'A'),
(334, 1, 29, 'A');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

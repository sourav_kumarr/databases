-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: ctesting
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_design_child`
--

DROP TABLE IF EXISTS `tbl_design_child`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_design_child` (
  `dchild_id` int(100) NOT NULL AUTO_INCREMENT,
  `dchild_child_id` varchar(100) NOT NULL,
  `dchild_child_revision` varchar(100) NOT NULL,
  `dchild_parent_id` varchar(100) NOT NULL,
  `dchild_design_id` varchar(100) NOT NULL,
  `dchild_revision` varchar(100) NOT NULL,
  `dchild_level` varchar(100) NOT NULL,
  `dchild_timestamp` datetime NOT NULL,
  PRIMARY KEY (`dchild_id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_design_child`
--

LOCK TABLES `tbl_design_child` WRITE;
/*!40000 ALTER TABLE `tbl_design_child` DISABLE KEYS */;
INSERT INTO `tbl_design_child` VALUES (46,'UNT_40978','1','UNT_40978','design_64724','1','0','2016-02-08 06:30:17'),(47,'BUG_50842','1','UNT_40978','design_64724','1','1','2016-02-08 06:30:17'),(48,'ECN98039','1','BUG_50842','design_64724','1','2','2016-02-08 06:30:17'),(49,'ECN25475','1','BUG_50842','design_64724','1','2','2016-02-08 06:30:17'),(50,'ECN34046','1','BUG_50842','design_64724','1','2','2016-02-08 06:30:17'),(51,'ECN72473','1','BUG_50842','design_64724','1','2','2016-02-08 06:30:17'),(52,'BUG_53602','1','UNT_40978','design_64724','1','1','2016-02-08 06:30:17'),(53,'BUG_38434','1','UNT_40978','design_64724','1','1','2016-02-08 06:30:17'),(54,'BUG_80110','1','UNT_40978','design_64724','1','1','2016-02-08 06:30:17'),(55,'UNT_65101','1','UNT_65101','design_48357','1','0','2016-03-11 11:08:54'),(56,'BUG_50842','1','UNT_65101','design_48357','1','1','2016-03-11 11:08:54'),(57,'ECN98039','1','BUG_50842','design_48357','1','2','2016-03-11 11:08:54'),(58,'ECN25475','1','BUG_50842','design_48357','1','2','2016-03-11 11:08:54'),(59,'ECN34046','1','BUG_50842','design_48357','1','2','2016-03-11 11:08:54'),(60,'ECN72473','1','BUG_50842','design_48357','1','2','2016-03-11 11:08:54'),(61,'BUG_53602','1','UNT_65101','design_48357','1','1','2016-03-11 11:08:54'),(62,'BUG_38434','1','UNT_65101','design_48357','1','1','2016-03-11 11:08:54'),(63,'BUG_80110','1','UNT_65101','design_48357','1','1','2016-03-11 11:08:54');
/*!40000 ALTER TABLE `tbl_design_child` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:53

-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: ctesting
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_user_preveilege`
--

DROP TABLE IF EXISTS `tbl_user_preveilege`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_user_preveilege` (
  `prev_id` int(11) NOT NULL AUTO_INCREMENT,
  `prev_name` varchar(100) NOT NULL,
  `prev_desc` text NOT NULL,
  `prev_timestamp` datetime NOT NULL,
  `prev_status` varchar(100) NOT NULL,
  PRIMARY KEY (`prev_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_user_preveilege`
--

LOCK TABLES `tbl_user_preveilege` WRITE;
/*!40000 ALTER TABLE `tbl_user_preveilege` DISABLE KEYS */;
INSERT INTO `tbl_user_preveilege` VALUES (1,'Create Unit','user can create unit by this previage','2015-09-17 00:00:00','A'),(2,'Edit Unit','user can edit unit change its value','2015-09-17 00:00:00','A'),(3,'Delete Unit','user can remove units via this preveilage','2015-09-17 00:00:00','A'),(4,'Create Test','user can create test via this previlage','2015-09-17 00:00:00','A'),(5,'Edit Test','user can edit test via this previliage','2015-09-17 00:00:00','A'),(6,'Delete Test','user can delete test via this prevelige.','2015-09-17 00:00:00','A'),(7,'Create Fault','user can create faults','2015-09-17 00:00:00','A'),(8,'Edit Fault','user can edit faults','2015-09-17 00:00:00','A'),(9,'Delete Fault','user can delete faults','2015-09-17 00:00:00','A'),(10,'Create Bugg','user can create bugg','2015-09-17 00:00:00','A'),(11,'Edit Bugg','user can edit bugg','2015-09-17 00:00:00','A'),(12,'Delete Bugg','user can delete bugg','2015-09-17 00:00:00','A'),(14,'All Privilege','user can do anythin with databas','2015-09-17 00:00:00','A'),(15,'Create Sol','','2015-10-21 00:00:00','A'),(16,'Edit Sol','','2015-10-21 00:00:00','A'),(17,'Delete Sol','','2015-10-21 00:00:00','A'),(18,'Create Ecn','','2015-10-21 00:00:00','A'),(19,'Edit Ecn','','2015-10-21 00:00:00','A'),(20,'Delete Ecn','','2015-10-21 00:00:00','A'),(21,'Edit User','','2015-10-21 00:00:00','A'),(22,'Create User','','2015-10-21 00:00:00','A'),(23,'Delete User','','2015-10-14 00:00:00','A'),(24,'Create Product','user can product','2015-11-04 00:00:00','A'),(25,'Edit Product','user can edit product','2015-11-04 00:00:00','A'),(26,'Delete Product    ','user can delete product','2015-11-04 00:00:00','A'),(27,'Create Format','user can create format here by','2015-11-04 00:00:00','A'),(28,'Edit Format','user can edit format','2015-11-04 00:00:00','A'),(29,'Delete Format','user can delete format ','2015-11-04 00:00:00','A'),(30,'Create Design','user can create design respolution','2016-03-08 04:07:18','A'),(31,'Edit Design','user can edit design resolution','2016-03-08 03:05:19','A'),(32,'Delete Design','user can delete design resolution','2016-03-08 03:07:10','A'),(33,'Create Process','user can create process wizard','2016-03-08 02:06:08','A'),(34,'Edit Process','user can edit process wizard','2016-03-08 03:05:07','A'),(35,'Delete Process','user can delete process wizard','2016-03-08 02:06:10','A');
/*!40000 ALTER TABLE `tbl_user_preveilege` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:54

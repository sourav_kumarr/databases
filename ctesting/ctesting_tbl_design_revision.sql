-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: ctesting
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_design_revision`
--

DROP TABLE IF EXISTS `tbl_design_revision`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_design_revision` (
  `rev_id` int(100) NOT NULL AUTO_INCREMENT,
  `rev_design_no` varchar(100) NOT NULL,
  `rev_design_id` varchar(100) NOT NULL,
  `rev_design_name` varchar(100) NOT NULL,
  `rev_design_unique` varchar(100) NOT NULL,
  `rev_unique_id` varchar(100) NOT NULL,
  `rev_design_unit` varchar(100) NOT NULL,
  `rev_design_data` text NOT NULL,
  `rev_design_units` text NOT NULL,
  `rev_design_status` varchar(100) NOT NULL,
  `rev_design_timestamp` varchar(100) NOT NULL,
  `rev_design_userid` varchar(100) NOT NULL,
  PRIMARY KEY (`rev_id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_design_revision`
--

LOCK TABLES `tbl_design_revision` WRITE;
/*!40000 ALTER TABLE `tbl_design_revision` DISABLE KEYS */;
INSERT INTO `tbl_design_revision` VALUES (55,'1','23','testing design here','design_64724','revision_43219','UNT_66244','[{\"attributes\":{\"id\":0,\"rel\":\"folder\"},\"data\":\"UNT_40978 Revision : 1\",\"unitRevision\":1,\"unitName\":\"SPD3\",\"unitId\":\"UNT_40978\",\"parentid\":\"\",\"level\":0,\"state\":\"open\",\"children\":[{\"attributes\":{\"id\":1,\"rel\":\"folder\"},\"data\":\"BUG_50842 Revision : 1\",\"buggRevision\":1,\"buggName\":\"software issues\",\"buggId\":\"BUG_50842\",\"parentid\":0,\"level\":1,\"state\":\"open\",\"children\":[{\"attributes\":{\"id\":5,\"rel\":\"folder\"},\"data\":\"ECN98039 Revision : 1\",\"ecnRevision\":1,\"ecnName\":\"mechanical change of backhaul\",\"ecnId\":\"ECN98039\",\"parentid\":1,\"level\":2,\"state\":\"open\",\"children\":[]},{\"attributes\":{\"id\":6,\"rel\":\"folder\"},\"data\":\"ECN25475 Revision : 1\",\"ecnRevision\":1,\"ecnName\":\"software change in UCC\",\"ecnId\":\"ECN25475\",\"parentid\":1,\"level\":2,\"state\":\"open\",\"children\":[]},{\"attributes\":{\"id\":7,\"rel\":\"folder\"},\"data\":\"ECN34046 Revision : 1\",\"ecnRevision\":1,\"ecnName\":\"software change in ECU\",\"ecnId\":\"ECN34046\",\"parentid\":1,\"level\":2,\"state\":\"open\",\"children\":[]},{\"attributes\":{\"id\":8,\"rel\":\"folder\"},\"data\":\"ECN72473 Revision : 1\",\"ecnRevision\":1,\"ecnName\":\"software change in ADP\",\"ecnId\":\"ECN72473\",\"parentid\":1,\"level\":2,\"state\":\"open\",\"children\":[]}]},{\"attributes\":{\"id\":2,\"rel\":\"folder\"},\"data\":\"BUG_53602 Revision : 1\",\"buggRevision\":1,\"buggName\":\"underrated or over rated parts\",\"buggId\":\"BUG_53602\",\"parentid\":0,\"level\":1,\"state\":\"open\",\"children\":[]},{\"attributes\":{\"id\":3,\"rel\":\"folder\"},\"data\":\"BUG_38434 Revision : 1\",\"buggRevision\":1,\"buggName\":\"wrong footprint\",\"buggId\":\"BUG_38434\",\"parentid\":0,\"level\":1,\"state\":\"open\",\"children\":[]},{\"attributes\":{\"id\":4,\"rel\":\"folder\"},\"data\":\"BUG_80110 Revision : 1\",\"buggRevision\":1,\"buggName\":\"wrong component value\",\"buggId\":\"BUG_80110\",\"parentid\":0,\"level\":1,\"state\":\"open\",\"children\":[]}]}]','[{\"attributes\":{\"id\":0,\"rel\":\"folder\"},\"data\":\"UNT_40978 Revision : 1\",\"unitRevision\":1,\"unitName\":\"SPD3\",\"unitId\":\"UNT_40978\",\"parentid\":\"\",\"level\":0,\"state\":\"open\"},{\"attributes\":{\"id\":1,\"rel\":\"folder\"},\"data\":\"BUG_50842 Revision : 1\",\"buggRevision\":1,\"buggName\":\"software issues\",\"buggId\":\"BUG_50842\",\"parentid\":0,\"level\":1,\"state\":\"open\"},{\"attributes\":{\"id\":2,\"rel\":\"folder\"},\"data\":\"BUG_53602 Revision : 1\",\"buggRevision\":1,\"buggName\":\"underrated or over rated parts\",\"buggId\":\"BUG_53602\",\"parentid\":0,\"level\":1,\"state\":\"open\"},{\"attributes\":{\"id\":3,\"rel\":\"folder\"},\"data\":\"BUG_38434 Revision : 1\",\"buggRevision\":1,\"buggName\":\"wrong footprint\",\"buggId\":\"BUG_38434\",\"parentid\":0,\"level\":1,\"state\":\"open\"},{\"attributes\":{\"id\":4,\"rel\":\"folder\"},\"data\":\"BUG_80110 Revision : 1\",\"buggRevision\":1,\"buggName\":\"wrong component value\",\"buggId\":\"BUG_80110\",\"parentid\":0,\"level\":1,\"state\":\"open\"},{\"attributes\":{\"id\":5,\"rel\":\"folder\"},\"data\":\"ECN98039 Revision : 1\",\"ecnRevision\":1,\"ecnName\":\"mechanical change of backhaul\",\"ecnId\":\"ECN98039\",\"parentid\":1,\"level\":2,\"state\":\"open\"},{\"attributes\":{\"id\":6,\"rel\":\"folder\"},\"data\":\"ECN25475 Revision : 1\",\"ecnRevision\":1,\"ecnName\":\"software change in UCC\",\"ecnId\":\"ECN25475\",\"parentid\":1,\"level\":2,\"state\":\"open\"},{\"attributes\":{\"id\":7,\"rel\":\"folder\"},\"data\":\"ECN34046 Revision : 1\",\"ecnRevision\":1,\"ecnName\":\"software change in ECU\",\"ecnId\":\"ECN34046\",\"parentid\":1,\"level\":2,\"state\":\"open\"},{\"attributes\":{\"id\":8,\"rel\":\"folder\"},\"data\":\"ECN72473 Revision : 1\",\"ecnRevision\":1,\"ecnName\":\"software change in ADP\",\"ecnId\":\"ECN72473\",\"parentid\":1,\"level\":2,\"state\":\"open\"}]','D','2016-02-08 06:30:17','1'),(56,'1','24','testing design here','design_48357','revision_65222','UNT_65101','[{\"attributes\":{\"id\":0,\"rel\":\"folder\"},\"data\":\"UNT_65101 Revision : 1\",\"unitRevision\":1,\"unitName\":\"ggg\",\"unitId\":\"UNT_65101\",\"parentid\":\"\",\"level\":0,\"state\":\"open\",\"children\":[{\"attributes\":{\"id\":1,\"rel\":\"folder\"},\"data\":\"BUG_50842 Revision : 1\",\"buggRevision\":1,\"buggName\":\"software issues\",\"buggId\":\"BUG_50842\",\"parentid\":0,\"level\":1,\"state\":\"open\",\"children\":[{\"attributes\":{\"id\":5,\"rel\":\"folder\"},\"data\":\"ECN98039 Revision : 1\",\"ecnRevision\":1,\"ecnName\":\"mechanical change of backhaul\",\"ecnId\":\"ECN98039\",\"parentid\":1,\"level\":2,\"state\":\"open\",\"children\":[]},{\"attributes\":{\"id\":6,\"rel\":\"folder\"},\"data\":\"ECN25475 Revision : 1\",\"ecnRevision\":1,\"ecnName\":\"software change in UCC\",\"ecnId\":\"ECN25475\",\"parentid\":1,\"level\":2,\"state\":\"open\",\"children\":[]},{\"attributes\":{\"id\":7,\"rel\":\"folder\"},\"data\":\"ECN34046 Revision : 1\",\"ecnRevision\":1,\"ecnName\":\"software change in ECU\",\"ecnId\":\"ECN34046\",\"parentid\":1,\"level\":2,\"state\":\"open\",\"children\":[]},{\"attributes\":{\"id\":8,\"rel\":\"folder\"},\"data\":\"ECN72473 Revision : 1\",\"ecnRevision\":1,\"ecnName\":\"software change in ADP\",\"ecnId\":\"ECN72473\",\"parentid\":1,\"level\":2,\"state\":\"open\",\"children\":[]}]},{\"attributes\":{\"id\":2,\"rel\":\"folder\"},\"data\":\"BUG_53602 Revision : 1\",\"buggRevision\":1,\"buggName\":\"underrated or over rated parts\",\"buggId\":\"BUG_53602\",\"parentid\":0,\"level\":1,\"state\":\"open\",\"children\":[]},{\"attributes\":{\"id\":3,\"rel\":\"folder\"},\"data\":\"BUG_38434 Revision : 1\",\"buggRevision\":1,\"buggName\":\"wrong footprint\",\"buggId\":\"BUG_38434\",\"parentid\":0,\"level\":1,\"state\":\"open\",\"children\":[]},{\"attributes\":{\"id\":4,\"rel\":\"folder\"},\"data\":\"BUG_80110 Revision : 1\",\"buggRevision\":1,\"buggName\":\"wrong component value\",\"buggId\":\"BUG_80110\",\"parentid\":0,\"level\":1,\"state\":\"open\",\"children\":[]}]}]','[{\"attributes\":{\"id\":0,\"rel\":\"folder\"},\"data\":\"UNT_65101 Revision : 1\",\"unitRevision\":1,\"unitName\":\"ggg\",\"unitId\":\"UNT_65101\",\"parentid\":\"\",\"level\":0,\"state\":\"open\"},{\"attributes\":{\"id\":1,\"rel\":\"folder\"},\"data\":\"BUG_50842 Revision : 1\",\"buggRevision\":1,\"buggName\":\"software issues\",\"buggId\":\"BUG_50842\",\"parentid\":0,\"level\":1,\"state\":\"open\"},{\"attributes\":{\"id\":2,\"rel\":\"folder\"},\"data\":\"BUG_53602 Revision : 1\",\"buggRevision\":1,\"buggName\":\"underrated or over rated parts\",\"buggId\":\"BUG_53602\",\"parentid\":0,\"level\":1,\"state\":\"open\"},{\"attributes\":{\"id\":3,\"rel\":\"folder\"},\"data\":\"BUG_38434 Revision : 1\",\"buggRevision\":1,\"buggName\":\"wrong footprint\",\"buggId\":\"BUG_38434\",\"parentid\":0,\"level\":1,\"state\":\"open\"},{\"attributes\":{\"id\":4,\"rel\":\"folder\"},\"data\":\"BUG_80110 Revision : 1\",\"buggRevision\":1,\"buggName\":\"wrong component value\",\"buggId\":\"BUG_80110\",\"parentid\":0,\"level\":1,\"state\":\"open\"},{\"attributes\":{\"id\":5,\"rel\":\"folder\"},\"data\":\"ECN98039 Revision : 1\",\"ecnRevision\":1,\"ecnName\":\"mechanical change of backhaul\",\"ecnId\":\"ECN98039\",\"parentid\":1,\"level\":2,\"state\":\"open\"},{\"attributes\":{\"id\":6,\"rel\":\"folder\"},\"data\":\"ECN25475 Revision : 1\",\"ecnRevision\":1,\"ecnName\":\"software change in UCC\",\"ecnId\":\"ECN25475\",\"parentid\":1,\"level\":2,\"state\":\"open\"},{\"attributes\":{\"id\":7,\"rel\":\"folder\"},\"data\":\"ECN34046 Revision : 1\",\"ecnRevision\":1,\"ecnName\":\"software change in ECU\",\"ecnId\":\"ECN34046\",\"parentid\":1,\"level\":2,\"state\":\"open\"},{\"attributes\":{\"id\":8,\"rel\":\"folder\"},\"data\":\"ECN72473 Revision : 1\",\"ecnRevision\":1,\"ecnName\":\"software change in ADP\",\"ecnId\":\"ECN72473\",\"parentid\":1,\"level\":2,\"state\":\"open\"}]','D','2016-03-11 11:08:54','7');
/*!40000 ALTER TABLE `tbl_design_revision` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:48

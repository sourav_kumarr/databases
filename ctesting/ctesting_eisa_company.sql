-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: ctesting
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `eisa_company`
--

DROP TABLE IF EXISTS `eisa_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eisa_company` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `company_id` varchar(100) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `company_logo` varchar(500) NOT NULL,
  `company_admin_name` varchar(100) NOT NULL,
  `company_admin_email` varchar(100) NOT NULL,
  `company_admin_contact` varchar(100) NOT NULL,
  `company_admin_address` text NOT NULL,
  `company_terms` text NOT NULL,
  `app_type` varchar(100) NOT NULL,
  `req_emp_id` varchar(100) NOT NULL,
  `email_veri` varchar(100) NOT NULL,
  `email_domain_ver` varchar(100) NOT NULL,
  `app_emp_id` varchar(100) NOT NULL,
  `auto_approval` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eisa_company`
--

LOCK TABLES `eisa_company` WRITE;
/*!40000 ALTER TABLE `eisa_company` DISABLE KEYS */;
INSERT INTO `eisa_company` VALUES (1,'Puls8895','Pulse Engineering','Pulse Engineering1084user2-160x160.jpg','Parveen','Parveen@gmail.com','9041100223','Phase 8b Mohali (Punjab) 160058','1.) Always Come in Formals\n2.)Exit Pass Required to Leave Before Time \n3.)Always Keep Silence','Auto','No','Yes','Yes','No','No'),(4,'Sach3983','Sachtech','Sachtech4058user2-160x160.jpg','Kapil','kapillikes@gmail.com','7087425488','Plot No. E110 Phase & Industrial Area Mohali (Punjab) 160055','1.) Always Come in Formals\r\n2.)Exit Pass Required to Leave Before Time \r\n3.)Always Keep Silence','Manual','Yes','No','No','Yes','Yes'),(5,'ABC 6224','ABC CO.','ABC CO.4065download.png','Thirumaran','thiru@bizsmartech.com','0124539171','na','this t n c','Auto','Yes','No','Yes','No','Yes');
/*!40000 ALTER TABLE `eisa_company` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:53

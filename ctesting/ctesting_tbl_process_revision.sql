-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: ctesting
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_process_revision`
--

DROP TABLE IF EXISTS `tbl_process_revision`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_process_revision` (
  `rev_id` int(100) NOT NULL AUTO_INCREMENT,
  `rev_unique` varchar(100) NOT NULL,
  `rev_process_id` varchar(100) NOT NULL,
  `rev_process_unique` varchar(100) NOT NULL,
  `rev_process_name` varchar(100) NOT NULL,
  `rev_process_desc` text NOT NULL,
  `rev_process_unit` varchar(100) NOT NULL,
  `rev_process_status` varchar(100) NOT NULL,
  `rev_process_revision` varchar(3) NOT NULL,
  `rev_process_user` varchar(100) NOT NULL,
  `rev_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`rev_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_process_revision`
--

LOCK TABLES `tbl_process_revision` WRITE;
/*!40000 ALTER TABLE `tbl_process_revision` DISABLE KEYS */;
INSERT INTO `tbl_process_revision` VALUES (32,'revision_61606','20','process_13588','fffff','uuuuuuuuuu','UNT_16699','D','1','1','2016-02-20 08:38:02'),(33,'revision_88515','21','process_45937','process test','process described here','UNT_65101','D','1','7','2016-03-11 11:13:55'),(34,'revision_77367','22','process_18787','process test1','process described here','UNT_94077','D','1','7','2016-03-11 11:13:55'),(35,'revision_25710','20','process_13588','fffff','					uuuuuuuuuuyyyyy','UNT_34835','D','1.1','1','2016-08-01 11:18:28');
/*!40000 ALTER TABLE `tbl_process_revision` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:49

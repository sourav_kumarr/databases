-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: ctesting
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_process_item`
--

DROP TABLE IF EXISTS `tbl_process_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_process_item` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_unique` varchar(100) NOT NULL,
  `item_phase` varchar(100) NOT NULL,
  `item_format_unique` varchar(100) NOT NULL,
  `item_process_id` varchar(100) NOT NULL,
  `item_process_revision` varchar(100) NOT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_process_item`
--

LOCK TABLES `tbl_process_item` WRITE;
/*!40000 ALTER TABLE `tbl_process_item` DISABLE KEYS */;
INSERT INTO `tbl_process_item` VALUES (57,'item_83886','Phase 1','format_59624','20','1'),(58,'item_83886','Phase 2','format_84294','20','1'),(59,'item_83886','Phase 3','format_25555','20','1'),(63,'item_65440','phase 0','format_25555','22','1'),(64,'item_65440','phase 1','format_84294','22','1'),(65,'item_65440','phase 2','format_59624','22','1'),(66,'item_95612','phase 0','format_25555','21','1'),(67,'item_95612','phase 1','format_84294','21','1'),(68,'item_95612','phase 2','format_59624','21','1'),(69,'item_17669','phase 0','format_25555','22','1'),(70,'item_17669','phase 1','format_84294','22','1'),(71,'item_17669','phase 2','format_59624','22','1'),(72,'item_91582','Phase 1','format_59624','20','1.1');
/*!40000 ALTER TABLE `tbl_process_item` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:50

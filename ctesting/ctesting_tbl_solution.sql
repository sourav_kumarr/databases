-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: ctesting
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_solution`
--

DROP TABLE IF EXISTS `tbl_solution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_solution` (
  `sol_id` int(10) NOT NULL AUTO_INCREMENT,
  `sol_unique_id` varchar(100) NOT NULL,
  `sol_name` varchar(1000) NOT NULL,
  `sol_description` varchar(1000) NOT NULL,
  `sol_revision` varchar(1000) NOT NULL,
  `sol_userid` varchar(1000) NOT NULL,
  `sol_status` varchar(100) NOT NULL DEFAULT 'D',
  `sol_timestamp` varchar(1000) NOT NULL,
  PRIMARY KEY (`sol_id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_solution`
--

LOCK TABLES `tbl_solution` WRITE;
/*!40000 ALTER TABLE `tbl_solution` DISABLE KEYS */;
INSERT INTO `tbl_solution` VALUES (45,'SLN76000','correct the polarity ','correct the polarity ','1','1','D','2016-02-03 01:14:04'),(46,'SLN76370','correct soldering','correct soldering','1','1','D','2016-02-03 01:14:04'),(47,'SLN68563','remove track shorting','remove track shorting','1','1','D','2016-02-03 01:14:04'),(48,'SLN42866','check component values','check component values','1','1','D','2016-02-03 01:14:04'),(49,'SLN73541','PWM IC pin values to check','PWM IC pin values to check','1','1','D','2016-02-03 01:14:04'),(50,'SLN89631','Switch off  & correct the test setup ','Switch off  & correct the test setup ','1','1','D','2016-02-03 01:14:04'),(51,'SLN36676','check voltage & buzzer also','check voltage & buzzer also','1','1','D','2016-02-03 01:14:04'),(52,'SLN36818','check one by one circuit','check one by one circuit','1','1','D','2016-02-03 01:14:04'),(53,'SLN34671','check noise ','check noise ','1','1','D','2016-02-03 01:14:04'),(54,'SLN75277','repeat & if fail again check IC','repeat & if fail again check IC','1','1','D','2016-02-03 01:14:04'),(55,'SLN33270','check shorting & load connection','check shorting & load connection','1','1','D','2016-02-03 01:14:04'),(56,'SLN24447','replace fuse ','replace fuse ','1','1','D','2016-02-03 01:14:04'),(57,'SLN86968','replace the componets & correct the setup','replace the componets & correct the setup','1','1','D','2016-02-03 01:14:04'),(58,'SLN94630','replace diode','replace diode','1','1','D','2016-02-03 01:14:04'),(59,'SLN27482','set required range input volatge','set required range input volatge','1','1','D','2016-02-03 01:14:04'),(60,'SLN51920','check reasons & correct them','check reasons & correct them','1','1','D','2016-02-03 01:14:04'),(61,'SLN44034','replace bridge ','replace bridge ','1','1','D','2016-02-03 01:14:04'),(62,'SLN67267','replace Mosfet & related components ','replace Mosfet & related components ','1','1','D','2016-02-03 01:14:04'),(63,'SLN51425','replace mosfets & circuitary','replace mosfets & circuitary','1','1','D','2016-02-03 01:14:04'),(64,'SLN20423','track the signal & IC also','track the signal & IC also','1','1','D','2016-02-03 01:14:04'),(65,'SLN34948','check correct values of capacitor & R','check correct values of capacitor & R','1','1','D','2016-02-03 01:14:04'),(66,'SLN58968','replace reset switch ','replace reset switch ','1','1','D','2016-02-03 01:14:04');
/*!40000 ALTER TABLE `tbl_solution` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:54

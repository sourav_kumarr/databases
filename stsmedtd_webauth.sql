-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 12, 2017 at 02:27 PM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `stsmedtd_webauth`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `avatar` text NOT NULL,
  `signup_date` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `avatar`, `signup_date`) VALUES
(1, 'satish', 'satish', 'hicatchme@gmail.com', ' ', 1439892454),
(2, 'mandar12', 'mandar12', 'mandarkallol@gmail.com', ' ', 1439896688),
(3, 'test123', 'test123', 'asdfasdf@sdfsdf.dsff', ' ', 1442311290),
(4, 'satish123', 'satish123', 'SDFsdf@asdas.asdasd', ' ', 1444230146),
(5, 'michael', 'michael', 'michael@gmail.com', ' ', 1445624777),
(6, 'heavenapps', 'heavenapps', 'heavenapps@gmail.com', ' ', 1445625133),
(7, 'test', '123456', 'test@gmail.com', '  ', 1451834660),
(8, 'admin', 'deathle55', 'deathless.test@gmail.com', ' ', 1452101352),
(9, 'Bader', 'Aa123456', 'bader@gmail.com', ' ', 1463295715),
(10, 'Saba', 'Appimize123', 'saba.amber@saremcotech.com', ' ', 1471415915),
(11, 'wealthvest', 'miller16', 'sbonander@wealthvest.com', ' ', 1472760182),
(12, 'demo1', '123456', 'demo1@gmail.com', '   ', 1474380162),
(13, 'atultiwari', '12345678', 'atultiwari88@gmail.com', ' ', 1476849282),
(14, 'loboadmin', 'loboadmin', 'loboadmin@me.com', '   ', 1478021292),
(15, 'akram', '123456', 'akram01127@gmail.com', ' ', 1479621049),
(16, 'aaaaa', 'aaaaaa', 'test@test.com', '  ', 1480442701),
(17, 'aligaas', 'mongoli4A', 'aligaas@live.com', ' ', 1480496250),
(18, 'manasak19', 'mrls191505', 'manasak19@gmail.com', ' ', 1481159220),
(19, 'superuser', 'Snow1!', 'sbonander@wealthvest.com', '  ', 1482871421),
(20, 'testuser', 'snowed', 'sbonander@wealthvest.com', '  ', 1482871484),
(21, 'asdfg', '123456', 'dealflow@runbox.com', '  ', 1483845571),
(22, 'pjotru@gmail.com', 'kolikoli', 'pjotru@gmail.com', ' ', 1484242489),
(23, 'pjotru', 'kolikoli', 'pj.otru@gmail.com', ' ', 1484242515),
(24, 'Doomfayez', 'alotibi511', 'doomfayez@gmail.com', ' ', 1485482291),
(25, 'yanos', 'krzeselko4', 'yanoshumoes@gmail.com', ' ', 1485646341),
(26, 'test1290', 'test1290', 'tanks10269@gmail.com', ' ', 1486426705),
(27, 'nikunduku', 'test@123', 'someone@123.com', '  ', 1488447628),
(28, 'nikundukunikunduku', 'test@123456', 'admin@1233.com', ' ', 1488447689),
(29, 'nikundukunikundukunikunduku', '12345678a', 'admin@jaffa.co', '  ', 1488447767),
(30, 'eric', 'karolly7', 'ericallyson@gmail.com', ' ', 1488562400),
(31, 'sabaody', 'Alchem1st', 'sabaody@yopmail.com', ' ', 1488685444);

-- --------------------------------------------------------

--
-- Table structure for table `user_pwd`
--

CREATE TABLE IF NOT EXISTS `user_pwd` (
  `name` char(30) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `pass` char(32) COLLATE latin1_general_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `user_pwd`
--

INSERT INTO `user_pwd` (`name`, `pass`) VALUES
('xampp', 'wampp');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user_pwd`
--
ALTER TABLE `user_pwd`
 ADD PRIMARY KEY (`name`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

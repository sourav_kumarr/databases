-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: grocery
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `grocery_category`
--

DROP TABLE IF EXISTS `grocery_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grocery_category` (
  `cat_id` bigint(100) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(100) NOT NULL,
  `cat_parent_id` int(100) NOT NULL,
  `cat_link` varchar(100) NOT NULL,
  `cat_sort_order` bigint(100) NOT NULL,
  `cat_loc_id` varchar(255) NOT NULL,
  `cat_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(255) NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grocery_category`
--

LOCK TABLES `grocery_category` WRITE;
/*!40000 ALTER TABLE `grocery_category` DISABLE KEYS */;
INSERT INTO `grocery_category` VALUES (17,'DRINK AND BEVERAGES',0,'#',1,'1','2016-09-13 08:14:49','1'),(18,'GULUCOSE',17,'#',1,'1','2016-10-03 14:00:14','1'),(19,'SOUPES',17,'soupes',1,'1','2016-09-13 08:14:49','1'),(20,'SODA',17,'#',1,'1','2016-10-04 05:29:43','1'),(21,'BREAKFAST AND CEREALS',0,'#',2,'1','2016-09-13 08:14:49','1'),(22,'FLAKES,CHOCOS,LOOPS',21,'flakes,chocos,loops',2,'1','2016-09-13 08:14:49','1'),(23,'OATS',21,'oats',2,'1','2016-09-13 08:14:49','1'),(24,'MOUSELI',21,'#',2,'1','2017-08-15 04:14:52','1'),(25,'SAUCES,JAMS AND PICKELES',0,'#',3,'1','2016-09-13 08:14:49','1'),(26,'PICKLES',25,'pickles',3,'1','2016-09-13 08:14:49','1'),(27,'SAUCES',25,'#',3,'1','2016-09-13 12:18:52','1'),(28,'OTHER JAMS',25,'other jams',3,'1','2016-09-13 08:14:49','1'),(29,'JAM',27,'jam',3,'1','2016-09-13 12:25:20','1'),(30,'JAM 1',27,'jam 1',3,'1','2016-09-13 12:28:40','1'),(40,'gggg',20,'#',1,'1','2016-10-04 06:00:00',''),(41,'iiii',40,'iiii',1,'1','2016-10-04 06:00:00',''),(42,'MOUSEL',24,'#',2,'1','2017-08-15 04:15:21',''),(43,'MOUSELrrrrrr',42,'mouselrrrrrr',2,'1','2017-08-15 04:15:21',''),(44,'SAUCE',27,'sauce',3,'1','2017-08-26 16:00:19','');
/*!40000 ALTER TABLE `grocery_category` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:34

-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: grocery
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `grocery_products`
--

DROP TABLE IF EXISTS `grocery_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grocery_products` (
  `prod_id` bigint(100) NOT NULL AUTO_INCREMENT,
  `prod_name` varchar(100) NOT NULL,
  `prod_desc` text NOT NULL,
  `prod_price` bigint(100) NOT NULL,
  `prod_discount_price` bigint(100) NOT NULL,
  `prod_unit` varchar(100) NOT NULL,
  `prod_quantity` bigint(100) NOT NULL,
  `prod_cat_id` bigint(100) NOT NULL,
  `prod_off_percentage` varchar(100) NOT NULL,
  `prod_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `prod_created_by` bigint(100) NOT NULL,
  PRIMARY KEY (`prod_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grocery_products`
--

LOCK TABLES `grocery_products` WRITE;
/*!40000 ALTER TABLE `grocery_products` DISABLE KEYS */;
INSERT INTO `grocery_products` VALUES (1,'GLUCON-D NIMBU PANI 500GM','GLUCON-D IS THE PREFERRED CHOICE IN SUMMER WHEN THE SCORCHING HEAT DRAINS OUT BODY GLUCOSE. GLUCON-D CONTAINS 99.4% GLUCOSE. IT IS EASILY ABSORBED BY BODY, THUS GIVING INSTANT ENERGY & REJUVENATION.',140,135,'GM',500,18,'10% off','2016-09-14 02:59:47',1),(2,'GLUCON-D AAM PANA 500GM','GLUCON-D AAM PANA 500GM\r\nGlucon-D is the preferred choice in summer when the scorching heat drains out body glucose. Glucon-D contains 99.4 percent glucose. It is easily absorbed by body, thus giving instant energy and rejuvenation. It restores energy 2 times faster compared to ordinary drinks.',140,135,'GM',500,18,'','2016-09-14 02:59:47',1),(3,'CHINGS INSTANT SOUP MANCHOW 15GM','CHINGS INSTANT SOUP MANCHOW 15GM\r\nSoup is an integral part of every Chinese meal, and is chosen to compliment the dishes being served. Soups provide a textural contrast,',10,10,'GM',15,19,'','2016-09-14 02:58:04',1),(4,'CHINGS INSTANT SOUP MIX VEG 15GM','CHINGS INSTANT SOUP MIX VEG 15GM\r\nSoup is an integral part of every Chinese meal, and is chosen to compliment the dishes being served. Soups provide a textural contrast,',10,10,'GM',15,19,'','2016-09-14 02:59:03',1);
/*!40000 ALTER TABLE `grocery_products` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:36

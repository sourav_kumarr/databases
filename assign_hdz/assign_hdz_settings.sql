-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: assign
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `hdz_settings`
--

DROP TABLE IF EXISTS `hdz_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hdz_settings` (
  `field` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  UNIQUE KEY `field` (`field`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hdz_settings`
--

LOCK TABLES `hdz_settings` WRITE;
/*!40000 ALTER TABLE `hdz_settings` DISABLE KEYS */;
INSERT INTO `hdz_settings` VALUES ('use_captcha','0'),('email_ticket','support@mysite.com'),('site_name','HelpDeskz Support Center'),('site_url','http://localhost/helpdesk'),('windows_title','HelpDeskZ Support Center'),('show_tickets','DESC'),('ticket_reopen','0'),('tickets_page','20'),('timezone','America/Lima'),('ticket_attachment','1'),('permalink','0'),('loginshare','1'),('loginshare_url','localhost/helpdesk/login.php'),('date_format','d F Y h:i a'),('page_size','25'),('login_attempt','300'),('login_attempt_minutes','0'),('overdue_time','72'),('knowledgebase_columns','2'),('knowledgebase_articlesundercat','2'),('knowledgebase_articlemaxchar','200'),('knowledgebase_mostpopular','yes'),('knowledgebase_mostpopulartotal','4'),('knowledgebase_newest','yes'),('knowledgebase_newesttotal','4'),('knowledgebase','yes'),('news','yes'),('news_page','4'),('homepage','knowledgebase'),('email_piping','yes'),('smtp','no'),('smtp_hostname','smtp.gmail.com'),('smtp_port','587'),('smtp_ssl','tls'),('smtp_username','mail@gmail.com'),('smtp_password','password'),('tickets_replies','10'),('helpdeskz_version','1.0.2'),('closeticket_time','72'),('client_language','english'),('staff_language','english'),('client_multilanguage','0'),('maintenance','0'),('facebookoauth','0'),('facebookappid',NULL),('facebookappsecret',NULL),('googleoauth','0'),('googleclientid',NULL),('googleclientsecret',NULL),('socialbuttonnews','0'),('socialbuttonkb','0');
/*!40000 ALTER TABLE `hdz_settings` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:22

-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 12, 2017 at 02:25 PM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `stsmedtd_ihavephone`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
`admin_id` int(100) NOT NULL,
  `admin_name` varchar(100) NOT NULL,
  `admin_email` varchar(100) NOT NULL,
  `admin_password` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_name`, `admin_email`, `admin_password`) VALUES
(1, 'Sunil', 'admin@gmail.com', '25d55ad283aa400af464c76d713c07ad');

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE IF NOT EXISTS `brand` (
`brand_id` int(100) NOT NULL,
  `brand_name` varchar(100) NOT NULL,
  `brand_status` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`brand_id`, `brand_name`, `brand_status`) VALUES
(9, 'Samsung', '1'),
(10, 'iPhone', '1');

-- --------------------------------------------------------

--
-- Table structure for table `cell_carrier`
--

CREATE TABLE IF NOT EXISTS `cell_carrier` (
`carrier_id` int(100) NOT NULL,
  `carrier_name` varchar(100) NOT NULL,
  `carrier_image` varchar(100) NOT NULL,
  `carrier_status` enum('0','1') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cell_carrier`
--

INSERT INTO `cell_carrier` (`carrier_id`, `carrier_name`, `carrier_image`, `carrier_status`) VALUES
(2, 'AT&T', 'File13059838.jpg', '1'),
(3, 'SPRINT', 'File296295074.jpg', '1'),
(4, 'VERIZON', 'File869295466.jpg', '1'),
(5, 'FACTORY UNLOCKED', 'File61626110.jpg', '1'),
(6, 'T-MOBILE', 'File1180394164.jpg', '1'),
(7, 'OTHER CARRIERS', 'File574187650.jpg', '1');

-- --------------------------------------------------------

--
-- Table structure for table `model`
--

CREATE TABLE IF NOT EXISTS `model` (
`model_id` int(100) NOT NULL,
  `model_name` varchar(100) NOT NULL,
  `brand_id` varchar(100) NOT NULL,
  `model_image` varchar(100) NOT NULL,
  `model_status` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `model`
--

INSERT INTO `model` (`model_id`, `model_name`, `brand_id`, `model_image`, `model_status`) VALUES
(15, '4', '10', 'File34059463.png', '1'),
(16, '4S', '10', 'File457279839.png', '1'),
(17, '5', '10', 'File1017146189.png', '1'),
(18, '5C', '10', 'File712503207.png', '1'),
(19, '5S', '10', 'File1100328453.png', '1'),
(20, '6', '10', 'File1120548941.png', '1'),
(21, '6S', '10', 'File1177500223.png', '1'),
(22, '6S Plus', '10', 'File224799881.png', '1'),
(23, '7', '10', 'File828631880.png', '1'),
(24, '7 Plus', '10', 'File547288836.png', '1'),
(25, 'Galaxy S3', '9', 'File647463729.png', '1'),
(26, 'Galaxy S4', '9', 'File764890966.png', '1'),
(27, 'Galaxy S5', '9', 'File1008019143.png', '1'),
(28, 'Galaxy S6', '9', 'File893114830.png', '1'),
(29, 'Galaxy S6 Edge', '9', 'File431605384.png', '1'),
(30, 'Galaxy S7', '9', 'File844622761.png', '1'),
(31, 'Galaxy S7 Edge', '9', 'File407674715.png', '1'),
(32, 'Galaxy S8', '9', 'File395394015.png', '1');

-- --------------------------------------------------------

--
-- Table structure for table `orderdetail`
--

CREATE TABLE IF NOT EXISTS `orderdetail` (
`detail_id` int(100) NOT NULL,
  `order_id` varchar(100) NOT NULL,
  `product_id` varchar(100) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `product_image_feature` varchar(100) NOT NULL,
  `product_brand` varchar(100) NOT NULL,
  `product_capacity` varchar(100) NOT NULL,
  `model_name` varchar(100) NOT NULL,
  `cellular_name` varchar(100) NOT NULL,
  `product_price` varchar(100) NOT NULL,
  `quantity` varchar(100) NOT NULL,
  `total_amount` varchar(100) NOT NULL,
  `product_condition` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orderdetail`
--

INSERT INTO `orderdetail` (`detail_id`, `order_id`, `product_id`, `product_name`, `product_image_feature`, `product_brand`, `product_capacity`, `model_name`, `cellular_name`, `product_price`, `quantity`, `total_amount`, `product_condition`) VALUES
(11, '16', '7', 'Galaxy S5', 'File1140027392.png', 'Samsung', '32 GB', 'Galaxy S5', 'FACTORY UNLOCKED', '210', '1', '210', 'Good'),
(12, '17', '7', 'Galaxy S5', 'File1140027392.png', 'Samsung', '32 GB', 'Galaxy S5', 'FACTORY UNLOCKED', '120', '1', '120', 'Damaged');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
`order_id` int(100) NOT NULL,
  `order_number` varchar(100) NOT NULL,
  `order_amount` varchar(100) NOT NULL,
  `order_dated` varchar(100) NOT NULL,
  `order_status` varchar(100) NOT NULL,
  `order_user_id` varchar(100) NOT NULL,
  `billing_fname` varchar(100) NOT NULL,
  `billing_lname` varchar(100) NOT NULL,
  `billing_address` text NOT NULL,
  `billing_country` varchar(100) NOT NULL,
  `billing_state` varchar(100) NOT NULL,
  `billing_city` varchar(100) NOT NULL,
  `billing_pincode` varchar(100) NOT NULL,
  `billing_phone` varchar(100) NOT NULL,
  `payment_type` varchar(100) NOT NULL,
  `paypal_id` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `order_number`, `order_amount`, `order_dated`, `order_status`, `order_user_id`, `billing_fname`, `billing_lname`, `billing_address`, `billing_country`, `billing_state`, `billing_city`, `billing_pincode`, `billing_phone`, `payment_type`, `paypal_id`) VALUES
(16, '790108', '210', '1496688210', 'Pending', '4', 'Sunil', 'Bhatia', '#E-110 Phase-7, Ind Area, ', 'India', 'Punjab', 'Mohali', '160055', '8872292478', 'paypal', 'sbsunilbhatia9@gmail.com'),
(17, '402701', '120', '1496744765', 'Pending', '4', 'ghh', 'gfg', 'ghgh gfg', 'Afghanistan', 'Bamiyan', 'Panjab', '140125', '9041634625', 'check', '');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
`product_id` int(100) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `model_id` int(100) NOT NULL,
  `brand_id` int(100) NOT NULL,
  `product_image_feature` varchar(100) NOT NULL,
  `sku` varchar(100) NOT NULL,
  `product_capacity` varchar(100) NOT NULL,
  `cell_carrier_id` int(100) NOT NULL,
  `product_quantity` int(100) NOT NULL,
  `product_back_image` varchar(100) NOT NULL,
  `product_color` varchar(100) NOT NULL,
  `damaged_price` varchar(100) NOT NULL,
  `damaged_desc` text NOT NULL,
  `good_price` varchar(100) NOT NULL,
  `good_desc` text NOT NULL,
  `flawless_price` varchar(100) NOT NULL,
  `flawless_desc` text NOT NULL,
  `product_status` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `product_name`, `model_id`, `brand_id`, `product_image_feature`, `sku`, `product_capacity`, `cell_carrier_id`, `product_quantity`, `product_back_image`, `product_color`, `damaged_price`, `damaged_desc`, `good_price`, `good_desc`, `flawless_price`, `flawless_desc`, `product_status`) VALUES
(5, 'IPhone 5S', 19, 10, 'File42518677.png', 'SKU0506201783', '16 GB', 5, 5, '', 'Black', '120', '<li>Damaged Condition</li>\r\n<li>Charger not Required</li>', '250', '<label>All good</label>', '350', '<p>Awesome and Nice Condition</p>', '1'),
(6, 'IPhone7 Plus', 23, 10, 'File589844615.png', 'SKU0506201787', '64 GB', 5, 1, '', 'Blood Red', '210', 'Minor Damaged', '320', 'Fine Condition', '410', 'Not any Single Scratch Availability', '1'),
(7, 'Galaxy S5', 27, 9, 'File1140027392.png', 'SKU0506201719', '32 GB', 5, 2, '', 'Black', '120', 'Totally Damaged', '210', 'Good One', '330', 'No scratch', '1');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`user_id` int(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` int(100) NOT NULL,
  `email_status` int(100) NOT NULL,
  `token` varchar(100) NOT NULL,
  `dated` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `email`, `password`, `status`, `email_status`, `token`, `dated`) VALUES
(4, 'sbsunilbhatia9@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 1, 0, 'niR93sLcuCRR2r9q5it9KthD0bsfJmcXJrq2VaNo', '1496688289');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
 ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
 ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `cell_carrier`
--
ALTER TABLE `cell_carrier`
 ADD PRIMARY KEY (`carrier_id`);

--
-- Indexes for table `model`
--
ALTER TABLE `model`
 ADD PRIMARY KEY (`model_id`);

--
-- Indexes for table `orderdetail`
--
ALTER TABLE `orderdetail`
 ADD PRIMARY KEY (`detail_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
 ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
 ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
MODIFY `admin_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
MODIFY `brand_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `cell_carrier`
--
ALTER TABLE `cell_carrier`
MODIFY `carrier_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `model`
--
ALTER TABLE `model`
MODIFY `model_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `orderdetail`
--
ALTER TABLE `orderdetail`
MODIFY `detail_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
MODIFY `order_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
MODIFY `product_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `user_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

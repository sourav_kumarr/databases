-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 12, 2017 at 02:25 PM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `stsmedtd_quiz`
--

-- --------------------------------------------------------

--
-- Table structure for table `push_notification`
--

CREATE TABLE IF NOT EXISTS `push_notification` (
  `DeviceID` varchar(256) COLLATE utf8_bin NOT NULL,
  `AppID` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `push_notification`
--

INSERT INTO `push_notification` (`DeviceID`, `AppID`) VALUES
('61811e988be708e844a9c0ead1707ae4f21a48b5285b8adc2dc6b3bd99b42dc4', 12345755),
('61811e988be708e844a9c0ead1707ae4f21a48b5285b8adc2dc6b3bd99b42dc4', 12345759),
('99b0cc628355cc5792125727ed1320c00b03ed715605430c3e335d6e67defd81', 12345755),
('99b0cc628355cc5792125727ed1320c00b03ed715605430c3e335d6e67defd81', 12345759);

-- --------------------------------------------------------

--
-- Table structure for table `quizapp`
--

CREATE TABLE IF NOT EXISTS `quizapp` (
`AppID` int(11) NOT NULL,
  `AppName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `AppiTunesURL` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `appdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `AppTnail` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `AppPackageName` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=12345786 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quizapp`
--

INSERT INTO `quizapp` (`AppID`, `AppName`, `AppiTunesURL`, `appdate`, `AppTnail`, `AppPackageName`) VALUES
(12345778, 'Quiz App Starterkit iOS and Android', '', '2017-03-08 08:36:35', 'app_pics/247981icon-180.png', 'com.heavenappsquiz.quizappnew'),
(12345785, 'xccxcx', '', '2017-07-18 06:48:08', 'app_pics/337189', 'com.example');

-- --------------------------------------------------------

--
-- Table structure for table `quizcategories`
--

CREATE TABLE IF NOT EXISTS `quizcategories` (
`CatID` int(11) NOT NULL,
  `AppID` int(11) DEFAULT NULL,
  `CatName` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `CatDescr` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `is_time_based` tinyint(1) NOT NULL,
  `CatThum` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `CatColor` varchar(7) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `QlimitID` int(3) DEFAULT NULL,
  `LBID` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ProductID` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `is_online` tinyint(1) NOT NULL,
  `catdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `priority` int(32) unsigned NOT NULL DEFAULT '0',
  `LBID_Android` varchar(256) DEFAULT NULL,
  `ProductID_Android` varchar(256) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=12466 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quizcategories`
--

INSERT INTO `quizcategories` (`CatID`, `AppID`, `CatName`, `CatDescr`, `is_time_based`, `CatThum`, `CatColor`, `QlimitID`, `LBID`, `ProductID`, `is_online`, `catdate`, `priority`, `LBID_Android`, `ProductID_Android`) VALUES
(12463, 12345778, 'Sports', 'Are you a real sport person then beat it…!!!', 0, 'thumbnails/sports_btns_icon.png', 'FF9933', 4, 'sports_leaderboard_id', '', 1, '2017-03-08 10:08:18', 0, 'CgkIt9uijqQBEAIQBw', ''),
(12464, 12345778, 'Cars', 'Test your knowledge about cars and logos', 1, 'thumbnails/entertainment.png', 'FF5050', 5, 'carsquiz_leaderboard_id', '', 1, '2017-03-08 10:08:18', 0, 'CgkIt9uijqQBEAIQCA', ''),
(12465, 12345778, 'People', 'How many of you know about these famous people?', 1, 'thumbnails/famous_ppl.png', '6666FF', 4, 'peoplequiz_leaderboard_id', 'com.heavenapps.quizapp.famouspeople', 1, '2017-03-08 10:08:18', 0, 'CgkIt9uijqQBEAIQCQ', '');

-- --------------------------------------------------------

--
-- Table structure for table `quizquestions`
--

CREATE TABLE IF NOT EXISTS `quizquestions` (
`qid` int(11) NOT NULL,
  `question` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `option1` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `option2` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `option3` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `option4` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `answer` int(11) NOT NULL,
  `type` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `media` tinyint(1) NOT NULL,
  `mediatype` int(1) NOT NULL,
  `media_url` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `points` int(11) NOT NULL,
  `negative_points` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  `correct_answer_explanation` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `wrong_answer_explanation` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `CatID` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=14629 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quizquestions`
--

INSERT INTO `quizquestions` (`qid`, `question`, `option1`, `option2`, `option3`, `option4`, `answer`, `type`, `media`, `mediatype`, `media_url`, `points`, `negative_points`, `duration`, `correct_answer_explanation`, `wrong_answer_explanation`, `CatID`) VALUES
(14607, 'This is sample true/false question.', 'true', 'false', '', '', 1, 'tf', 0, 1, 'media/', 10, 5, 20, '', '', 12464),
(14608, 'Which of below is german car company?', 'BMW Brand', 'Intelligent Car', 'Nisan', 'Maruti', 1, 'optio', 0, 1, 'media/', 15, 2, 15, '', '', 12464),
(14609, 'What car brand is this?', 'Chevrolet', 'Volksvagen', 'Mercedes Benz', 'Aston Martin', 1, 'optio', 1, 2, 'media/chevrolet-logo2.png', 15, 2, 15, '', '', 12464),
(14610, 'What car brand is this?', 'Lotus', 'Rose', 'Mercedes Benz', 'Bently', 1, 'optio', 1, 2, 'media/lotus-logo2.png', 15, 2, 15, '', '', 12464),
(14611, 'Sample video question.', 'Correct answer', 'Wrong answer', 'Wrong answer', 'Wrong answer', 1, 'optio', 1, 3, 'media/sample_video.mp4', 15, 2, 20, 'This is correct answer explanation. You can have your own correct/wrong explanation text for every question. If this text is not added, app will automatically take you to next question without showing this popup.', 'This is wrong answer explanation. You can have your own correct/wrong explanation text for every question. If this text is not added, app will automatically take you to next question without showing this popup.', 12464),
(14612, 'Hello, I am Shane watson. In which year did I Play my 1st international Cricket match', '2002 against Africa', '2001 against Australia', '2002 against Newzeeland', '2002 against Australia', 1, 'optio', 1, 2, 'media/Australia2001_0.jpg', 10, 5, 20, 'Shane Robert Watson (born 17 June 1981) is an Australian cricketer. He is a right-handed batsman and a right-handed fast medium bowler.', 'Shane Robert Watson (born 17 June 1981) is an Australian cricketer. He is a right-handed batsman and a right-handed fast medium bowler.', 12463),
(14613, 'who am I? Hint: Cricket personality.', 'Kapil Dev', 'Sunil Gavaskar', 'Ravi Shastri', 'Arjun Rampal', 1, 'optio', 1, 2, 'media/kapil_dev.jpg', 10, 5, 20, 'Kapil Dev Ramlal Nikhan (born 6 January 1959), better known as Kapil Dev, is a former Indian cricketer. He captained the Indian cricket team which won the 1983 Cricket World Cup.', 'Kapil Dev Ramlal Nikhan (born 6 January 1959), better known as Kapil Dev, is a former Indian cricketer. He captained the Indian cricket team which won the 1983 Cricket World Cup.', 12463),
(14614, 'Which sport is played on the largest pitch?', 'Polo', 'Football', 'Basketball', 'None', 1, 'optio', 0, 1, 'media/', 10, 5, 20, '', '', 12463),
(14615, 'What is the connection between Volleyball, Squash and Badminton? Clue: This connection is not shared with tennis or table tennis.', 'Only the server can score', 'Both can score', 'No one scores', 'No connections', 1, 'optio', 0, 1, 'media/', 10, 5, 20, '', '', 12463),
(14616, 'Which sports playing area is 2.7 metres by 1.5 metres?', 'Table Tennis', 'Polo', 'Basketball', 'None', 1, 'optio', 0, 1, 'media/', 10, 5, 20, '', '', 12463),
(14617, 'What is the highest possible break in snooker?', '155', '147', '165', '115', 1, 'optio', 0, 1, 'media/', 10, 5, 20, '', '', 12463),
(14618, 'Who is this hot man?', 'David Beckham', 'Zinedine Zidane', 'Luis Figo', 'Jhon', 1, 'optio', 1, 2, 'media/david_bakham.jpg', 10, 5, 20, '', '', 12463),
(14619, 'This is sample true/false question.', 'true', 'false', '', '', 1, 'tf', 0, 1, 'media/', 10, 5, 20, '', '', 12463),
(14620, 'Identify this famous personality.', '14th Dalai lama', 'Bhodidharma', 'Chagdud Tulku Rinpoche', 'Angarika Govinda', 1, 'optio', 1, 2, 'media/Q16Dalai.jpg', 10, 5, 20, 'The Dalai Lama is a lama of the Gelug or &quot;Yellow Hat&quot; school of Tibetan Buddhism, founded by Tsongkhapa (1357–1419). The 14th and current Dalai Lama is Tenzin Gyatso, recognized since 1950.', 'The Dalai Lama is a lama of the Gelug or &quot;Yellow Hat&quot; school of Tibetan Buddhism, founded by Tsongkhapa (1357–1419). The 14th and current Dalai Lama is Tenzin Gyatso, recognized since 1950.', 12465),
(14621, 'Identify this famous personality.', 'Queen Elizabeth', 'Princes Diana', 'Duchess Catherine', 'Monalisa', 1, 'optio', 1, 2, 'media/Q9Elizabethe.jpg', 10, 5, 20, 'Elizabeth II (Elizabeth Alexandra Mary; born 21 April 1926)[ is the Queen of 16 of the 53 member states in the Commonwealth of Nations. She is also Head of the Commonwealth and Supreme Governor of the Church of England.', 'Elizabeth II (Elizabeth Alexandra Mary; born 21 April 1926) is the Queen of 16 of the 53 member states in the Commonwealth of Nations. She is also Head of the Commonwealth and Supreme Governor of the Church of England.', 12465),
(14622, 'Identify this famous personality.', 'Pope John Paul', 'Pope st. pius X', 'Prince Philip', 'Pope Benedict', 1, 'optio', 1, 2, 'media/Q15Pop.jpg', 10, 5, 20, '', '', 12465),
(14623, 'Identify this famous personality.', 'Walt Disney', 'Peter Sellers', 'Winston Churchill', 'Bill Gates', 1, 'optio', 1, 2, 'media/Q18WaltDisney.jpg', 10, 5, 20, '', '', 12465),
(14624, 'Identify this famous personality.', 'Steve Jobs', 'Steve Johnson', 'Elvis Presley', 'Bill Gates', 1, 'optio', 1, 2, 'media/Q7Steve.jpg', 10, 5, 20, '', '', 12465),
(14625, 'Identify this famous personality.', 'Mark Zukerburg', 'Robert Redford', 'Elvis Presley', 'Bill Gates', 1, 'optio', 1, 2, 'media/Q3Mark.jpg', 10, 5, 20, '', '', 12465),
(14628, 'who is bahubali', 'media/File3180album_poster.jpg', 'media/File7567^0A6A9103C18295CC9D02B2CA21C91A99BF43EC36C90DC12EEF^pimgpsh_fullsize_distr.jpg', 'media/File8079^0A6A9103C18295CC9D02B2CA21C91A99BF43EC36C90DC12EEF^pimgpsh_fullsize_distr.jpg', 'media/File7475^0A6A9103C18295CC9D02B2CA21C91A99BF43EC36C90DC12EEF^pimgpsh_fullsize_distr.jpg', 1, 'owp', 1, 2, 'media/File9049^68718500AC86AEEE23052AAF6D6130437EE367462B7EFCC1B7^pimgpsh_fullsize_distr.jpg', 3, -5, 60, 'First is right answer    ', '    you are wrong    ', 12463);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `push_notification`
--
ALTER TABLE `push_notification`
 ADD UNIQUE KEY `Device_app_unique` (`DeviceID`,`AppID`), ADD KEY `AppID` (`AppID`);

--
-- Indexes for table `quizapp`
--
ALTER TABLE `quizapp`
 ADD PRIMARY KEY (`AppID`);

--
-- Indexes for table `quizcategories`
--
ALTER TABLE `quizcategories`
 ADD PRIMARY KEY (`CatID`);

--
-- Indexes for table `quizquestions`
--
ALTER TABLE `quizquestions`
 ADD PRIMARY KEY (`qid`), ADD KEY `qid` (`qid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `quizapp`
--
ALTER TABLE `quizapp`
MODIFY `AppID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12345786;
--
-- AUTO_INCREMENT for table `quizcategories`
--
ALTER TABLE `quizcategories`
MODIFY `CatID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12466;
--
-- AUTO_INCREMENT for table `quizquestions`
--
ALTER TABLE `quizquestions`
MODIFY `qid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14629;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

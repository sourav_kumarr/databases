-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: justforwomen
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `jfw_orders`
--

DROP TABLE IF EXISTS `jfw_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jfw_orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_date` text NOT NULL,
  `order_state` varchar(100) NOT NULL,
  `order_payment_status` varchar(100) NOT NULL,
  `order_number` varchar(100) NOT NULL,
  `order_total` varchar(100) NOT NULL,
  `order_user_id` int(50) NOT NULL,
  `paid_amount` varchar(100) NOT NULL,
  `product_type` varchar(100) NOT NULL,
  `order_txn_id` varchar(100) NOT NULL,
  `order_currency_code` varchar(100) NOT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jfw_orders`
--

LOCK TABLES `jfw_orders` WRITE;
/*!40000 ALTER TABLE `jfw_orders` DISABLE KEYS */;
INSERT INTO `jfw_orders` VALUES (21,'2016-08-24','open','Pending','484924','1.4',0,'0.00','Custom Suit','',''),(22,'2016-08-24','open','Pending','323015','1.4',0,'0.00','Custom Suit','',''),(23,'2016-08-24','open','Pending','683069','1.4',0,'0.00','Custom Suit','',''),(24,'2016-08-24','open','Pending','271832','1.4',741,'0.00','Custom Suit','',''),(25,'2016-08-24','open','Pending','281758','1.4',741,'0.00','Custom Suit','',''),(26,'2016-08-24','open','Pending','903148','1.4',741,'0.00','Custom Suit','',''),(27,'2016-08-24','open','Pending','373772','1.4',741,'0.00','Custom Suit','',''),(28,'2016-08-24','open','Pending','766946','1.4',741,'0.00','Custom Suit','','');
/*!40000 ALTER TABLE `jfw_orders` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:33

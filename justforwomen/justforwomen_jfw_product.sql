-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: justforwomen
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `jfw_product`
--

DROP TABLE IF EXISTS `jfw_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jfw_product` (
  `prod_id` int(100) NOT NULL AUTO_INCREMENT,
  `prod_name` varchar(100) NOT NULL,
  `prod_fabrics` varchar(100) NOT NULL,
  `prod_base_price` varchar(100) NOT NULL,
  `prod_image1` varchar(100) NOT NULL,
  `prod_image2` varchar(100) NOT NULL,
  `prod_image3` varchar(100) NOT NULL,
  `prod_obj` varchar(100) NOT NULL,
  `prod_style_id` varchar(100) NOT NULL,
  `prod_short_desc` text NOT NULL,
  `prod_long_desc` text NOT NULL,
  `survey_tags` varchar(1000) NOT NULL,
  `created_date` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`prod_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jfw_product`
--

LOCK TABLES `jfw_product` WRITE;
/*!40000 ALTER TABLE `jfw_product` DISABLE KEYS */;
INSERT INTO `jfw_product` VALUES (1,'LONG TUNIC WITH SLIT','1,2,3,4,','1.40','prod_224577.jpeg','prod_451231.jpeg','prod_336548.jpeg','','1','Look enviably cool in this trend-forward long tunic with a waist high front slit.','Look enviably cool in this trend-forward long tunic with a waist high front slit.Look enviably cool in this trend-forward long tunic with a waist high front slit.Look enviably cool in this trend-forward long tunic with a waist high front slit.Look enviably cool in this trend-forward long tunic with a waist high front slit.','male:1,2,3,4,5,','12/08/2016',1),(2,'MERMAID SKIRT','2,3,4,5,','1.40','prod_225577.jpeg','','','','1','Stun the room in this curve-accentuating mermaid skirt; floor length with beautiful applique option','Stun the room in this curve-accentuating mermaid skirt; floor length with beautiful applique optionStun the room in this curve-accentuating mermaid skirt; floor length with beautiful applique option','male:1,2,3,4,5,','12/08/2016',1),(3,'CAP SLEEVE FITTED SHEATH DRESS','1,2,3,4,','1.40','prod_224667.jpeg','prod_457899.jpeg','prod_336748.jpeg','','1','Get into a party mood with this flattering cap sleeve sheath dress, styled with a waistline belt and lace overlay\n','Get into a party mood with this flattering cap sleeve sheath dress, styled with a waistline belt and lace overlay','male:1,2,3,4,5,','12/08/2016',1),(4,'FLOOR LENGTH GOWN','1,2,3,4,','1.40','prod_264577.jpeg','prod_457102.jpeg','prod_336848.jpeg','','1','Floor length, boat neck, cap sleeve, and princess line bodice will be stunning and elegant for any special occasion','Floor length, boat neck, cap sleeve, and princess line bodice will be stunning and elegant for any special occasion','male:1,2,3,4,5,','12/08/2016',1),(5,'LONG SLEEVE TRUMPET FLOOR LENGTH GOWN','1,2,3,4,','1.40','prod_228977.jpeg','prod_457991.jpeg','prod_336948.jpeg','','1','Long sleeves, boat neck, trumpet skirt and a floor length hem make this dress ideal for those occasions when modesty and elegance are both desired,','Long sleeves, boat neck, trumpet skirt and a floor length hem make this dress ideal for those occasions when modesty and elegance are both desired,','male:1,2,3,4,5,','12/08/2016',1),(6,'V-NECK TRUMPET FLOOR LENGTH GOWN','1,2,3,4,','1.40','prod_224477.jpeg','prod_458841.jpeg','prod_337048.jpeg','','1','Unlock your inner goddess in this floor length fitted gown. A trumpet skirt combined with the V-neck is perfect for any evening occasion.','Unlock your inner goddess in this floor length fitted gown. A trumpet skirt combined with the V-neck is perfect for any evening occasion.','male:1,2,3,4,5,','12/08/2016',1),(7,'TIE COLLAR BUTTON FRONT BLOUSE','1,2,3,4,','1.40','','','','','1','This long sleeve button front blouse with a tie collar can be paired with any suit for a more feminine look at work and play.','This long sleeve button front blouse with a tie collar can be paired with any suit for a more feminine look at work and play.','male:1,2,3,4,5,','12/08/2016',1),(8,'CAP SLEEVE FITTED SHEATH DRESS','1,2,3,4,','1.40','prod_224997.jpeg','prod_456841.jpeg','','','1','Get into a party mood with this flattering cap sleeve sheath dress, styled with a waistline belt and lace overlay','Get into a party mood with this flattering cap sleeve sheath dress, styled with a waistline belt and lace overlay','male:1,2,3,4,5,','12/08/2016',1),(9,'STRAPLESS FITTED SHEATH DRESS WITH LACE OVERLAY','1,2,3,4,','1.40','','','','','1','Nothing\'s as elegant or alluring as lace for an evening affair, and this chic sheath is case in point. The lace overlay that covers the strapless sheath is flirty-yet-refined','Nothing\'s as elegant or alluring as lace for an evening affair, and this chic sheath is case in point. The lace overlay that covers the strapless sheath is flirty-yet-refined','male:1,2,3,4,5,','12/08/2016',1),(10,'SQUARE NECK FLOOR LENGTH GOWN','1,2,3,4,','1.40','','','','','1','Strike a pose in this floor length gown. A square neck and princess line bodice add elegance with a tough of modesty for that special occasion','Strike a pose in this floor length gown. A square neck and princess line bodice add elegance with a tough of modesty for that special occasion','male:1,2,3,4,5,','12/08/2016',1);
/*!40000 ALTER TABLE `jfw_product` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:33

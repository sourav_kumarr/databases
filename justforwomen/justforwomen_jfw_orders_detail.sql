-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: justforwomen
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `jfw_orders_detail`
--

DROP TABLE IF EXISTS `jfw_orders_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jfw_orders_detail` (
  `det_id` int(100) NOT NULL AUTO_INCREMENT,
  `det_product_id` varchar(100) NOT NULL,
  `det_order_id` varchar(100) NOT NULL,
  `det_price` varchar(100) NOT NULL,
  `det_quantity` varchar(100) NOT NULL,
  `det_status` varchar(100) NOT NULL,
  `det_user_id` varchar(100) NOT NULL,
  PRIMARY KEY (`det_id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jfw_orders_detail`
--

LOCK TABLES `jfw_orders_detail` WRITE;
/*!40000 ALTER TABLE `jfw_orders_detail` DISABLE KEYS */;
INSERT INTO `jfw_orders_detail` VALUES (17,'1','9','1.4','1','A','1261'),(18,'1','10','1.4','2','A','1261'),(19,'1','11','1.4','2','A','1261'),(20,'3','11','1.4','1','A','1261'),(21,'1','12','1.4','1','A','741'),(22,'1','13','1.4','2','A','741'),(23,'1','14','1.4','5','A','741'),(24,'1','15','1.4','5','A','741'),(25,'1','16','1.4','5','A','741'),(26,'6','16','1.4','1','A','741'),(27,'1','17','1.4','5','A','741'),(28,'6','17','1.4','1','A','741'),(29,'1','18','1.4','5','A','741'),(30,'6','18','1.4','1','A','741'),(31,'1','19','1.4','5','A','741'),(32,'6','19','1.4','1','A','741'),(33,'1','20','1.4','5','A','741'),(34,'6','20','1.4','1','A','741'),(35,'1','21','1.4','5','A','/'),(36,'6','21','1.4','1','A','/'),(37,'1','22','1.4','5','A','/'),(38,'6','22','1.4','1','A','/'),(39,'1','23','1.4','5','A','/'),(40,'6','23','1.4','1','A','/'),(41,'1','24','1.4','5','A','741'),(42,'6','24','1.4','1','A','741'),(43,'1','25','1.4','5','A','741'),(44,'6','25','1.4','1','A','741'),(45,'1','26','1.4','5','A','741'),(46,'6','26','1.4','1','A','741'),(47,'1','27','1.4','5','A','741'),(48,'6','27','1.4','1','A','741'),(49,'1','28','1.4','5','A','741'),(50,'6','28','1.4','1','A','741');
/*!40000 ALTER TABLE `jfw_orders_detail` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:31

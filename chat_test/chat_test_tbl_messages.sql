-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: chat_test
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_messages`
--

DROP TABLE IF EXISTS `tbl_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_messages` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `msg_to_id` varchar(100) NOT NULL,
  `msg_from_id` bigint(100) NOT NULL,
  `msg_text` text NOT NULL,
  `msg_read_status` varchar(100) NOT NULL,
  `msg_send_status` varchar(100) NOT NULL,
  `msg_read_id` text NOT NULL,
  `msg_status` varchar(100) NOT NULL,
  `msg_timestamp` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=370 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_messages`
--

LOCK TABLES `tbl_messages` WRITE;
/*!40000 ALTER TABLE `tbl_messages` DISABLE KEYS */;
INSERT INTO `tbl_messages` VALUES (291,'3',2,'hello','read','send','read_0','A','2016-09-29 07:18:54'),(292,'3',2,'i mfine bro','read','send','read_1','A','2016-09-29 07:19:35'),(293,'3',2,'ggg','read','send','read_2','A','2016-09-29 07:21:31'),(294,'3',2,'ggg','read','send','read_3','A','2016-09-29 07:21:37'),(295,'2',3,'hello bro','read','send','read_4','A','2016-09-29 09:46:36'),(296,'3',2,'i m fine bro','read','send','read_5','A','2016-09-29 09:46:51'),(297,'2',3,'hiii','read','send','read_6','A','2016-09-29 15:52:03'),(298,'2',1,'helloo','read','send','read_7','A','2016-09-29 15:54:30'),(299,'2',1,'hiiiiiiiiiiii','read','send','read_8','A','2016-09-29 15:54:43'),(300,'2',1,'gggggggggggg','read','send','read_9','A','2016-09-29 15:54:56'),(301,'2',1,'sdsdsdsdsd','read','send','read_10','A','2016-09-29 15:57:39'),(302,'2',1,'sdsdsd','read','send','read_11','A','2016-09-29 15:58:03'),(303,'2',1,'hlo','read','send','read_5','A','2016-09-29 16:29:31'),(304,'2',3,'hlo','not read','send','read_7','A','2016-09-29 16:29:45'),(305,'2',3,'how r u','not read','send','read_8','A','2016-09-29 16:30:01'),(306,'2',3,'mjgghmn ','not read','send','read_9','A','2016-09-29 16:30:09'),(307,'2',3,'bcvnhvnhgngh','not read','send','read_10','A','2016-09-29 16:30:12'),(308,'2',3,'bvcn','not read','send','read_11','A','2016-09-29 16:30:13'),(309,'2',1,'hello bhai g kaise ho','read','send','read_6','A','2016-09-30 09:15:03'),(310,'2',1,'hello sandeep','read','send','read_7','A','2016-09-30 09:16:38'),(311,'2',1,'ddd','read','send','read_8','A','2016-09-30 09:16:46'),(312,'2',1,'dd','read','send','read_9','A','2016-09-30 09:16:50'),(313,'2',1,'dd','read','send','read_10','A','2016-09-30 09:16:53'),(314,'2',1,'dd','read','send','read_11','A','2016-09-30 09:16:56'),(315,'2',1,'dd','read','send','read_12','A','2016-09-30 09:17:00'),(316,'1',2,'sdsds','read','send','read_13','A','2016-10-03 09:22:58'),(317,'1',2,'hhhhh','read','send','read_14','A','2016-10-03 09:23:07'),(318,'1',2,'jjjjj','read','send','read_15','A','2016-10-03 09:31:29'),(319,'1',2,'dfdfdf','read','send','read_16','A','2016-10-03 09:36:47'),(320,'1',2,'hhhhhhhhhhhhh','read','send','read_17','A','2016-10-03 09:37:08'),(321,'1',2,'hhhhhhhhhhhhh','read','send','read_18','A','2016-10-03 09:37:11'),(322,'1',2,'hhhhhh','read','send','read_19','A','2016-10-03 09:37:15'),(323,'1',2,'fff','read','send','read_20','A','2016-10-03 09:52:36'),(324,'1',2,'fff','read','send','read_21','A','2016-10-03 09:52:43'),(325,'1',2,'ttt','read','send','read_22','A','2016-10-03 09:52:47'),(326,'1',2,'hhhh','read','send','read_23','A','2016-10-03 09:52:51'),(327,'1',2,'hhh','read','send','read_24','A','2016-10-03 09:52:55'),(328,'1',2,'hhh','read','send','read_25','A','2016-10-03 09:52:59'),(329,'1',2,'hello','read','send','read_26','A','2016-10-05 08:33:49'),(330,'1',2,'hiiii','read','send','read_27','A','2016-10-05 08:35:58'),(331,'2',1,'hellll','read','send','read_28','A','2016-10-05 09:01:24'),(332,'2',1,'ddd','read','send','read_29','A','2016-10-05 09:03:27'),(333,'2',1,'ddd','read','send','read_30','A','2016-10-05 09:05:23'),(334,'2',1,'hhhhhh','read','send','read_31','A','2016-10-05 09:11:01'),(335,'2',1,'oooo','read','send','read_32','A','2016-10-05 09:11:26'),(336,'2',1,'jjjj','read','send','read_33','A','2016-10-05 09:16:24'),(337,'2',1,'iii','read','send','read_34','A','2016-10-05 09:16:37'),(338,'2',1,'ddd','read','send','read_35','A','2016-10-05 09:29:12'),(339,'2',1,'dddd','read','send','read_36','A','2016-10-05 09:29:27'),(340,'2',1,'kkkkk','read','send','read_37','A','2016-10-05 09:30:35'),(341,'2',1,'hii','read','send','read_38','A','2016-10-05 09:32:30'),(342,'2',1,'hhhhhhhhh','read','send','read_39','A','2016-10-05 09:32:44'),(343,'2',1,'jjjjjj','read','send','read_40','A','2016-10-05 09:33:44'),(344,'2',1,'hhh','read','send','read_41','A','2016-10-05 09:36:41'),(345,'2',1,'uuuuuu','read','send','read_42','A','2016-10-05 09:38:08'),(346,'2',1,'kkkkk','read','send','read_43','A','2016-10-05 09:38:20'),(347,'2',1,'jjjjjjjjjjjjjjj','read','send','read_44','A','2016-10-05 09:40:32'),(348,'2',1,'ddddddddddddd','read','send','read_45','A','2016-10-05 09:40:43'),(349,'2',1,'jjjjjjj','read','send','read_46','A','2016-10-05 09:42:52'),(350,'2',1,'llllll','read','send','read_47','A','2016-10-05 09:43:05'),(351,'2',1,'jjjjjjjjj','read','send','read_48','A','2016-10-05 09:45:38'),(352,'2',1,'ttttttttt','read','send','read_49','A','2016-10-05 09:45:42'),(353,'2',1,'ttttttttttttt','read','send','read_50','A','2016-10-05 09:45:50'),(354,'2',1,'yyyyyyyyyyyy','read','send','read_51','A','2016-10-05 09:45:54'),(355,'2',1,'uuuuuuuuuuuuu','read','send','read_52','A','2016-10-05 09:45:58'),(356,'2',1,'nnnnnnnnnnn','read','send','read_53','A','2016-10-05 09:46:02'),(357,'2',1,'gggggggg','read','send','read_54','A','2016-10-05 09:46:21'),(358,'2',1,'hiiiii','read','send','read_55','A','2016-10-05 09:49:49'),(359,'2',1,'heelllooo','read','send','read_56','A','2016-10-05 09:50:00'),(360,'2',1,'kkkkk','read','send','read_57','A','2016-10-05 09:50:10'),(361,'2',1,'dddd','read','send','read_58','A','2016-10-06 16:22:57'),(362,'2',1,'sdsdsdsd','read','send','read_59','A','2016-10-06 16:25:01'),(363,'2',1,'fffff','read','send','read_60','A','2016-10-06 16:36:56'),(364,'1',2,'dddd','read','send','read_61','A','2016-10-07 20:44:23'),(365,'1',2,'gggggg','read','send','read_62','A','2016-10-07 20:44:37'),(366,'2',1,'dd','read','send','read_63','A','2017-01-17 10:36:54'),(367,'1',2,'sdsds','read','send','read_64','A','2017-01-17 10:37:05'),(368,'1',2,'helll','read','send','read_65','A','2017-02-08 23:47:56'),(369,'2',1,'i m fine','read','send','read_66','A','2017-02-08 23:48:09');
/*!40000 ALTER TABLE `tbl_messages` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:46

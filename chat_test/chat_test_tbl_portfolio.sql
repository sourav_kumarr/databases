-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: chat_test
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_portfolio`
--

DROP TABLE IF EXISTS `tbl_portfolio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_portfolio` (
  `id` bigint(100) NOT NULL AUTO_INCREMENT,
  `port_name` text NOT NULL,
  `port_description` text NOT NULL,
  `port_link` text NOT NULL,
  `port_image` text NOT NULL,
  `port_type` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_portfolio`
--

LOCK TABLES `tbl_portfolio` WRITE;
/*!40000 ALTER TABLE `tbl_portfolio` DISABLE KEYS */;
INSERT INTO `tbl_portfolio` VALUES (1,'Pulse Power Sensor','In this all required type of sensors information is available on site, e.g We provide PM1,PM 2.5 ,CO,CO2,Temprature,Noise and Humidity information to users  ','http://pulsepower.in','','php'),(2,'Electronic PCB Management','Electronic PCB Management manages all data for PCB\'s from manufacturing to end users. ','http://sachtechsolution.com/push/','','php'),(3,'Home Grocery','Home Grocery manages all information in grocery store. It manages all store information like Items in store,price orders and inventory for store.','http://sachtechsolution.com/homegrocery/admin','','php'),(4,'Text Share','Text Share is to share text with another user.It is just like text sharing application that share only text with another user','#','','nodejs'),(5,'Audio Call','Audio call is used to call with one peer to another peer over internet','#','','nodejs'),(6,'Video Call','Video Call help user to interact with video calling ..Two user from anywhere can see each other expressions','#','','nodejs'),(7,'Local Stores','Local Store is used for local shopping only.Normal user can purchase grocery items from this application','#','','android'),(8,'Jagranjosh News','Jagran Josh gives information about daily news about jobs . It displays all information about govt jobs.','https://play.google.com/store/apps/details?id=com.applyforgovtjob.jobs&hl=en','','android'),(9,'TXParts','TXParts has all type of parts of mobiles . User can purchase part of mobile and pay online also. ','https://play.google.com/store/apps/details?id=com.sachtech.txpartsus','','android'),(10,'OCR Reader','OCR(Optical Character Reader) is used to read characters from image.It reads all characters from image in multiple languages. ','#','','android'),(11,'PTS','PTS is used to test PCB using bluetooth and usb communications.It loads all tests from server and gives these tests to embedded system and embedded system tests itself.','#','','android'),(12,'PTS Sensor','PTS Sensor make communication with sensor using bluetooth.It senses particles in air and gives us airquality information to user','#','',''),(13,'Route Navigation','Route Navigation gives information about routes.User have to give source and destination path line will be drawan on map and distance will be given ','https://play.google.com/store/apps/details?id=com.sachtech.navigationmapp','','android'),(14,'Text Editor','Text Editor will edit text e.g font size,font style and set image on dashboard . User can also take print of the same.','#','','android'),(15,'WeTube','WeTube is used to get videos from YouTube and video subscription from YouTube. It fetches all data from YouTube server and display','#','','android');
/*!40000 ALTER TABLE `tbl_portfolio` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:47

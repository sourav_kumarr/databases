-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 12, 2017 at 02:27 PM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `stsmedtd_storibud`
--

-- --------------------------------------------------------

--
-- Table structure for table `advert`
--

CREATE TABLE IF NOT EXISTS `advert` (
`advert_id` int(100) NOT NULL,
  `advert_title` varchar(200) NOT NULL,
  `advert_desc` text NOT NULL,
  `advert_title_file` text NOT NULL,
  `advert_filetype` varchar(200) NOT NULL,
  `advert_file` text NOT NULL,
  `advert_added_by` varchar(200) NOT NULL,
  `advert_added_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `advert`
--

INSERT INTO `advert` (`advert_id`, `advert_title`, `advert_desc`, `advert_title_file`, `advert_filetype`, `advert_file`, `advert_added_by`, `advert_added_on`) VALUES
(2, 'Audio Add1', '', 'File53537915.png', 'audio', 'File172931548.mp3', '15', '2017-05-22 13:46:16'),
(5, 'new', 'data found', 'File53537915.png', 'image', 'File38660088.png', '11', '2017-05-22 13:46:13'),
(6, 'music', 'music test added', 'File53537915.png', 'audio', 'File1063004029.mp3', '11', '2017-05-22 13:46:11'),
(10, 'sds', 'dsdsd', 'File828891593.png', 'audio', 'File898531695.mp3', '11', '2017-05-22 16:07:11'),
(11, 'vide check', 'sdsddsdsdsdssd', 'File66412244.png', 'video', '', '11', '2017-05-22 16:25:28'),
(12, 'book', 'book show added', 'File1069014523.png', 'books', '', '11', '2017-05-22 16:31:48');

-- --------------------------------------------------------

--
-- Table structure for table `approvals`
--

CREATE TABLE IF NOT EXISTS `approvals` (
`row_id` int(100) NOT NULL,
  `element_type` varchar(100) NOT NULL,
  `element_id` varchar(100) NOT NULL,
  `approval_status` varchar(100) NOT NULL,
  `admin_id` int(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=533 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `approvals`
--

INSERT INTO `approvals` (`row_id`, `element_type`, `element_id`, `approval_status`, `admin_id`) VALUES
(45, 'category', '33', '1', 10),
(46, 'category', '33', '1', 11),
(47, 'category', '33', '1', 12),
(48, 'category', '34', '1', 10),
(49, 'category', '34', '1', 11),
(50, 'category', '34', '1', 12),
(51, 'category', '35', '1', 10),
(52, 'category', '35', '1', 11),
(53, 'category', '35', '1', 12),
(54, 'category', '36', '1', 10),
(55, 'category', '36', '1', 11),
(56, 'category', '36', '1', 12),
(57, 'category', '37', '1', 10),
(58, 'category', '37', '1', 11),
(59, 'category', '37', '1', 12),
(60, 'category', '38', '1', 10),
(61, 'category', '38', '1', 11),
(62, 'category', '38', '1', 12),
(63, 'category', '39', '1', 10),
(64, 'category', '39', '1', 11),
(65, 'category', '39', '1', 12),
(66, 'category', '40', '1', 10),
(67, 'category', '40', '1', 11),
(68, 'category', '40', '1', 12),
(69, 'category', '41', '1', 10),
(70, 'category', '41', '1', 11),
(71, 'category', '41', '1', 12),
(72, 'category', '42', '1', 10),
(73, 'category', '42', '1', 11),
(74, 'category', '42', '1', 12),
(75, 'category', '43', '1', 10),
(76, 'category', '43', '1', 11),
(77, 'category', '43', '1', 12),
(78, 'category', '44', '1', 10),
(79, 'category', '44', '1', 11),
(80, 'category', '44', '1', 12),
(81, 'category', '45', '1', 10),
(82, 'category', '45', '1', 11),
(83, 'category', '45', '1', 12),
(84, 'category', '46', '1', 10),
(85, 'category', '46', '1', 11),
(86, 'category', '46', '1', 12),
(87, 'category', '47', '1', 10),
(88, 'category', '47', '1', 11),
(89, 'category', '47', '1', 12),
(90, 'category', '48', '1', 10),
(91, 'category', '48', '1', 11),
(92, 'category', '48', '1', 12),
(93, 'category', '49', '1', 10),
(94, 'category', '49', '1', 11),
(95, 'category', '49', '1', 12),
(96, 'category', '50', '1', 10),
(97, 'category', '50', '1', 11),
(98, 'category', '50', '1', 12),
(99, 'category', '51', '1', 10),
(100, 'category', '51', '1', 11),
(101, 'category', '51', '1', 12),
(102, 'category', '52', '1', 10),
(103, 'category', '52', '1', 11),
(104, 'category', '52', '1', 12),
(105, 'category', '53', '1', 10),
(106, 'category', '53', '1', 11),
(107, 'category', '53', '1', 12),
(108, 'category', '54', '1', 10),
(109, 'category', '54', '1', 11),
(110, 'category', '54', '1', 12),
(111, 'category', '55', '1', 10),
(112, 'category', '55', '1', 11),
(113, 'category', '55', '1', 12),
(114, 'category', '56', '1', 10),
(115, 'category', '56', '1', 11),
(116, 'category', '56', '1', 12),
(117, 'category', '57', '1', 10),
(118, 'category', '57', '1', 11),
(119, 'category', '57', '1', 12),
(120, 'category', '58', '1', 10),
(121, 'category', '58', '1', 11),
(122, 'category', '58', '1', 12),
(123, 'category', '59', '1', 10),
(124, 'category', '59', '1', 11),
(125, 'category', '59', '1', 12),
(126, 'category', '60', '1', 10),
(127, 'category', '60', '1', 11),
(128, 'category', '60', '1', 12),
(129, 'category', '61', '1', 10),
(130, 'category', '61', '1', 11),
(131, 'category', '61', '1', 12),
(132, 'category', '62', '1', 10),
(133, 'category', '62', '1', 11),
(134, 'category', '62', '1', 12),
(135, 'category', '63', '1', 10),
(136, 'category', '63', '1', 11),
(137, 'category', '63', '1', 12),
(138, 'category', '64', '1', 10),
(139, 'category', '64', '1', 11),
(140, 'category', '64', '1', 12),
(141, 'category', '65', '1', 10),
(142, 'category', '65', '1', 11),
(143, 'category', '65', '1', 12),
(144, 'category', '66', '1', 10),
(145, 'category', '66', '1', 11),
(146, 'category', '66', '1', 12),
(147, 'category', '67', '1', 10),
(148, 'category', '67', '1', 11),
(149, 'category', '67', '1', 12),
(150, 'category', '68', '1', 10),
(151, 'category', '68', '1', 11),
(152, 'category', '68', '1', 12),
(153, 'category', '69', '1', 10),
(154, 'category', '69', '1', 11),
(155, 'category', '69', '1', 12),
(156, 'category', '70', '1', 10),
(157, 'category', '70', '1', 11),
(158, 'category', '70', '1', 12),
(159, 'category', '71', '1', 10),
(160, 'category', '71', '1', 11),
(161, 'category', '71', '1', 12),
(162, 'category', '72', '1', 10),
(163, 'category', '72', '1', 11),
(164, 'category', '72', '1', 12),
(165, 'category', '73', '1', 10),
(166, 'category', '73', '1', 11),
(167, 'category', '73', '1', 12),
(168, 'category', '74', '1', 10),
(169, 'category', '74', '1', 11),
(170, 'category', '74', '1', 12),
(171, 'category', '75', '1', 10),
(172, 'category', '75', '1', 11),
(173, 'category', '75', '1', 12),
(174, 'category', '76', '1', 10),
(175, 'category', '76', '1', 11),
(176, 'category', '76', '1', 12),
(177, 'category', '77', '1', 10),
(178, 'category', '77', '1', 11),
(179, 'category', '77', '1', 12),
(180, 'category', '78', '1', 10),
(181, 'category', '78', '1', 11),
(182, 'category', '78', '1', 12),
(183, 'category', '79', '1', 10),
(184, 'category', '79', '1', 11),
(185, 'category', '79', '1', 12),
(186, 'category', '80', '1', 10),
(187, 'category', '80', '1', 11),
(188, 'category', '80', '1', 12),
(189, 'category', '81', '1', 10),
(190, 'category', '81', '1', 11),
(191, 'category', '81', '1', 12),
(192, 'category', '82', '1', 10),
(193, 'category', '82', '1', 11),
(194, 'category', '82', '1', 12),
(195, 'category', '83', '1', 10),
(196, 'category', '83', '1', 11),
(197, 'category', '83', '1', 12),
(198, 'category', '84', '1', 10),
(199, 'category', '84', '1', 11),
(200, 'category', '84', '1', 12),
(201, 'category', '85', '1', 10),
(202, 'category', '85', '1', 11),
(203, 'category', '85', '1', 12),
(204, 'category', '86', '1', 10),
(205, 'category', '86', '1', 11),
(206, 'category', '86', '1', 12),
(207, 'category', '87', '1', 10),
(208, 'category', '87', '1', 11),
(209, 'category', '87', '1', 12),
(210, 'category', '88', '1', 10),
(211, 'category', '88', '1', 11),
(212, 'category', '88', '1', 12),
(213, 'category', '89', '1', 10),
(214, 'category', '89', '1', 11),
(215, 'category', '89', '1', 12),
(216, 'category', '90', '1', 10),
(217, 'category', '90', '1', 11),
(218, 'category', '90', '1', 12),
(219, 'category', '91', '1', 10),
(220, 'category', '91', '1', 11),
(221, 'category', '91', '1', 12),
(222, 'category', '92', '1', 10),
(223, 'category', '92', '1', 11),
(224, 'category', '92', '1', 12),
(225, 'category', '93', '1', 10),
(226, 'category', '93', '1', 11),
(227, 'category', '93', '1', 12),
(228, 'category', '94', '1', 10),
(229, 'category', '94', '1', 11),
(230, 'category', '94', '1', 12),
(231, 'category', '95', '1', 10),
(232, 'category', '95', '1', 11),
(233, 'category', '95', '1', 12),
(234, 'category', '96', '1', 10),
(235, 'category', '96', '1', 11),
(236, 'category', '96', '1', 12),
(237, 'category', '97', '1', 10),
(238, 'category', '97', '1', 11),
(239, 'category', '97', '1', 12),
(240, 'category', '98', '1', 10),
(241, 'category', '98', '1', 11),
(242, 'category', '98', '1', 12),
(243, 'category', '99', '1', 10),
(244, 'category', '99', '1', 11),
(245, 'category', '99', '1', 12),
(246, 'category', '100', '1', 10),
(247, 'category', '100', '1', 11),
(248, 'category', '100', '1', 12),
(249, 'category', '101', '1', 10),
(250, 'category', '101', '1', 11),
(251, 'category', '101', '1', 12),
(252, 'category', '102', '1', 10),
(253, 'category', '102', '1', 11),
(254, 'category', '102', '1', 12),
(255, 'category', '103', '1', 10),
(256, 'category', '103', '1', 11),
(257, 'category', '103', '1', 12),
(258, 'category', '104', '1', 10),
(259, 'category', '104', '1', 11),
(260, 'category', '104', '1', 12),
(261, 'category', '105', '1', 10),
(262, 'category', '105', '1', 11),
(263, 'category', '105', '1', 12),
(264, 'category', '106', '1', 10),
(265, 'category', '106', '1', 11),
(266, 'category', '106', '1', 12),
(267, 'category', '107', '1', 10),
(268, 'category', '107', '1', 11),
(269, 'category', '107', '1', 12),
(270, 'category', '108', '1', 10),
(271, 'category', '108', '1', 11),
(272, 'category', '108', '1', 12),
(273, 'category', '109', '1', 10),
(274, 'category', '109', '1', 11),
(275, 'category', '109', '1', 12),
(276, 'category', '110', '1', 10),
(277, 'category', '110', '1', 11),
(278, 'category', '110', '1', 12),
(279, 'category', '111', '1', 10),
(280, 'category', '111', '1', 11),
(281, 'category', '111', '1', 12),
(282, 'category', '112', '1', 10),
(283, 'category', '112', '1', 11),
(284, 'category', '112', '1', 12),
(285, 'category', '113', '1', 10),
(286, 'category', '113', '1', 11),
(287, 'category', '113', '1', 12),
(288, 'category', '114', '1', 10),
(289, 'category', '114', '1', 11),
(290, 'category', '114', '1', 12),
(291, 'category', '115', '1', 10),
(292, 'category', '115', '1', 11),
(293, 'category', '115', '1', 12),
(294, 'category', '116', '1', 10),
(295, 'category', '116', '1', 11),
(296, 'category', '116', '1', 12),
(297, 'category', '117', '1', 10),
(298, 'category', '117', '1', 11),
(299, 'category', '117', '1', 12),
(300, 'category', '118', '1', 10),
(301, 'category', '118', '1', 11),
(302, 'category', '118', '1', 12),
(303, 'category', '119', '1', 10),
(304, 'category', '119', '1', 11),
(305, 'category', '119', '1', 12),
(306, 'category', '120', '1', 10),
(307, 'category', '120', '1', 11),
(308, 'category', '120', '1', 12),
(309, 'category', '121', '1', 10),
(310, 'category', '121', '1', 11),
(311, 'category', '121', '1', 12),
(312, 'category', '122', '1', 10),
(313, 'category', '122', '1', 11),
(314, 'category', '122', '1', 12),
(315, 'category', '123', '1', 10),
(316, 'category', '123', '1', 11),
(317, 'category', '123', '1', 12),
(318, 'category', '124', '1', 10),
(319, 'category', '124', '1', 11),
(320, 'category', '124', '1', 12),
(321, 'category', '125', '1', 10),
(322, 'category', '125', '1', 11),
(323, 'category', '125', '1', 12),
(324, 'product', '5', '0', 10),
(325, 'product', '5', '0', 11),
(326, 'product', '5', '0', 12),
(330, 'product', '7', '0', 10),
(331, 'product', '7', '0', 11),
(332, 'product', '7', '0', 12),
(333, 'product', '8', '0', 10),
(334, 'product', '8', '0', 11),
(335, 'product', '8', '0', 12),
(345, 'product', '12', '1', 10),
(346, 'product', '12', '1', 11),
(347, 'product', '12', '0', 12),
(351, 'product', '16', '1', 10),
(352, 'product', '16', '0', 11),
(353, 'product', '16', '0', 12),
(354, 'product', '17', '1', 10),
(355, 'product', '17', '0', 11),
(356, 'product', '17', '0', 12),
(357, 'product', '18', '0', 10),
(358, 'product', '18', '0', 11),
(359, 'product', '18', '0', 12),
(360, 'product', '19', '0', 10),
(361, 'product', '19', '0', 11),
(362, 'product', '19', '0', 12),
(363, 'product', '20', '0', 10),
(364, 'product', '20', '1', 11),
(365, 'product', '20', '0', 12),
(366, 'product', '21', '0', 10),
(367, 'product', '21', '1', 11),
(368, 'product', '21', '0', 12),
(369, 'product', '22', '0', 10),
(370, 'product', '22', '0', 11),
(371, 'product', '22', '0', 12),
(383, 'category', '131', '0', 10),
(384, 'category', '131', '0', 11),
(385, 'category', '131', '0', 12),
(389, 'product', '23', '0', 10),
(390, 'product', '23', '0', 11),
(391, 'product', '23', '0', 12),
(392, 'product', '24', '1', 10),
(393, 'product', '24', '1', 11),
(394, 'product', '24', '1', 12),
(395, 'category', '0', '1', 10),
(396, 'category', '0', '1', 11),
(397, 'category', '0', '1', 12),
(398, 'category', '0', '1', 10),
(399, 'category', '0', '1', 11),
(400, 'category', '0', '1', 12),
(401, 'category', '0', '1', 10),
(402, 'category', '0', '1', 11),
(403, 'category', '0', '1', 12),
(404, 'category', '136', '1', 10),
(405, 'category', '136', '1', 11),
(406, 'category', '136', '1', 12),
(407, 'product', '25', '0', 10),
(408, 'product', '25', '0', 11),
(409, 'product', '25', '0', 12),
(410, 'product', '26', '0', 10),
(411, 'product', '26', '0', 11),
(412, 'product', '26', '0', 12),
(413, 'product', '27', '0', 10),
(414, 'product', '27', '0', 11),
(415, 'product', '27', '0', 12),
(416, 'product', '28', '0', 10),
(417, 'product', '28', '0', 11),
(418, 'product', '28', '0', 12),
(419, 'product', '29', '0', 10),
(420, 'product', '29', '0', 11),
(421, 'product', '29', '0', 12),
(422, 'product', '30', '0', 10),
(423, 'product', '30', '0', 11),
(424, 'product', '30', '0', 12),
(425, 'product', '31', '0', 10),
(426, 'product', '31', '0', 11),
(427, 'product', '31', '0', 12),
(428, 'product', '32', '0', 10),
(429, 'product', '32', '0', 11),
(430, 'product', '32', '0', 12),
(431, 'product', '33', '0', 10),
(432, 'product', '33', '0', 11),
(433, 'product', '33', '0', 12),
(434, 'product', '34', '0', 10),
(435, 'product', '34', '0', 11),
(436, 'product', '34', '0', 12),
(437, 'product', '35', '0', 10),
(438, 'product', '35', '0', 11),
(439, 'product', '35', '0', 12),
(440, 'product', '36', '0', 10),
(441, 'product', '36', '1', 11),
(442, 'product', '36', '0', 12),
(443, 'product', '37', '0', 10),
(444, 'product', '37', '1', 11),
(445, 'product', '37', '1', 12),
(446, 'product', '38', '0', 10),
(447, 'product', '38', '0', 11),
(448, 'product', '38', '1', 12),
(449, 'product', '39', '1', 10),
(450, 'product', '39', '1', 11),
(451, 'product', '39', '1', 12),
(452, 'product', '40', '1', 10),
(453, 'product', '40', '1', 11),
(454, 'product', '40', '1', 12),
(455, 'product', '41', '1', 10),
(456, 'product', '41', '1', 11),
(457, 'product', '41', '1', 12),
(458, 'product', '42', '1', 10),
(459, 'product', '42', '1', 11),
(460, 'product', '42', '1', 12),
(461, 'product', '43', '0', 10),
(462, 'product', '43', '0', 11),
(463, 'product', '43', '0', 12),
(464, 'product', '44', '1', 10),
(465, 'product', '44', '1', 11),
(466, 'product', '44', '1', 12),
(467, 'product', '45', '1', 10),
(468, 'product', '45', '1', 11),
(469, 'product', '45', '1', 12),
(470, 'product', '46', '0', 10),
(471, 'product', '46', '0', 11),
(472, 'product', '46', '0', 12),
(473, 'product', '47', '0', 10),
(474, 'product', '47', '0', 11),
(475, 'product', '47', '0', 12),
(476, 'product', '48', '0', 10),
(477, 'product', '48', '0', 11),
(478, 'product', '48', '0', 12),
(479, 'product', '49', '0', 10),
(480, 'product', '49', '0', 11),
(481, 'product', '49', '0', 12),
(482, 'product', '50', '0', 10),
(483, 'product', '50', '0', 11),
(484, 'product', '50', '0', 12),
(485, 'product', '51', '0', 10),
(486, 'product', '51', '0', 11),
(487, 'product', '51', '0', 12),
(488, 'product', '52', '0', 10),
(489, 'product', '52', '0', 11),
(490, 'product', '52', '0', 12),
(491, 'product', '53', '0', 10),
(492, 'product', '53', '0', 11),
(493, 'product', '53', '0', 12),
(500, 'product', '56', '1', 10),
(501, 'product', '56', '1', 11),
(502, 'product', '56', '1', 12),
(503, 'category', '137', '0', 10),
(504, 'category', '137', '0', 11),
(505, 'category', '137', '0', 12),
(506, 'category', '138', '0', 10),
(507, 'category', '138', '0', 11),
(508, 'category', '138', '0', 12),
(509, 'category', '139', '0', 10),
(510, 'category', '139', '0', 11),
(511, 'category', '139', '0', 12),
(512, 'category', '140', '0', 10),
(513, 'category', '140', '0', 11),
(514, 'category', '140', '0', 12),
(515, 'product', '57', '0', 10),
(516, 'product', '57', '0', 11),
(517, 'product', '57', '0', 12),
(518, 'product', '58', '0', 10),
(519, 'product', '58', '0', 11),
(520, 'product', '58', '0', 12),
(521, 'product', '59', '0', 10),
(522, 'product', '59', '0', 11),
(523, 'product', '59', '0', 12),
(524, 'product', '60', '0', 10),
(525, 'product', '60', '0', 11),
(526, 'product', '60', '0', 12),
(527, 'product', '61', '0', 10),
(528, 'product', '61', '0', 11),
(529, 'product', '61', '0', 12),
(530, 'product', '62', '0', 10),
(531, 'product', '62', '0', 11),
(532, 'product', '62', '0', 12);

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE IF NOT EXISTS `cart` (
`cart_id` int(100) NOT NULL,
  `product_id` int(100) NOT NULL,
  `user_id` int(100) NOT NULL,
  `added_on` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`cart_id`, `product_id`, `user_id`, `added_on`) VALUES
(37, 7, 15, '1493386951');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
`cat_id` int(100) NOT NULL,
  `cat_name` varchar(100) NOT NULL,
  `cat_image` text NOT NULL,
  `cat_desc` text NOT NULL,
  `added_on` varchar(100) NOT NULL,
  `added_by` varchar(100) NOT NULL,
  `parent_id` varchar(10) NOT NULL,
  `level` varchar(10) NOT NULL,
  `cat_type` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`cat_id`, `cat_name`, `cat_image`, `cat_desc`, `added_on`, `added_by`, `parent_id`, `level`, `cat_type`) VALUES
(33, 'Literature', 'File560394.png', 'All genre of books rangng from academic literature,script writing,Novels,short novels,journals,columnist articles', '1492414545', '11', '0', '0', 'Main Category'),
(34, 'Art Work', 'File412841.png', 'All forms of Artistry ranging from Drawings to paintings and photography', '1492414748', '11', '0', '0', 'Main Category'),
(35, 'Music', 'File1495.png', 'All genre of music,audio books and podcast', '1492414592', '11', '0', '0', 'Main Category'),
(36, 'Video', 'File399108.png', 'All genre of videos,movies,short films,comedy and  live videocast', '1492414609', '11', '0', '0', 'Main Category'),
(37, 'Comedy', 'File641571.png', '', '1492451651', '10', '36', '1', 'Genre'),
(38, 'Short Films', 'File873901.png', '', '1492451775', '10', '36', '1', 'Genre'),
(39, 'Films', 'File198608.png', '', '1492451804', '10', '36', '1', 'Genre'),
(40, 'Music Videos', 'File247985.png', '', '1492451877', '10', '36', '1', 'Genre'),
(41, 'Interviews', 'File188446.png', '', '1492451920', '10', '36', '1', 'Genre'),
(42, 'Stage Play', 'File807983.png', '', '1492451958', '10', '36', '1', 'Genre'),
(43, 'Cartoons', 'File837554.png', '', '1492452008', '10', '36', '1', 'Genre'),
(44, 'Weird Corner', 'File967102.png', '', '1492452055', '10', '36', '1', 'Genre'),
(45, 'Movie Trailers', 'File694488.png', '', '1492452108', '10', '36', '1', 'Genre'),
(46, 'Funk', 'File622863.png', '', '1492452766', '10', '35', '1', 'Genre'),
(47, 'Jazz', 'File612640.png', '', '1492452797', '10', '35', '1', 'Genre'),
(48, 'Afrobeats', 'File357666.png', '', '1492452820', '10', '35', '1', 'Genre'),
(49, 'Hip Hop', 'File237335.png', '', '1492452840', '10', '35', '1', 'Genre'),
(50, 'Reggae', 'File355651.png', '', '1492452867', '10', '35', '1', 'Genre'),
(51, 'Juju', 'File799041.png', '', '1492452900', '10', '35', '1', 'Genre'),
(52, 'Fuji', 'File737915.png', '', '1492452919', '10', '35', '1', 'Genre'),
(53, 'Refixes', 'File496978.png', '', '1492452940', '10', '35', '1', 'Genre'),
(54, 'Hindi', 'File884613.png', '', '1492452964', '10', '35', '1', 'Genre'),
(55, 'Latino', 'File752685.png', '', '1492452979', '10', '35', '1', 'Genre'),
(56, 'Rock', 'File44464.png', '', '1492453000', '10', '35', '1', 'Genre'),
(57, 'Reggaeton', 'File82702.png', '', '1492453034', '10', '35', '1', 'Genre'),
(58, 'Antiquities', 'File797576.png', '', '1492453119', '10', '34', '1', 'Genre'),
(59, 'Photography', 'File328247.png', '', '1492453160', '10', '34', '1', 'Genre'),
(60, 'Drawing', 'File593170.png', '', '1492453202', '10', '34', '1', 'Genre'),
(61, 'Poetry', 'File219299.png', '', '1492453250', '10', '34', '1', 'Genre'),
(62, 'Cartoon', 'File454162.png', '', '1492453314', '10', '34', '1', 'Genre'),
(63, 'Other', 'File599212.png', '', '1492453338', '10', '34', '1', 'Genre'),
(64, 'Books', 'File249114.png', '', '1492490490', '10', '33', '1', 'Sub Category'),
(65, 'Articles', 'File858398.png', '', '1492490523', '10', '33', '1', 'Sub Category'),
(66, 'Columnists', 'File134887.png', '', '1492490686', '10', '33', '1', 'Sub Category'),
(67, 'World News', 'File324066.png', '', '1492490591', '10', '33', '1', 'Sub Category'),
(68, 'Poems', 'File653076.png', '', '1492490615', '10', '33', '1', 'Sub Category'),
(69, 'Drama', 'File187042.png', '', '1492490629', '10', '33', '1', 'Sub Category'),
(70, 'ScreenPlay', 'File623718.png', '', '1492490648', '10', '33', '1', 'Sub Category'),
(71, 'Others', 'File768280.png', '', '1492490676', '10', '33', '1', 'Sub Category'),
(72, 'Comedy', 'File62866.png', '', '1492491047', '10', '71', '2', 'Genre'),
(73, 'Thriller', 'File981933.png', '', '1492491221', '10', '71', '2', 'Genre'),
(74, 'Romance', 'File315277.png', '', '1492491115', '10', '71', '2', 'Genre'),
(75, 'Action', 'File195068.png', '', '1492491134', '10', '71', '2', 'Genre'),
(76, 'Religious', 'File516998.png', '', '1492491173', '10', '71', '2', 'Genre'),
(77, 'Educational', 'File151428.png', '', '1492491187', '10', '71', '2', 'Genre'),
(78, 'Horor', 'File601165.png', '', '1492491208', '10', '71', '2', 'Genre'),
(79, 'Comedy', 'File657501.png', '', '1492491522', '10', '70', '2', 'Genre'),
(80, 'Thriller', 'File28747.png', '', '1492491536', '10', '70', '2', 'Genre'),
(81, 'Action', 'File541473.png', '', '1492491547', '10', '70', '2', 'Genre'),
(82, 'Romance', 'File537963.png', '', '1492491558', '10', '70', '2', 'Genre'),
(83, 'Religious', 'File77301.png', '', '1492491571', '10', '70', '2', 'Genre'),
(84, 'Horor', 'File349426.png', '', '1492491591', '10', '70', '2', 'Genre'),
(85, 'Educational', 'File807952.png', '', '1492491614', '10', '70', '2', 'Genre'),
(86, 'Action', 'File259429.png', '', '1492491666', '10', '69', '2', 'Genre'),
(87, 'Horor', 'File951477.png', '', '1492491678', '10', '69', '2', 'Genre'),
(88, 'Thriller', 'File181488.png', '', '1492491699', '10', '69', '2', 'Genre'),
(89, 'Religious', 'File112060.png', '', '1492491712', '10', '69', '2', 'Genre'),
(90, 'Educational', 'File216339.png', '', '1492491729', '10', '69', '2', 'Genre'),
(91, 'Romance', 'File817077.png', '', '1492491748', '10', '69', '2', 'Genre'),
(92, 'Comedy', 'File844451.png', '', '1492491767', '10', '69', '2', 'Genre'),
(93, 'Comedy', 'File457855.png', '', '1492491926', '10', '68', '2', 'Genre'),
(94, 'Thriller', 'File736083.png', '', '1492491946', '10', '68', '2', 'Genre'),
(95, 'Action', 'File734893.png', '', '1492491964', '10', '68', '2', 'Genre'),
(96, 'Romance', 'File839538.png', '', '1492491976', '10', '68', '2', 'Genre'),
(97, 'Educational', 'File792694.png', '', '1492492005', '10', '68', '2', 'Genre'),
(98, 'Religious', 'File646728.png', '', '1492492076', '10', '68', '2', 'Genre'),
(99, 'Horor', 'File745147.png', '', '1492492094', '10', '68', '2', 'Genre'),
(100, 'Comedy', 'File296020.png', '', '1492492137', '10', '66', '2', 'Genre'),
(101, 'Thriller', 'File580474.png', '', '1492492149', '10', '66', '2', 'Genre'),
(102, 'Action', 'File570373.png', '', '1492492158', '10', '66', '2', 'Genre'),
(103, 'Romance', 'File766204.png', '', '1492492172', '10', '66', '2', 'Genre'),
(104, 'Religious', 'File471191.png', '', '1492492184', '10', '66', '2', 'Genre'),
(105, 'Educational', 'File486602.png', '', '1492492197', '10', '66', '2', 'Genre'),
(106, 'Horor', 'File900817.png', '', '1492492210', '10', '66', '2', 'Genre'),
(107, 'Comedy', 'File97137.png', '', '1492492266', '10', '65', '2', 'Genre'),
(108, 'Thriller', 'File605346.png', '', '1492492282', '10', '65', '2', 'Genre'),
(109, 'Action', 'File922027.png', '', '1492492304', '10', '65', '2', 'Genre'),
(110, 'Romance', 'File924621.png', '', '1492492367', '10', '65', '2', 'Genre'),
(111, 'Religious', 'File4241.png', '', '1492492387', '10', '65', '2', 'Genre'),
(112, 'Educational', 'File542236.png', '', '1492492404', '10', '65', '2', 'Genre'),
(113, 'Horor', 'File855499.png', '', '1492492432', '10', '65', '2', 'Genre'),
(114, 'Action', 'File769042.png', '', '1492492522', '10', '64', '2', 'Genre'),
(115, 'Thriller', 'File452423.png', '', '1492492547', '10', '64', '2', 'Genre'),
(116, 'Comedy', 'File675231.png', '', '1492492561', '10', '64', '2', 'Genre'),
(117, 'Educational', 'File8270.png', '', '1492492581', '10', '64', '2', 'Genre'),
(118, 'Religious', 'File418823.png', '', '1492492594', '10', '64', '2', 'Genre'),
(119, 'Horor', 'File764190.png', '', '1492492639', '10', '64', '2', 'Genre'),
(120, 'Romance', 'File32897.png', '', '1492492654', '10', '64', '2', 'Genre'),
(121, 'World News', 'File868865.png', '', '1492492931', '10', '67', '2', 'Genre'),
(122, 'Entertainment News', 'File598754.png', '', '1492492949', '10', '67', '2', 'Genre'),
(123, 'Sports News', 'File578521.png', '', '1492492970', '10', '67', '2', 'Genre'),
(124, 'Science & Technology', 'File357482.png', '', '1492492996', '10', '67', '2', 'Genre'),
(125, 'Wierd News', 'File561126.png', '', '1492493022', '10', '67', '2', 'Genre'),
(131, 'admin test', 'File248953161.png', '', '1494411376', '11', '35', '1', ''),
(136, 'super test', 'File249620994.png', '', '1494426660', '10', '35', '1', 'Genre'),
(137, 'Other1', 'File29718551.png', '', '1496384468', '11', '33', '1', 'Sub Category'),
(138, 'prod1', 'File721889965.png', '', '1496387354', '11', '137', '2', 'Genre'),
(139, 'lit test', 'File211777145.png', '', '1496388197', '11', '33', '1', 'Sub Category'),
(140, 'test1', 'File467668346.png', '', '1496388413', '11', '139', '2', 'Genre');

-- --------------------------------------------------------

--
-- Table structure for table `comment_data`
--

CREATE TABLE IF NOT EXISTS `comment_data` (
`comment_id` int(100) NOT NULL,
  `comment_text` text NOT NULL,
  `user_id` int(100) NOT NULL,
  `comment_on` varchar(100) NOT NULL,
  `product_id` int(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment_data`
--

INSERT INTO `comment_data` (`comment_id`, `comment_text`, `user_id`, `comment_on`, `product_id`) VALUES
(1, 'Awesome Music Ever', 9, '1423652145', 5),
(7, 'sdasda', 9, '1492775938', 5),
(8, '', 15, '1492776021', 5),
(9, '', 15, '1492776021', 5),
(10, '', 15, '1492776363', 5),
(11, '', 15, '1492784133', 12),
(12, '', 15, '1492784173', 12),
(13, '', 15, '1492784954', 13),
(14, '', 15, '1492785516', 5),
(15, '', 15, '1492787907', 5),
(16, '', 15, '1492788182', 12),
(17, 'hiiii', 15, '1493010773', 12),
(18, 'hi', 15, '1493131678', 8),
(19, 'ffcgg', 15, '1493278676', 13),
(20, 'hgbghhh', 15, '1493290274', 17),
(21, 'GH vffg', 15, '1493290766', 17),
(22, 'iiiii', 15, '1493290786', 17),
(23, 'dfgcv', 15, '1493357828', 8),
(24, 'xfcv', 15, '1493358047', 12);

-- --------------------------------------------------------

--
-- Table structure for table `counter`
--

CREATE TABLE IF NOT EXISTS `counter` (
`counter_id` int(100) NOT NULL,
  `product_id` int(100) NOT NULL,
  `user_id` int(100) NOT NULL,
  `counter_type` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `counter`
--

INSERT INTO `counter` (`counter_id`, `product_id`, `user_id`, `counter_type`) VALUES
(5, 5, 15, 'playtime'),
(6, 5, 15, 'playtime'),
(7, 5, 15, 'downloads'),
(13, 5, 15, 'downloads'),
(14, 5, 15, 'downloads'),
(15, 5, 9, 'likes'),
(16, 12, 15, 'downloads'),
(17, 12, 15, 'downloads'),
(19, 5, 15, 'downloads'),
(20, 5, 15, 'downloads'),
(26, 12, 15, 'downloads'),
(32, 13, 15, 'downloads'),
(53, 5, 15, 'downloads'),
(67, 12, 15, 'downloads'),
(76, 12, 15, 'downloads'),
(82, 5, 15, 'likes'),
(85, 12, 15, 'dislikes'),
(86, 13, 15, 'downloads'),
(87, 13, 15, 'downloads'),
(88, 8, 15, 'downloads'),
(89, 8, 15, 'downloads'),
(90, 8, 15, 'downloads'),
(91, 8, 15, 'downloads'),
(92, 8, 15, 'downloads'),
(93, 13, 15, 'downloads'),
(94, 8, 15, 'downloads'),
(95, 8, 15, 'downloads'),
(102, 13, 15, 'likes'),
(104, 8, 15, 'dislikes'),
(105, 8, 15, 'downloads'),
(106, 8, 15, 'downloads');

-- --------------------------------------------------------

--
-- Table structure for table `discount_coupon`
--

CREATE TABLE IF NOT EXISTS `discount_coupon` (
`coupon_id` int(100) NOT NULL,
  `coupon_code` varchar(100) NOT NULL,
  `coupon_value` text NOT NULL,
  `generated_on` varchar(100) NOT NULL,
  `expired_on` varchar(100) NOT NULL,
  `coupon_status` enum('0','1') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `discount_coupon`
--

INSERT INTO `discount_coupon` (`coupon_id`, `coupon_code`, `coupon_value`, `generated_on`, `expired_on`, `coupon_status`) VALUES
(1, 'ABCD1020', '100.0', '1490137200', '1493330400', '1'),
(3, 'BKB33', '33.10', '1490137200', '1492552800', '1'),
(5, 'Code50', '50.00', '1491861600', '1491948000', '0');

-- --------------------------------------------------------

--
-- Table structure for table `membership`
--

CREATE TABLE IF NOT EXISTS `membership` (
`plan_id` int(100) NOT NULL,
  `plan_name` varchar(100) NOT NULL,
  `plan_price` varchar(100) NOT NULL,
  `renewal_type` varchar(100) NOT NULL,
  `plan_status` enum('0','1') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `membership`
--

INSERT INTO `membership` (`plan_id`, `plan_name`, `plan_price`, `renewal_type`, `plan_status`) VALUES
(5, 'Basic', '0', 'all', '1'),
(6, 'Premium', '120', 'all', '1'),
(7, 'newtest', '57', 'all', '1'),
(8, 'sda', '120.88', 'all', '1');

-- --------------------------------------------------------

--
-- Table structure for table `orderdetail`
--

CREATE TABLE IF NOT EXISTS `orderdetail` (
`detail_id` int(100) NOT NULL,
  `order_id` int(100) NOT NULL,
  `item_id` int(100) NOT NULL,
  `user_id` int(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orderdetail`
--

INSERT INTO `orderdetail` (`detail_id`, `order_id`, `item_id`, `user_id`) VALUES
(28, 21, 8, 15),
(29, 22, 13, 15),
(30, 23, 12, 15),
(31, 24, 8, 15),
(32, 25, 12, 15),
(33, 26, 12, 15);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
`order_id` int(100) NOT NULL,
  `order_number` varchar(100) NOT NULL,
  `transaction_id` text NOT NULL,
  `amount` varchar(100) NOT NULL,
  `payment_status` varchar(100) NOT NULL,
  `order_dated` varchar(100) NOT NULL,
  `order_userId` varchar(100) NOT NULL,
  `order_status` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `order_number`, `transaction_id`, `amount`, `payment_status`, `order_dated`, `order_userId`, `order_status`) VALUES
(21, '840410516', 'PAY-37U50819T6332520SLEAF6OY', '120', 'Success', '1493196586', '15', 'Success'),
(22, '124268363', '21546551616545', '120', 'Failure', '1493200927', '15', 'Pending'),
(23, '735991485', 'Not Found', '100', 'Failed', '1493290985', '15', 'Pending'),
(24, '805834590', 'Not Found', '120', 'Failed', '1493357552', '15', 'Pending'),
(25, '111293138', '', '100', 'Pending', '1493359222', '15', 'Pending'),
(26, '649669114', '', '100', 'Pending', '1493359223', '15', 'Pending');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
`product_id` int(100) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `product_image` varchar(100) NOT NULL,
  `product_desc` text NOT NULL,
  `prod_file_type` text NOT NULL,
  `prod_artist_name` varchar(200) NOT NULL,
  `file_type` varchar(100) NOT NULL,
  `category_id` int(100) NOT NULL,
  `file_name` varchar(100) NOT NULL,
  `product_price` float NOT NULL,
  `likes` int(100) NOT NULL,
  `dislikes` int(100) NOT NULL,
  `downloads` int(100) NOT NULL,
  `playtime` int(100) NOT NULL,
  `uploaded_by` varchar(100) NOT NULL,
  `user_id` int(100) NOT NULL,
  `added_on` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_name`, `product_image`, `product_desc`, `prod_file_type`, `prod_artist_name`, `file_type`, `category_id`, `file_name`, `product_price`, `likes`, `dislikes`, `downloads`, `playtime`, `uploaded_by`, `user_id`, `added_on`) VALUES
(5, '$prod_name', 'File1065972174.jpg', '$prod_desc', '$prod_file_type', '$artist_name', 'Music', 54, '$file_name', 0, 1, 0, 0, 0, '$uploaded_by', 0, '1492493022'),
(7, 'Abc', 'File843546809.jpg', 'sdasdas', '', '', 'Video', 45, 'File309392014.mp4', 120, 0, 0, 0, 0, 'admin', 11, '1492589504'),
(8, 'Bahubali', 'File483214006.jpg', 'asdf', '', '', 'Video', 45, 'demo_song.3gp', 120, 0, 0, 0, 0, 'admin', 11, '1492604531'),
(12, 'Song1', 'File14861834840.png', 'Demo one', '', '', 'Music', 54, 'File98331602569.mp3', 100, 0, 0, 0, 0, 'admin', 10, '1492683411'),
(16, 'docx file', 'File21753821289.png', 'Comedy Image', '', '', 'Literature', 92, 'File32140608783.docx', 120, 0, 0, 0, 0, 'admin', 10, '1493282179'),
(17, 'pdf file', 'File50807401118.png', 'dfsfsdfsd', '', '', 'Literature', 92, 'File53517434094.pdf', 110, 0, 0, 0, 0, 'admin', 10, '1493282273'),
(18, 'derf', 'File71168733108.png', 'frfrr', '', '', 'Art', 125, 'File67084948020.png', 0, 0, 0, 0, 0, 'user', 15, '1493792153'),
(19, 'new product', 'File31726607913.png', 'nxjdncj', '', '', 'Art', 125, 'File63855255208.png', 11, 0, 0, 0, 0, 'user', 15, '1493792511'),
(20, 'cg f', 'File37002051854.png', 'vgc', '', '', 'Art', 61, 'File20131131820.png', 0, 0, 0, 0, 0, 'user', 15, '1493792658'),
(21, 'hxhdb', 'File78257786436.png', 'jxjxjxn', '', '', 'Art Work', 61, 'File17102183680.png', 0, 0, 0, 0, 0, 'user', 15, '1493798787'),
(22, 'new product', 'File72325547365.png', 'product added', '', '', 'Art Work', 60, 'File47792816068.png', 12, 0, 0, 0, 0, 'user', 15, '1493810988'),
(23, 'dfd', 'File624275097.png', 'dddddd', '', '', 'Literature', 78, 'File787337563.docx', 21, 0, 0, 0, 0, 'admin', 11, '1494421903'),
(24, 'super test', 'File1061705466.png', 'sdsd', '', '', 'Literature', 78, 'File839799525.docx', 23, 0, 0, 0, 0, 'admin', 10, '1494422069'),
(25, 'new music', 'File1111718709.png', 'new product added', '', 'naruu', 'Music', 136, 'File580828874.mp3', 0, 0, 0, 0, 0, 'admin', 11, '1494843253'),
(26, 'ddd', 'File190925927.png', 'ddd', '', '', 'Video', 45, '', 1, 0, 0, 0, 0, 'admin', 11, '1495177908'),
(27, 'sdsd', 'File658037746.png', 'sdsd', 'prod_link', '', 'Video', 45, '', 1, 0, 0, 0, 0, 'admin', 11, '1495178559'),
(28, 'dfd', 'File411199387.png', 'dfdf', 'prod_link', '', 'Video', 45, 'https://www.youtube.com/watch?v=kpB5v6es6n0', 1, 0, 0, 0, 0, 'admin', 11, '1495179101'),
(29, 'ghgh', 'File10017489.png', 'fgf', 'prod_file', '', 'Video', 45, 'File1016626764.3gp', 12, 0, 0, 0, 0, 'admin', 11, '1495192687'),
(30, 'fgfg', 'File352059098.png', 'fgfg', 'prod_link', '', 'Video', 45, 'https://www.youtube.com/watch?v=HPYRBqiJcOc', 1, 0, 0, 0, 0, 'admin', 11, '1495193259'),
(31, 'ffffffffffffffffffffff', 'File288132675.png', 'fff', 'prod_file', 'hhhh', 'Video', 45, 'File554301078.3gp', 1, 0, 0, 0, 0, 'admin', 11, '1495193324'),
(32, 'sds', 'File365304445.png', 'dddd', 'prod_link', '', 'Video', 45, 'https://www.youtube.com/watch?v=HPYRBqiJcOc', 1, 0, 0, 0, 0, 'admin', 11, '1495201522'),
(33, 'sss', 'File980044377.png', 'ddddd', 'prod_link', '', 'Video', 45, 'https://vimeo.com/206768959', 1, 0, 0, 0, 0, 'admin', 11, '1495201906'),
(34, 'sds', 'File713096836.png', 'sdsds', '', 'nardeep', 'Music', 136, 'File317257598.mp3', 1, 0, 0, 0, 0, 'admin', 11, '1495264301'),
(35, 'sdsd', 'File411644609.png', 'sdsds', '', 'sandhu', 'Music', 136, 'File1035400281.mp3', 1, 0, 0, 0, 0, 'admin', 11, '1495264482'),
(36, 'new_proddss', 'File1003900842.png', 'dsd', 'prod_file', 'sourav', 'Video', 45, 'File605390274.3gp', 1, 0, 0, 0, 0, 'admin', 11, '1495264740'),
(37, 'dfdf', 'File1167482733.png', 'ddddddddd', 'prod_file', 'df', 'Video', 45, 'File894969921.mp4', 1, 0, 0, 0, 0, 'admin', 11, '1495542425'),
(38, 'song', 'File329835113.png', 'song added', '', 'sk', 'Music', 136, 'File226951787.mp3', 1, 0, 0, 0, 0, 'admin', 12, '1495794365'),
(39, 'song test', 'File584576157.png', 'song added successfully', '', 'sk', 'Music', 136, 'File1089865742.mp3', 1, 0, 0, 0, 0, 'admin', 10, '1495794582'),
(40, 'mail check', 'File23893567.png', 'mailer check is done', '', 'mailer', 'Music', 136, 'File765447493.mp3', 1, 0, 0, 0, 0, 'admin', 10, '1495810257'),
(41, 'mailer test', 'File62998877.png', 'mailer test is done', '', 'new mailer', 'Music', 136, 'File1162807905.mp3', 1, 0, 0, 0, 0, 'admin', 10, '1495810731'),
(42, 'mailer test1', 'File635776659.png', 'sdsdsdsdsdsdsds', '', 'mailerr', 'Music', 136, 'File904208272.mp3', 1, 0, 0, 0, 0, 'admin', 10, '1495810957'),
(43, 'sdsds', 'File929882726.png', 'sdsds', '', 'sd', 'Music', 136, 'File794498212.mp3', 1, 0, 0, 0, 0, 'admin', 11, '1496061952'),
(44, 'dd', 'File844511456.png', 'sdsd', '', 'sds', 'Music', 136, 'File83887198.mp3', 1, 0, 0, 0, 0, 'admin', 10, '1496062023'),
(45, 'hi test', 'File783293465.png', 'sdsdsd', '', 'sds', 'Music', 136, 'File834456864.mp3', 1, 0, 0, 0, 0, 'admin', 10, '1496062387'),
(46, 'kailash kher', 'File725563045.png', 'kailash kher music added', '', 'R.D rehmaan', 'Music', 136, 'File208623491.mp3', 1, 0, 0, 0, 0, 'admin', 11, '1496223115'),
(47, 'prod added', 'File392388769.png', 'product added successfully', '', 'first', 'Music', 136, 'File941013270.mp3', 1, 0, 0, 0, 0, 'admin', 11, '1496223552'),
(48, 'mail', 'File91826985.png', 'mail test', '', 'first', 'Music', 131, 'File1063560557.mp3', 1, 0, 0, 0, 0, 'admin', 11, '1496313980'),
(49, 'First', 'File832675977.png', 'first code added here', '', 'A.R rehmaan', 'Music', 131, 'File512672845.mp3', 1, 0, 0, 0, 0, 'admin', 11, '1496314216'),
(50, 'mail', 'File995738443.png', 'mail test is added', '', 'A.r Rehmaan', 'Music', 57, 'File1110716960.mp3', 1, 0, 0, 0, 0, 'admin', 12, '1496314507'),
(51, 'cs', 'File781364171.png', 'sdsdsds', '', 'A.R', 'Music', 57, 'File69714305.mp3', 1, 0, 0, 0, 0, 'admin', 12, '1496315214'),
(52, 'css', 'File573519817.png', 'nakhre', '', 'fucker', 'Music', 57, 'File1055954685.mp3', 1, 0, 0, 0, 0, 'admin', 12, '1496315457'),
(53, 'cssss', 'File1054173798.png', 'sdsdsdsdsd', '', 'css', 'Music', 57, 'File449525560.mp3', 1, 0, 0, 0, 0, 'admin', 12, '1496315831'),
(56, 'mail test', 'File1022266239.png', 'mail test added', '', 'mailer', 'Music', 54, 'File988095470.mp3', 1, 0, 0, 0, 0, 'admin', 10, '1496317401'),
(57, 'rehmaan1', 'File382668094.jpg', 'prod added successfully', '', 'A.r rehmaan', 'Music', 49, 'File1155981172.mp3', 12, 0, 0, 0, 0, 'admin', 11, '1498299288'),
(58, 'prod1', 'File311952039.jpg', 'prod added successfully', '', 'AR rehmaan', 'Music', 46, 'File947246374.mp3', 12, 0, 0, 0, 0, 'admin', 11, '1498300134'),
(59, 'prod2', 'File431568283.jpg', 'sdsdsd', '', 'ghgh', 'Music', 46, 'File946949560.mp3', 1, 0, 0, 0, 0, 'admin', 11, '1498300283'),
(60, 'sd', 'File1022563054.jpg', 'sdsd', '', 'dsd', 'Music', 46, 'File638151175.mp3', 0, 0, 0, 0, 0, 'admin', 11, '1498300328'),
(61, 'prod3', 'File241532799.jpg', 'tested ffff', '', 'ad', 'Music', 46, 'File388567282.mp3', 1, 0, 0, 0, 0, 'admin', 11, '1498300472'),
(62, 'new prodd', 'File461694954.jpg', 'prod added', '', 'prod3', 'Music', 46, 'File76541039.mp3', 12, 0, 0, 0, 0, 'admin', 11, '1498300644');

-- --------------------------------------------------------

--
-- Table structure for table `purchased_record`
--

CREATE TABLE IF NOT EXISTS `purchased_record` (
`row_id` int(100) NOT NULL,
  `item_id` int(100) NOT NULL,
  `user_id` int(100) NOT NULL,
  `purchased_on` varchar(100) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchased_record`
--

INSERT INTO `purchased_record` (`row_id`, `item_id`, `user_id`, `purchased_on`) VALUES
(2, 13, 15, '1493204930'),
(3, 8, 15, '1493357586');

-- --------------------------------------------------------

--
-- Table structure for table `refixfm`
--

CREATE TABLE IF NOT EXISTS `refixfm` (
`fm_id` int(100) NOT NULL,
  `fm_playlist_name` varchar(100) NOT NULL,
  `fm_playlist_desc` text NOT NULL,
  `fm_playlist_file` text NOT NULL,
  `fm_file_type` varchar(200) NOT NULL,
  `fm_type` varchar(200) NOT NULL,
  `fm_user_id` varchar(100) NOT NULL,
  `fm_added_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `refixfm`
--

INSERT INTO `refixfm` (`fm_id`, `fm_playlist_name`, `fm_playlist_desc`, `fm_playlist_file`, `fm_file_type`, `fm_type`, `fm_user_id`, `fm_added_on`) VALUES
(12, 'new player', 'new player selected', '', '', 'fm', '11', '2017-05-18 11:36:31'),
(13, 'new playlist', 'playlist added here', '', '', 'fm', '11', '2017-05-18 11:36:35'),
(14, 'babad', 'jhj', 'File1059479357.png', '', 'fm', '11', '2017-05-22 20:24:35'),
(15, 'd', 'ddd', 'File1040854247.png', '', 'fm', '11', '2017-05-22 20:23:02'),
(20, 'new playere', 'sdsdsd', 'File136497568.png', '', 'fm', '11', '2017-06-22 17:16:29'),
(21, 'literature', 'add new literature', 'File215042105.png', '', 'literature', '11', '2017-05-23 15:32:36'),
(22, 'test', 'test done here', 'File710462607.png', '', 'literature', '11', '2017-05-23 15:54:40'),
(30, 'hi', 'hi one addd', 'File929474606.png', 'youtube', 'tv', '11', '2017-05-31 11:51:39'),
(31, 'uploaded', 'uploaded video', 'File436985147.png', 'uploaded', 'tv', '11', '2017-05-31 11:52:34');

-- --------------------------------------------------------

--
-- Table structure for table `refixfm_ref`
--

CREATE TABLE IF NOT EXISTS `refixfm_ref` (
`ref_id` int(11) NOT NULL,
  `ref_product_id` int(100) NOT NULL,
  `ref_type` varchar(200) NOT NULL,
  `ref_file_type` varchar(200) NOT NULL,
  `ref_fm_id` int(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=267 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `refixfm_ref`
--

INSERT INTO `refixfm_ref` (`ref_id`, `ref_product_id`, `ref_type`, `ref_file_type`, `ref_fm_id`) VALUES
(30, 25, 'music', '', 12),
(31, 12, 'music', '', 12),
(32, 13, 'music', '', 12),
(33, 2, 'add', '', 12),
(148, 24, 'books', '', 22),
(149, 23, 'books', '', 22),
(150, 17, 'books', '', 22),
(151, 16, 'books', '', 22),
(152, 12, 'add', '', 22),
(153, 35, 'music', '', 13),
(154, 34, 'music', '', 13),
(155, 10, 'add', '', 13),
(156, 6, 'add', '', 13),
(157, 2, 'add', '', 13),
(158, 25, 'music', '', 13),
(162, 24, 'books', '', 21),
(163, 23, 'books', '', 21),
(164, 12, 'add', '', 21),
(165, 5, 'add', '', 21),
(193, 37, 'video', '', 31),
(194, 36, 'video', '', 31),
(195, 31, 'video', '', 31),
(196, 33, 'video', '', 30),
(197, 32, 'video', '', 30),
(224, 35, 'music', '', 15),
(225, 34, 'music', '', 15),
(226, 12, 'music', '', 15),
(227, 13, 'music', '', 15),
(240, 45, 'music', '', 14),
(241, 47, 'music', '', 14),
(242, 35, 'music', '', 14),
(243, 34, 'music', '', 14),
(244, 12, 'music', '', 14),
(245, 13, 'music', '', 14),
(262, 47, 'music', '', 20),
(263, 46, 'music', '', 20),
(264, 6, 'add', '', 20),
(265, 34, 'music', '', 20),
(266, 35, 'music', '', 20);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`user_id` int(100) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_fcm_id` text NOT NULL,
  `password` text NOT NULL,
  `user_status` enum('0','1') NOT NULL,
  `register_source` varchar(100) NOT NULL,
  `user_profile` varchar(500) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_email`, `user_fcm_id`, `password`, `user_status`, `register_source`, `user_profile`, `user_type`, `created_at`) VALUES
(9, 'sunil', 'mlmmaster711@gmail.com', '', '25f9e794323b453885f5181f1b624d0b', '1', 'email', '', 'user', '2017-06-22 19:43:17'),
(10, 'admin', 'admin@gmail.com', '', '25f9e794323b453885f5181f1b624d0b', '1', 'email', '', 'super', '2017-06-22 19:43:17'),
(11, 'sourav', 'nk3527@gmail.com', 'czPjyQl1YJA:APA91bEbKe8Hlqsh-JvP7HeXPCSsp7feEjABx2QTd-2XMdP9df7jmdDeV6-rUBEhHY3yIsBjhgKCEvkFtcoAWOHVewyW81XC7kT3z2axVgf6Pdlk5YH_ffHj4ciHw3g96JWTLW_4JyUI', '25f9e794323b453885f5181f1b624d0b', '1', 'email', '', 'admin', '2017-06-22 19:43:17'),
(12, 'kapil', 'kapillikess@gmail.com', 'ePcBlynwxsw:APA91bFa5yDTdB1LWi4VEWGZQill20a3HwGuN3Iicsf3BjbBCkeKNQ6eJBDgdZPca81AiBW5EXMwIDuGMO7VViiASlHPIWFRen7cITA27iJuCrya2gvx8uP6mYb_W5cEvoCbz1nk_Ynk', '25f9e794323b453885f5181f1b624d0b', '1', 'email', '', 'admin', '2017-06-22 19:43:17'),
(15, 'Sourav Kumar', 'sksh3527@gmail.com', '', '', '0', 'facebook', 'http://graph.facebook.com/1889935161228100/picture?type=square', 'user', '2017-06-22 19:43:17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `advert`
--
ALTER TABLE `advert`
 ADD PRIMARY KEY (`advert_id`);

--
-- Indexes for table `approvals`
--
ALTER TABLE `approvals`
 ADD PRIMARY KEY (`row_id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
 ADD PRIMARY KEY (`cart_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
 ADD PRIMARY KEY (`cat_id`), ADD KEY `cat_id` (`cat_id`);

--
-- Indexes for table `comment_data`
--
ALTER TABLE `comment_data`
 ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `counter`
--
ALTER TABLE `counter`
 ADD PRIMARY KEY (`counter_id`);

--
-- Indexes for table `discount_coupon`
--
ALTER TABLE `discount_coupon`
 ADD PRIMARY KEY (`coupon_id`);

--
-- Indexes for table `membership`
--
ALTER TABLE `membership`
 ADD PRIMARY KEY (`plan_id`);

--
-- Indexes for table `orderdetail`
--
ALTER TABLE `orderdetail`
 ADD PRIMARY KEY (`detail_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
 ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
 ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `purchased_record`
--
ALTER TABLE `purchased_record`
 ADD PRIMARY KEY (`row_id`);

--
-- Indexes for table `refixfm`
--
ALTER TABLE `refixfm`
 ADD PRIMARY KEY (`fm_id`);

--
-- Indexes for table `refixfm_ref`
--
ALTER TABLE `refixfm_ref`
 ADD PRIMARY KEY (`ref_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `advert`
--
ALTER TABLE `advert`
MODIFY `advert_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `approvals`
--
ALTER TABLE `approvals`
MODIFY `row_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=533;
--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
MODIFY `cart_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
MODIFY `cat_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=141;
--
-- AUTO_INCREMENT for table `comment_data`
--
ALTER TABLE `comment_data`
MODIFY `comment_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `counter`
--
ALTER TABLE `counter`
MODIFY `counter_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=107;
--
-- AUTO_INCREMENT for table `discount_coupon`
--
ALTER TABLE `discount_coupon`
MODIFY `coupon_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `membership`
--
ALTER TABLE `membership`
MODIFY `plan_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `orderdetail`
--
ALTER TABLE `orderdetail`
MODIFY `detail_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
MODIFY `order_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
MODIFY `product_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `purchased_record`
--
ALTER TABLE `purchased_record`
MODIFY `row_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `refixfm`
--
ALTER TABLE `refixfm`
MODIFY `fm_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `refixfm_ref`
--
ALTER TABLE `refixfm_ref`
MODIFY `ref_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=267;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `user_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

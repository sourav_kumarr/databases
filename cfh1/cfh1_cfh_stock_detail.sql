-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: cfh1
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cfh_stock_detail`
--

DROP TABLE IF EXISTS `cfh_stock_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cfh_stock_detail` (
  `stock_detail_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `stock_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `serial_number` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_id` bigint(20) NOT NULL,
  `quantity` bigint(20) NOT NULL DEFAULT '0',
  `unit` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `itemType` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `supplier_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(100) COLLATE utf8_unicode_ci DEFAULT '0',
  `created_at` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`stock_detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='store transaction information when Shop administrator import stock for Fabric';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cfh_stock_detail`
--

LOCK TABLES `cfh_stock_detail` WRITE;
/*!40000 ALTER TABLE `cfh_stock_detail` DISABLE KEYS */;
INSERT INTO `cfh_stock_detail` VALUES (3,'21','446',8,3,'Yard ','Fabric','fff','0','Aug 25, 2016 04:15'),(4,'21','446',8,6,'Yard ','Fabric','hhhhh','0','Aug 25, 2016 04:15'),(5,'22','556',8,4,'Yard ','Fabric','gggg','A','Aug 25, 2016 04:27'),(6,'22','556',8,77,'Yard ','Fabric','ggg','A','Aug 25, 2016 04:27'),(7,'23','44',8,44,'Yard ','Fabric','ggg','A','Aug 25, 2016 04:30'),(8,'24','555',8,4,'Yard ','Fabric','gg','A','Aug 25, 2016 04:31'),(9,'25','444',8,22,'Yard ','Fabric','fff','A','Aug 25, 2016 04:34'),(10,'25','444',8,66,'Yard ','Fabric','ggg','A','Aug 25, 2016 04:34'),(11,'26','55555',8,200,'Yard ','Fabric','hhhh','A','Aug 25, 2016 04:37'),(12,'26','55555',8,55,'Yard ','Fabric','ttttt','A','Aug 25, 2016 04:37'),(13,'26','55555',8,55,'Yard ','Fabric','uuuu','A','Aug 25, 2016 04:37'),(14,'27','447',8,4,'Yard ','Fabric','t','A','Aug 25, 2016 04:50');
/*!40000 ALTER TABLE `cfh_stock_detail` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:44

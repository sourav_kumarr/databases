-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: cfh1
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cfh_orders_detail`
--

DROP TABLE IF EXISTS `cfh_orders_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cfh_orders_detail` (
  `det_id` int(100) NOT NULL AUTO_INCREMENT,
  `det_product_id` varchar(100) NOT NULL,
  `det_order_id` varchar(100) NOT NULL,
  `det_price` varchar(100) NOT NULL,
  `det_quantity` varchar(100) NOT NULL,
  `det_status` varchar(100) NOT NULL,
  `det_user_id` varchar(100) NOT NULL,
  PRIMARY KEY (`det_id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cfh_orders_detail`
--

LOCK TABLES `cfh_orders_detail` WRITE;
/*!40000 ALTER TABLE `cfh_orders_detail` DISABLE KEYS */;
INSERT INTO `cfh_orders_detail` VALUES (1,'1','1','1.4','3','A','1261'),(2,'2','1','1.4','0','A','1261'),(3,'3','1','1.4','0','A','1261'),(4,'1','2','1.4','3','A','1261'),(5,'2','2','1.4','0','A','1261'),(6,'3','2','1.4','0','A','1261'),(7,'1','3','1.4','3','A','1261'),(8,'2','3','1.4','0','A','1261'),(9,'3','3','1.4','0','A','1261'),(10,'4','4','1.4','1','A','1261'),(11,'5','4','1.4','0','A','1261'),(12,'6','4','1.4','0','A','1261'),(13,'7','5','1.4','0','A','1261'),(14,'8','6','1.4','0','A','1261'),(15,'9','7','1.4','0','A','1261'),(16,'10','8','1.4','0','A','1261'),(17,'11','9','1.4','0','A','1261'),(18,'12','10','1.4','0','A','1261'),(19,'13','11','1.4','0','A','1261'),(20,'14','12','1.4','0','A','1261'),(21,'15','13','1.4','0','A','1261'),(25,'19','17','1.4','0','A','1141'),(26,'20','18','1.4','0','A','1141'),(27,'21','19','1.4','0','A','1141'),(28,'21','20','1.4','0','A','1141'),(29,'21','21','1.4','0','A','1141'),(30,'21','22','1.4','0','A','1141'),(31,'22','23','1.4','0','A','1141'),(32,'23','24','1.4','0','A','1141'),(33,'24','25','1.4','0','A','1141'),(34,'25','26','1.4','0','A','1141'),(45,'5','38','1.4','1','A','1271'),(46,'1','39','1.4','1','A','741'),(47,'5','40','1.4','1','A','1271'),(48,'5','41','1.4','1','A','1271'),(49,'5','42','1.4','1','A','1271'),(50,'118','43','1.4','0','A','1141'),(51,'118','44','1.4','1','A','1141'),(52,'118','45','1.4','1','A','1141'),(53,'119','45','1.4','0','A','1141'),(54,'1','46','1.4','2','A','891'),(55,'1','47','1.4','1','A','891'),(56,'122','48','1.4','0','A','1301'),(57,'123','48','1.4','0','A','1301'),(58,'122','49','1.4','0','A','1301'),(59,'123','49','1.4','0','A','1301'),(60,'1','50','1.4','5','A','901');
/*!40000 ALTER TABLE `cfh_orders_detail` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:45

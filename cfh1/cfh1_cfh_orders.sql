-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: cfh1
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cfh_orders`
--

DROP TABLE IF EXISTS `cfh_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cfh_orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_date` text NOT NULL,
  `order_state` varchar(100) NOT NULL,
  `order_payment_status` varchar(100) NOT NULL,
  `order_number` varchar(100) NOT NULL,
  `order_total` varchar(100) NOT NULL,
  `order_user_id` int(50) NOT NULL,
  `paid_amount` varchar(100) NOT NULL,
  `product_type` varchar(100) NOT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cfh_orders`
--

LOCK TABLES `cfh_orders` WRITE;
/*!40000 ALTER TABLE `cfh_orders` DISABLE KEYS */;
INSERT INTO `cfh_orders` VALUES (1,'2016-08-14','open','Pending','412189','7.2',1261,'0.00','Custom Suit'),(2,'2016-08-14','open','Pending','672981','7.2',1261,'0.00','Custom Suit'),(3,'2016-08-14','open','Pending','149161','7.2',1261,'0.00','Custom Suit'),(4,'2016-08-14','Cancelled','Pending','453705','7.2',1261,'0.00','Custom Suit'),(5,'2016-08-14','open','Pending','736292','4.4',1261,'0.00','Custom Suit'),(6,'2016-08-14','open','Pending','945995','4.4',1261,'0.00','Custom Suit'),(7,'2016-08-14','Cancelled','Pending','933963','4.4',1261,'0.00','Custom Suit'),(8,'2016-08-14','open','Pending','447798','4.4',1261,'0.00','Custom Suit'),(9,'2016-08-14','open','Pending','121791','4.4',1261,'0.00','Custom Suit'),(10,'2016-08-14','Cancelled','Pending','794600','4.4',1261,'0.00','Accessory'),(11,'2016-08-14','Cancelled','Pending','337321','4.4',1261,'0.00','Accessory'),(12,'2016-08-14','open','Pending','680218','4.4',1261,'0.00','Accessory'),(13,'2016-08-14','Cancelled','Pending','844664','4.4',1261,'0.00','Custom Suit'),(14,'2016-08-15','shipped','Pending','766436','4.4',1261,'0.00','Custom Suit'),(15,'2016-08-15','shipped','Pending','132389','4.4',1141,'0.00','Custom Suit'),(16,'2016-08-15','completed','Pending','793790','4.4',1141,'0.00','Custom Suit'),(17,'2016-08-15','open','Pending','934261','4.4',1141,'0.00','Accessory'),(18,'2016-08-15','open','Pending','903905','4.4',1141,'0.00','Custom Suit'),(19,'2016-08-15','open','Pending','665084','4.4',1141,'0.00','Accessory'),(20,'2016-08-15','open','Pending','571107','4.4',1141,'0.00','Custom Suit'),(21,'2016-08-15','open','Pending','348824','4.4',1141,'0.00','Accessory'),(22,'2016-08-15','open','Pending','314739','4.4',1141,'0.00','Accessory'),(23,'2016-08-15','open','Pending','366896','4.4',1141,'0.00','Custom Suit'),(24,'2016-08-15','open','Pending','658528','4.4',1141,'0.00','Accessory'),(25,'2016-08-15','open','Pending','681857','4.4',1141,'0.00','Custom Suit'),(26,'2016-08-15','open','Pending','779776','4.4',1141,'0.00','Accessory'),(27,'2016-08-15','completed','Pending','150043','4.4',1141,'0.00','Accessory'),(28,'2016-08-15','completed','Pending','616538','4.4',1141,'0.00','Custom Suit'),(29,'2016-08-15','shipped','Pending','724944','4.4',1141,'0.00','Accessory'),(30,'2016-08-15','shipped','Pending','957083','4.4',1141,'0.00','Custom Suit'),(31,'2016-08-15','shipped','Pending','192532','0',1141,'0.00',''),(32,'2016-08-15','shipped','Pending','932116','0',1141,'0.00',''),(33,'2016-08-15','shipped','Pending','618636','4.4',1141,'0.00',''),(34,'2016-08-15','shipped','Pending','346546','4.4',1141,'0.00',''),(35,'2016-08-15','shipped','Pending','659044','5.8',1141,'0.00',''),(37,'2016-08-15','completed','Pending','398767','4.4',1261,'0.00',''),(40,'2016-08-17','open','Pending','860788','88978745',1271,'0.00',''),(41,'2016-08-17','open','Pending','493857','88978745',1271,'0.00',''),(42,'2016-08-17','open','Pending','908403','88978745',1271,'0.00',''),(43,'Aug 17, 2016 13:36','open','Pending','323103','4.4',1141,'0.00','Custom Suit'),(44,'Aug 17, 2016 13:39','open','Pending','471977','4.4',1141,'0.00','Custom Suit'),(45,'Aug 17, 2016 13:52','open','Pending','327452','5.8',1141,'0.00','Custom Suit'),(46,'Aug 18, 2016 14:49','open','Pending','194642','88978745',891,'0.00','Custom Suit'),(47,'Aug 18, 2016 14:51','open','Pending','964131','88978745',891,'0.00','Custom Suit'),(48,'Aug 19, 2016 07:02','open','Pending','435676','5.8',1301,'0.00','Custom Suit'),(49,'Aug 19, 2016 08:52','open','Pending','328216','5.8',1301,'0.00','Custom Suit'),(50,'2016-08-21','open','Pending','687563','88978745',901,'0.00','');
/*!40000 ALTER TABLE `cfh_orders` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:42

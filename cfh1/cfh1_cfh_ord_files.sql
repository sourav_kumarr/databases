-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: cfh1
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cfh_ord_files`
--

DROP TABLE IF EXISTS `cfh_ord_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cfh_ord_files` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `filename` varchar(100) NOT NULL,
  `filepath` varchar(100) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `dated` varchar(100) NOT NULL,
  `status` enum('1','0') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cfh_ord_files`
--

LOCK TABLES `cfh_ord_files` WRITE;
/*!40000 ALTER TABLE `cfh_ord_files` DISABLE KEYS */;
INSERT INTO `cfh_ord_files` VALUES (12,'12idkk.ord','http://www.scan2fit.com/customizefh/admin/webserver/uploads/userdata/user891/ordfiles/12idkk.ord','891','02_21_05_PM_Aug_18_2016','1'),(13,'13id2016-08-12.ord','http://www.scan2fit.com/customizefh/admin/webserver/uploads/userdata/user741/ordfiles/13id2016-08-12','741','02_26_11_PM_Aug_18_2016','1'),(14,'14idJohn+Fijen+March+21+2016.ord','http://www.scan2fit.com/customizefh/admin/webserver/uploads/userdata/user93/ordfiles/14idJohn+Fijen+','93','03_13_25_PM_Aug_18_2016','1'),(15,'15idg.ord','http://www.scan2fit.com/customizefh/admin/webserver/uploads/userdata/user891/ordfiles/15idg.ord','891','06_33_41_AM_Aug_19_2016','1');
/*!40000 ALTER TABLE `cfh_ord_files` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:43

-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 12, 2017 at 02:26 PM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `stsmedtd_ritmu`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_login`
--

CREATE TABLE IF NOT EXISTS `admin_login` (
`admin_id` int(100) NOT NULL,
  `admin_name` varchar(100) NOT NULL,
  `admin_email` varchar(100) NOT NULL,
  `admin_password` varchar(500) NOT NULL,
  `admin_token` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_login`
--

INSERT INTO `admin_login` (`admin_id`, `admin_name`, `admin_email`, `admin_password`, `admin_token`) VALUES
(1, 'Admin', 'sbsunilbhatia9@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'edd88f948868a7de531a5ca81aab6cbf99d881cf8c7b7894be410dfadde5ec');

-- --------------------------------------------------------

--
-- Table structure for table `folders`
--

CREATE TABLE IF NOT EXISTS `folders` (
`folder_id` int(70) NOT NULL,
  `folder_name` varchar(200) NOT NULL,
  `folder_created_at` text NOT NULL,
  `folder_user_id` varchar(200) NOT NULL,
  `folder_parent_id` varchar(200) NOT NULL,
  `folder_path` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `folders`
--

INSERT INTO `folders` (`folder_id`, `folder_name`, `folder_created_at`, `folder_user_id`, `folder_parent_id`, `folder_path`) VALUES
(8, 'blablat', '1502517873', '1', '0', 'C:/xampp/htdocs/Ritmu/admin/api/Files/video/blablat'),
(18, 'newrrr', '1502517873', '1', '8', 'C:/xampp/htdocs/Ritmu/admin/api/Files/video/blablat/newrt'),
(19, 'sdsd', '1502364464', '1', '0', 'C:/xampp/htdocs/Ritmu/admin/api/Files/video/sdsd');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
`msg_id` int(200) NOT NULL,
  `message` text NOT NULL,
  `msg_start` varchar(200) NOT NULL,
  `msg_end` varchar(200) NOT NULL,
  `msg_updated_on` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`msg_id`, `message`, `msg_start`, `msg_end`, `msg_updated_on`) VALUES
(1, 'M65465', '1502525352', '1502528420', '1502525321');

-- --------------------------------------------------------

--
-- Table structure for table `schedule`
--

CREATE TABLE IF NOT EXISTS `schedule` (
`sch_id` int(100) NOT NULL,
  `sch_name` varchar(100) NOT NULL,
  `sch_start_time` varchar(100) NOT NULL,
  `sch_end_time` varchar(100) NOT NULL,
  `sch_status` varchar(100) NOT NULL,
  `sch_desc` text NOT NULL,
  `sch_priority` varchar(100) NOT NULL,
  `sch_date` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schedule`
--

INSERT INTO `schedule` (`sch_id`, `sch_name`, `sch_start_time`, `sch_end_time`, `sch_status`, `sch_desc`, `sch_priority`, `sch_date`) VALUES
(52, 'new test', '1501308012', '1501308462', '1', 'new test started', 'normal', '1501279200'),
(62, 'new test2', '1501308463', '1501308688', '1', 'sds', 'normal', '1501279200'),
(63, '31 july test', '1501495327', '1501495552', '1', 'sdsdsd', 'normal', '1501452000'),
(64, '30 july test', '1501408953', '1501409178', '1', 'sdsd', 'normal', '1501365600'),
(65, 'july high test', '1501408992', '1501409217', '1', 'sdsd', 'high', '1501365600'),
(67, 'timezone test', '1502272831', '1502273056', '1', 'sdsd', 'normal', '1502229600'),
(68, 'sdsd', '1502273636', '1502273861', '1', 'sds', 'normal', '1502229600'),
(69, 'test sch', '1502278242', '1502278467', '1', 'sdsd', 'normal', '1502217000'),
(70, 'rtrt', '1502280000', '1502280225', '1', 'sdsdsd', 'normal', '1502217000');

-- --------------------------------------------------------

--
-- Table structure for table `sch_detail`
--

CREATE TABLE IF NOT EXISTS `sch_detail` (
`detail_id` int(100) NOT NULL,
  `video_id` varchar(100) NOT NULL,
  `start_time` varchar(100) NOT NULL,
  `end_time` varchar(100) NOT NULL,
  `sch_id` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=167 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sch_detail`
--

INSERT INTO `sch_detail` (`detail_id`, `video_id`, `start_time`, `end_time`, `sch_id`) VALUES
(107, '140', '1500881611', '1500881836', '32'),
(108, '140', '1500881600', '1500881825', 'Guest711686'),
(109, '140', '1500881634', '1500881859', 'Guest76903'),
(114, '140', '1501143804', '1501144029', 'Guest291275'),
(118, '140', '1501144215', '1501144440', 'Guest979138'),
(127, '140', '1501248758', '1501248983', 'Guest296240'),
(133, '140', '1501237481', '1501237706', 'Guest140281'),
(134, '140', '1501237706', '1501237931', 'Guest140281'),
(138, '140', '1501308012', '1501308237', '52'),
(139, '140', '1501308237', '1501308462', '52'),
(141, '140', '1501308523', '1501308748', 'Guest502532'),
(142, '140', '1501308136', '1501308361', 'Guest373390'),
(144, '140', '1501309810', '1501310035', 'Guest560434'),
(145, '140', '1501309802', '1501310027', 'Guest808172'),
(146, '140', '1501307750', '1501307975', 'Guest762148'),
(154, '140', '1501412401', '1501412626', 'Guest662990'),
(155, '140', '1501308463', '1501308688', '62'),
(156, '140', '1501495327', '1501495552', '63'),
(157, '140', '1501408953', '1501409178', '64'),
(158, '140', '1501408992', '1501409217', '65'),
(159, '140', '1501409171', '1501409396', 'Guest21981'),
(161, '140', '1502272831', '1502273056', '67'),
(162, '140', '1502272.838', '1502497', 'Guest178831'),
(163, '140', '1502272804', '1502273029', 'Guest625262'),
(164, '140', '1502273636', '1502273861', '68'),
(165, '140', '1502278242', '1502278467', '69'),
(166, '140', '1502280000', '1502280225', '70');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`user_id` int(100) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_contact` varchar(100) NOT NULL,
  `active_plan` varchar(100) NOT NULL,
  `activation_date` varchar(100) NOT NULL,
  `plan_expiry_date` varchar(100) NOT NULL,
  `user_status` enum('0','1') NOT NULL,
  `user_token` text NOT NULL,
  `user_address` text NOT NULL,
  `register_source` varchar(100) NOT NULL,
  `renewal_type` varchar(100) NOT NULL,
  `auto_renewal` varchar(100) NOT NULL,
  `email_verified` enum('Yes','No') NOT NULL,
  `user_profile` varchar(500) NOT NULL,
  `subscribed` varchar(100) NOT NULL,
  `subscribed_for` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_email`, `user_contact`, `active_plan`, `activation_date`, `plan_expiry_date`, `user_status`, `user_token`, `user_address`, `register_source`, `renewal_type`, `auto_renewal`, `email_verified`, `user_profile`, `subscribed`, `subscribed_for`) VALUES
(19, 'Sunil Bhatia', 'sbsunilbhatia9@gmail.com', '8872292478', '6', '1498203568', '1529653168', '1', '05e328d0616037c76a7f329cadcaf50c071e2be3422792fd0bc1f4d62eb80f', 'majitha', 'Facebook', 'Yearly', 'Yes', 'No', 'images url', 'Yes', '1501055124'),
(20, 'SachTech Solution', 'sachtech4016@gmail.com', 'dasddas', '8', '1498483664', '1500989264', '1', '9295da85b9a05b0e67013aa26964412f7a763f415602edc2bae4095071b94e', 'dads', 'Google', 'Monthly', '0', 'No', 'Google', 'Yes', ''),
(21, 'SachTech Solution', 'sachtech40126@gmail.com', '7874545456', '5', '1498542488', '1501048088', '1', 'ffb85c8470fff07a291ea33dc9d1216b09f537d0c7258056ae4ee88c8bfa2c', 'sad', 'Google', 'Monthly', '1', 'No', 'Google', 'No', '');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE IF NOT EXISTS `videos` (
`video_id` int(11) NOT NULL,
  `video_name` text NOT NULL,
  `video_file` text NOT NULL,
  `added_on` varchar(100) NOT NULL,
  `video_status` varchar(100) NOT NULL,
  `video_duration` varchar(100) NOT NULL,
  `video_thumb` longtext NOT NULL,
  `video_parent_id` varchar(200) NOT NULL,
  `video_path` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=148 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`video_id`, `video_name`, `video_file`, `added_on`, `video_status`, `video_duration`, `video_thumb`, `video_parent_id`, `video_path`) VALUES
(130, '24536;PATAKE (Full Video) __ SUNANDA SHARMA __ Latest Punjabi Songs 2016 __ AMAR A.mp4', '24536;PATAKE (Full Video) __ SUNANDA SHARMA __ Latest Punjabi Songs 2016 __ AMAR A.mp4', '1500199038', '1', '207', '', '', ''),
(131, '988677;Makhna (à¨®à©±à¨–à¨£à¨¾)- Gurdas Maan Jatinder Shah R.Swami Punjab The Album - New Punjabi Song Saga Music.mp4', '988677;Makhna (à¨®à©±à¨–à¨£à¨¾)- Gurdas Maan Jatinder Shah R.Swami Punjab The Album - New Punjabi Song Saga Music.mp4', '1500199038', '1', '180', '', '', ''),
(132, '64514;Gurdas Maan - PUNJAB - Jatinder Shah - Gurickk G Maan - New Punjabi Songs 2017 - Saga Music.mp4', '64514;Gurdas Maan - PUNJAB - Jatinder Shah - Gurickk G Maan - New Punjabi Songs 2017 - Saga Music.mp4', '1500199038', '1', '482', '', '', ''),
(133, '627532;akshay.mp4', '627532;akshay.mp4', '1500211959', '1', '180', '', '', ''),
(134, '355957;dabang3.mp4', '355957;dabang3.mp4', '1500211959', '1', '103', '', '', ''),
(135, '926055;jio.mp4', '926055;jio.mp4', '1500211959', '1', '42', '', '', ''),
(136, '801208;krish.mp4', '801208;krish.mp4', '1500211959', '1', '134', '', '', ''),
(137, '239715;parmish.mp4', '239715;parmish.mp4', '1500211959', '1', '63', '', '', ''),
(138, '146362;robot.mp4', '146362;robot.mp4', '1500211959', '1', '123', '', '', ''),
(139, '892730;runout2.mp4', '892730;runout.mp4', '1500211959', '1', '123', '', '', ''),
(140, '800476;Jud - I Love NY (Falak Version) - MP4.mp4', '800476;Judaai - I Love NY (Falak Version) - MP4.mp4', '1500644453', '1', '225', '', '8', 'C:/xampp/htdocs/Ritmu/admin/api/Files/video/blablat/800476;Judaai - I Love NY (Falak Version) - MP4.mp4'),
(147, '712707;Judaai - I Love NY (Falak Version) - MP4.mp4', '712707;Judaai - I Love NY (Falak Version) - MP4.mp4', '1502365304', '1', '', '', '19', 'C:/xampp/htdocs/Ritmu/admin/api/Files/video/sdsd/712707;Judaai - I Love NY (Falak Version) - MP4.mp4');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_login`
--
ALTER TABLE `admin_login`
 ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `folders`
--
ALTER TABLE `folders`
 ADD PRIMARY KEY (`folder_id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
 ADD PRIMARY KEY (`msg_id`);

--
-- Indexes for table `schedule`
--
ALTER TABLE `schedule`
 ADD PRIMARY KEY (`sch_id`);

--
-- Indexes for table `sch_detail`
--
ALTER TABLE `sch_detail`
 ADD PRIMARY KEY (`detail_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
 ADD PRIMARY KEY (`video_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_login`
--
ALTER TABLE `admin_login`
MODIFY `admin_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `folders`
--
ALTER TABLE `folders`
MODIFY `folder_id` int(70) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
MODIFY `msg_id` int(200) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `schedule`
--
ALTER TABLE `schedule`
MODIFY `sch_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `sch_detail`
--
ALTER TABLE `sch_detail`
MODIFY `detail_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=167;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `user_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
MODIFY `video_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=148;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

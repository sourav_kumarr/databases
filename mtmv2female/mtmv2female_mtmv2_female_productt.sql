-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: mtmv2female
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `mtmv2_female_productt`
--

DROP TABLE IF EXISTS `mtmv2_female_productt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mtmv2_female_productt` (
  `prod_id` int(100) NOT NULL AUTO_INCREMENT,
  `prod_name` varchar(100) NOT NULL,
  `prod_code` varchar(100) NOT NULL,
  `prod_style_number` varchar(100) NOT NULL,
  `prod_febric_color` varchar(100) NOT NULL,
  `prod_neck_style` varchar(100) NOT NULL,
  `prod_sleeve_style` varchar(100) NOT NULL,
  `prod_trim_area` varchar(100) NOT NULL,
  `prod_trim_type` varchar(100) NOT NULL,
  `prod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `prod_status` varchar(100) NOT NULL,
  `prod_price` varchar(100) NOT NULL,
  `prod_average` float NOT NULL,
  `prod_image` varchar(100) NOT NULL,
  PRIMARY KEY (`prod_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mtmv2_female_productt`
--

LOCK TABLES `mtmv2_female_productt` WRITE;
/*!40000 ALTER TABLE `mtmv2_female_productt` DISABLE KEYS */;
INSERT INTO `mtmv2_female_productt` VALUES (1,'Sample Product for Women 1','100','style 1','style 2','style 3','style 4','style 5','style 6','2016-07-30 08:41:23','A','100',0,'http://scan2fit.com/justforwomen/admin/webserver/uploads/sample_1.png'),(2,'Sample Product for Women 2','101','style1','style2','style3','style4','style5','style6','2016-07-30 08:34:58','A','130',7,'http://scan2fit.com/justforwomen/admin/webserver/uploads/sample_2.png'),(3,'Sample Product for Women 3','102','style1','style2','style3','style4','style5','style6','2016-07-30 08:35:16','A','110',4,'http://scan2fit.com/justforwomen/admin/webserver/uploads/sample_3.png'),(4,'Sample Product for Women 4','101','style1','style2','style3','style4','style5','style6','2016-07-30 08:35:31','A','95',0,'http://scan2fit.com/justforwomen/admin/webserver/uploads/sample_4.png'),(5,'sample product 5','105','25','54','25','25','254','25','2016-07-30 09:32:06','A','52',4,'http://scan2fit.com/justforwomen/admin/webserver/uploads/sample_5.png'),(6,'sample product6','105','25','54','25','25','254','25','2016-07-30 09:31:56','A','52',4,'http://scan2fit.com/justforwomen/admin/webserver/uploads/sample_6.png'),(7,'sample product7','162','2','5','2','65','2','62','2016-07-30 09:30:23','A','35',5,'http://scan2fit.com/justforwomen/admin/webserver/uploads/sample_7.png'),(8,'sample product8','162','2','5','2','65','2','62','2016-07-29 18:30:00','A','35',5,'http://scan2fit.com/justforwomen/admin/webserver/uploads/sample_8.png');
/*!40000 ALTER TABLE `mtmv2_female_productt` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:42

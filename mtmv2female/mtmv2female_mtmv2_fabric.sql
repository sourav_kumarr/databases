-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: mtmv2female
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `mtmv2_fabric`
--

DROP TABLE IF EXISTS `mtmv2_fabric`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mtmv2_fabric` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `fabric_name` varchar(100) NOT NULL,
  `mrp` varchar(100) NOT NULL,
  `discount` varchar(100) NOT NULL,
  `selling_price` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  `fabric_image` varchar(100) NOT NULL,
  `date` varchar(100) NOT NULL,
  `quantity` varchar(100) NOT NULL,
  `usedfabric` varchar(100) NOT NULL,
  `restfabric` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mtmv2_fabric`
--

LOCK TABLES `mtmv2_fabric` WRITE;
/*!40000 ALTER TABLE `mtmv2_fabric` DISABLE KEYS */;
INSERT INTO `mtmv2_fabric` VALUES (1,'Acrylic fiber','1.79','0.179','1.611','Acrylic fibers are synthetic fibers made from a polymer (polyacrylonitrile) with an average molecula','set1.jpg','27/May/2016 12:59:11 PM','9000','4200','4800','1');
/*!40000 ALTER TABLE `mtmv2_fabric` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:41

-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: mtmv2female
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `mtmv2_female_user`
--

DROP TABLE IF EXISTS `mtmv2_female_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mtmv2_female_user` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `billing_address` varchar(100) NOT NULL,
  `pincode` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` text NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mtmv2_female_user`
--

LOCK TABLES `mtmv2_female_user` WRITE;
/*!40000 ALTER TABLE `mtmv2_female_user` DISABLE KEYS */;
INSERT INTO `mtmv2_female_user` VALUES (1,'nk@g.com','hello','lovepreet','singh','jgjg','675767','mohali','punjab','india','9872991155','2016-06-23 08:26:02',''),(13,'hello@gmail.com','hellohello','hello','hello','','','','','','45749852','2016-06-06 21:07:22','D'),(14,'dh@gmail.com','qwerty','dd','dd','rftgtf\nesdcsef','434555','dfgvd','fgd','','9592394097','2016-06-13 12:22:47','D'),(15,'harry@gmail.com','123','harry','kumar','','','','','','9779784701','2016-07-28 07:50:08','D'),(16,'harwinderkumar820@gmail.com','123','harry','kumar','','','','','','9779784701','2016-07-28 10:54:39','D'),(17,'harwinderkumar0001@yahoo.in','123','harry','kumar','7 phase','160055','mohali','punjab','india','9779784701','2016-07-28 10:54:51','D'),(18,'kapillikes@gmail.com','ddstha','kapil','kapil','','','','','','7087425488','2016-06-08 19:59:26','D'),(19,'naro@gmaill.com','123456789','Nardeep','Nardeep','','','','','','4565789','2016-07-28 12:09:53','D'),(20,'naro5@gmail.com','123456789','Nardeep','Nardeep','','','','','','4565789','2016-06-09 05:42:20','D'),(21,'na@gmail.com','124567','naro','naro','','','','','','87778000','2016-06-09 10:53:28','D'),(22,'john.fijen@gmail.com','12345678','John ','Fijen','SachTech Solution \nPhase 7 industrial area','160055','mohali','punjab','india','9197066336','2016-06-23 10:10:34','D'),(23,'aman1@gmail.com','amandeep','aman','aman','refvre\nfdg','343546','fgdfg','fvfbf','','4763256387297','2016-06-14 04:22:57','D'),(24,'hello1@gmail.com','hellohello','gdfcd','gdfcd','','','','','','67344','2016-06-15 05:34:58','D'),(25,'avtar1@gmail.com','avtaravtar','avtar','avtar','','','','','','86875875','2016-06-15 06:17:08','D'),(26,'avtar@gmail.com','avtaravtar','avtar','avtar','fdrfgr\nerfeeee','4645','fsdrfr','sfrde','','8675565','2016-06-15 08:23:56','D'),(27,'amandeep@gmail.com','amandeep','amandeep','amandeep','','','','','','3243543','2016-06-15 12:29:33','D'),(28,'amann@gmail.com','amanaman','amannnnn','amannnnn','','','','','','783465736583','2016-06-15 12:33:18','D'),(29,'fr@gmail.com','djfksdf','gdgtftf','dfgrd','','','','','','3498798375','2016-06-15 13:36:02','D'),(30,'ak@gmail.com','amandeep','amandeep','kaur','hdrg\nxgxhx','28282','xyd','ydyf','','5727876','2016-06-16 05:55:59','D'),(31,'amank@gmail.com','amandeep','amandeep','kaur','dsfsf\nfgdf','23531','mohali','punjab','India','62478683768','2016-06-23 08:46:43','D'),(32,'kapillikes1@gmail.com','12345678','Kapil','Dhawan','Vpo sui\ndistt Bhiwani','127030','Bhiwani','haryana','India','7087425488','2016-06-23 11:57:23','D'),(34,'java.programming4016@gmail.com','12345678','Mohit','Asija','New Southern \nUnited states','452399','Sothwell','Uk','UK','7087425388','2016-06-23 13:49:42','D'),(37,'kapillikes112@gmail.com','123456','kapil','dhawan','','','','','','1122112211','2016-06-23 19:15:44','D');
/*!40000 ALTER TABLE `mtmv2_female_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:41

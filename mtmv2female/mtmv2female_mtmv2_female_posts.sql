-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: mtmv2female
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `mtmv2_female_posts`
--

DROP TABLE IF EXISTS `mtmv2_female_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mtmv2_female_posts` (
  `post_id` int(100) NOT NULL AUTO_INCREMENT,
  `post_text` text NOT NULL,
  `post_description` text NOT NULL,
  `post_image` varchar(100) NOT NULL,
  `post_type` varchar(100) NOT NULL,
  `post_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `post_status` varchar(100) NOT NULL,
  `post_userid` bigint(100) NOT NULL,
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mtmv2_female_posts`
--

LOCK TABLES `mtmv2_female_posts` WRITE;
/*!40000 ALTER TABLE `mtmv2_female_posts` DISABLE KEYS */;
INSERT INTO `mtmv2_female_posts` VALUES (1,'hello,first post','','','testmonials','2016-06-09 12:10:40','A',1),(2,'hello,first post','','','testmonials','2016-06-09 12:37:38','A',1),(6,'sdsds','<p></p>hello<p></p>','','blogs','2016-06-23 11:18:48','D',5),(9,'blog test','<p></p><p></p><p></p><p></p><p></p><p></p><p></p><h1>best way to do i<br></h1><p></p><p></p><p></p><p></p><p></p><p></p><p></p>','','blogs','2016-06-23 13:16:49','D',5);
/*!40000 ALTER TABLE `mtmv2_female_posts` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  9:48:42

-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 12, 2017 at 02:25 PM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `stsmedtd_boltikitaab`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_login`
--

CREATE TABLE IF NOT EXISTS `admin_login` (
`admin_id` int(100) NOT NULL,
  `admin_name` varchar(100) NOT NULL,
  `admin_email` varchar(100) NOT NULL,
  `admin_password` varchar(500) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_login`
--

INSERT INTO `admin_login` (`admin_id`, `admin_name`, `admin_email`, `admin_password`) VALUES
(1, 'Admin', 'admin@gmail.com', 'e10adc3949ba59abbe56e057f20f883e');

-- --------------------------------------------------------

--
-- Table structure for table `app_rating`
--

CREATE TABLE IF NOT EXISTS `app_rating` (
`rating_id` int(11) NOT NULL,
  `rat_msg` text NOT NULL,
  `rat_rating` varchar(200) NOT NULL,
  `rat_userid` varchar(200) NOT NULL,
  `rate_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_rating`
--

INSERT INTO `app_rating` (`rating_id`, `rat_msg`, `rat_rating`, `rat_userid`, `rate_timestamp`) VALUES
(1, 'nice app', '4', '19', '2017-06-30 14:16:57');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE IF NOT EXISTS `books` (
`book_id` int(100) NOT NULL,
  `cat_id` int(100) NOT NULL,
  `book_name` varchar(100) NOT NULL,
  `book_desc` text NOT NULL,
  `book_author` varchar(100) NOT NULL,
  `book_narrator` varchar(100) NOT NULL,
  `play_time` varchar(100) NOT NULL,
  `list_price` float NOT NULL,
  `discounted_price` varchar(100) NOT NULL,
  `discount_id` varchar(100) NOT NULL,
  `book_status` enum('0','1') NOT NULL,
  `audio_file` varchar(100) NOT NULL,
  `short_audio_file` varchar(1000) NOT NULL,
  `front_look` varchar(100) NOT NULL,
  `average_rate` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`book_id`, `cat_id`, `book_name`, `book_desc`, `book_author`, `book_narrator`, `play_time`, `list_price`, `discounted_price`, `discount_id`, `book_status`, `audio_file`, `short_audio_file`, `front_look`, `average_rate`) VALUES
(6, 14, 'New Test', 'Helloooooooo', 'Harry', 'RaviKant', '00:04:04', 0, '', '5', '1', 'File529541.mp3', 'File971710.mp3', 'File402526.jpg', '4.5'),
(7, 15, 'New Test', 'Helloooooooo', 'Harry', 'ar', '00:04:04', 120, '', '5', '1', 'File529541.mp3', 'File971710.mp3', 'File402526.jpg', '4.25'),
(8, 14, 'New Test', 'Helloooooooo', 'Harry', 'ay', '00:04:04', 0, '90', '3', '1', 'File529541.mp3', 'File971710.mp3', 'File402526.jpg', '5');

-- --------------------------------------------------------

--
-- Table structure for table `book_list`
--

CREATE TABLE IF NOT EXISTS `book_list` (
`list_id` int(100) NOT NULL,
  `list_name` varchar(255) NOT NULL,
  `list_userid` varchar(255) NOT NULL,
  `list_created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_list`
--

INSERT INTO `book_list` (`list_id`, `list_name`, `list_userid`, `list_created_at`) VALUES
(1, 'first test', '23', '2017-07-06 12:20:30'),
(2, 'second test', '23', '2017-07-06 12:20:30'),
(17, '', '23', '2017-07-06 17:25:20'),
(18, '', '23', '2017-07-06 17:25:46'),
(19, '', '23', '2017-07-06 17:26:51'),
(20, '', '23', '2017-07-06 17:27:18'),
(21, '', '23', '2017-07-06 17:27:38'),
(22, '', '23', '2017-07-06 17:27:55'),
(23, '', '23', '2017-07-06 17:28:43'),
(24, '', '23', '2017-07-06 17:29:52'),
(25, '', '23', '2017-07-06 17:31:18'),
(26, '', '23', '2017-07-06 17:31:29'),
(27, '', '23', '2017-07-06 17:32:26'),
(28, 'title1', '23', '2017-07-06 17:32:53'),
(29, 'title1', '23', '2017-07-06 17:33:53'),
(30, 'title1', '23', '2017-07-06 17:34:17'),
(31, 'title1', '23', '2017-07-06 17:58:56'),
(32, 'title1', '23', '2017-07-06 17:59:26');

-- --------------------------------------------------------

--
-- Table structure for table `book_list_detail`
--

CREATE TABLE IF NOT EXISTS `book_list_detail` (
`detail_id` int(100) NOT NULL,
  `det_list_id` int(100) NOT NULL,
  `det_book_id` int(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_list_detail`
--

INSERT INTO `book_list_detail` (`detail_id`, `det_list_id`, `det_book_id`) VALUES
(1, 1, 6),
(2, 1, 7),
(3, 2, 6),
(4, 2, 7),
(5, 28, 1),
(6, 28, 2),
(7, 28, 3),
(8, 29, 1),
(9, 29, 2),
(10, 29, 3),
(11, 30, 1),
(12, 30, 2),
(13, 30, 3),
(14, 31, 1),
(15, 31, 2),
(16, 31, 3),
(17, 32, 1),
(18, 32, 2),
(19, 32, 3);

-- --------------------------------------------------------

--
-- Table structure for table `book_rating`
--

CREATE TABLE IF NOT EXISTS `book_rating` (
`rating_id` int(100) NOT NULL,
  `book_id` varchar(100) NOT NULL,
  `user_rate` varchar(100) NOT NULL,
  `user_review` text NOT NULL,
  `user_id` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_rating`
--

INSERT INTO `book_rating` (`rating_id`, `book_id`, `user_rate`, `user_review`, `user_id`) VALUES
(1, '7', '3.5', 'Awesome Book I love to Download This', '9'),
(2, '6', '4.0', 'Awesome Book I love to Download This', '9'),
(3, '6', '1', 'dsafghjhlk', '23'),
(4, '7', '5.0', 'dsafghjhlk', '23');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
`cat_id` int(100) NOT NULL,
  `cat_name` varchar(100) NOT NULL,
  `cat_image` varchar(100) NOT NULL,
  `added_on` varchar(100) NOT NULL,
  `cat_status` enum('0','1') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`cat_id`, `cat_name`, `cat_image`, `added_on`, `cat_status`) VALUES
(14, 'Kapil', 'File10009.png', '21 Mar 2017 12:54:22 PM', '1'),
(15, 'Kapil', 'File658935.jpg', '23 Mar 2017 04:00:05 AM', '1');

-- --------------------------------------------------------

--
-- Table structure for table `discount_coupon`
--

CREATE TABLE IF NOT EXISTS `discount_coupon` (
`coupon_id` int(100) NOT NULL,
  `coupon_code` varchar(100) NOT NULL,
  `coupon_value` text NOT NULL,
  `generated_on` varchar(100) NOT NULL,
  `expired_on` varchar(100) NOT NULL,
  `coupon_status` enum('0','1') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `discount_coupon`
--

INSERT INTO `discount_coupon` (`coupon_id`, `coupon_code`, `coupon_value`, `generated_on`, `expired_on`, `coupon_status`) VALUES
(1, 'ABCD1020', '100.0', '1490137200', '1493330400', '1'),
(3, 'BKB33', '33.10', '1490137200', '1492552800', '1'),
(5, 'Code50', '50.00', '1491861600', '1491948000', '0');

-- --------------------------------------------------------

--
-- Table structure for table `membership`
--

CREATE TABLE IF NOT EXISTS `membership` (
`plan_id` int(100) NOT NULL,
  `plan_name` varchar(100) NOT NULL,
  `plan_price` varchar(100) NOT NULL,
  `renewal_type` varchar(100) NOT NULL,
  `plan_status` enum('0','1') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `membership`
--

INSERT INTO `membership` (`plan_id`, `plan_name`, `plan_price`, `renewal_type`, `plan_status`) VALUES
(5, 'Basic', '0', 'all', '1'),
(6, 'Premium', '120', 'all', '1'),
(7, 'newtest', '57', 'all', '1'),
(8, 'sda', '120.88', 'all', '1');

-- --------------------------------------------------------

--
-- Table structure for table `orderdetail`
--

CREATE TABLE IF NOT EXISTS `orderdetail` (
`detail_id` int(100) NOT NULL,
  `order_id` int(100) NOT NULL,
  `item_id` int(100) NOT NULL,
  `item_price` int(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orderdetail`
--

INSERT INTO `orderdetail` (`detail_id`, `order_id`, `item_id`, `item_price`) VALUES
(1, 1, 1, 120),
(2, 2, 4, 120),
(3, 2, 2, 95),
(4, 4, 6, 100),
(5, 4, 7, 23),
(6, 5, 7, 120),
(7, 6, 7, 120),
(8, 7, 7, 120),
(9, 8, 7, 120);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
`order_id` int(100) NOT NULL,
  `order_number` varchar(100) NOT NULL,
  `transaction_id` varchar(100) NOT NULL,
  `amount` varchar(100) NOT NULL,
  `payment_status` varchar(100) NOT NULL,
  `order_dated` varchar(100) NOT NULL,
  `order_userId` varchar(100) NOT NULL,
  `order_status` varchar(100) NOT NULL,
  `refund_generated` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `order_number`, `transaction_id`, `amount`, `payment_status`, `order_dated`, `order_userId`, `order_status`, `refund_generated`) VALUES
(1, '5641446', '4sa5d465sa1d51sa56d456sa156d4sa5615dsad56sa156', '215', 'Success', '1488733403', '9', 'Completed', 'No'),
(2, '546545643', '4sa5d465sa1d51sa56d456sa156d4sa5615dsad56sa156', '215', 'Success', '1491152603', '9', 'Completed', 'No'),
(3, '21656481', '4sa5d465sa1d51sa56d456sa156d4sa5615dsad56sa156', '215', 'Success', '1491152603', '9', 'Completed', 'Yes'),
(4, 'BKB290620171242577205', '', '123', 'Pending', '1498732977', '23', 'Pending', 'No'),
(5, 'BKB290620170338438767', '', '120', 'Pending', '1498743523', '20', 'Pending', 'No'),
(6, 'BKB290620170344402669', '', '120', 'Pending', '1498743880', '20', 'Pending', 'No'),
(7, 'BKB290620170349594383', '', '120', 'Pending', '1498744199', '20', 'Pending', 'No'),
(8, 'BKB300620170708015239', '', '120', 'Pending', '1498799281', '20', 'Pending', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_book`
--

CREATE TABLE IF NOT EXISTS `purchase_book` (
`id` int(11) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `book_id` varchar(100) NOT NULL,
  `amount_paid` varchar(100) NOT NULL,
  `payment_status` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_book`
--

INSERT INTO `purchase_book` (`id`, `user_id`, `book_id`, `amount_paid`, `payment_status`) VALUES
(5, '23', '6', '120', 'Success'),
(6, '23', '7', '95', 'Success'),
(7, '23', '8', '120', 'Success');

-- --------------------------------------------------------

--
-- Table structure for table `recentviewed`
--

CREATE TABLE IF NOT EXISTS `recentviewed` (
`_id` int(100) NOT NULL,
  `book_id` varchar(100) NOT NULL,
  `last_seen` varchar(100) NOT NULL,
  `user_id` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recentviewed`
--

INSERT INTO `recentviewed` (`_id`, `book_id`, `last_seen`, `user_id`) VALUES
(1, '6', '1498206757', '19'),
(2, '1', '1498222497', '2'),
(3, '7', '1498482814', '874FA7FD-879F-420C-97ED-7CDECD708C54'),
(4, '8', '1498482254', '874FA7FD-879F-420C-97ED-7CDECD708C54'),
(5, '6', '1498483324', '874FA7FD-879F-420C-97ED-7CDECD708C54'),
(6, '7', '1498632125', '21'),
(7, '8', '1498626361', '21'),
(8, '7', '1498651520', '25'),
(9, '6', '1498731903', '25'),
(10, '7', '1498817957', '20'),
(11, '8', '1498818828', '20'),
(12, '6', '1498818777', '20');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`user_id` int(100) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_contact` varchar(100) NOT NULL,
  `active_plan` varchar(100) NOT NULL,
  `activation_date` varchar(100) NOT NULL,
  `plan_expiry_date` varchar(100) NOT NULL,
  `user_status` enum('0','1') NOT NULL,
  `user_token` text NOT NULL,
  `user_address` text NOT NULL,
  `register_source` varchar(100) NOT NULL,
  `renewal_type` varchar(100) NOT NULL,
  `auto_renewal` varchar(100) NOT NULL,
  `email_verified` enum('Yes','No') NOT NULL,
  `user_profile` varchar(500) NOT NULL,
  `subscribed` varchar(100) NOT NULL,
  `subscribed_for` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_email`, `user_contact`, `active_plan`, `activation_date`, `plan_expiry_date`, `user_status`, `user_token`, `user_address`, `register_source`, `renewal_type`, `auto_renewal`, `email_verified`, `user_profile`, `subscribed`, `subscribed_for`) VALUES
(9, 'mlmmaster711', 'mlmmaster711@gmail.com', '8872292478', '5', '1498732202', '1501237802', '1', '2f5c8817f5ce3df47d916a175170fa3d7048891173a4f4702870e6154b582c', 'Sunil Ambersaria', 'Twitter', 'Monthly', '0', 'No', 'Twitter', 'No', ''),
(20, 'SachTech Solution', 'sachtech4016@gmail.com', 'dasddas', '5', '1498742523', '1501248123', '1', '9295da85b9a05b0e67013aa26964412f7a763f415602edc2bae4095071b94e', 'dads', 'Google', 'Monthly', '0', 'No', 'Google', 'Yes', ''),
(21, 'SachTech Solution', 'sachtech40126@gmail.com', '7874545456', '6', '1498542488', '1501048088', '1', '7c1e76a8ed9409f5a96734fd8a33f35c4dd4d1de9ef6bc4f39555e5beacd54', 'sad', 'Google', 'Monthly', '0', 'No', 'File783508.jpeg', 'Yes', '1499145438'),
(23, 'Sunil Bhatiass', 'sbsunilbhatia9@gmail.com', '8872292478', '6', '', '', '1', '04401b268e2bab2b17b520c47bd946ce6723eae49670a7d2d48b6a75fe11ce', 'mohali asr', 'facebook', '', '0', 'No', 'File403106.png', 'No', ''),
(24, 'asdfcvbn', 'sbsaunilbhatia9@gmail.com', '222222222222', '5', '', '', '1', '7df99a639da6dce717a1c616a8a4249c0a7cb1550ac54bed545a9c20c4f941', 'sdfsd', 'facebook', '', '', 'No', '', 'No', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_login`
--
ALTER TABLE `admin_login`
 ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `app_rating`
--
ALTER TABLE `app_rating`
 ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
 ADD PRIMARY KEY (`book_id`);

--
-- Indexes for table `book_list`
--
ALTER TABLE `book_list`
 ADD PRIMARY KEY (`list_id`);

--
-- Indexes for table `book_list_detail`
--
ALTER TABLE `book_list_detail`
 ADD PRIMARY KEY (`detail_id`);

--
-- Indexes for table `book_rating`
--
ALTER TABLE `book_rating`
 ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
 ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `discount_coupon`
--
ALTER TABLE `discount_coupon`
 ADD PRIMARY KEY (`coupon_id`);

--
-- Indexes for table `membership`
--
ALTER TABLE `membership`
 ADD PRIMARY KEY (`plan_id`);

--
-- Indexes for table `orderdetail`
--
ALTER TABLE `orderdetail`
 ADD PRIMARY KEY (`detail_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
 ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `purchase_book`
--
ALTER TABLE `purchase_book`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recentviewed`
--
ALTER TABLE `recentviewed`
 ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_login`
--
ALTER TABLE `admin_login`
MODIFY `admin_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `app_rating`
--
ALTER TABLE `app_rating`
MODIFY `rating_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
MODIFY `book_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `book_list`
--
ALTER TABLE `book_list`
MODIFY `list_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `book_list_detail`
--
ALTER TABLE `book_list_detail`
MODIFY `detail_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `book_rating`
--
ALTER TABLE `book_rating`
MODIFY `rating_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
MODIFY `cat_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `discount_coupon`
--
ALTER TABLE `discount_coupon`
MODIFY `coupon_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `membership`
--
ALTER TABLE `membership`
MODIFY `plan_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `orderdetail`
--
ALTER TABLE `orderdetail`
MODIFY `detail_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
MODIFY `order_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `purchase_book`
--
ALTER TABLE `purchase_book`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `recentviewed`
--
ALTER TABLE `recentviewed`
MODIFY `_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `user_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

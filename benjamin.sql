-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 12, 2017 at 02:33 PM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `benjamin`
--

-- --------------------------------------------------------

--
-- Table structure for table `benj_cart`
--

CREATE TABLE IF NOT EXISTS `benj_cart` (
`cart_id` int(100) NOT NULL,
  `product_type` varchar(100) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `product_price` varchar(100) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `quantity` varchar(100) NOT NULL,
  `total_amount` varchar(100) NOT NULL,
  `product_image` varchar(100) NOT NULL,
  `tr_type` varchar(100) NOT NULL,
  `tr_fabric` varchar(100) NOT NULL,
  `tr_fit` varchar(100) NOT NULL,
  `tr_sidepocket` varchar(100) NOT NULL,
  `tr_backpocket` varchar(100) NOT NULL,
  `tr_seam` varchar(100) NOT NULL,
  `tr_coinpocket` varchar(100) NOT NULL,
  `tr_button_color` varchar(100) NOT NULL,
  `tr_thread_color` varchar(100) NOT NULL,
  `sh_fit` varchar(100) NOT NULL,
  `sh_fabric` varchar(100) NOT NULL,
  `sh_sleeve` varchar(100) NOT NULL,
  `sh_collar` varchar(100) NOT NULL,
  `sh_cuff` varchar(100) NOT NULL,
  `sh_placket` varchar(100) NOT NULL,
  `sh_bottomcut` varchar(100) NOT NULL,
  `sh_pocket` varchar(100) NOT NULL,
  `sh_button_color` varchar(100) NOT NULL,
  `sh_thread_color` varchar(100) NOT NULL,
  `su_fabric` varchar(100) NOT NULL,
  `su_fit` varchar(100) NOT NULL,
  `su_button` varchar(100) NOT NULL,
  `su_lapel` varchar(100) NOT NULL,
  `su_sleeve_button` varchar(100) NOT NULL,
  `su_lapel_stich` varchar(100) NOT NULL,
  `su_pocket` varchar(100) NOT NULL,
  `su_ticket_pocket` varchar(100) NOT NULL,
  `su_vent` varchar(100) NOT NULL,
  `su_elbow_patch` varchar(100) NOT NULL,
  `su_elbow_fabric` varchar(100) NOT NULL,
  `su_suit_linning` varchar(100) NOT NULL,
  `su_button_color` varchar(100) NOT NULL,
  `su_thread_color` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `benj_cart`
--

INSERT INTO `benj_cart` (`cart_id`, `product_type`, `product_name`, `product_price`, `user_id`, `quantity`, `total_amount`, `product_image`, `tr_type`, `tr_fabric`, `tr_fit`, `tr_sidepocket`, `tr_backpocket`, `tr_seam`, `tr_coinpocket`, `tr_button_color`, `tr_thread_color`, `sh_fit`, `sh_fabric`, `sh_sleeve`, `sh_collar`, `sh_cuff`, `sh_placket`, `sh_bottomcut`, `sh_pocket`, `sh_button_color`, `sh_thread_color`, `su_fabric`, `su_fit`, `su_button`, `su_lapel`, `su_sleeve_button`, `su_lapel_stich`, `su_pocket`, `su_ticket_pocket`, `su_vent`, `su_elbow_patch`, `su_elbow_fabric`, `su_suit_linning`, `su_button_color`, `su_thread_color`) VALUES
(3, 'men_suit', 'Men Suit5388', '550', '75', '1', '550', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'fabric_0', 'regular_fit', 'one_button', 'standard_lapel', 'four_button', 'yes', 'welt_pocket', 'no', 'one_vent', 'no', '', '000000', 'f1f4e5', 'ffffff'),
(4, 'men_trouser', 'Trouser7834', '450', '75', '1', '450', '', 'regular', 'fa_0', 'tapered', 'rounded', 'both_single_welt_with_hook', 'hidden_seam', 'no', 'f1f4e5', 'c0c0bb', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `benj_orders`
--

CREATE TABLE IF NOT EXISTS `benj_orders` (
`order_id` int(11) NOT NULL,
  `order_date` text NOT NULL,
  `order_state` varchar(100) NOT NULL,
  `order_payment_status` varchar(100) NOT NULL,
  `order_number` varchar(100) NOT NULL,
  `order_total` varchar(100) NOT NULL,
  `order_user_id` int(50) NOT NULL,
  `paid_amount` varchar(100) NOT NULL,
  `product_type` varchar(100) NOT NULL,
  `order_txn_id` text NOT NULL,
  `order_currency_code` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `benj_orders`
--

INSERT INTO `benj_orders` (`order_id`, `order_date`, `order_state`, `order_payment_status`, `order_number`, `order_total`, `order_user_id`, `paid_amount`, `product_type`, `order_txn_id`, `order_currency_code`) VALUES
(58, 'May 16, 2017 12:26', 'open', 'Pending', '873945', '450', 71, '0.00', '', '', ''),
(59, 'May 16, 2017 12:40', 'open', 'Pending', '143276', '450', 71, '0.00', '', '', ''),
(60, 'May 17, 2017 08:11', 'open', 'Pending', '473577', '250', 71, '0.00', '', '', ''),
(61, 'May 27, 2017 08:31', 'open', 'Pending', '291896', '500', 71, '0.00', '', '', ''),
(62, 'Jun 27, 2017 07:25', 'open', 'Pending', '677876', '250', 75, '0.00', '', '', ''),
(63, 'Jun 27, 2017 14:10', 'open', 'Pending', '206763', '1350', 75, '0.00', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `benj_orders_detail`
--

CREATE TABLE IF NOT EXISTS `benj_orders_detail` (
`det_id` int(100) NOT NULL,
  `det_order_id` varchar(100) NOT NULL,
  `det_price` varchar(100) NOT NULL,
  `det_quantity` varchar(100) NOT NULL,
  `det_user_id` varchar(100) NOT NULL,
  `det_product_type` varchar(100) NOT NULL,
  `tr_type` varchar(100) NOT NULL,
  `tr_fabric` varchar(100) NOT NULL,
  `tr_fit` varchar(100) NOT NULL,
  `tr_sidepocket` varchar(100) NOT NULL,
  `tr_backpocket` varchar(100) NOT NULL,
  `tr_seam` varchar(100) NOT NULL,
  `tr_coinpocket` varchar(100) NOT NULL,
  `tr_button_color` varchar(100) NOT NULL,
  `tr_thread_color` varchar(100) NOT NULL,
  `sh_fit` varchar(100) NOT NULL,
  `sh_fabric` varchar(100) NOT NULL,
  `sh_sleeve` varchar(100) NOT NULL,
  `sh_collar` varchar(100) NOT NULL,
  `sh_cuff` varchar(100) NOT NULL,
  `sh_placket` varchar(100) NOT NULL,
  `sh_bottomcut` varchar(100) NOT NULL,
  `sh_pocket` varchar(100) NOT NULL,
  `sh_button_color` varchar(100) NOT NULL,
  `sh_thread_color` varchar(100) NOT NULL,
  `su_fabric` varchar(100) NOT NULL,
  `su_fit` varchar(100) NOT NULL,
  `su_button` varchar(100) NOT NULL,
  `su_lapel` varchar(100) NOT NULL,
  `su_sleeve_button` varchar(100) NOT NULL,
  `su_lapel_stich` varchar(100) NOT NULL,
  `su_pocket` varchar(100) NOT NULL,
  `su_ticket_pocket` varchar(100) NOT NULL,
  `su_vent` varchar(100) NOT NULL,
  `su_elbow_patch` varchar(100) NOT NULL,
  `su_elbow_fabric` varchar(100) NOT NULL,
  `su_suit_linning` varchar(100) NOT NULL,
  `su_button_color` varchar(100) NOT NULL,
  `su_thread_color` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `benj_orders_detail`
--

INSERT INTO `benj_orders_detail` (`det_id`, `det_order_id`, `det_price`, `det_quantity`, `det_user_id`, `det_product_type`, `tr_type`, `tr_fabric`, `tr_fit`, `tr_sidepocket`, `tr_backpocket`, `tr_seam`, `tr_coinpocket`, `tr_button_color`, `tr_thread_color`, `sh_fit`, `sh_fabric`, `sh_sleeve`, `sh_collar`, `sh_cuff`, `sh_placket`, `sh_bottomcut`, `sh_pocket`, `sh_button_color`, `sh_thread_color`, `su_fabric`, `su_fit`, `su_button`, `su_lapel`, `su_sleeve_button`, `su_lapel_stich`, `su_pocket`, `su_ticket_pocket`, `su_vent`, `su_elbow_patch`, `su_elbow_fabric`, `su_suit_linning`, `su_button_color`, `su_thread_color`) VALUES
(23, '58', '450', '1', '71', 'men_trouser', 'regular', 'fa_0', 'tapered', 'rounded', 'both_single_welt_with_hook', 'hidden_seam', 'no', 'f1f4e5', 'c0c0bb', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(24, '59', '450', '1', '71', 'men_trouser', 'regular', 'fa_0', 'tapered', 'rounded', 'both_single_welt_with_hook', 'hidden_seam', 'no', 'f1f4e5', 'c0c0bb', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(25, '60', '250', '1', '71', 'men_shirt', '', '', '', '', '', '', '', '', '', 'slim_fit', 'fa_0', 'long_sleeve', 'collar_business_classic', 'one_button_rounded', 'yes', 'modern', 'straight', 'f1f4e5', 'e7e8e7', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(26, '61', '250', '2', '71', 'men_shirt', '', '', '', '', '', '', '', '', '', 'loose_fit', 'fa_1', 'long_sleeve', 'collar_cutaway_classic', 'one_button_beveled', 'no', 'modern', 'straight', '161817', 'd3a5b2', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(27, '62', '250', '1', '75', 'men_shirt', '', '', '', '', '', '', '', '', '', 'slim_fit', 'fa_52', 'long_sleeve', 'collar_modern_club', 'one_button_rounded', 'yes', 'modern', 'straight', '161817', 'f5d84b', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(28, '63', '250', '1', '75', 'men_shirt', '', '', '', '', '', '', '', '', '', 'slim_fit', 'fa_52', 'long_sleeve', 'collar_modern_club', 'one_button_rounded', 'yes', 'modern', 'straight', '161817', 'f5d84b', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(29, '63', '550', '2', '75', 'men_suit', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'fabric_0', 'regular_fit', 'one_button', 'standard_lapel', 'four_button', 'yes', 'welt_pocket', 'no', 'one_vent', 'no', '', '000000', 'f1f4e5', 'ffffff');

-- --------------------------------------------------------

--
-- Table structure for table `benj_users`
--

CREATE TABLE IF NOT EXISTS `benj_users` (
  `user_id` int(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `dob` varchar(100) NOT NULL,
  `height` varchar(100) NOT NULL,
  `weight` varchar(100) NOT NULL,
  `user_profile` varchar(1000) NOT NULL,
  `stylist` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `pincode` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` text NOT NULL,
  `contact` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `benj_users`
--

INSERT INTO `benj_users` (`user_id`, `email`, `password`, `fname`, `lname`, `gender`, `dob`, `height`, `weight`, `user_profile`, `stylist`, `address`, `pincode`, `city`, `state`, `country`, `contact`, `status`) VALUES
(75, 'sbsunilbhatia9@gmail.com', '123456', 'Sunil', 'Bhatia', 'Male', '15/02/1993', '65 Inches', '165 Kilograms', 'http://localhost/size/api/Files/users/File306274.png', '', 'E110', '143601', 'Majitha', 'Punjab', 'India', '8872292478', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `benj_cart`
--
ALTER TABLE `benj_cart`
 ADD PRIMARY KEY (`cart_id`);

--
-- Indexes for table `benj_orders`
--
ALTER TABLE `benj_orders`
 ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `benj_orders_detail`
--
ALTER TABLE `benj_orders_detail`
 ADD PRIMARY KEY (`det_id`);

--
-- Indexes for table `benj_users`
--
ALTER TABLE `benj_users`
 ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `benj_cart`
--
ALTER TABLE `benj_cart`
MODIFY `cart_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `benj_orders`
--
ALTER TABLE `benj_orders`
MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `benj_orders_detail`
--
ALTER TABLE `benj_orders_detail`
MODIFY `det_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

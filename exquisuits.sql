-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 12, 2017 at 02:33 PM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `exquisuits`
--

-- --------------------------------------------------------

--
-- Table structure for table `benj_cart`
--

CREATE TABLE IF NOT EXISTS `benj_cart` (
`cart_id` int(100) NOT NULL,
  `product_type` varchar(100) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `product_price` varchar(100) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `quantity` varchar(100) NOT NULL,
  `total_amount` varchar(100) NOT NULL,
  `product_image` varchar(100) NOT NULL,
  `tr_type` varchar(100) NOT NULL,
  `tr_fabric` varchar(100) NOT NULL,
  `tr_fit` varchar(100) NOT NULL,
  `tr_sidepocket` varchar(100) NOT NULL,
  `tr_backpocket` varchar(100) NOT NULL,
  `tr_seam` varchar(100) NOT NULL,
  `tr_coinpocket` varchar(100) NOT NULL,
  `tr_button_color` varchar(100) NOT NULL,
  `tr_thread_color` varchar(100) NOT NULL,
  `sh_fit` varchar(100) NOT NULL,
  `sh_fabric` varchar(100) NOT NULL,
  `sh_sleeve` varchar(100) NOT NULL,
  `sh_collar` varchar(100) NOT NULL,
  `sh_cuff` varchar(100) NOT NULL,
  `sh_placket` varchar(100) NOT NULL,
  `sh_bottomcut` varchar(100) NOT NULL,
  `sh_pocket` varchar(100) NOT NULL,
  `sh_button_color` varchar(100) NOT NULL,
  `sh_thread_color` varchar(100) NOT NULL,
  `su_fabric` varchar(100) NOT NULL,
  `su_fit` varchar(100) NOT NULL,
  `su_button` varchar(100) NOT NULL,
  `su_lapel` varchar(100) NOT NULL,
  `su_sleeve_button` varchar(100) NOT NULL,
  `su_lapel_stich` varchar(100) NOT NULL,
  `su_pocket` varchar(100) NOT NULL,
  `su_ticket_pocket` varchar(100) NOT NULL,
  `su_vent` varchar(100) NOT NULL,
  `su_elbow_patch` varchar(100) NOT NULL,
  `su_elbow_fabric` varchar(100) NOT NULL,
  `su_suit_linning` varchar(100) NOT NULL,
  `su_button_color` varchar(100) NOT NULL,
  `su_thread_color` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `benj_cart`
--

INSERT INTO `benj_cart` (`cart_id`, `product_type`, `product_name`, `product_price`, `user_id`, `quantity`, `total_amount`, `product_image`, `tr_type`, `tr_fabric`, `tr_fit`, `tr_sidepocket`, `tr_backpocket`, `tr_seam`, `tr_coinpocket`, `tr_button_color`, `tr_thread_color`, `sh_fit`, `sh_fabric`, `sh_sleeve`, `sh_collar`, `sh_cuff`, `sh_placket`, `sh_bottomcut`, `sh_pocket`, `sh_button_color`, `sh_thread_color`, `su_fabric`, `su_fit`, `su_button`, `su_lapel`, `su_sleeve_button`, `su_lapel_stich`, `su_pocket`, `su_ticket_pocket`, `su_vent`, `su_elbow_patch`, `su_elbow_fabric`, `su_suit_linning`, `su_button_color`, `su_thread_color`) VALUES
(9, 'men_trouser', 'Trouser4311', '450', '91', '3', '1350.0', '', 'regular', 'fa_0', 'tapered', 'rounded', 'both_single_welt_with_hook', 'hidden_seam', 'no', 'f1f4e5', 'c0c0bb', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12, 'men_trouser', 'Trouser4428', '450', '89', '1', '450', '', 'regular', 'fa_0', 'tapered', 'rounded', 'both_single_welt_with_hook', 'hidden_seam', 'no', 'f1f4e5', 'c0c0bb', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(16, 'men_shirt', 'Men Shirt8892', '250', '11', '1', '250', '', '', '', '', '', '', '', '', '', '', 'slim_fit', 'fa_2', 'long_sleeve', 'collar_business_classic', 'one_button_rounded', 'yes', 'modern', 'straight', 'f1f4e5', 'e7e8e7', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `benj_orders`
--

CREATE TABLE IF NOT EXISTS `benj_orders` (
`order_id` int(11) NOT NULL,
  `order_date` text NOT NULL,
  `order_state` varchar(100) NOT NULL,
  `order_payment_status` varchar(100) NOT NULL,
  `order_number` varchar(100) NOT NULL,
  `order_total` varchar(100) NOT NULL,
  `order_user_id` int(50) NOT NULL,
  `paid_amount` varchar(100) NOT NULL,
  `product_type` varchar(100) NOT NULL,
  `order_txn_id` text NOT NULL,
  `order_currency_code` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `benj_orders`
--

INSERT INTO `benj_orders` (`order_id`, `order_date`, `order_state`, `order_payment_status`, `order_number`, `order_total`, `order_user_id`, `paid_amount`, `product_type`, `order_txn_id`, `order_currency_code`) VALUES
(1, 'Jun 12, 2017 15:09', 'open', 'Pending', '314621', '1000', 89, '0.00', '', '', ''),
(2, 'Jun 13, 2017 07:29', 'open', 'Pending', '943795', '800', 91, '0.00', '', '', ''),
(3, 'Jun 13, 2017 12:23', 'open', 'Pending', '583212', '750', 11, '0.00', '', '', ''),
(4, 'Jun 13, 2017 12:27', 'open', 'Pending', '695762', '500', 11, '0.00', '', '', ''),
(5, 'Jun 20, 2017 11:58', 'open', 'Pending', '272397', '1600', 11, '0.00', '', '', ''),
(6, 'Jun 20, 2017 12:21', 'open', 'Pending', '165583', '250', 11, '0.00', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `benj_orders_detail`
--

CREATE TABLE IF NOT EXISTS `benj_orders_detail` (
`det_id` int(100) NOT NULL,
  `det_order_id` varchar(100) NOT NULL,
  `det_price` varchar(100) NOT NULL,
  `det_quantity` varchar(100) NOT NULL,
  `det_user_id` varchar(100) NOT NULL,
  `det_product_type` varchar(100) NOT NULL,
  `tr_type` varchar(100) NOT NULL,
  `tr_fabric` varchar(100) NOT NULL,
  `tr_fit` varchar(100) NOT NULL,
  `tr_sidepocket` varchar(100) NOT NULL,
  `tr_backpocket` varchar(100) NOT NULL,
  `tr_seam` varchar(100) NOT NULL,
  `tr_coinpocket` varchar(100) NOT NULL,
  `tr_button_color` varchar(100) NOT NULL,
  `tr_thread_color` varchar(100) NOT NULL,
  `sh_fit` varchar(100) NOT NULL,
  `sh_fabric` varchar(100) NOT NULL,
  `sh_sleeve` varchar(100) NOT NULL,
  `sh_collar` varchar(100) NOT NULL,
  `sh_cuff` varchar(100) NOT NULL,
  `sh_placket` varchar(100) NOT NULL,
  `sh_bottomcut` varchar(100) NOT NULL,
  `sh_pocket` varchar(100) NOT NULL,
  `sh_button_color` varchar(100) NOT NULL,
  `sh_thread_color` varchar(100) NOT NULL,
  `su_fabric` varchar(100) NOT NULL,
  `su_fit` varchar(100) NOT NULL,
  `su_button` varchar(100) NOT NULL,
  `su_lapel` varchar(100) NOT NULL,
  `su_sleeve_button` varchar(100) NOT NULL,
  `su_lapel_stich` varchar(100) NOT NULL,
  `su_pocket` varchar(100) NOT NULL,
  `su_ticket_pocket` varchar(100) NOT NULL,
  `su_vent` varchar(100) NOT NULL,
  `su_elbow_patch` varchar(100) NOT NULL,
  `su_elbow_fabric` varchar(100) NOT NULL,
  `su_suit_linning` varchar(100) NOT NULL,
  `su_button_color` varchar(100) NOT NULL,
  `su_thread_color` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `benj_orders_detail`
--

INSERT INTO `benj_orders_detail` (`det_id`, `det_order_id`, `det_price`, `det_quantity`, `det_user_id`, `det_product_type`, `tr_type`, `tr_fabric`, `tr_fit`, `tr_sidepocket`, `tr_backpocket`, `tr_seam`, `tr_coinpocket`, `tr_button_color`, `tr_thread_color`, `sh_fit`, `sh_fabric`, `sh_sleeve`, `sh_collar`, `sh_cuff`, `sh_placket`, `sh_bottomcut`, `sh_pocket`, `sh_button_color`, `sh_thread_color`, `su_fabric`, `su_fit`, `su_button`, `su_lapel`, `su_sleeve_button`, `su_lapel_stich`, `su_pocket`, `su_ticket_pocket`, `su_vent`, `su_elbow_patch`, `su_elbow_fabric`, `su_suit_linning`, `su_button_color`, `su_thread_color`) VALUES
(1, '1', '450', '1', '89', 'men_trouser', 'regular', 'fa_3', 'tapered', 'rounded', 'both_single_welt_with_hook', 'hidden_seam', 'no', 'f1f4e5', 'c0c0bb', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2, '1', '550', '1', '89', 'men_suit', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'fabric_0', 'regular_fit', 'one_button', 'standard_lapel', 'four_button', 'yes', 'welt_pocket', 'no', 'one_vent', 'no', '', '000000', 'f1f4e5', 'ffffff'),
(3, '2', '250', '1', '91', 'men_shirt', '', '', '', '', '', '', '', '', '', 'slim_fit', 'fa_0', 'long_sleeve', 'collar_business_classic', 'one_button_rounded', 'yes', 'modern', 'straight', 'f1f4e5', 'e7e8e7', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(4, '2', '550', '1', '91', 'men_suit', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'fabric_0', 'regular_fit', 'one_button', 'standard_lapel', 'four_button', 'yes', 'welt_pocket', 'no', 'one_vent', 'no', '', '000000', 'f1f4e5', 'ffffff'),
(5, '3', '250', '3', '11', 'men_shirt', '', '', '', '', '', '', '', '', '', 'slim_fit', 'fa_0', 'long_sleeve', 'collar_business_classic', 'one_button_rounded', 'yes', 'modern', 'straight', 'f1f4e5', 'e7e8e7', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(6, '4', '250', '2', '11', 'men_shirt', '', '', '', '', '', '', '', '', '', 'slim_fit', 'fa_0', 'long_sleeve', 'collar_business_classic', 'one_button_rounded', 'yes', 'modern', 'straight', 'f1f4e5', 'e7e8e7', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7, '5', '250', '1', '11', 'men_shirt', '', '', '', '', '', '', '', '', '', 'slim_fit', 'fa_2', 'short_sleeve', 'collar_pin', 'one_button_rounded', 'narrow', 'classic', 'straight', 'e7dcd1', '62615e', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(8, '5', '450', '3', '11', 'men_trouser', 'regular', 'fa_16', 'loose', 'beveled', 'both_single_welt', 'hidden_seam', 'no', '8a5c61', '27231e', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(9, '6', '250', '1', '11', 'men_shirt', '', '', '', '', '', '', '', '', '', 'normal_fit', 'fa_0', 'rollup_sleeve', 'collar_business_classic', 'one_button_rounded', 'yes', 'modern', 'straight', 'f1f4e5', 'e7e8e7', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `benj_users`
--

CREATE TABLE IF NOT EXISTS `benj_users` (
  `user_id` int(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `dob` varchar(100) NOT NULL,
  `height` varchar(100) NOT NULL,
  `weight` varchar(100) NOT NULL,
  `user_profile` varchar(1000) NOT NULL,
  `stylist` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `pincode` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` text NOT NULL,
  `contact` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `benj_users`
--

INSERT INTO `benj_users` (`user_id`, `email`, `password`, `fname`, `lname`, `gender`, `dob`, `height`, `weight`, `user_profile`, `stylist`, `address`, `pincode`, `city`, `state`, `country`, `contact`, `status`) VALUES
(11, 'john.fijen@gmail.com', '12345678', 'John', 'Fijen', 'Male', '10/12/1969', '70 Inches', '220 Kilograms', 'http://scan2fit.com/size/api/Files/users/File792630.jpg', 'Model Style', '272 Hogans Valley Way', '123456', 'Cary', 'North Carolina', 'United States', '1 919 706 6336', '1'),
(72, 'sbsunilbhatia9@gmail.com', '12345678', 'Sunil', 'Bhatia', 'Male', '06/09/2017', '68 Inches', '165 Kilograms', 'http://scan2fit.com/size/api/Files/users/File285266.png', 'Kapil', '', '', '', '', '', '8872292478', '1'),
(88, 'harry1@gmail.com', '123456', 'harry', 'kumar', 'Male', '07/06/2015', '64 Inches', '200 Pounds', 'http://scan2fit.com/benjamin/admin/api/Files/users/File733414.png', 'abcd', 'shanti nagar', '144514', 'Nawashahr', 'Punjab', 'India', '9779784701', '1'),
(89, 'harry2@gmail.com', '123456', 'harry', 'kumar', 'Male', '06/14/2017', '64 Inches', '200 Pounds', 'http://scan2fit.com/benjamin/admin/api/Files/users/File914763.png', 'stylist', 'bjhs dfknlj', '144514', 'Nawashahr', 'Punjab', 'India', '9779784701', '1'),
(90, 'harry3@gmail.com', '123456', 'harr', 'asasd', 'Male', 'jk', '211 Inches', '12 Pounds', 'http://scan2fit.com/benjamin/admin/api/Files/users/File744860.png', 'FDF', '', '', 'undefined', 'Sector claimed by Argentina/UK', 'Antarctica', '', '1'),
(91, 'harr@gmail.com', '123456', 'har', 'jksa', 'Male', 'jsk', '121 Inches', '12 Pounds', 'http://scan2fit.com/benjamin/admin/api/Files/users/File792602.png', 'djk', 'sa as', 'asd', 'Peshkopi', 'Dibre', 'Albania', 'asdasd', '1'),
(92, 'naru@gmail.com', '123456', 'Dhawan', 'Kapil', 'Male', '06/16/2017', '150 Inches', '154 Pounds', 'http://scan2fit.com/benjamin/admin/api/Files/users/File749458.png', 'Kpail', '', '', 'Beri', 'Haryana', 'India', '', '1'),
(93, 'harry12@gmail.com', '123456', 'harr', 'kumar', 'Male', '06/01/2017', '64 Inches', '120 Pounds', 'http://scan2fit.com/benjamin/admin/api/Files/users/File519715.png', 'asd', '', '', 'Fairbanks', 'Alaska', 'United States', '', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `benj_cart`
--
ALTER TABLE `benj_cart`
 ADD PRIMARY KEY (`cart_id`);

--
-- Indexes for table `benj_orders`
--
ALTER TABLE `benj_orders`
 ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `benj_orders_detail`
--
ALTER TABLE `benj_orders_detail`
 ADD PRIMARY KEY (`det_id`);

--
-- Indexes for table `benj_users`
--
ALTER TABLE `benj_users`
 ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `benj_cart`
--
ALTER TABLE `benj_cart`
MODIFY `cart_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `benj_orders`
--
ALTER TABLE `benj_orders`
MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `benj_orders_detail`
--
ALTER TABLE `benj_orders_detail`
MODIFY `det_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

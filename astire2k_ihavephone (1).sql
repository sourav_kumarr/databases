-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 12, 2017 at 02:28 PM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `astire2k_ihavephone`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
`admin_id` int(100) NOT NULL,
  `admin_name` varchar(100) NOT NULL,
  `admin_email` varchar(100) NOT NULL,
  `admin_password` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_name`, `admin_email`, `admin_password`) VALUES
(1, 'Sunil', 'admin@gmail.com', 'e10adc3949ba59abbe56e057f20f883e');

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE IF NOT EXISTS `brand` (
`brand_id` int(100) NOT NULL,
  `brand_name` varchar(100) NOT NULL,
  `brand_status` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`brand_id`, `brand_name`, `brand_status`) VALUES
(9, 'Samsung', '1'),
(10, 'iPhone', '1');

-- --------------------------------------------------------

--
-- Table structure for table `cell_carrier`
--

CREATE TABLE IF NOT EXISTS `cell_carrier` (
`carrier_id` int(100) NOT NULL,
  `carrier_name` varchar(100) NOT NULL,
  `carrier_image` varchar(100) NOT NULL,
  `carrier_status` enum('0','1') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cell_carrier`
--

INSERT INTO `cell_carrier` (`carrier_id`, `carrier_name`, `carrier_image`, `carrier_status`) VALUES
(2, 'AT&T', 'File13059838.jpg', '1'),
(3, 'SPRINT', 'File296295074.jpg', '1'),
(4, 'VERIZON', 'File869295466.jpg', '1'),
(5, 'FACTORY UNLOCKED', 'File61626110.jpg', '1'),
(6, 'T-MOBILE', 'File1180394164.jpg', '1'),
(7, 'OTHER CARRIERS', 'File574187650.jpg', '1');

-- --------------------------------------------------------

--
-- Table structure for table `model`
--

CREATE TABLE IF NOT EXISTS `model` (
`model_id` int(100) NOT NULL,
  `model_name` varchar(100) NOT NULL,
  `brand_id` varchar(100) NOT NULL,
  `model_image` varchar(100) NOT NULL,
  `model_status` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `model`
--

INSERT INTO `model` (`model_id`, `model_name`, `brand_id`, `model_image`, `model_status`) VALUES
(17, '5', '10', 'File1017146189.png', '1'),
(18, '5C', '10', 'File712503207.png', '1'),
(19, 'i5S', '10', 'File1100328453.png', '1'),
(20, '6', '10', 'File1120548941.png', '1'),
(21, '6S', '10', 'File1177500223.png', '1'),
(22, '6S Plus', '10', 'File224799881.png', '1'),
(23, '7', '10', 'File828631880.png', '1'),
(24, '7 Plus', '10', 'File547288836.png', '1'),
(26, 'Galaxy S4', '9', 'File764890966.png', '1'),
(27, 'Galaxy S5', '9', 'File1008019143.png', '1'),
(28, 'Galaxy S6', '9', 'File893114830.png', '1'),
(29, 'Galaxy S6 Edge', '9', 'File431605384.png', '1'),
(30, 'Galaxy S7', '9', 'File844622761.png', '1'),
(31, 'Galaxy S7 Edge', '9', 'File407674715.png', '1'),
(32, 'Galaxy S8', '9', 'File395394015.png', '1'),
(33, 'Galaxy Note', '9', 'File11659087566.png', '1'),
(34, '6 Plus', '10', 'File14080603281.jpg', '1');

-- --------------------------------------------------------

--
-- Table structure for table `orderdetail`
--

CREATE TABLE IF NOT EXISTS `orderdetail` (
`detail_id` int(100) NOT NULL,
  `order_id` varchar(100) NOT NULL,
  `product_id` varchar(100) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `product_image_feature` varchar(100) NOT NULL,
  `product_brand` varchar(100) NOT NULL,
  `product_capacity` varchar(100) NOT NULL,
  `model_name` varchar(100) NOT NULL,
  `cellular_name` varchar(100) NOT NULL,
  `product_price` varchar(100) NOT NULL,
  `quantity` varchar(100) NOT NULL,
  `total_amount` varchar(100) NOT NULL,
  `product_condition` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orderdetail`
--

INSERT INTO `orderdetail` (`detail_id`, `order_id`, `product_id`, `product_name`, `product_image_feature`, `product_brand`, `product_capacity`, `model_name`, `cellular_name`, `product_price`, `quantity`, `total_amount`, `product_condition`) VALUES
(13, '18', '6', 'IPhone7 Plus', 'File589844615.png', 'iPhone', '64 GB', '7', 'FACTORY UNLOCKED', '410', '1', '410', 'Flawless'),
(19, '24', '7', 'Galaxy S5', 'File1140027392.png', 'Samsung', '32 GB', 'Galaxy S5', 'FACTORY UNLOCKED', '330', '1', '330', 'Flawless'),
(20, '25', '7', 'Galaxy S5', 'File1140027392.png', 'Samsung', '32 GB', 'Galaxy S5', 'FACTORY UNLOCKED', '120', '1', '120', 'Damaged'),
(21, '26', '5', 'IPhone 5S', 'File42518677.png', 'iPhone', '16 GB', '5S', 'FACTORY UNLOCKED', '350', '1', '350', 'Flawless'),
(22, '27', '6', 'IPhone7 Plus', 'File589844615.png', 'iPhone', '64 GB', '7', 'FACTORY UNLOCKED', '410', '1', '410', 'Flawless'),
(23, '28', '9', 'Samsung Galaxy Note 5', 'File13571048434.jpg', 'Samsung', '256 GB', 'Galaxy Note', 'FACTORY UNLOCKED', '260', '1', '260', 'Good');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
`order_id` int(100) NOT NULL,
  `order_number` varchar(100) NOT NULL,
  `order_amount` varchar(100) NOT NULL,
  `order_dated` varchar(100) NOT NULL,
  `order_status` varchar(100) NOT NULL,
  `order_user_id` varchar(100) NOT NULL,
  `billing_fname` varchar(100) NOT NULL,
  `billing_lname` varchar(100) NOT NULL,
  `billing_address` text NOT NULL,
  `billing_country` varchar(100) NOT NULL,
  `billing_state` varchar(100) NOT NULL,
  `billing_city` varchar(100) NOT NULL,
  `billing_pincode` varchar(100) NOT NULL,
  `billing_phone` varchar(100) NOT NULL,
  `payment_type` varchar(100) NOT NULL,
  `paypal_id` varchar(100) NOT NULL,
  `payee_name` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `order_number`, `order_amount`, `order_dated`, `order_status`, `order_user_id`, `billing_fname`, `billing_lname`, `billing_address`, `billing_country`, `billing_state`, `billing_city`, `billing_pincode`, `billing_phone`, `payment_type`, `paypal_id`, `payee_name`) VALUES
(18, '876230', '410', '1497301713', 'Pending', '10', 'Adnan', 'test', 'test ', 'United States', 'Texas', 'Alamo', '34566', '43535534', 'check', '', ''),
(24, '247734', '330', '1497466679', 'Pending', '15', 'Sunil', 'Bhatia', 'Plot No E110 Phase7 Industrial Area', 'India', 'Punjab', 'Mohali', '160055', '8872292478', 'check', '', 'Sunil Bhatia'),
(25, '957608', '120', '1497500319', 'Pending', '16', 'as', 'asas', 'fdsfd gfsdgdf', 'Algeria', 'Biskrah', 'Sidi Khalid', 'dsfg', 'dfg', 'check', '', 'asas'),
(26, '580416', '350', '1497728899', 'Completed', '17', 'Farhan', 'Pirzada', '12547 Greenbriar dr ', 'United States', 'Texas', 'Houston', '77845', '713-253-5837', 'check', '', 'Farhan Pirzada'),
(27, '722953', '410', '1497906060', 'Pending', '18', 'Adnan', 'Azam', 'test2 test', 'United States', 'Alaska', 'College', '54353', '232423424', 'paypal', 'adnan.azam@gmail.com', ''),
(28, '753822', '260', '1498720225', 'Pending', '19', 'Sunil', 'Bhatia', 'hno 103 ward no 13 vpo majitha ', 'India', 'Punjab', 'Mohali', '160055', '8872292478', 'paypal', 'sbsunilbhatia9@gmail.com', '');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
`product_id` int(100) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `model_id` int(100) NOT NULL,
  `brand_id` int(100) NOT NULL,
  `product_image_feature` varchar(100) NOT NULL,
  `sku` varchar(100) NOT NULL,
  `product_capacity` varchar(100) NOT NULL,
  `cell_carrier_id` int(100) NOT NULL,
  `product_quantity` int(100) NOT NULL,
  `product_back_image` varchar(100) NOT NULL,
  `product_color` varchar(100) NOT NULL,
  `damaged_price` varchar(100) NOT NULL,
  `damaged_desc` text NOT NULL,
  `good_price` varchar(100) NOT NULL,
  `good_desc` text NOT NULL,
  `flawless_price` varchar(100) NOT NULL,
  `flawless_desc` text NOT NULL,
  `product_status` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=125 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `product_name`, `model_id`, `brand_id`, `product_image_feature`, `sku`, `product_capacity`, `cell_carrier_id`, `product_quantity`, `product_back_image`, `product_color`, `damaged_price`, `damaged_desc`, `good_price`, `good_desc`, `flawless_price`, `flawless_desc`, `product_status`) VALUES
(10, 'iPhone 7 Plus', 24, 10, 'File268431613a.png', 'SKU0707201729', '128 GB', 5, 1, '', 'red', '120', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '340', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '390', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(11, 'iPhone 7 Plus', 24, 10, 'File268431613a.png', 'SKU0707201750', '32 GB', 5, 1, '', 'Red', '110', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '310', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '350', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(12, 'IPhone7 Plus', 24, 10, 'File268431613a.png', 'SKU0707201742', '256 GB', 5, 1, '', 'Red', '92', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '370', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '420', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(13, 'iPhone 7 Plus', 24, 10, 'File268431613a.png', 'SKU0707201758', '32 GB', 2, 1, '', 'red', '128', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '310', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '330', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(14, 'iPhone 7 Plus', 24, 10, 'File268431613a.png', 'SKU0707201748', '128 GB', 2, 1, '', 'red', '123', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offe', '325', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '360', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(15, 'iPhone 7 Plus', 24, 10, 'File268431613a.png', 'SKU0707201779', '256 GB', 2, 1, '', 'Red', '120', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '395', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '420', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(16, 'iPhone 7 Plus', 24, 10, 'File268431613a.png', 'SKU0707201798', '32 GB', 3, 1, '', 'Red', '96', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '278', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '302', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(17, 'iPhone 7 Plus', 24, 10, 'File268431613a.png', 'SKU0707201740', '128 GB', 3, 1, '', 'red', '95', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '292', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '330', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(18, 'iPhone 7 Plus', 24, 10, 'File268431613a.png', 'SKU0707201779', '256 GB', 3, 1, '', 'red', '122', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '325', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '375', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(19, 'iPhone 7 Plus', 24, 10, 'File268431613a.png', 'SKU0707201791', '32 GB', 4, 1, '', 'red', '109', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '338', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '341', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(20, 'iPhone 7 Plus', 24, 10, 'File268431613a.png', 'SKU0707201783', '128 GB', 4, 1, '', 'Red', '123', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '350', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '377', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(21, 'iPhone 7 Plus', 24, 10, 'File268431613a.png', 'SKU0707201781', '256 GB', 4, 1, '', 'red', '132', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '362', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '425', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(22, 'iPhone 7 Plus', 24, 10, 'File268431613a.png', 'SKU0707201712', '32 GB', 6, 1, '', 'Red', '105', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '278', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '282', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(23, 'iPhone 7 Plus', 24, 10, 'File268431613a.png', 'SKU0707201728', '128 GB', 6, 1, '', 'Red', '88', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '327', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '329', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(24, 'iPhone 7 Plus', 24, 10, 'File268431613a.png', 'SKU0707201743', '256 GB', 6, 1, '', 'Red', '82', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '345', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '365', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(25, 'iPhone 7 Plus', 24, 10, 'File268431613a.png', 'SKU0707201738', '32 GB', 7, 1, '', 'Red', '83', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '271', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '300', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(26, 'iPhone 7 Plus', 24, 10, 'File268431613a.png', 'SKU0707201767', '128 GB', 7, 1, '', 'Red', '73', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '300', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '335', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(27, 'iPhone 7 Plus', 24, 10, 'File268431613a.png', 'SKU0707201737', '256 GB', 7, 2, '', 'Red', '55', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '336', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '370', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(28, 'iPhone 7', 23, 10, 'File134160154.png', 'SKU0707201758', '32 GB', 2, 1, '', 'Black', '138', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '323', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '328', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(29, 'iPhone 7', 23, 10, 'File134160154.png', 'SKU0707201711', '128 GB', 2, 1, '', 'Black', '152', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '309', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '342', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(30, 'iPhone 7', 23, 10, 'File134160154.png', 'SKU0707201765', '256 GB', 2, 1, '', 'BLK', '137', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '333', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '376', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(31, 'iPhone 7', 23, 10, 'File134160154.png', 'SKU0707201746', '32 GB', 3, 4, '', 'Black', '100', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '242', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '268', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(32, 'iPhone 7', 23, 10, 'File134160154.png', 'SKU0707201783', '128 GB', 3, 1, '', 'Black', '119', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '279', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '314', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(33, 'iPhone 7', 23, 10, 'File134160154.png', 'SKU0707201721', '256 GB', 3, 1, '', 'Black', '100', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '300', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '330', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(34, 'iPhone 7', 23, 10, 'File134160154.png', 'SKU0707201739', '32 GB', 4, 1, '', 'Black', '129', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '304', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '335', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(35, 'iPhone 7', 23, 10, 'File134160154.png', 'SKU0707201799', '128 GB', 4, 1, '', 'Black', '139', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '333', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '360', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(36, 'iPhone 7', 23, 10, 'File134160154.png', 'SKU0707201768', '256 GB', 4, 1, '', 'Black', '129', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '349', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '378', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(37, 'iPhone 7', 23, 10, 'File134160154.png', 'SKU0707201750', '32 GB', 6, 1, '', 'Black', '102', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '250', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '277', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(38, 'iPhone 7', 23, 10, 'File134160154.png', 'SKU0707201731', '128 GB', 6, 1, '', 'Black', '118', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '287', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '320', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(39, 'iPhone 7', 23, 10, 'File134160154.png', 'SKU0707201731', '256 GB', 6, 1, '', 'Black', '97', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '313', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '342', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(40, 'iPhone 7', 23, 10, 'File134160154.png', 'SKU0707201740', '32 GB', 5, 1, '', 'Black', '102', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '268', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '307', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(41, 'iPhone 7', 23, 10, 'File134160154.png', 'SKU0707201771', '128 GB', 5, 1, '', 'Black', '102', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '310', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '347', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(42, 'iPhone 7', 23, 10, 'File134160154.png', 'SKU0707201753', '256 GB', 5, 1, '', 'Black', '102', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '340', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '377', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(43, 'iPhone 7', 23, 10, 'File134160154.png', 'SKU0707201781', '32 GB', 7, 1, '', 'Black', '73', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '235', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '266', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(44, 'iPhone 7', 23, 10, 'File134160154.png', 'SKU0707201783', '128 GB', 7, 1, '', 'Black', '64', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '272', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '300', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(45, 'iPhone 7', 23, 10, 'File134160154.png', 'SKU0707201744', '256 GB', 7, 1, '', 'Black', '73', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '305', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '330', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(47, 'iPhone 6S Plus', 22, 10, 'File35263353446.jpg', 'SKU1707201787', '16 GB', 2, 1, '', 'White', '80', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '201', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '230', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(48, 'iPhone 6S Plus', 22, 10, 'File35263353446.jpg', 'SKU1707201760', '64 GB', 2, 1, '', 'White', '84', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '225', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '275', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(49, 'iPhone 6S Plus', 22, 10, 'File35263353446.jpg', 'SKU1707201735', '128 GB', 2, 1, '', 'White', '76', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '212', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '240', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(50, 'iPhone 6S Plus', 22, 10, 'File35263353446.jpg', 'SKU1707201713', '16 GB', 3, 1, '', 'White', '75', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '186', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '230', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(51, 'iPhone 6S Plus', 22, 10, 'File35263353446.jpg', 'SKU1707201783', '64 GB', 3, 1, '', 'White', '78', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '210', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '237', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(52, 'iPhone 6S Plus', 22, 10, 'File35263353446.jpg', 'SKU1707201724', '128 GB', 3, 1, '', 'White', '67', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '235', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '280', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(53, 'iPhone 6S Plus', 22, 10, 'File35263353446.jpg', 'SKU1707201776', '16 GB', 4, 1, '', 'White', '80', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '230', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '256', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(54, 'iPhone 6S Plus', 22, 10, 'File35263353446.jpg', 'SKU1707201740', '64 GB', 4, 1, '', 'White', '85', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '240', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '270', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(55, 'iPhone 6S Plus', 22, 10, 'File35263353446.jpg', 'SKU1707201763', '128 GB', 4, 1, '', 'White', '90', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '265', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '315', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(56, 'iPhone 6S Plus', 22, 10, 'File35263353446.jpg', 'SKU1707201761', '16 GB', 6, 1, '', 'White', '59', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '186', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '220', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(57, 'iPhone 6S Plus', 22, 10, 'File35263353446.jpg', 'SKU1707201799', '64 GB', 6, 1, '', 'White', '70', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '177', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '235', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(58, 'iPhone 6S Plus', 22, 10, 'File35263353446.jpg', 'SKU1707201789', '128 GB', 6, 1, '', 'White', '52', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '250', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '270', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(59, 'iPhone 6S Plus', 22, 10, 'File35263353446.jpg', 'SKU1707201752', '128 GB', 7, 1, '', 'White', '49', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '200', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '240', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(60, 'iPhone 6S Plus', 22, 10, 'File35263353446.jpg', 'SKU1707201778', '16 GB', 5, 1, '', 'White', '65', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '186', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '215', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(61, 'iPhone 6S Plus', 22, 10, 'File35263353446.jpg', 'SKU1707201791', '64 GB', 5, 1, '', 'White', '92', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '223', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '245', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(62, 'iPhone 6S Plus', 22, 10, 'File35263353446.jpg', 'SKU1707201773', '128 GB', 5, 1, '', 'White', '94', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '255', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '280', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(63, 'iPhone 6S', 21, 10, 'File88187723653.jpg', 'SKU1707201733', '16 GB', 2, 1, '', 'White', '71', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '180', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '199', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(64, 'iPhone 6S', 21, 10, 'File88187723653.jpg', 'SKU1707201735', '16 GB', 2, 1, '', 'White', '81', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '185', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '231', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(65, 'iPhone 6S', 21, 10, 'File88187723653.jpg', 'SKU1707201776', '128 GB', 2, 1, '', 'White', '85', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '219', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '260', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(66, 'iPhone 6S', 21, 10, 'File88187723653.jpg', 'SKU1707201774', '16 GB', 3, 1, '', 'White', '79', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '150', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '193', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(67, 'iPhone 6S', 21, 10, 'File88187723653.jpg', 'SKU1707201731', '64 GB', 3, 1, '', 'White', '74', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '185', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '249', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(68, 'iPhone 6S', 21, 10, 'File88187723653.jpg', 'SKU1707201765', '128 GB', 3, 1, '', 'White', '75', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '204', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '269', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(69, 'iPhone 6S', 21, 10, 'File88187723653.jpg', 'SKU1707201722', '16 GB', 4, 1, '', 'White', '76', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '199', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '201', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(70, 'iPhone 6S', 21, 10, 'File88187723653.jpg', 'SKU1707201738', '64 GB', 4, 1, '', 'White', '93', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '227', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '267', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(71, 'iPhone 6S', 21, 10, 'File88187723653.jpg', 'SKU1707201794', '128 GB', 4, 1, '', 'White', '94', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '208', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '263', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(72, 'iPhone 6S', 21, 10, 'File88187723653.jpg', 'SKU1707201746', '16 GB', 6, 1, '', 'White', '37', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '149', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '167', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(73, 'iPhone 6S', 21, 10, 'File88187723653.jpg', 'SKU1707201719', '64 GB', 6, 1, '', 'White', '62', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '202', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '245', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(74, 'iPhone 6S', 21, 10, 'File88187723653.jpg', 'SKU1707201732', '128 GB', 6, 1, '', 'White', '46', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '217', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '253', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(75, 'iPhone 6S', 21, 10, 'File88187723653.jpg', 'SKU1707201773', '16 GB', 7, 1, '', 'White', '33', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '160', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '150', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(76, 'iPhone 6S', 21, 10, 'File88187723653.jpg', 'SKU1707201787', '64 GB', 7, 1, '', 'White', '34', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '184', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '207', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(77, 'iPhone 6S', 21, 10, 'File88187723653.jpg', 'SKU1707201793', '128 GB', 7, 1, '', 'White', '41', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '193', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '225', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(78, 'iPhone 6S', 21, 10, 'File88187723653.jpg', 'SKU1707201750', '16 GB', 5, 1, '', 'White', '86', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '150', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '217', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1');
INSERT INTO `product` (`product_id`, `product_name`, `model_id`, `brand_id`, `product_image_feature`, `sku`, `product_capacity`, `cell_carrier_id`, `product_quantity`, `product_back_image`, `product_color`, `damaged_price`, `damaged_desc`, `good_price`, `good_desc`, `flawless_price`, `flawless_desc`, `product_status`) VALUES
(79, 'iPhone 6S', 21, 10, 'File88187723653.jpg', 'SKU1707201720', '64 GB', 5, 1, '', 'White', '95', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '211', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '246', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(80, 'iPhone 6S', 21, 10, 'File88187723653.jpg', 'SKU1707201766', '128 GB', 5, 1, '', 'White', '93', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '226', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '295', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(81, 'iPhone 6 Plus', 34, 10, 'File88187723653.jpg', 'SKU1707201781', '16 GB', 2, 1, '', 'White', '56', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '135', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '166', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(82, 'iPhone 6 Plus', 34, 10, 'File88187723653.jpg', 'SKU1707201747', '64 GB', 2, 1, '', 'White', '50', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '166', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '205', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(83, 'iPhone 6 Plus', 34, 10, 'File88187723653.jpg', 'SKU1707201717', '128 GB', 2, 1, '', 'White', '54', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '163', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '265', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(84, 'iPhone 6 Plus', 34, 10, 'File88187723653.jpg', 'SKU1707201721', '16 GB', 3, 1, '', 'White', '50', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '123', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '180', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(85, 'iPhone 6 Plus', 34, 10, 'File88187723653.jpg', 'SKU1707201777', '64 GB', 3, 1, '', 'White', '43', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '154', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '189', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(86, 'iPhone 6 Plus', 34, 10, 'File88187723653.jpg', 'SKU1707201736', '128 GB', 3, 1, '', 'White', '50', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '165', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '230', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(87, 'iPhone 6 Plus', 34, 10, 'File88187723653.jpg', 'SKU1707201757', '16 GB', 4, 1, '', 'White', '47', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '153', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '197', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(88, 'iPhone 6 Plus', 34, 10, 'File88187723653.jpg', 'SKU1707201773', '64 GB', 4, 1, '', 'White', '53', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '185', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '218', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(89, 'iPhone 6 Plus', 34, 10, 'File88187723653.jpg', 'SKU1707201782', '128 GB', 4, 1, '', 'White', '60', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '220', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '245', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(90, 'iPhone 6 Plus', 34, 10, 'File88187723653.jpg', 'SKU1707201765', '16 GB', 6, 1, '', 'White', '31', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '122', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '150', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(91, 'iPhone 6 Plus', 34, 10, 'File88187723653.jpg', 'SKU1707201735', '64 GB', 6, 1, '', 'White', '32', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '144', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '157', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(92, 'iPhone 6 Plus', 34, 10, 'File88187723653.jpg', 'SKU1707201710', '128 GB', 6, 1, '', 'White', '30', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '148', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '182', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(93, 'iPhone 6 Plus', 34, 10, 'File88187723653.jpg', 'SKU1707201794', '16 GB', 7, 1, '', 'White', '27', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '101', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '143', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(94, 'iPhone 6 Plus', 34, 10, 'File88187723653.jpg', 'SKU1707201735', '64 GB', 7, 1, '', 'White', '42', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '143', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '165', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(95, 'iPhone 6 Plus', 34, 10, 'File88187723653.jpg', 'SKU1707201742', '16 GB', 5, 1, '', 'White', '46', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '137', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '175', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(96, 'iPhone 6 Plus', 34, 10, 'File88187723653.jpg', 'SKU1707201780', '64 GB', 5, 1, '', 'White', '58', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '137', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '215', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(97, 'iPhone 6 Plus', 34, 10, 'File88187723653.jpg', 'SKU1707201798', '64 GB', 5, 1, '', 'White', '60', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '165', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '229', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(98, 'iPhone 6', 20, 10, 'File88187723653.jpg', 'SKU1707201792', '16 GB', 2, 1, '', 'White', '65', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '107', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '145', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(99, 'iPhone 6', 20, 10, 'File88187723653.jpg', 'SKU1707201771', '64 GB', 2, 1, '', 'White', '66', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '125', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '150', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(100, 'iPhone 6', 20, 10, 'File88187723653.jpg', 'SKU1707201744', '128 GB', 2, 1, '', 'White', '56', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '122', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '177', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(101, 'iPhone 6', 20, 10, 'File88187723653.jpg', 'SKU1707201762', '16 GB', 3, 1, '', 'White', '47', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '100', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '147', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(102, 'iPhone 6', 20, 10, 'File88187723653.jpg', 'SKU1707201784', '64 GB', 3, 1, '', 'White', '55', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '131', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '171', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(103, 'iPhone 6', 20, 10, 'File88187723653.jpg', 'SKU1707201762', '128 GB', 3, 1, '', 'White', '35', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '140', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '173', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(104, 'iPhone 6', 20, 10, 'File88187723653.jpg', 'SKU1707201776', '16 GB', 4, 1, '', 'White', '56', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '129', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '145', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(105, 'iPhone 6', 34, 10, 'File88187723653.jpg', 'SKU1707201731', '64 GB', 4, 1, '', 'White', '70', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '145', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '180', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(106, 'iPhone 6', 34, 10, 'File88187723653.jpg', 'SKU1707201779', '128 GB', 4, 1, '', 'White', '76', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '152', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '181', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(107, 'iPhone 6', 20, 10, 'File88187723653.jpg', 'SKU1707201712', '16 GB', 6, 1, '', 'White', '50', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '100', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '112', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(108, 'iPhone 6', 20, 10, 'File88187723653.jpg', 'SKU1707201767', '64 GB', 6, 1, '', 'White', '45', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '139', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '163', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(109, 'iPhone 6', 20, 10, 'File88187723653.jpg', 'SKU1707201761', '128 GB', 6, 1, '', 'White', '52', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '135', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '155', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(110, 'iPhone 6', 20, 10, 'File88187723653.jpg', 'SKU1707201768', '16 GB', 7, 1, '', 'White', '17', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '95', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '107', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(111, 'iPhone 6', 20, 10, 'File88187723653.jpg', 'SKU1707201776', '64 GB', 7, 1, '', 'White', '30', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '120', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '131', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(112, 'iPhone 6', 20, 10, 'File88187723653.jpg', 'SKU1707201710', '128 GB', 7, 1, '', 'White', '23', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '142', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '157', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(113, 'iPhone 6', 20, 10, 'File88187723653.jpg', 'SKU1707201761', '16 GB', 5, 1, '', 'White', '17', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '94', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '107', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(114, 'iPhone 6', 20, 10, 'File88187723653.jpg', 'SKU1707201736', '16 GB', 5, 1, '', 'White', '30', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '120', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '131', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(115, 'iPhone 6', 20, 10, 'File88187723653.jpg', 'SKU1707201765', '16 GB', 5, 1, '', 'White', '23', 'Your iPhone is Damaged condition if any of these are true:\r\nâ€¢ it does not power on\r\nâ€¢ it has a cracked screen or body\r\nâ€¢ it is not 100% functional\r\nNOTE: If your iPhone is cracked in half, bent or missing parts, it may be subject to receiving a revised offer', '142', 'Your iPhone is Good condition if it:\r\nâ€¢ powers on\r\nâ€¢ is fully functional, including all buttons and ports\r\nâ€¢ has no cracks on the screen or body', '155', 'Your iPhone is Flawless condition if it:\r\nâ€¢ appears to be new out of the box\r\nâ€¢ has absolutely no scratches, scuffs or other marks', '1'),
(123, 'new prod', 26, 9, 'File534896830.jpg', 'SKU2907201772', '', 7, 4, '', 'ee', '45', 'sdsdsd', '', '', '', '', '1'),
(124, 'new prodfff', 26, 9, 'File534896830.jpg', 'SKU2907201741', '', 7, 4, '', 'ee', '45', 'sdsdsd', '', '', '', '', '1');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`user_id` int(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` int(100) NOT NULL,
  `email_status` int(100) NOT NULL,
  `token` varchar(100) NOT NULL,
  `dated` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `email`, `password`, `status`, `email_status`, `token`, `dated`) VALUES
(17, 'farhanpirzada@hotmail.com', 'a4979ef8f65f030ec710e1759a8b012d', 1, 0, 'fydeUQunKUsi2yGqS2QTXZNKPfs69oUleA1U3QCu', '1497728728'),
(18, 'adnan.azam@gmail.com', '3e22ac9fa75cca3eca09dcb1f96202e0', 1, 0, 'Fk7XZKE56ls98aLuTAAtSi3lm10ju7Fuh5FuDgss', '1497905581'),
(19, 'ravikant@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 1, 0, 'oZyNaYYcfPGAOnkvnksD8NJypsqkhqYTVmlaceIj', '1498720130');

-- --------------------------------------------------------

--
-- Table structure for table `user_reviews`
--

CREATE TABLE IF NOT EXISTS `user_reviews` (
`review_id` bigint(200) NOT NULL,
  `review_title` text NOT NULL,
  `review_msg` text NOT NULL,
  `review_img` varchar(200) NOT NULL,
  `review_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `review_status` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_reviews`
--

INSERT INTO `user_reviews` (`review_id`, `review_title`, `review_msg`, `review_img`, `review_timestamp`, `review_status`) VALUES
(7, 'Nice tutorials found ever', 'sdsds sssss ssssssssss ssssss s', 'File1001452123.png', '2017-08-04 14:05:28', '1'),
(8, 'new data', 'test data added successfully for 2nd test', 'File1185402909.gif', '2017-08-04 14:32:51', '1'),
(9, 'hello', 'hell is first test to check for data in paragraph test', 'File490931183.jpg', '2017-08-04 14:33:31', '1'),
(10, 'overlap test', 'overlap test for to check whether enter shift down or not', 'File227322805.gif', '2017-08-04 14:35:39', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
 ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
 ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `cell_carrier`
--
ALTER TABLE `cell_carrier`
 ADD PRIMARY KEY (`carrier_id`);

--
-- Indexes for table `model`
--
ALTER TABLE `model`
 ADD PRIMARY KEY (`model_id`);

--
-- Indexes for table `orderdetail`
--
ALTER TABLE `orderdetail`
 ADD PRIMARY KEY (`detail_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
 ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
 ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_reviews`
--
ALTER TABLE `user_reviews`
 ADD PRIMARY KEY (`review_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
MODIFY `admin_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
MODIFY `brand_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `cell_carrier`
--
ALTER TABLE `cell_carrier`
MODIFY `carrier_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `model`
--
ALTER TABLE `model`
MODIFY `model_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `orderdetail`
--
ALTER TABLE `orderdetail`
MODIFY `detail_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
MODIFY `order_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
MODIFY `product_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=125;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `user_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `user_reviews`
--
ALTER TABLE `user_reviews`
MODIFY `review_id` bigint(200) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
